var generateSeoData = {

    url: '',

    form: null,

    logItemTemplate: new Template('<tr class="log-item"><td colspan="2" id="#{id}" class="value">#{text}</td></tr>'),

    start: function () {
        if (!generateSeoData.form.validate()) return;
        $('result_fieldset').setStyle({'display': 'block'});
        this.prepareLog();
        new Ajax.Request(
            generateSeoData.url,
            {
                parameters: $(generateSeoData.form.formId).serialize(true),
                onSuccess: generateSeoData.onSuccess,
                loaderArea: false
            }
        );
    },

    onSuccess: function(transport) {
        var response = transport.responseJSON;
        generateSeoData.updateLog(response.log.text, response.log.lineId);
        if (response.url) {
            new Ajax.Request(
                response.url,
                {
                    onSuccess: generateSeoData.onSuccess,
                    loaderArea: false
                }
            );
        } else {
            generateSeoData.updateLog('Complete at ' + new Date().toLocaleString(), false);
            $$('#result_fieldset table tr:last-child').first().hide();
        }
    },

    prepareLog: function () {
        $$('#result_fieldset table tr.log-item').each(function(e){e.remove()});
        $$('#result_fieldset table tr:last-child').first().show();
        var select = $('entity_type');
        var text = select.title + ' ';
        text += select.options[select.selectedIndex].innerHTML;
        this.updateLog(text, false);
        this.updateLog('Start at ' + new Date().toLocaleString(), false);
    },

    updateLog: function (text, lineId) {
        if (lineId && $(lineId)) {
            $(lineId).update(text);
            return;
        }
        var newLine = this.logItemTemplate.evaluate(
            {
                'id': lineId,
                'text': text
            }
        );
        $$('#result_fieldset table tr:last-child')
            .first()
            .insert({'before': newLine});
    },

    setForm: function (formId) {
        this.form = new varienForm(formId);
        return this;
    },

    setUrl: function (url) {
        this.url = url;
        return this;
    }

};
