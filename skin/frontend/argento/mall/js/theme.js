document.observe('dom:loaded', function() {
    var mobileMapping = {
        'toggle-search': {
            on: function() {
                $('search_mini_form').addClassName('shown');
            },
            off: function() {
                $('search_mini_form').removeClassName('shown');
            }
        }
    };
    for (var i in mobileMapping) {
        Argento.togglers.add(i, mobileMapping[i]);
    }

    /**
     * 1. Move top menu to mobile togglers on tablet and mobile
     * 2. Move laguage switcher to top-toolbar on tablet and mobile
     */
    var mql = matchMedia('(max-width: 768px)');
    if (mql.matches) {
        Argento.Mover.move('.nav-container', '.header-content');
        Argento.Mover.move('.header-content .form-language', '.top-toolbar');
    }
    mql.addListener(function (e) {
        if (e.matches) {
            Argento.Mover.move('.nav-container', '.header-content');
            Argento.Mover.move('.header-content .form-language', '.top-toolbar');
        } else {
            Argento.Mover.restore('.nav-container');
            Argento.Mover.restore('.top-toolbar .form-language');
        }
    });

});
