document.observe('dom:loaded', function() {

    /**
     * Load Google fonts
     */
    if (typeof WebFont == "object") {
        WebFont.load({
            inactive: function() {
                // faild to load fonts but still add class
                document.documentElement.addClassName('wf-active');
            },
            google: {
                families: ['Source Sans Pro']
            }
        });
    };

    var mobileMapping = {
        'toggle-search': {
            on: function() {
                $('search_mini_form').addClassName('shown');
            },
            off: function() {
                $('search_mini_form').removeClassName('shown');
            }
        }
    };
    for (var i in mobileMapping) {
        Argento.togglers.add(i, mobileMapping[i]);
    }

    /**
     * Move top menu to mobile togglers on tablet and mobile
     */
    var mql = matchMedia('(max-width: 768px)');
    if (mql.matches) {
        Argento.Mover.move('.nav-container', '.header-content');
    }
    mql.addListener(function (e) {
        if (e.matches) {
            Argento.Mover.move('.nav-container', '.header-content');
        } else {
            Argento.Mover.restore('.nav-container');
        }
    });
});
