/* Search form scripts */
function activateSearchField(field, form, emtyText, event) {
    if (form.hasClassName('shown')) {
        return true;
    }

    form.addClassName('shown');
    field.addClassName('shown');
    field.focus();

    if (field.value && field.value != emtyText) {
        Event.stop(event);
        // http://stackoverflow.com/questions/13071106/set-caret-to-end-of-textbox-on-focus/13071184#13071184
        setTimeout(function() {
            if (field.createTextRange) {
                var r = field.createTextRange();
                r.collapse(true);
                r.moveEnd("character", field.value.length);
                r.moveStart("character", field.value.length);
                r.select();
            } else {
                field.selectionStart = field.selectionEnd = field.value.length;
            }
        }.bind(this), 13);
        return false;
    } else if (field.value == emtyText || field.value === '') {
        field.setValue('');
    }
    return true;
}
function deactivateSearchField(field, form) {
    field.removeClassName('shown');
    form.removeClassName('shown');
}
function submitSearchForm() {
    $('search_mini_form').simulate('submit');
}

document.observe('dom:loaded', function() {
    if (typeof AjaxsearchAutocomplete == 'undefined') {
        Varien.searchForm.prototype.submit = Varien.searchForm.prototype.submit.wrap(function(o, event) {
            if (!activateSearchField(this.field, this.form, this.emptyText, event)) {
                return false;
            }
            o(event);
        });

        var formId = 'search_mini_form';
        if ($(formId) && $(formId).hasAttribute('data-config')) {
            var config = JSON.parse($(formId).getAttribute('data-config'));
            window.searchForm = new Varien.searchForm(formId, 'search', config.searchtext);
            searchForm.initAutocomplete(config.suggestUrl, 'search_autocomplete');
        }

        var close = $$('.form-search .search-close').first();
        if (close) {
            close.observe('click', function() {
                deactivateSearchField(searchForm.field, searchForm.field.form);
            });
        }

    }
});
/* End of search form scripts */

document.observe('dom:loaded', function() {

    /**
     * Load Google fonts
     */
    if (typeof WebFont == "object") {
        WebFont.load({
            inactive: function() {
                // faild to load fonts but still add class
                document.documentElement.addClassName('wf-active');
            },
            google: {
                families: ['Muli:300,400,700,800']
            }
        });
    }

    $$('.quick-links').each(function(el) {
        el.addClassName('dropdown-menu');
    });

    if ($$('.menu-toggle').length ) {
        var luxuryTogglers = MobileTogglers($('header-togglers'), {
            'quick-links-toggle': {
                on: function() {
                    $$('.header .quick-links').each(function(el) {
                        el.addClassName('shown');
                    });
                },
                off: function() {
                    $$('.header .quick-links').each(function(el) {
                        el.removeClassName('shown');
                    });
                }
            },
            'header-cart-toggle': {
                on: function() {
                    $$('.header .mini-products-list').each(function(el) {
                        el.addClassName('shown');
                    });
                },
                off: function() {
                    $$('.header .mini-products-list').each(function(el) {
                        el.removeClassName('shown');
                    });
                }
            },
            'menu-toggle': {
                on: function() {
                    $$('.menu-toggle').each(function(el) {
                        var elem = $(el.readAttribute('data-menu'));
                        elem.addClassName('shown');
                    });
                },
                off: function() {
                    $$('.menu-toggle').each(function(el) {
                        var elem = $(el.readAttribute('data-menu'));
                        elem.removeClassName('shown');
                    });
                }
            }
        },
        [
            '.header .quick-links',
            '.header .mini-products-list',
            '.header #' + $$('.menu-toggle').first().readAttribute('data-menu')
        ],
        [$('mobile-togglers')]);
    }

    var toggleHeader = function (mql) {
        if (mql.matches) {
            Argento.Mover.move('.menu-toggle', '.mobile-togglers');
            Argento.Mover.move('.btn-search', '.mobile-togglers');
            Argento.Mover.move('.quick-links-toggle', '.mobile-togglers');
            Argento.Mover.move('.header-cart-toggle', '.mobile-togglers');

            Argento.Mover.move('.header-content', '.header');
            Argento.Mover.move('.nav-container', '.header-content');

            if (typeof AjaxsearchAutocomplete == 'undefined') {
                $$('.btn-search')[0].observe('click', submitSearchForm);
            }
        } else {
            Argento.Mover.restore('.nav-container');
            Argento.Mover.restore('.header-content');

            Argento.Mover.restore('.header-cart-toggle');
            Argento.Mover.restore('.quick-links-toggle');
            Argento.Mover.restore('.btn-search');
            Argento.Mover.restore('.menu-toggle');

            if (typeof AjaxsearchAutocomplete == 'undefined') {
                $$('.btn-search')[0].stopObserving('click', submitSearchForm);
            }
        }
    };
    var mqlHeader = matchMedia('(max-width: 768px)');
    toggleHeader(mqlHeader);
    mqlHeader.addListener(toggleHeader);

    /**
     * Initialize block togglers for mobile devices
     */
    new BlockToggler({
        maxWidth: 480,
        useEffect: false,
        block: '.footer-cms .footer-links > li',
        header: ' div',
        content: ' ul'
    });

    /**
     * Initialize cart block togglers
     */
    Argento.discountBlockToggler = new BlockToggler({
        maxWidth: null,
        useEffect: false,
        block: '.discount',
        header: ' h2',
        content: ' .discount-form'
    });
    Argento.shippingBlockToggler = new BlockToggler({
        maxWidth: null,
        useEffect: false,
        block: '.shipping',
        header: ' h2',
        content: ' .shipping-form'
    });

    [
        'AjaxPro:onComplete:checkout:cart:after',
        'AjaxPro:onComplete:wishlist:index:after'
    ]
    .map(function(eventName){
        document.observe(eventName, function() {
            Argento.discountBlockToggler.init();
            Argento.shippingBlockToggler.init();
        });
    });

    /**
     * Initialize sticky header
     */
    if (document.body.hasClassName('sticky-header')
        && !document.body.hasClassName('no-sticky'))
    {
        var toggleStickyHeader = function (mql) {
            if (mql.matches) {
                Argento.StickyKit.stick('.nav-container', { parent: $$('.page').first() });
                Argento.stickyHeaderEnabled = true;
            } else {
                Argento.StickyKit.unstick('.nav-container');
                Argento.stickyHeaderEnabled = false;
            }
        };
        /* @see theme.css (max-width: 768px): nav is vertical and could be longer than tablet screen */
        var mqlHeader = matchMedia('(min-width: 769px) and (min-height: 751px), (min-width: 1250px) and (min-height: 501px)');
        toggleStickyHeader(mqlHeader);
        mqlHeader.addListener(toggleStickyHeader);
    }
});

document.observe("AjaxPro:spinner:init:after", function(e){
    e.memo.spinner.addClassName('luxury');
    e.memo.spinner.insert({
        bottom: '<div class="spinner"><div class="part1"></div><div class="part2"></div><div class="part3"></div></div>'
    });
});
