;
(function (window, $){

    if (typeof $ === "undefined") {
        console.warn("jQuery is not defined.");
        window.MoreViewsSlick = function(){};
        return;
    }

    function MoreViewsSlick() {

        var mainImage = '.product-img-box .main-image',
            views = '.product-img-box .more-views';

        function getMainImageHeight(){
            return $(mainImage).outerHeight();
        }

        function calcSlidesToShow(){
            // slick calculates heights of visible areas -- heights of fist slide multiplied by slides to show
            // slick does not include borders and margins when get heights of first slide
            var height = getMainImageHeight();
            height -= (60); // margin for buttons
            var slidesToShow = height / $(views).children().first().outerHeight(true);
            return (slidesToShow < 3) ? 3 : slidesToShow;
        }

        function toogleViews(mql){
            $(views).css('max-height', $(mainImage).outerHeight() + 'px');
            $(views).addClass('show-all');

            if (mql.matches) {
                // horisontal more views
                if ($(views).hasClass('slick-initialized')) {
                    // destroy vertical slick;
                    $(views).slick('unslick');
                }
            } else {
                // vertical more views
                if ($(views).outerHeight() >= $(mainImage).outerHeight()) {
                    // add slick only when more-views is bigger than main image
                    // remove more views header
                    $(views).children('h2').remove();
                    $(views).slick({
                        slidesToShow: calcSlidesToShow(),
                        slidesToScroll: 3,
                        arrows: true,
                        vertical: true,
                        verticalSwiping: true,
                        infinite: false
                    });
                }
            }
            $(views).css('max-height', '');
        }

        // listen windows size change
        var mql = matchMedia('(max-width: 768px) and (min-width: 481px)');
        toogleViews(mql);
        mql.addListener(toogleViews);

    }

    window.MoreViewsSlick = MoreViewsSlick;

})(window, jQuery);

window.addEventListener('load', function() {
    // it has to be window load; wait when all images loaded
    MoreViewsSlick();
}, false);
