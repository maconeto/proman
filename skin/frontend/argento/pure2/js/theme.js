document.observe('dom:loaded', function() {

    /**
     * Load Google fonts
     */
    if (typeof WebFont == "object") {
        WebFont.load({
            inactive: function() {
                // faild to load fonts but still add class
                document.documentElement.addClassName('wf-active');
            },
            google: {
                families: ['Noto Sans']
            }
        });
    }

    var mobileMapping = {
        'toggle-search': {
            on: function() {
                $('search_mini_form').addClassName('shown');
            },
            off: function() {
                $('search_mini_form').removeClassName('shown');
            }
        }
    };
    for (var i in mobileMapping) {
        Argento.togglers.add(i, mobileMapping[i]);
    }

    /**
     * 1. Move search filed to mobile togglers on tablet and mobile
     * 2. Move move header cart to mobile togglers on tablet and mobile
     */
    var toggleHeaderContent = function (mql) {
        if (mql.matches) {
            Argento.Mover.move('#search_mini_form', '.header-content');
            Argento.Mover.move('.header-cart-wrapper', '.header-content');
        } else {
            Argento.Mover.restore('#search_mini_form');
            Argento.Mover.restore('.header-cart-wrapper');
        }
    };
    var mql = matchMedia('(max-width: 768px)');
    toggleHeaderContent(mql);
    mql.addListener(toggleHeaderContent);

    /**
     * Initialize sticky header
     */
    if (document.body.hasClassName('sticky-header')
        && !document.body.hasClassName('no-sticky'))
    {
        var toggleStickyHeader = function (mql) {
            if (mql.matches) {
                Argento.StickyKit.stick('.header-content', { parent: $$('.page').first() });
                Argento.stickyHeaderEnabled = true;
            } else {
                Argento.StickyKit.unstick('.header-content');
                Argento.stickyHeaderEnabled = false;
            }
        };
        /* @see theme.css (max-width: 768px): nav is vertical and could be longer than tablet screen */
        var mqlHeader = matchMedia('(min-width: 769px) and (min-height: 751px), (min-width: 1250px) and (min-height: 501px)');
        toggleStickyHeader(mqlHeader);
        mqlHeader.addListener(toggleStickyHeader);
    }
    /**
     * Initialize sticky sidebar
     */
    if (document.body.hasClassName('sticky-sidebar')) {
        var toggleStickySidebar = function (mql) {
            if (mql.matches) {
                var offset = 0;
                if (Argento.stickyHeaderEnabled) {
                    var nav = $$('.header-content').first();
                    if (nav) {
                        offset = nav.getHeight();
                    }
                }
                Argento.StickyKit.stick('.sidebar', { offset_top: offset });
            } else {
                Argento.StickyKit.unstick('.sidebar');
            }
        };
        var mqlSidebar = matchMedia('(min-width: 979px) and (min-height: 751px)');
        toggleStickySidebar(mqlSidebar);
        mqlSidebar.addListener(toggleStickySidebar);
    }

    // language switcher
    if (typeof EasyFlagsSelect === 'undefined') {
        // easyflags disabled; create chosen select
        $$('#select-language').each(function(el){
            new Chosen(el, {
                disable_search_threshold: 10,
                search_contains: true,
                width: 'auto'
            });
        });
    };
});
