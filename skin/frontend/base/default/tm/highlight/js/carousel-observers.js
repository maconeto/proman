;
(function ($){

    /**
     * Fix cut collateral info for last row of products
     *
     * @return void
     */
    function fixHiddenCollateralInfo () {
        $(this).each(function () {
            var hMax = 0;
            // find max heights of collateral info
            $(this).find('ul.last .collateral-info').each(
                function () {
                    var h = 28;
                    $(this).children().each(
                        function () {
                            h += $(this).outerHeight(true);
                        }
                    );
                    if (h > hMax) {
                        hMax = h;
                    }
                }
            );
            if (hMax == 0) {
                return false;
            }

            $(this).find('.slick-list')
                .css('padding-bottom', hMax + 'px')
                .css('margin-bottom', '-' + hMax+'px');
            $(this).on('mouseenter', 'ul.last', function() {
                    $(this).closest('.slick-list').css('z-index', '19');
                })
                .on('mouseleave', 'ul.last', function() {
                    $(this).closest('.slick-list').css('z-index', '');
                });
        });
    }

    /**
     * Add new slide using ajax load
     *
     * @param  int slideIndex index where to insert new slide
     * @return promise object
     */
    function loadSlide (slideIndex, slick) {
        var self = this;
        return $.ajax({
                method: "POST",
                slideIndex: slideIndex,
                url: slick.slickGetOption('dataSourceUrl'),
                data: {
                    'highlight_page': slideIndex+1,
                    'block_data': slick.slickGetOption('blockData')
                }
            })
            .done(function (json) {
                var newSlide,
                    productsGrid;
                newSlide = $(json.html).find('.block-content:not(.loading)');
                productsGrid = newSlide.find('ul.products-grid');

                // add new slide
                $(self).slick(
                    'slickAdd',
                    newSlide.length ? newSlide : $(json.html),
                    this.slideIndex,
                    true // insert before slide with index
                );
                // decorate new slide
                if (typeof decorateGeneric == 'function'
                    && productsGrid.length > 0
                ) {
                    decorateGeneric(
                        productsGrid.get(),
                        ['odd','even','first','last']
                    );
                }

                // remove dummy slide with loading because when it is last page
                if (json.isLastPage) {
                    $(self).slick('slickRemove', this.slideIndex+1);
                }

                // fire prototype event for other extensions
                Element.fire(document, 'highlight:loadSlide:after', newSlide.get());
            });
    };

    // observe slider initialization to laod first page
    // and add observer for 'beforeChange' event from slick slider
    $('div.block-highlight[data-slick]').on('init', function (event, slick) {
        // observe before Slides change for current instance
        $(this).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            if (slick.$slides.eq(nextSlide).hasClass('loading')) {
                loadSlide.call(this, nextSlide, slick);
            }
        });
        // fix hidden collateral info for last row
        fixHiddenCollateralInfo.call(this);
    });

})(jQuery);
