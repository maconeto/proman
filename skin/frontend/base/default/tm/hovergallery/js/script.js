var HoverGallery = Class.create();
HoverGallery.prototype = {
    initialize: function(selector) {
        var self = this;
        $$(selector).each(function(el, index) {
            el.observe('mouseover', self.showImage);
            el.observe('mouseout', self.hideImage);
        });
    },

    showImage: function(e) {
        var hoverImg = this.down('.hover-image');
        hoverImg && hoverImg.addClassName('active');
    },

    hideImage: function(e) {
        var hoverImg = this.down('.hover-image');
        hoverImg && hoverImg.removeClassName('active');
    }
};

if (!navigator.userAgent.match(/iPhone|iPad|iPod|Mobile|mobile/i)) {
    function initHoverGalleryJs(){
        new HoverGallery('.category-products .item a.product-image');
    }

    var events = [
        "dom:loaded",
        "ajaxlayerednavigation:ready",
        "AjaxPro:onSuccess:after",
        "AjaxPro:onComplete:after",
        "highlight:loadSlide:after"
    ];
    events.each(function(eventName) {
        document.observe(eventName, function() {
            initHoverGalleryJs();
        });
    });
};
