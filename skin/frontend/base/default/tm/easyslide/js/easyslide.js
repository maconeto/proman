document.observe('dom:loaded', ('getComputedStyle' in window) ? function() {

    var sliders = $$('.swiper-container');
    if (sliders.length > 0) {
        // initialize sliders
        sliders.each(function(el){
            // get slider config
            var config = JSON.parse(el.readAttribute('data-config'));

            config.onInit = function(swiper){
                // observe click on slides
                swiper.container[0].select('.easyslide-link').each(function(el){
                    el.observe('click', function(e) {
                        if (this.hasClassName('target-self'))
                            return true;
                        e.stop();
                        var options = '';
                        if (this.hasClassName('target-popup'))
                            options = 'width=600,height=400';
                        window.open(this.href, this.up().id, options);
                    });
                });
                // observers to listen mouse hover
                if (swiper.params.stopOnHover) {
                    swiper.container[0].observe('mouseenter', function(){
                        this.swiper.stopAutoplay()
                    });
                    swiper.container[0].observe('mouseleave', function(){
                        this.swiper.startAutoplay()
                    });
                }
                if (swiper.params.lazyLoading) {
                    swiper.container[0].resizeSlider = function(){
                        var layout = new Element.Layout(this.down('.swiper-slide-active'));
                        var padding = layout.get('padding-bottom') + layout.get('padding-top');
                        var w = this.swiper.params.sliderSizeWidth;
                        var h = this.swiper.params.sliderSizeHeight;
                        if (!h || !w) return;
                        var newHeight = this.offsetWidth * (h - padding) / w + padding;
                        this.setStyle({maxHeight: Math.round(newHeight) + 'px'});
                    };
                    swiper.container[0].resizeSlider();
                }
            };

            new Swiper(el, config);
        });
        // observe window resize to resize slider
        window.easyslideResizeTimer = null;
        Event.observe(window, 'resize', function(){
            clearTimeout(window.easyslideResizeTimer);
            window.easyslideResizeTimer = setTimeout(function() {
                $$('.swiper-container').each(function (el){
                    if (typeof el.resizeSlider !== 'undefined')
                        el.resizeSlider();
                });
            }, 250);
        });
    }

} : null);
