document.observe("dom:loaded", function() {
    $$('ul.navpro').each(function (menu){
        var config = JSON.parse(menu.readAttribute('data-config'));
        if (!config) {
            console.warn('Fail to initialize NavPro menu "' + menu.id + '"');
            return;
        }
        config.constraint_el = eval(config.constraint_el);
        navPro(menu.id, config);
        new accordion(menu.id);
    });
});
