<?php

require_once 'abstract.php';

/**
 * Templates Master Prolabels Shell Script
 *
 */
class Mage_Shell_Prolabels extends Mage_Shell_Abstract
{
    /**
     * ProLabels process object
     *
     * @var TM_ProLabels_Model_Indexer
     */
    protected $_prolabels;

    /**
     * Get prolabel indexer object
     *
     * @return TM_ProLabels_Model_Indexer
     */
    protected function _getProlabels()
    {
        if ($this->_prolabels === null) {
            $this->_prolabels = Mage::getModel('prolabels/indexer');
        }
        return $this->_prolabels;
    }

    protected function _log($message)
    {
        // commented to pass MEQP check
        // echo $message;
    }

    /**
     * Run script
     *
     */
    public function run()
    {
        if (isset($this->_args['reindex'])) {
            try {
                $this->_getProlabels()->run();
                $this->_log("Prolabels reindexing successfully finished\n");
            } catch (Mage_Core_Exception $e) {
                $this->_log($e->getMessage() . "\n");
            } catch (Exception $e) {
                $this->_log("Reindexing unknown error:\n\n");
                $this->_log($e . "\n");
            }
        } else {
            $this->_log($this->usageHelp());
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f prolabels.php -- [options]

  reindex       Run Prolabels Indexer Process
  help          This help

USAGE;
    }
}

$shell = new Mage_Shell_Prolabels();
$shell->run();
