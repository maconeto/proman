<?php

require_once __DIR__ . '/../abstract.php';
class Potato_Shell_Warmer extends Mage_Shell_Abstract
{
    public function run()
    {
        try {
            Mage::getModel('po_crawler/cron_warmer')->process();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}
$shell = new Potato_Shell_Warmer();
$shell->run();