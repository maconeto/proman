**********************************************
  ** POSTEN/BRING FRAKTMODUL FOR MAGENTO **
**********************************************

 Approved by Posten/Bring
 Developed and sold by Trollweb Solutions AS
 For details go to www.trollweb.no

**********************************************************************************

** INSTALLATION **
1. Unzip the file
2. Transfer all files to your magento root directory
3. Login to the Magento admin panel.
4. Go to System -> Cache and update all cache
5. Go to System -> Configuration -> Shipping settings
   - Make sure Zip/Postal Code is filled with you local postal code.
6. Go to System -> Configuration -> Shipping Methods.
   - Expand the "Bring Fraktguiden" area
   - Add the methods you want to be available
   - Set it to active
   - Save configuration.

Make sure there is a weight on all the products in the "weight" attribute.

**********************************************************************************

Posten/Bring-integrasjonen utviklet av Trollweb Solutions AS. 
Kopiering/distribusjon er ikke tillatt. For mer informasjon og bestilling av modul: http://www.trollweb.no/
Lisensen for modulen gjelder pr Magento-installasjon (domene). Dette gjeler ogs� for Partnere/Forhandlere.


