<?php
class Bss_PercentageTierPrice_Model_Catalog_Resource_Eav_Mysql4_Product_Attribute_Backend_Tierprice extends Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Attribute_Backend_Tierprice {
	public function loadProductPrices($product, $attribute) {
        //add new column name to the sql
        $select = $this->_getReadAdapter()->select()
            ->from($this>getMainTable(), array(
                'website_id', 'all_groups', 'cust_group' => 'customer_group_id',
                'price_qty' => 'qty', 'tier_type', 'percent_of', 'type_value', 'price' => 'value', 
            ))
            ->where('entity_id=?', $product->getId())
            ->order('qty');
        if ($attribute->isScopeGlobal()) {
            $select->where('website_id=?', 0);
        }
        else {
            if ($storeId = $product->getStoreId()) {
                $select->where('website_id IN (?)', array(0, Mage::app()->getStore($storeId)->getWebsiteId()));
            }
        }
        return $this->_getReadAdapter()->fetchAll($select);
    }
}
		