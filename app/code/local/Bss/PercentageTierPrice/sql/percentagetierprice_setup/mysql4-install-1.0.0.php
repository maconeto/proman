<?php
	$installer = $this;
	$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
	$installer->startSetup();

	try {
	$installer->run(
			"ALTER TABLE {$this->getTable('catalog_product_entity_tier_price')}  ADD tier_type INT NOT NULL AFTER qty ; ");
	}
	catch(exception $e) {
	}

	try {
	$installer->run(
			"ALTER TABLE {$this->getTable('catalog_product_entity_tier_price')}  ADD percent_of INT NOT NULL AFTER tier_type ; ");
	}
	catch(exception $e) {
	}

	try {
	$installer->run(
			"ALTER TABLE {$this->getTable('catalog_product_entity_tier_price')}  ADD type_value FLOAT NOT NULL AFTER percent_of ; ");
	}
	catch(exception $e) {
	}

	$installer->endSetup(); 