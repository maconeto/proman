<?php
class Bss_PercentageTierPrice_Helper_Data extends Mage_Core_Helper_Abstract {
	public function getGroupPrice($product, $customerGroupId) {
		$groupPrices = $product->getData('group_price');
		
        if (is_null($groupPrices)) {
            $attribute = $product->getResource()->getAttribute('group_price');
            if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $groupPrices = $product->getData('group_price');
            }
        }

        if (is_null($groupPrices) || !is_array($groupPrices)) {
            return $product->getPrice();
        }

        $customerGroup = $customerGroupId;

        $matchedPrice = $product->getPrice();
		
        foreach ($groupPrices as $groupPrice) {
            if ($groupPrice['cust_group'] == $customerGroup && $groupPrice['price'] < $matchedPrice) {
                $matchedPrice = $groupPrice['price'];
                break;
            }
        }
        return $matchedPrice;
	}
	
	protected function _getCustomerGroupId($product)
    {
        if ($product->getCustomerGroupId()) {
            return $product->getCustomerGroupId();
        }
        return Mage::getSingleton('customer/session')->getCustomerGroupId();
    }
}
	 