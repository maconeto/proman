<?php
require_once("Mage/Adminhtml/controllers/Sales/Order/ShipmentController.php");
class Maconeto_Bring_Adminhtml_Sales_Order_ShipmentController extends Mage_Adminhtml_Sales_Order_ShipmentController
{


    /**
     * Initialize shipment model instance
     *
     * @return Mage_Sales_Model_Order_Shipment|bool
     */
    protected function _initShipment()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Shipments'));

        $shipment = false;
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        
        $orderId = $this->getRequest()->getParam('order_id');
        
        if ($shipmentId) {
            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
        } elseif ($orderId) {
            $order      = Mage::getModel('sales/order')->load($orderId);

            /**
             * Check order existing
             */
            if (!$order->getId()) {
                $this->_getSession()->addError($this->__('The order no longer exists.'));
                return false;
            }
            /**
             * Check shipment is available to create separate from invoice
             */
            if ($order->getForcedDoShipmentWithInvoice()) {
                $this->_getSession()->addError($this->__('Cannot do shipment for the order separately from invoice.'));
                return false;
            }
            /**
             * Check shipment create availability
             */
            if (!$order->canShip()) {
                $this->_getSession()->addError($this->__('Cannot do shipment for the order.'));
                return false;
            }
            
     
            
            $order_currency_code    = $order->getOrderCurrencyCode();     
            $increment_id    =        $order->getIncrementId();     
            
            $savedQtys = $this->_getItemQtys();             
            $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($savedQtys);
            
            //hasShipment return the number of shipments that order is having
            $packageCorrelationId = $order->hasShipments() + 1;
            
            $tracks = $this->getRequest()->getPost('tracking');
          
            if ($tracks) {
                foreach ($tracks as $data) {
                    if (empty($data['number'])) {
                        Mage::throwException($this->__('Tracking number cannot be empty.'));
                    }
                    $track = Mage::getModel('sales/order_shipment_track')
                        ->addData($data);
                    $shipment->addTrack($track);
                }
            }
            
            //bring shipping booking 
              if($this->getRequest()->getPost('bring_booking')){
                    if(!$this->getRequest()->getPost('package_weight')  
                    || !$this->getRequest()->getPost('package_height')
                    || !$this->getRequest()->getPost('package_length')    
                          )
                  {
                      $this->_getSession()->addError('MISSING REQUIRED VALUES - length, width, height are required inputs to book a shipment from Bring');
                      return false;
                  }
                  $packageWeight = floatval($this->getRequest()->getPost('package_weight'));
                  $packageHeight = intval($this->getRequest()->getPost('package_height'));
                  $packageWidth  = intval($this->getRequest()->getPost('package_width'));
                  $packageLength = intval($this->getRequest()->getPost('package_length'));
                  $packageDescription = $this->getRequest()->getPost('package_description');
                  if(!$packageDescription) $packageDescription = '';
                  $productId = $this->getRequest()->getPost('product_id');
                  
                  $shippingAddress = $order->getShippingAddress();
               
                  if(Mage::getStoreConfig('carriers/mybring/use_company') && $shippingAddress->getCompany()){
                      $recipient['name'] =  $shippingAddress->getCompany();
                  }else{
                       $recipient['name'] = $shippingAddress->getFirstname() . ' '. $shippingAddress->getLastname();
                  }
                  
                  $street = $shippingAddress->getStreet();
                  if(is_array($street)) $recipient['address'] = $street[0];
                  $recipient['city'] = strtoupper($shippingAddress->getCity());
                  $recipient['postcode'] = $shippingAddress->getPostcode();
                  $recipient['country'] = strtolower($shippingAddress->getCountryId());
                  $recipient['email'] = $shippingAddress->getEmail();
                  $recipient['phone'] = $shippingAddress->getTelephone();
                  
                  $sender['sender'] = Mage::getStoreConfig('carriers/mybring/sender');
                  $sender['name'] = Mage::getStoreConfig('carriers/mybring/sender_name');
                  $sender['address'] = Mage::getStoreConfig('carriers/mybring/sender_address');
                  $sender['email'] = Mage::getStoreConfig('carriers/mybring/sender_email');
                  $sender['phone'] = Mage::getStoreConfig('carriers/mybring/sender_phone');
                  $sender['city']= strtoupper(Mage::getStoreConfig('carriers/mybring/sender_city'));
                  $sender['postcode'] = Mage::getStoreConfig('carriers/mybring/sender_postcode');
                  $sender['country_code'] = strtolower(Mage::getStoreConfig('general/country/default'));
                  $testMode = Mage::getStoreConfig('carriers/mybring/test_mode')?'true':'false';
                  $apiUser = Mage::getStoreConfig('carriers/mybring/api_user');
                  $apiKey = Mage::getStoreConfig('carriers/mybring/api_key');
                  $customerNumber = Mage::getStoreConfig('carriers/mybring/customer_number');
                  $customerReference = substr($customerNumber, 15);
                  $accountNumber = Mage::getStoreConfig('carriers/mybring/account_number');
                  $shippingDate = (time() + (2 * 24 * 60 * 60)) * 1000 ; //time in miliseconds
             //   echo strtotime("2012-07-03T18:07:42.153+02:00");die;
             //     echo $shippingDate; 1341334309   / 1341333795200
              //    echo date(DATE_ATOM,1341334309000);die;
                  
                  $services = '"services":{';
                  if(Mage::getStoreConfig('carriers/bring/email_sms_notify_enable'))
                    {
                        $services .= '"recipientNotification": {
                                            "email": "'.$recipient['email'].'",
                                            "mobile": "'.$recipient['phone'].'"
                                       },';
                    }
                    
                    
                  //add cash on delivery info  
                  if($this->getRequest()->getPost('cod') 
                          && $this->getRequest()->getPost('cod_amount') 
                          && $this->getRequest()->getPost('product_id') != 'MINIPAKKE'
                          && $this->getRequest()->getPost('product_id') != 'BEDRIFTSPAKKE'
                          && $this->getRequest()->getPost('product_id') != 'EKSPRESS09'
                   )  
                  {
                        //get invoice in this order to generate kid
                        if ($order->hasInvoices() > 0) {
                             $inv = $order->getInvoiceCollection()->getLastItem();
                             $invIncrementId = $inv->getIncrementId();
                             $currentTime = time();
                             $kidPrefix = $invIncrementId.date("ymd", Mage::getModel('core/date')->timestamp($currentTime));
                            
                         } else {
                              Mage::throwException($this->__('KID generation failed: there is no invoice found for this order. Please create invoice for this shipment first.'));
                         }
                      
                      
                      $kid = Mage::helper('bring')->generateKid($kidPrefix);
                      
                      
                      $codAmount = $this->getRequest()->getPost('cod_amount');
                      
                      $services .=  '"cashOnDelivery": {
                            "accountNumber": "'.$accountNumber.'",
                            "amount": '.$codAmount.',
                            "currencyCode": "'.$order_currency_code.'",
                            "message": {
                                "value":"'.$kid.'",
                                 "type":"kid"
                             }
                          },';
                  } 
                  $services .= '"quickPackEnvelope": null,
                                "flexDelivery": null}';

                    

                  
                  $json_bring = '{
                                "testIndicator": '.$testMode.',
                                "schemaVersion": 1,
                                "consignments": [
                                    {
                                        "correlationId": "'.$orderId.'",
                                        "shippingDateTime": '.$shippingDate.',
                                        "parties": {
                                            "sender": {
                                                "name": "'.$sender['sender'].'",
                                                "addressLine": "'.$sender['address'].'",
                                                "addressLine2": null,
                                                "postalCode": "'.$sender['postcode'].'",
                                                "city": "'.$sender['city'].'",
                                                "countryCode": "'.$sender['country_code'].'",
                                                "reference": "'.$customerReference.'",
                                                "contact": {
                                                    "name": "'.$sender['name'].'",
                                                    "email": "'.$sender['email'].'",
                                                    "phoneNumber": "'.$sender['phone'].'"
                                                }
                                            },
                                            "recipient": {
                                                "name": "'.$recipient['name'].'",
                                                "addressLine": "'.$recipient['address'].'",
                                                "addressLine2": null,
                                                "postalCode": "'.$recipient['postcode'].'",
                                                "city": "'.$recipient['city'].'",
                                                "countryCode": "'. $recipient['country'].'",
                                                "reference": "'.$increment_id.'",
                                                "contact": {
                                                    "name": "'.$recipient['name'].'",
                                                    "email": "'.$recipient['email'].'",
                                                    "phoneNumber": "'.$recipient['phone'].'"
                                                }
                                            }
                                        },
                                        "product": {
                                            "id": "'.$productId.'",
                                            "customerNumber": "'.$customerNumber.'",
                                            '.$services.',
                                            "customsDeclaration": null    
                                             
                                        },
                                        "purchaseOrder": null,
                                        "packages": [
                                            {
                                                "correlationId": "'.$packageCorrelationId.'",
                                                "weightInKg": '.$packageWeight.',
                                                "goodsDescription": "'.$packageDescription.'",
                                                "dimensions": {
                                                    "heightInCm": '.$packageHeight.',
                                                    "widthInCm": '.$packageWidth.',
                                                    "lengthInCm": '.$packageLength.'
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }';
                  
                           
                            
                            $data_string = $json_bring;
                            
                            //var_dump($data_string);die;
                            $ch = curl_init('https://www.mybring.com/booking/api/booking.json');
                            curl_setopt($ch, CURLOPT_POST,1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                            # ignore SSL errors
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                    curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
                    curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Accept: application/json',
                            'Connection: keep-alive',
                            'X-MyBring-API-Uid: '.$apiUser,
                            'X-MyBring-API-Key: '.$apiKey,
                            'Content-Length: ' . strlen($data_string))
                            );
                            $result = curl_exec($ch);
                            // Check if any error occured
                            if(curl_errno($ch))
                            {
                                   $this->_getSession()->addError(curl_error($ch));
                                   return false;
                            }
                            
                            
                            $result = Mage::helper('core')->jsonDecode($result);
                            // print_r('<pre>'.$result.'</pre>');die;
                            
                            //check error exist
                            if(is_array($result['consignments'][0]['errors'])){
                                $err = array();
                                foreach($result['consignments'][0]['errors'] as $errorArr){
                                    if(is_array($errorArr['messages'])){
                                        foreach($errorArr['messages'] as $error){
                                            $err[] = 'mybring.com: '.$error['message'];
                                        }
                                    }
                                }
                                foreach($err as $message){
                                  $this->_getSession()->addError($message);
                                }
                             return false;  
                                
                           } elseif(is_array($result['consignments'][0]['confirmation'])){
                               $consignmentId = $result['consignments'][0]['confirmation']['consignmentNumber'];
                               $remoteLink = $result['consignments'][0]['confirmation']['links']['labels'];
                               $packageNumber = $result['consignments'][0]['confirmation']['packages'][0]['packageNumber'];
                               $correlationId = $result['consignments'][0]['confirmation']['packages'][0]['correlationId'];
                               if(isset($inv) && $inv->getId()) $correlationId = $inv->getId();
                               $localFile = Mage::getBaseDir('media').DS.'mybring'.DS.$consignmentId.'.pdf';
                               $trackingModel = Mage::getModel('bring/tracking');
                               $trackingModel
                               ->setData('remote_path',$remoteLink)
                               ->setData('package_number',$packageNumber)
                               ->setData('correlation_id',$correlationId)
                               ->setData('consignment_id',$consignmentId);
                               
                               if($this->getRequest()->getPost('cod') && isset($kid)){
                                   $trackingModel->setData('is_cod',1); 
                               }
                               
                               if(copy($remoteLink, $localFile)){
                                   $trackingModel->setData('file_path',$localFile);
                               }
                               
                               try{
                                   //save bring tracking info
                                   $trackingModel->save();
                                   //save magento tracking info
                                   $trackData = array(
                                       'carrier_code' => 'custom',
                                       'title' => 'bring.no',
                                       'number' => $consignmentId
                                   );
                                   $track = Mage::getModel('sales/order_shipment_track')
                                          ->addData($trackData);
                                   $shipment->addTrack($track);
                                   
                                   //save kid info
                                   if(isset($kid)){
                                         $kidModel = Mage::getModel('bring/kid');
                                         $kidModel->setData(array(
                                         'tracking_id' => $trackingModel->getId(),
                                         'kid' => $kid,
                                         'is_paid' => 0,
                                         'date_created' =>  date("Y-m-d", Mage::getModel('core/date')->timestamp($currentTime))  
                                     ));
                                   
                                         $kidModel->save();
                                   }
                                   
                          
                                   
                                   
                               } catch(Exception $e){
                                   $this->_getSession()->addError($e->getMessage());
                                   return false;
                               }
                               
                           } 
              } 
            
        }
              Mage::register('current_shipment', $shipment);
              // Output shipment label if necessary
              // this code was moved to ups label extension
              if($this->getRequest()->getPost('output_label') && is_file($localFile)){
               Mage::register('bring_label', $localFile);

//            header('Content-Description: File Transfer');
//            header('Content-Type: application/octet-stream');
//            header('Content-Disposition: attachment; filename='.basename($localFile));
//            header('Content-Transfer-Encoding: binary');
//            header('Expires: 0');
//            header('Cache-Control: must-revalidate');
//            header('Pragma: public');
//            header('Content-Length: ' . filesize($localFile));
//            ob_clean();
//            flush();
//            @readfile($localFile);
           }   
        
        return $shipment;
    }

  


    
     public function printBookingLabelAction()
    {
       
        /** @see Mage_Adminhtml_Sales_Order_InvoiceController */
        if ($shipmentId = $this->getRequest()->getParam('invoice_id')) { // invoice_id o_0
            if ($shipment = Mage::getModel('sales/order_shipment')->load($shipmentId)) {
                //var_dump($shipment->getData());die;
                $trackings= Mage::getModel('sales/order_shipment_track')
                           ->getCollection()
                           ->addFieldToFilter('parent_id',$shipment->getId());
                if(count($trackings) < 1){
                     $this->_getSession()
                    ->addError(Mage::helper('sales')->__('you haven\'t create tracking data for this shipment'));
                     $this->_redirect('*/*/view', array('shipment_id' => $shipment->getId()));
                     return;
                 } else {
                     
                     foreach($trackings as $tracking){
                           //only look for bring.no track
                           if($tracking->getTitle() == 'bring.no'){
                                $trackingNumber =  $tracking->getTrackNumber();
                                if($trackingNumber == NULL)$trackingNumber =  $tracking->getNumber(); //magento 1.5
                                $bringTracking = Mage::getModel('bring/tracking')
                                ->getCollection()
                                ->addFieldToFilter('consignment_id', $trackingNumber);
                                
                                if($bringTracking != null){
                                    $label = $bringTracking->getFirstItem()->getFilePath();
                                    
                                        $content = array(
                                            'type' => 'filename',
                                            'value'=> $label
                                        );
    
                                     if(is_file($label) && filesize($label) > 1024){   
                                         $this->_prepareDownloadResponse('packingslip-'.$trackingNumber.'.pdf', $content, 'application/pdf');   
                                         $this->_redirect('*/*/view', array('shipment_id' => $shipment->getId()));
                                         return;
                                     } else {
                                        $remoteFile = $bringTracking->getFirstItem()->getRemotePath();
                                        header("Location: $remoteFile");
                                        exit;
                                     }
                                     
                                }
                           } else {
                                $this->_getSession()
                                ->addError(Mage::helper('sales')->__('you haven\'t booked bring shipping service for this shipment'));
                                $this->_redirect('*/*/view', array('shipment_id' => $shipment->getId()));
                                return;
                           }
                      }
                 }
                  $this->_getSession()
                   ->addError(Mage::helper('sales')->__('there is no bring booking associated with this shipment'));
                      $this->_redirect('*/*/view', array('shipment_id' => $shipment->getId()));
                      return;
            }
        }
        else {
            $this->_forward('noRoute');
        }
    }
 
    
    
}
