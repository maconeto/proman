<?php
class Maconeto_Bring_AjaxController extends Mage_Core_Controller_Front_Action
{

    public function trackAction()
    {
       $result = array();
       if($this->getRequest()->getPost('id'))
       {
        $packageId = $this->getRequest()->getPost('id');
        $REST_request = new Zend_Rest_Client('http://sporing.bring.no');
                $REST_result = $REST_request->restGet('/sporing.xml',array(
                    'q' =>  $packageId
                ));
                
           if($REST_result->isError() || !$REST_result->isSuccessful())
           {
                $result['error'] = true;
                if($REST_result->getbody())
                {
                $result['errmsg'] = $REST_result->getbody();
                } else {
                $result['errmsg'] = 'cannot send request now. please try again later';
                }
           } else {
                $responseObj = simplexml_load_string($REST_result->getBody()); 
                $shipments = array();
                $shipmentNodes = $responseObj->Consignment;
                foreach($shipmentNodes as $shipment)
                {
                   $shipmentId = (string)$shipment['consignmentId'];
                   $packages = array();
                   $packageNodes = $shipment->PackageSet->Package;
                   foreach($packageNodes as $package)
                   {
                       
                       $packageId = (string)$package['packageId'];
                       $pCount = 0;
                       $packages[$pCount]['package_id'] = $packageId;
                       $packages[$pCount]['service_name'] = (string)$package->ProductName;
                       $packages[$pCount]['status_description'] = (string)$package->StatusDescription;
                       $packages[$pCount]['weight'] = (string)$package->Weight;
                       $packages[$pCount]['volume'] = (string)$package->Volume;
                       $packages[$pCount]['pickup_code'] = (string)$package->PickupCode;
                       $packages[$pCount]['lrdate'] = (string)$package->LastRetrievalDate;
                       
                                $events = array();
                                $eventNodes = $package->EventSet->Event;
                                for($i = 0 ; $i < count($eventNodes); $i++)
                                {
                                    $events[$i]['event_description'] = (string) $eventNodes[$i]->Description;
                                    $events[$i]['event_status'] = (string) $eventNodes[$i]->Status;
                                    $events[$i]['unit_id'] = (string) $eventNodes[$i]->UnitId;
                                    $events[$i]['postal_code'] = (string) $eventNodes[$i]->PostalCode;
                                    $events[$i]['city'] = (string) $eventNodes[$i]->City;
                                    $events[$i]['country'] = (string) $eventNodes[$i]->Country;
                                    $events[$i]['date_arrive'] = (string) $eventNodes[$i]->OccuredAtDisplayDate;
                                }  
                       $packages[$pCount]['events'] = $events;        
                       $pCount++;
                   }
                   $sCount = 0;
                   $shipments[$sCount]['id'] = $shipmentId;
                   $shipments[$sCount]['data'] = $packages;
                   $sCount++; 
                }
                $result['error'] = false;
                $result['shipment'] = $shipments;    
                
 //               var_dump($shipments);
       
           }
       }
         $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
    
    
    public function pickupAction()
    {
       $result = array();
       if($this->getRequest()->getPost('id'))
       {
        $packageId = $this->getRequest()->getPost('id');
        $REST_request = new Zend_Rest_Client('http://sporing.bring.no');
                $REST_result = $REST_request->restGet('/sporing.xml',array(
                    'q' =>  $packageId
                ));
                
           if($REST_result->isError() || !$REST_result->isSuccessful())
           {
                $result['error'] = true;
                if($REST_result->getbody())
                {
                $result['errmsg'] = $REST_result->getbody();
                } else {
                $result['errmsg'] = 'cannot send request now. please try again later';
                }
           } else {
                $responseObj = simplexml_load_string($REST_result->getBody()); 
                $shipments = array();
                $shipmentNodes = $responseObj->Consignment;
                foreach($shipmentNodes as $shipment)
                {
                   $shipmentId = (string)$shipment['consignmentId'];
                   $packages = array();
                   $packageNodes = $shipment->PackageSet->Package;
                   foreach($packageNodes as $package)
                   {
                       
                       $packageId = (string)$package['packageId'];
                       $pCount = 0;
                       $packages[$pCount]['package_id'] = $packageId;
                       $packages[$pCount]['service_name'] = (string)$package->ProductName;
                       $packages[$pCount]['status_description'] = (string)$package->StatusDescription;
                       $packages[$pCount]['weight'] = (string)$package->Weight;
                       $packages[$pCount]['volume'] = (string)$package->Volume;
                       $packages[$pCount]['pickup_code'] = (string)$package->PickupCode;
                       $packages[$pCount]['lrdate'] = (string)$package->LastRetrievalDate;
                       
                                $events = array();
                                $eventNodes = $package->EventSet->Event;
                                for($i = 0 ; $i < count($eventNodes); $i++)
                                {
                                    $events[$i]['event_description'] = (string) $eventNodes[$i]->Description;
                                    $events[$i]['event_status'] = (string) $eventNodes[$i]->Status;
                                    $events[$i]['unit_id'] = (string) $eventNodes[$i]->UnitId;
                                    $events[$i]['postal_code'] = (string) $eventNodes[$i]->PostalCode;
                                    $events[$i]['city'] = (string) $eventNodes[$i]->City;
                                    $events[$i]['country'] = (string) $eventNodes[$i]->Country;
                                    $events[$i]['date_arrive'] = (string) $eventNodes[$i]->OccuredAtDisplayDate;
                                }  
                       $packages[$pCount]['events'] = $events;        
                       $pCount++;
                   }
                   $sCount = 0;
                   $shipments[$sCount]['id'] = $shipmentId;
                   $shipments[$sCount]['data'] = $packages;
                   $sCount++; 
                }
                $result['error'] = false;
                $result['shipment'] = $shipments;    
                
 //               var_dump($shipments);
       
           }
       }
         $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

}
