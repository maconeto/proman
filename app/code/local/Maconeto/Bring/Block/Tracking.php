<?php
class Maconeto_Bring_Block_Tracking extends Mage_Core_Block_Template
{
     public function getCustomerTrackingCollection($customerId = null) {

        if (is_null($customerId)) {
            $customerId = Mage::getSingleton('customer/session')->getId();
        }

        $trackingCollection = Mage::getResourceModel('sales/order_shipment_track_collection')->addFieldToFilter('carrier_code', 'custom');

        $trackingCollection->getSelect()->join(array('sales_shipment' => Mage::getSingleton('core/resource')->getTableName('sales/shipment')),
                'main_table.parent_id = sales_shipment.entity_id', 
                array('sales_shipment.customer_id', 'sales_shipment.order_id', 'sales_shipment.increment_id'))
                ->where('sales_shipment.customer_id = ?', $customerId)
                ->order('created_at DESC')
                ->limit(30);

        return $trackingCollection;
    }
    
}
