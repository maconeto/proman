<?php

$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$volumeAtt = Mage::getModel('catalog/resource_eav_attribute')
    ->loadByCode('catalog_product','volume');

if(!$volumeAtt->getId())
{
$setup->addAttribute('catalog_product', 'volume', array(
        'type'              => 'text',
        'label'             => 'Volume',
        'input'             => 'text',
        'note'              => '(dm3)',
        'frontend_class'     => 'validate-digits',
        'source'            => '',
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'sort_order'        => 6,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'is_wysiwyg_enabled'   => false,
        'is_html_allowed_on_front' => false,
        'visible_on_front'  => false,
        'visible_in_advanced_search' => false,
        'unique'            => false,
    ));
}



$installer->endSetup();
