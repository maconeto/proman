<?php

$installer = $this;
$installer->startSetup();



$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('bring/tracking')}` (
    `id` int(10) unsigned NOT NULL auto_increment,
    `consignment_id`  varchar(255) NOT NULL default '',
    `file_path`  varchar(255) NOT NULL default '',
    `remote_path`varchar(255) NOT NULL default '',
    `correlation_id`varchar(255),
    `package_number`varchar(255),
    PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

/**
 * Creating folder for uploads storage
 */

$path = Mage::getBaseDir('media').DS.'mybring';
if(!file_exists($path))
    @mkdir($path);


//$setup->removeAttribute( 'catalog_product', 'bring_allow' );
//$setup->removeAttribute( 'catalog_product', 'bring_cod' );
//   $setup->addAttribute('catalog_product', 'bring_allow', array(
//	'label' => 'shipped by Bring',
//        'type'     => 'int',
//	'default' => '1',
//        'note'    => 'if select no, order contains this product cannot be shipped with Bring',
//	'input' => 'boolean',
//	'source' => 'eav/entity_attribute_source_boolean',
//	'visible' => true,
//	'required' => false,
//	'position' => 12,
//        'searchable'        => false,
//        'filterable'        => false,
//        'comparable'        => false,
//       'visible_on_front' => false
//    ));
//   
//   $setup->addAttribute('catalog_product', 'bring_cod', array(
//	'label' => 'allow COD with Bring',
//        'type'     => 'int',
//	'default' => '1',
//        'note'    => 'if select no, order contains this product cannot be shipped with Bring - CashOnDelivery',
//	'input' => 'boolean',
//	'source' => 'eav/entity_attribute_source_boolean',
//	'visible' => true,
//	'required' => false,
//	'position' => 12,
//        'searchable'        => false,
//        'filterable'        => false,
//        'comparable'        => false,
//        'visible_on_front' => false
//    ));

$installer->endSetup();
