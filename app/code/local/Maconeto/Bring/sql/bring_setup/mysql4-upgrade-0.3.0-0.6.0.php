<?php

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('bring/kid')}` (
    `id` int(10) unsigned NOT NULL auto_increment,
    `tracking_id` int(10) NOT NULL,
    `kid`  varchar(255) NOT NULL,
    `is_paid` tinyint(1) unsigned NOT NULL default '0',
    `date_created` date DEFAULT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");



$installer->getConnection()->addColumn(
        $this->getTable('bring/tracking'), //table name
        'is_cod',      //column name
        'smallint(1) NOT NULL DEFAULT 0'  
        );


$installer->endSetup();
