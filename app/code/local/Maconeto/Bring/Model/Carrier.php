<?php
class Maconeto_Bring_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract
{

    protected $_code = 'bring';
    public $packageValue;
    public $baseCurrencyCode;
    public $originZIP;
    public $originCountry;
    public $desZIP;
    public $desCountry;
    public $handlingFee = 0;
    public $flagCod = false;
 
    

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigData('active'))
        {
            return false;
        } else {

            if(Mage::helper('bring')->canShip())
            {
                
                $this->initShipment($request);
                $result = Mage::getModel('shipping/rate_result');
                
                
                //If free shipping is enabled and satisfied, return free method only 
                if ($this->getConfigData('free_shipping_enable') && $request->getPackageValue() >= $this->getConfigData('free_shipping_subtotal'))
                {
                    $method = Mage::getModel('shipping/rate_result_method');
                    $methodTitle = 'Gratis Frakt!';
                    $method->setPrice(0);
                    $method->setCarrier($this->_code);
                    $method->setCarrierTitle($this->getConfigData('title'));
                    $method->setMethod($methodTitle);
                    $method->setMethodTitle($methodTitle);
                    $result->append($method);
                    return $result;
                } 
                
              
                $metric  = $this->getConfigData('weight_metric');
                if($metric == 'gram')
                {
                    $weight  = ceil($request->getPackageWeight());
                } elseif($metric == 'kilogram') {
                    $weight  = ceil($request->getPackageWeight() * 1000);
                }
         
                $packageVolume = 0.0;
                
                     foreach ($request->getAllItems() as $item) {
                        if ($item->getParentItem()) { continue; }
                        $product = Mage::getModel('catalog/product')->load($item->getProductId());

                        //get volume of each item
                        if($this->getConfigData('enable_sending_dimension')){
                             $productVolume = $product->getVolume()?$product->getVolume():$this->getConfigData('default_volume');
                             $packageVolume += $productVolume * $item->getQty();
                             $packageVolume = ceil($packageVolume);
                        }
                     }
                 if($packageVolume > 0){
                     $volumeRequestString = '&volume='.$packageVolume;
                 } else {
                     $volumeRequestString ='';
                 }        

               $productsString = '';
               $codProductsString = '';
               
               
               $allowMethods = $this->getAllowedMethods();
             
               foreach($allowMethods as $m){
                   $productsString .= '&product='.$m;
               }
               
               $baseURL = 'https://api.bring.com/shippingguide/products/all.xml?'
               .'from='.$this->originZIP
               .'&to='.$this->desZIP
               .'&fromCountry='.$this->originCountry
               .'&toCountry='.$this->desCountry
               .'&clientUrl='.$_SERVER['SERVER_NAME']      
               .'&weightInGrams='.$weight.$volumeRequestString;
               if($this->getConfigData('email_sms_notify_enable'))
               {
                   $baseURL .= '&additional=evarsling';
               }
               if($this->getConfigData('ship_from_post_office'))
               {
                   $baseURL .= '&postingAtPostoffice=true';
               }
               if(!$this->getConfigData('edi_registered'))
               {
                   $baseURL .= '&edi=false';
               }
               if($this->getConfigData('public_id'))
               {
                   $baseURL .= '&pid='.trim($this->getConfigData('public_id'));
               }
                
               $requestURL = $baseURL . $productsString;
               if($this->getConfigData('debug')){
                     Mage::log('Request Sent To Bring: '.$requestURL, null, 'bring.log');
                             
                }
              // var_dump($requestURL);die;
               $responseObj = $this->sendRequest($requestURL);
               if(!$responseObj)
               {
                  if($this->getConfigData('debug')){
                     Mage::log('Answer return from Bring: '.Zend_Debug::dump($responseObj), null, 'bring.log');
                    }
                   return false;
               }   
               //    var_dump($responseObj);die;
                   
                    
                  
                    $this->__loadMethod($result, $responseObj);
                    
                    //if COD is enabled, add more method with COD option to result
                    if($this->flagCod){
                        $methodArr = array_intersect($allowMethods, array('A-POST','B-POST','BPAKKE_DOR-DOR', 'SERVICEPAKKE', 'EKSPRESS09'));
                        foreach($methodArr as $m){
                            $codProductsString .= '&product='.$m;
                        }
                        $codRequestURL = $baseURL . $codProductsString . '&additional=POSTOPPKRAV';
                        $responseObj2 = $this->sendRequest($codRequestURL);
                        if(!$responseObj2)return false;
                        $responseObj2->addChild('cod', '1');;
                        $this->__loadMethod($result, $responseObj2);

                   }
                 
                return $result;
            }
            if($this->getConfigData('debug')){
                Mage::log('license not valid for domain name:'.$_SERVER['SERVER_NAME'],null,'bring.log');
            }
            return false;
         }
    }
    
    
    public function initShipment($request){
                $this->baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
                $this->originZIP          = $request->getPostcode();
                $this->originCountry      = $request->getOrigCountryId() != null?$request->getOrigCountryId():$request->getCountryId();
                $this->desZIP             = $request->getDestPostcode();
                $this->desCountry         = $request->getDestCountryId();
                $this->flagCod =  $this->getConfigData('cod')?true:false;
                $this->handlingFee = $this->getConfigData('handling_fee');
                $this->alowMethods = $this->getAllowedMethods();
                $this->packageValue = $request->getPackageValue();
        
    }
    
    //@param1 model shipping/rate_result
    private function __loadMethod(&$result, $responseObj){
         $serviceNodes =  $responseObj->Product; 
         for($i=0;$i<count($serviceNodes);$i++)
                    {
                        $method = Mage::getModel('shipping/rate_result_method');
                        $methodCode = (string)$serviceNodes[$i]->ProductId;
                        $methodTitle = Mage::helper('bring')->getServiceName($methodCode);
                 
                        
                        if($serviceNodes[$i]->Price->PackagePriceWithAdditionalServices){
                        $price = floatval($serviceNodes[$i]->Price->PackagePriceWithAdditionalServices->AmountWithVAT);    
                            if($responseObj->cod){
                                    $methodTitle .= Mage::helper('bring')->__(' (postoppkrav)');
                                    $methodCode .= '_COD';
                            }
                        
                        } else {
                        $price = floatval($serviceNodes[$i]->Price->PackagePriceWithoutAdditionalServices->AmountWithVAT);
                        }
                        
                        if($this->originCountry .'K' !== $this->baseCurrencyCode) //eg: NO(country) -> NOK(currency) or SE -> SEK
                        {
                          $price = $this->convertCurrency($price);
                        }
                        $price = ceil($price);
                        
                        
                        if(!$price || empty($price))
                            continue;
                        
                         if($this->getConfigData('handling_type') == 'F')
                            $method->setPrice($price +  $this->handlingFee);
                         if($this->getConfigData('handling_type') == 'P')
                            $method->setPrice($price + $this->packageValue *  $this->handlingFee / 100);
                            
                        $method->setCarrier($this->_code);
                        $method->setCarrierTitle($this->getConfigData('title'));
                        $method->setMethod($methodCode);
                        $method->setMethodTitle($methodTitle);
                          
                     $result->append($method);
                    
                    }
    }
    
    

    public function getAllowedMethods()
    {
        $allowed = explode(',', $this->getConfigData('allowed_methods'));
        return $allowed;
    }
    
    
    public function sendRequest($requestURL){
                    $requestURL = preg_replace('/\s+/', '', $requestURL);
                    $ch = curl_init();
                    # specify the URL to be retrieved
                    curl_setopt($ch, CURLOPT_URL,$requestURL);
                     
                    # we want to get the contents of the URL and store it in a variable
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                 
                    # specify the useragent: this is a required courtesy to site owners
                    curl_setopt($ch, CURLOPT_USERAGENT, 'cURL');
                     
                     # ignore SSL errors
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                   // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                    curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
                   // curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA');
               
                    
                     /**
                     * Execute the cURL session
                     */
                     $contents = curl_exec($ch);                 
                     /**
                     * Close cURL session
                     */
               
                    if(curl_errno($ch) && $this->getConfigData('debug')){
                         $error_message = curl_strerror($errno);
                         Mage::log($error_message,null,'bring.log');
                         return false;
                     }
                     curl_close ($ch);
                     $responseObj = @simplexml_load_string($contents);
                     return $responseObj;

                  
    }
    
    
    
    public  function  convertCurrency($price){
                            //try to get price in base currency. please note that
                            //if $originCountry != NO , returned currency is in EUR.
                           if($this->originCountry == 'DK'){
                               $originalCurrency = 'DKK';
                           }elseif($this->originCountry == 'SE'){
                               $originalCurrency = 'SEK';
                           }elseif($this->originCountry == 'FI'){
                               $originalCurrency = 'EUR';
                           }else{
                               $originalCurrency ='NOK';
                           }
                            
                            try{
                            $rate = Mage::getModel('directory/currency')->load($this->baseCurrencyCode)->getAnyRate($originalCurrency);
                            $price = $price/$rate;
                            } catch(Mage_Core_Exception $e) {
                                if($this->getConfigData('debug')){
                                    Mage::log('Error in converting currency' . $e->getMessage(), null, 'bring.log');
                                    return false;
                                }
                            }
                            return $price;
       } 
}
