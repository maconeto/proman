<?php
class Maconeto_Bring_Model_Source_Weight
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'gram', 'label' => Mage::helper('bring')->__('Gram')),
            array('value' => 'kilogram', 'label' => Mage::helper('bring')->__('Kilogram')),
        );
    }
}
?>