<?php

class Maconeto_Bring_Model_Source_Method
{
    public function toOptionArray()
    {
           
            $arr = array( array('value' => 'SERVICEPAKKE' , 'label' => 'Klimanøytral Servicepakke'),
                          array('value' => 'PA_DOREN' , 'label' => 'På Døren'),
                         array('value' => 'EKSPRESS09' , 'label' => 'Ekspress 09'),
//                         array('value' => 'A-POST' , 'label' => 'A-Prioritert'),
                         array('value' => 'MAIL' , 'label' => 'Brev'),
                         array('value' => 'QUICKPACK' , 'label' => 'QuickPack'),
                         array('value' => 'COURIER_VIP' , 'label' => 'Bud VIP'),
                         array('value' => 'COURIER_1H' , 'label' => 'Bud 1 time'),
                         array('value' => 'COURIER_2H' , 'label' => 'Bud 2 time'),
                         array('value' => 'COURIER_4H' , 'label' => 'Bud 4 time'),
                         array('value' => 'COURIER_6H' , 'label' => 'Bud 6 time'),
                         array('value' => 'BUSINESS_PARCEL' , 'label' => 'PickUp Business'),
                         array('value' => 'PICKUP_PARCEL' , 'label' => 'PickUp Parcel')
                );
        return $arr;
    }
}
