<?php

class TM_SoldTogether_Adminhtml_Soldtogether_CustomerController
    extends TM_SoldTogether_Controller_Adminhtml_Abstract
{
    /**
     * Title of grid in admin
     *
     * @var string
     */
    protected $_soldtogetherTitle = 'Customers Also Bought';

    /**
     * Message "in progress" for reindexAction
     *
     * @var string
     */
    protected $_inProgressMessage = '%d customer(s) reindexed';

    /**
     * Message "on success" for reindexAction
     *
     * @var string
     */
    protected $_successMessage = '%d customer(s) was successfully reindexed';
}
