<?php

class TM_SoldTogether_Adminhtml_Soldtogether_OrderController
    extends TM_SoldTogether_Controller_Adminhtml_Abstract
{
     /**
     * Title of grid in admin
     *
     * @var string
     */
    protected $_soldtogetherTitle = 'Frequently Bought Together';

    /**
     * Message "in progress" for reindexAction
     *
     * @var string
     */
    protected $_inProgressMessage = '%d order(s) reindexed';

    /**
     * Message "on success" for reindexAction
     *
     * @var string
     */
    protected $_successMessage = '%d order(s) was successfully reindexed';
}
