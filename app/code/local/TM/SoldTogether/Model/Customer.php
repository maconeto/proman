<?php

class TM_SoldTogether_Model_Customer extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('soldtogether/customer');
    }

    public function reindexRelations($page)
    {
        $collection = Mage::getResourceModel('customer/customer_collection');
        $collection->setCurPage($page)
            ->setPageSize(50);
        if ($page > $collection->getLastPageNumber()) {
            return 0;
        }

        $collection->walk(array($this->_getResource(), 'addOrderProductData'));
        return $collection->count();
    }
}