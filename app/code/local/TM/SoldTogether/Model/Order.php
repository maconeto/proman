<?php

class TM_SoldTogether_Model_Order extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('soldtogether/order');
    }

    public function reindexRelations($page)
    {
        $minDate = Mage::helper('soldtogether')->getReindexMinDate()
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $statuses = Mage::helper('soldtogether')->getReindexStatuses();
        $collection = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('created_at', array('gteq' => $minDate))
            ->addFieldToFilter('status', array('in' => $statuses))
            ->setCurPage($page)
            ->setPageSize(100);
        if ($page > $collection->getLastPageNumber()) {
            return 0;
        }

        $collection->walk(array($this->_getResource(), 'addOrderProductData'));
        return $collection->count();
    }
}
