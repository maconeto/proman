<?php
/**
 * Fallback model to lock process at Magento lower then 1.9.1
 */
class TM_SoldTogether_Model_Cron_Lock
{
    /**
     * Process lock properties
     */
    protected $_isLocked = null;
    protected $_lockFile = null;

    /**
     * Get lock file resource
     *
     * @return resource
     */
    protected function _getLockFile($name)
    {
        if ($this->_lockFile === null) {
            $varDir = Mage::getConfig()->getVarDir('locks');
            $file = $varDir . DS . $name.'.lock';
            if (is_file($file)) {
                $this->_lockFile = fopen($file, 'w');
            } else {
                $this->_lockFile = fopen($file, 'x');
            }
            fwrite($this->_lockFile, date('r'));
        }
        return $this->_lockFile;
    }

    /**
     * Set lock
     *
     * @param string $lockName
     * @param boolean $useFileLock (ignored - always file lock)
     * @return TM_SoldTogether_Model_Cron_Lock
     */
    public function setLock($lockName, $useFileLock)
    {
        $this->_isLocked = true;
        flock($this->_getLockFile($lockName), LOCK_EX | LOCK_NB);
        return $this;
    }

    /**
     * Release lock
     *
     * @param string $lockName
     * @param boolean $useFileLock (ignored - always file lock)
     * @return TM_SoldTogether_Model_Cron_Lock
     */
    public function releaseLock($lockName, $useFileLock)
    {
        $this->_isLocked = false;
        flock($this->_getLockFile($lockName), LOCK_UN);
        return $this;
    }

    /**
     * Check if lock exists
     *
     * @param string $lockName
     * @param boolean $useFileLock (ignored - always file lock)
     * @return boolean
     */
    public function isLockExists($lockName, $useFileLock)
    {
        if ($this->_isLocked !== null) {
            return $this->_isLocked;
        } else {
            $fp = $this->_getLockFile($lockName);
            if (flock($fp, LOCK_EX | LOCK_NB)) {
                flock($fp, LOCK_UN);
                return false;
            }

            return true;
        }
    }
}
