<?php

class TM_SoldTogether_Block_Adminhtml_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'soldtogether';
        $this->_controller = 'adminhtml';
        $this->_updateButton('save', 'label', $this->__('Save Item'));
        $this->_updateButton('delete', 'label', $this->__('Delete Item'));
    }

    public function getHeaderText()
    {
        if( Mage::registry('soldtogether_data') && Mage::registry('soldtogether_data')->getId() ) {
            return $this->__("Edit Item");
        } else {
            return $this->__('Add Item');
        }
    }

    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save', array('_current' => true));
    }
}
