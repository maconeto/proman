<?php

class TM_SoldTogether_Block_Adminhtml_Catalog_Product_Edit_Tab_Customer
    extends TM_SoldTogether_Block_Adminhtml_Catalog_Product_Edit_Tab_Order
{
    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('soldtogether_customer_product_grid');
    }

    /**
     * Retrieve selected related products
     *
     * @return array
     */
    protected function _getSelectedProducts()
    {
        $products = $this->getRequest()->getPost('soldtogether_customer', null);
        if (!is_array($products)) {
            $products = array_keys($this->getSelectedRelatedProducts());
        }
        return $products;
    }

    public function getResourceModel()
    {
        return Mage::getResourceModel('soldtogether/customer');
    }
}
