<?php

class TM_SoldTogether_Block_Adminhtml_Customer
    extends TM_SoldTogether_Block_Adminhtml_Order
{
    /**
     * Get header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return $this->__('Products Bought by Same Customer Manager');
    }

    /**
     * Get url to controller action
     *
     * @param  string $actionName
     * @return string
     */
    public function getActionUrl($actionName)
    {
        return $this->getUrl('adminhtml/soldtogether_customer/' . $actionName);
    }
}
