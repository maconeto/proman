<?php
class TM_SoldTogether_Block_Adminhtml_Order
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * So called "container controller" to specify group of blocks participating in some action
     *
     * @var string
     */
    protected $_controller = 'adminhtml';

    /**
     * Block group (use to create block with grid)
     *
     * @var string
     */
    protected $_blockGroup = 'soldtogether';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_removeButton('add');
        $reindexUrl = $this->getActionUrl('reindex');
        $indexUrl = $this->getActionUrl('index');
        $this->_addButton('reindex', array(
            'label'     => Mage::helper('soldtogether')->__('Reindex Data'),
            'onclick'   => "
			function sendRequest(clearSession) {
				new Ajax.Request('" . $reindexUrl . "', {
					method: 'post',
                    parameters: {
                        clear_session: clearSession
                    },
					onSuccess: showResponse
					});
				}

			function showResponse(response) {
                var response = response.responseText.evalJSON();
                if (!response.completed) {
                    sendRequest(0);
                    var imageSrc = $('loading_mask_loader').select('img')[0].src;
    				$('loading_mask_loader').innerHTML =
                        '<img src=\'' + imageSrc + '\'/><br/>' + response.message;
                } else {
                    window.location = '" . $indexUrl . "'
                }
			}
            sendRequest(1);
            ",
        ));
    }

    /**
     * Get header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return $this->__('Products Bought Together Manager');
    }

    /**
     * Get url to controller action
     *
     * @param  string $actionName
     * @return string
     */
    public function getActionUrl($actionName)
    {
        return $this->getUrl('adminhtml/soldtogether_order/' . $actionName);
    }
}