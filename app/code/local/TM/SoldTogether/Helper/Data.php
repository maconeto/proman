<?php

class TM_SoldTogether_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCheckoutSuccessColCount()
    {
        return Mage::getStoreConfig('checkoutsuccess/mockupsets/soldtogether_columns');
    }

    public function getCheckoutSuccessProductsCount()
    {
        return Mage::getStoreConfig('checkoutsuccess/mockupsets/soldtogether_productscount');
    }

    public function getReindexMinDate()
    {
        $date = Mage::app()->getLocale()->date();
        $days = Mage::getStoreConfig('soldtogether/general/reindex_days');
        $date->subDay(is_numeric($days) ? $days : 0);
        return $date;
    }

    public function getReindexStatuses()
    {
        return explode(
            ',',
            Mage::getStoreConfig('soldtogether/general/reindex_statuses')
        );
    }
}
