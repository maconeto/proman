<?php

class TM_ArgentoPure_Upgrade_1_0_0 extends TM_Argento_Model_Upgrade
{
    /**
     * Create new and recommended products, if they are not exists
     */
    public function up()
    {
        // Create new and recommended products if they are not exists
        $this->setupProducts(array('new', 'recommended'));
    }

    public function getOperations()
    {
        return array(
            'configuration' => $this->_getConfiguration(),
            'cmsblock'      => $this->_getCmsBlocks(),
            'cmspage'       => $this->_getCmsPages(),
            'easytabs'      => $this->_getEasytabsTabs(),
            'easyslide'     => $this->_getSlider(),
            'easybanner'    => $this->_getEasybanner(),
            'productAttribute' => $this->_getProductAttribute()
        );
    }

    private function _getConfiguration()
    {
        $config = array_merge(
            // default argento package configuration
            $this->getDefaultConfiguration('pure'),
            // override configuration with some pure specific values
            array(
                'argento_pure/old_css/use' => 0
            )
        );
        return $config;
    }

    /**
     * header_links
     * header_callout
     * footer_links
     * footer_contacts
     * footer_social
     */
    private function _getCmsBlocks()
    {
        return array(
            'header_callout' => array(
                'title' => 'header_callout',
                'identifier' => 'header_callout',
                'status' => 1,
                'content' => <<<HTML
<img class="header-callout hidden-phone hidden-tablet" src="{{skin url="images/media/header_callout.gif"}}" alt="Toll-Free Customer Support 24/7" style="margin: 0; position: absolute; left: 300px; top: 2px;"/>
HTML
            ),
            'scroll_up' => array(
                'title'      => 'scroll_up',
                'identifier' => 'scroll_up',
                'status'     => 1,
                'content'    => <<<HTML
<p id="scroll-up" class="hidden-desktop hidden-tablet hidden-phone">
    <a href="#">Back to top</a>
</p>
HTML
            ),
            'footer_cms' => array(
                'title' => 'footer_cms',
                'identifier' => 'footer_cms',
                'status' => 1,
                'content' => <<<HTML
<div class="footer-cms-container">
    <div class="footer-cms">
      <div class="row footer-set">
          <div class="col-md-5 col-sm-6 col-customer-info">
              <div class="block block-footer-links">
              <div class="block-title"><span>Customer Information</span></div>
              <div class="block-content">
                  <ul class="footer-links">
                    <li>
                      <ul>
                        <li><a href="{{store direct_url="block"}}">Blog</a></li>
                        <li><a href="{{store direct_url="location"}}">Store locator</a></li>
                        <li><a href="{{store direct_url="media"}}">Media</a></li>
                        <li><a href="{{store direct_url="help-center"}}">Help Center</a></li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <li><a href="{{store direct_url="customer/account"}}">My Account</a></li>
                        <li><a href="{{store direct_url="sales/order/history"}}">Order Status</a></li>
                        <li><a href="{{store direct_url="wishlist"}}">Wishlist</a></li>
                        <li><a href="{{store direct_url="exchanges"}}">Returns and Exchanges</a></li>
                      </ul>
                    </li>
                    <li class="last">
                      <ul>
                        <li><a href="{{store direct_url="our-company"}}">Our Company</a></li>
                        <li><a href="{{store direct_url="about"}}">About us</a></li>
                        <li><a href="{{store direct_url="careers"}}">Careers</a></li>
                        <li><a href="{{store direct_url="shipping"}}">Shipping</a></li>
                      </ul>
                    </li>
                  </ul>
              </div>
              </div>
              <img width="200" height="60" style="margin-top: 10px;" src="{{skin url="images/security_sign.gif"}}" srcset="{{skin url="images/security_sign@2x.gif"}} 2x" alt="Security Seal"/>
          </div>
          <div class="col-md-3 col-sm-6 col-about">
              <div class="block footer-about">
                  <div class="block-title"><span>About us</span></div>
                  <div class="block-content">
                      <p>Argento is more than just another template created for Magento. It was created right from the ground based on the best ecommerce stores practices.</p>
                  </div>
              </div>
              <div class="block footer-social">
                  <div class="block-title"><span>Join our community</span></div>
                  <div class="block-content">
                      <ul class="icons">
                          <li class="facebook"><a href="facebook.com">Facebook</a></li>
                          <li class="twitter"><a href="twitter.com">Twitter</a></li>
                          <li class="youtube"><a href="youtube.com">YouTube</a></li>
                          <li class="rss"><a href="rss.com">Rss</a></li>
                      </ul>
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-sm-12 col-contact">
              {{block type="newsletter/subscribe" name="newsletter.footer" template="newsletter/subscribe.phtml"}}
              <div class="block footer-call-us">
                  <div class="block-title"><span>Call us</span></div>
                  <div class="block-content">
                      <p class="footer-phone">1.800.555.1903</p>
                      <p>We're available 24/7. Please note the more accurate the information you can provide us with the quicker we can respond to your query.</p>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>
HTML
            )
        );
    }

    /**
     * home
     */
    private function _getCmsPages()
    {
        return array(
            'home' => array(
                'title'             => 'home',
                'identifier'        => 'home',
                'root_template'     => 'one_column',
                'meta_keywords'     => '',
                'meta_description'  => '',
                'content_heading'   => '',
                'is_active'         => 1,
                'content'           => <<<HTML
<div class="row">
    <div class="col-md-12">
        <div class="homeslider">
            {{widget type="easyslide/insert" slider_id="argento_pure"}}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{widget type="easycatalogimg/widget_list" category_count="4" column_count="4" show_image="1" resize_image="1" image_width="180" image_height="180" subcategory_count="1" template="tm/easycatalogimg/list.phtml"}}
    </div>
</div>

<div class="row hidden-tablet hidden-phone a-center">
    <div class="col-md-12">
        {{widget type="easybanner/widget_placeholder" placeholder_name="argento-home-content"}}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {{widget type="easytabs/widget" filter_tabs="new-arrivals,bestsellers,recommended-products,on-sale,about-us" template="tm/easytabs/tabs.phtml"}}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="block brands-home hidden-phone">
            <div class="block-title">
                <span>Featured Brands</span>
            </div>
            <div class="block-content">
                <div id="slider-brands-container" class="slider-wrapper">
                    <div class="slick" id="slider-brands" data-slick='{"slidesToShow": 6, "slidesToScroll": 6, "infinite": true, "swipeToSlide": true, "autoplay": true, "autoplaySpeed": 2000, "arrows": true, "responsive": [ {"breakpoint": 770, "settings": {"slidesToShow": 4, "slidesToScroll": 4}}, {"breakpoint": 640, "settings": {"slidesToShow": 3, "slidesToScroll": 3}}, {"breakpoint": 480, "settings": {"slidesToShow": 2, "slidesToScroll": 2}}, {"breakpoint": 321, "settings": {"slidesToShow": 1, "slidesToScroll": 1, "dots": false}}]}'>
                        <a href="#"><img src="{{skin url="images/catalog/brands/gucci.jpg"}}" alt="" width="150" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/lv.jpg"}}" alt="" width="100" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/ck.jpg"}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/chanel.jpg"}}" alt="" width="170" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/guess.jpg"}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/versace.jpg"}}" alt="" width="145" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/gucci.jpg"}}" alt="" width="150" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/lv.jpg"}}" alt="" width="100" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/ck.jpg"}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/chanel.jpg"}}" alt="" width="170" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/guess.jpg"}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/versace.jpg"}}" alt="" width="145" height="80"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
HTML
,
                'layout_update_xml' => ''
            )
        );
    }

    private function _getSlider()
    {
        return array(
            array(
                'identifier'    => 'argento_pure',
                'title'         => 'Argento Pure',
                'width'         => 958,
                'height'        => 349,
                'duration'      => 0.5,
                'frequency'     => 4.0,
                'autoglide'     => 1,
                'controls_type' => 'number',
                'status'        => 1,
                'slides'        => array(
                    array(
                        'url'   => 'argento/pure/argento_pure_slider1.jpg',
                        'image' => 'Slide 1',
                        'description' => 'Semi-Annual Sale: Extra 10% off Select fashion brands'
                    ),
                    array(
                        'url'   => 'argento/pure/argento_pure_slider2.jpg',
                        'image' => 'Slide 2',
                        'description' => 'Semi-Annual Sale: Extra 10% off Select fashion brands'
                    ),
                    array(
                        'url'   => 'argento/pure/argento_pure_slider3.jpg',
                        'image' => 'Slide 3',
                        'description' => 'Semi-Annual Sale: Extra 10% off Select fashion brands'
                    ),
                    array(
                        'url'   => 'argento/pure/argento_pure_slider4.jpg',
                        'image' => 'Slide 4',
                        'description' => 'Semi-Annual Sale: Extra 10% off Select fashion brands'
                    ),
                    array(
                        'url'   => 'argento/pure/argento_pure_slider5.jpg',
                        'image' => 'Slide 5',
                        'description' => 'Semi-Annual Sale: Extra 10% off Select fashion brands'
                    )
                )
            )
        );
    }

    private function _getEasybanner()
    {
        return array(
            array(
                'name'         => 'argento-home-content',
                'parent_block' => 'non-existing-block',
                'banners'      => array(
                    array(
                        'identifier' => 'argento_pure-home-content1',
                        'title'      => 'HP Envy 17',
                        'url'        => 'discounts',
                        'image'      => 'argento/pure/argento_pure_callout_home_content1.gif',
                        'width'          => 960,
                        'height'         => 147,
                        'resize_image'   => 1,
                        'retina_support' => 0
                    )
                )
            )
        );
    }

    private function _getProductAttribute()
    {
        return array(
            array(
                'attribute_code' => 'featured',
                'frontend_label' => array('Featured'),
                'default_value'  => 0
            ),
            array(
                'attribute_code' => 'recommended',
                'frontend_label' => array('Recommended'),
                'default_value'  => 0
            )
        );
    }

    private function _getEasytabsTabs()
    {
        return array(
            array(
                'title' => 'New arrivals',
                'alias' => 'new-arrivals',
                'block' => 'easytabs/tab_html',
                'sort_order' => 10,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_new" title="" products_count="12" column_count="4" template="tm/highlight/product/grid.phtml" class_name="highlight-new" show_page_link="1" page_title="All new products &raquo;" img_width="200" img_height="200"}}'
            ),
            array(
                'title' => 'Bestsellers',
                'alias' => 'bestsellers',
                'block' => 'easytabs/tab_html',
                'sort_order' => 20,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_bestseller" title="" products_count="12" column_count="4" template="tm/highlight/product/grid.phtml" class_name="highlight-bestsellers" show_page_link="1" page_title="All bestsellers &raquo;" img_width="200" img_height="200"}}'
            ),
            array(
                'title' => 'Recommended products',
                'alias' => 'recommended-products',
                'block' => 'easytabs/tab_html',
                'sort_order' => 30,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_attribute_yesno" attribute_code="recommended" title="" products_count="12" column_count="4" template="tm/highlight/product/grid.phtml" class_name="highlight-attribute-recommended" img_width="200" img_height="200"}}'
            ),
            array(
                'title' => 'On Sale',
                'alias' => 'on-sale',
                'block' => 'easytabs/tab_html',
                'sort_order' => 40,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_special" title="" products_count="12" column_count="4" template="tm/highlight/product/grid.phtml" class_name="highlight-special" show_page_link="1" page_title="All on sale products &raquo;" img_width="200" img_height="200"}}'
            ),
            array(
                'title' => 'About Us',
                'alias' => 'about-us',
                'block' => 'easytabs/tab_html',
                'sort_order' => 50,
                'status' => 1,
                'product_tab' => 0,
                'content' => <<<HTML
<div class="row">
    <div class="col-md-4 col-sm-4">
        <p style="line-height:1.2em;"><small>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede.</small></p>
        <p style="color:#888; font:1.2em/1.4em georgia, serif;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta.</p>
    </div>
    <div class="col-md-4 col-sm-4">
        <p><strong style="color:#de036f;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit.</strong></p>
        <p>Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo. </p>
        <p>Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus. Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi. Vestibulum sapien dolor, aliquet nec, porta ac, malesuada a, libero. Praesent feugiat purus eget est. Nulla facilisi. Vestibulum tincidunt sapien eu velit. Mauris purus. Maecenas eget mauris eu orci accumsan feugiat. Pellentesque eget velit. Nunc tincidunt.</p>
    </div>
    <div class="col-md-4 col-sm-4">
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper </p>
        <p><strong style="color:#de036f;">Maecenas ullamcorper, odio vel tempus egestas, dui orci faucibus orci, sit amet aliquet lectus dolor et quam. Pellentesque consequat luctus purus.</strong></p>
        <p>Nunc et risus. Etiam a nibh. Phasellus dignissim metus eget nisi.</p>
        <div class="divider"></div>
        <p>To all of you, from all of us at Magento Demo Store - Thank you and Happy eCommerce!</p>
        <p style="line-height:1.2em;"><strong style="font:italic 2em Georgia, serif;">John Doe</strong><br/><small>Some important guy</small></p>
    </div>
</div>
HTML
            )
        );
    }
}
