<?php

class TM_ArgentoPure_Upgrade_1_5_1 extends TM_Core_Model_Module_Upgrade
{
    public function up()
    {
        // change slider type to Swiper slider
        $slider = Mage::getModel('easyslide/easyslide')->load('argento_pure');
        if (!$slider->getId()) {
            return;
        }
        $slider->setSliderType(2);
        $slider->setModifiedTime(new Zend_Db_Expr('NOW()'));
        $slider->save();
    }
}
