<?php

class TM_Argento_Block_Product_Email_Friend extends Mage_Core_Block_Template
{
    /**
     * Check if product can be emailed to friend
     *
     * @return bool
     */
    public function canEmailToFriend()
    {
        $sendToFriendModel = Mage::registry('send_to_friend_model');
        return $sendToFriendModel && $sendToFriendModel->canEmailToFriend();
    }

    public function getProduct()
    {
        return Mage::registry('product');
    }
}
