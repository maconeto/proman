<?php

class TM_Argento_Block_Product_Review_Summary extends Mage_Core_Block_Abstract
{
    protected $_reviewsHelperBlock;

    /**
     * Create reviews summary helper block once
     *
     * @return boolean
     */
    protected function _initReviewsHelperBlock()
    {
        if (!$this->_reviewsHelperBlock) {
            if (!Mage::helper('catalog')->isModuleEnabled('Mage_Review')) {
                return false;
            } else {
                $this->_reviewsHelperBlock = $this->getLayout()->createBlock('review/helper');
            }
        }

        return true;
    }

    protected function _toHtml()
    {
        if ($this->_initReviewsHelperBlock() && Mage::registry('product')) {
            $product = Mage::registry('product');
            return $this->_reviewsHelperBlock->getSummaryHtml($product, false, true);
        }
        return '';
    }
}
