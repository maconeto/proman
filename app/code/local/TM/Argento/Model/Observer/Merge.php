<?php

class TM_Argento_Model_Observer_Merge
{

    public function addBackendCssFile($observer)
    {
        $observer->getCss()->setBackendCssAdded(false);
        $files = $observer->getCss()->getFiles();
        foreach ($files as $src) {
            // add backend styles to group where argento default css found
            if (strstr($src, 'skin/frontend/argento/default/css/default.css')) {
                $layout = Mage::app()->getLayout();
                if ($layout && $head = $layout->getBlock('argento.head')) {
                    $backendCss = $head->getBackendCss('path');
                }
                if ($backendCss) {
                    $observer->getCss()->setBackendCssAdded(true);
                    array_push($files, $backendCss);
                }
                break;
            }
        }
        $observer->getCss()->setFiles($files);
    }

    public function disableBackendCssOutput($observer)
    {
        $css = $observer->getCss();
        if ($css->getMergedUrl() && $css->getBackendCssAdded()) {
            $layout = Mage::app()->getLayout();
            if ($layout && $head = $layout->getBlock('argento.head')) {
                $head->setCssMerged(true);
            }
        }
    }

}
