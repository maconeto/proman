<?php

class TM_Argento_Model_Observer_Amp
{
    /**
     * Set 2x logo for TM_Amp theme
     *
     * @param  $observer
     * @return void
     */
    public function setLogo2xSrc($observer)
    {
        if (!Mage::helper('tmcore')->isDesignPackageEquals('argento')) {
            return;
        }

        $observer->getThemeValues()->setData(
            'logo_2x_src',
            Mage::helper('argento')->getLogo2xSrc()
        );
    }
}
