<?php

if (Mage::helper('core')->isModuleOutputEnabled('Fooman_SpeedsterAdvanced')) {
    Mage::helper('tmcore')->requireOnce('TM/Argento/Model/Core/Design/Package/FoomanSpeedsterAdvanced.php');
} else {
    class TM_Argento_Model_Core_Design_PackageAbstract extends Mage_Core_Model_Design_Package {}
}
