<?php

class TM_Argento_Helper_Swatches_Mediafallback extends
    Mage_ConfigurableSwatches_Helper_Mediafallback
{
    /**
     * Resize product image for swatches (override original method)
     *
     * @param Mage_Catalog_Model_Product $product
     * @param string $type
     * @param bool $keepFrame
     * @param string $image
     * @param bool $placeholder
     * @return string|bool
     */
    protected function _resizeProductImage($product, $type, $keepFrame, $image = null, $placeholder = false)
    {
        $hasTypeData = $product->hasData($type) && $product->getData($type) != 'no_selection';
        if ($image == 'no_selection') {
            $image = null;
        }

        if (Mage::getDesign()->getPackageName() == 'argento' // works for themes from argento package only
            && ($hasTypeData || $placeholder || $image)) {
            // check above from parent method
            if ($type == 'small_image') {
                // resize swatches pictures the same as listing sizes
                $listBlock = Mage::app()->getLayout()->getBlock('product_list');
                if ($listBlock) {
                    return Mage::helper('argento/product_image')
                        ->resize($product, $type, $listBlock->getData(), $image);
                } else {
                    // block not found so we assume it is not a product listing
                    // and there are no need in small image
                    return false;
                }
            } elseif ($type == 'image') {
                // resize swatches pisture the same as product page size
                $params = Mage::helper('argento/product_image')->getConfig();
                return Mage::helper('argento/product_image')
                    ->resize($product, $type, $params, $image);
            }
        }

        return parent::_resizeProductImage($product, $type, $keepFrame, $image, $placeholder);
    }
}
