<?php

class TM_Argento_Helper_Product_List extends Mage_Core_Helper_Abstract
{
    /**
     * @var string
     */
    protected $_collateralInfoDisabled = null;

    /**
     * @var array
     */
    protected $_image = array();

    /**
     * Get config value for argento theme
     *
     * @param  string $key
     * @return string
     */
    protected function _getArgentoConfig($key)
    {
        $theme = Mage::getDesign()->getTheme('template');
        $path = array(
            'argento_' . $theme . '/' . $key,
            'argento/' . $key,
        );
        $value = Mage::getStoreConfig(reset($path));
        if ($value === null) {
            $value = Mage::getStoreConfig(next($path));
        }
        return $value;
    }

    /**
     * Get add to cart button position in listing
     *
     * @param  string $mode
     * @return string
     */
    public function getPlaceForAddToCart($mode = 'grid')
    {
        return $this->_getArgentoConfig('product_list/'.$mode.'/add_to_cart');
    }

    /**
     * Get size of image in product listing
     *
     * @param  string $mode
     * @return array
     */
    public function getImageConfig($mode = 'grid')
    {
        if (!isset($this->_image[$mode])) {
            $image = $this->_getArgentoConfig('product_list/'.$mode.'/image')
                + array(
                    'width' => 200,
                    'height' => null,
                    'keep_frame' => true
                );
            $bgColor = $this->_getArgentoConfig('product_list/image-background');
            $image['bg_color'] = $bgColor ? $bgColor : '#FFFFFF';
            $this->_image[$mode] = $image;
        }

        return $this->_image[$mode];
    }

    /**
     * Check if collateral info is disabled for product in listing
     *
     * @return boolean
     */
    public function isCollateralInfoDisabled()
    {
        return (bool)$this->_getArgentoConfig(
            'product_list/grid/collateral_info_disabled'
        );
    }

    /**
     * Programatically create layout blocks for list block
     *
     * @param  Mage_Core_Block_Abstract $listBlock
     * @return $this
     */
    public function initChildren(Mage_Core_Block_Abstract $listBlock)
    {
        if (!$listBlock->getChild('add.to.links')) {
            // create child block 'add.to.links' when there are no such
            $listBlock->setChild(
                'add.to.links',
                $listBlock->getLayout()->createBlock('core/template')
                    ->setTemplate('catalog/product/list/add-to-links.phtml')
                    ->getNameInLayout()
            );
        }

        if (!$listBlock->getChild('actions')) {
            // create child block 'actions' when there are no such
            $listBlock->setChild(
                'actions',
                $listBlock->getLayout()->createBlock('core/template')
                    ->setTemplate('catalog/product/list/actions.phtml')
                    ->getNameInLayout()
            );
        }

        if (!$listBlock->getChild('product.image')) {
            // create child block 'product.image' when there are no such
            $listBlock->setChild(
                'product.image',
                $listBlock->getLayout()->createBlock('core/template')
                    ->setTemplate('catalog/product/list/image.phtml')
                    ->getNameInLayout()
            );
        }

        // set image sizes for listing
        $mode = $listBlock->getMode();
        $imageConfig = $this->getImageConfig($mode);
        $image = array();

        foreach ($imageConfig as $key => $value) {
            if ($listBlock->hasData('img_'.$key)) {
                $image['img_'.$key] = $listBlock->getData('img_'.$key);
            } elseif ($listBlock->hasData('image_'.$key)) {
                $image['img_'.$key] = $listBlock->getData('image_'.$key);
            } else {
                $image['img_'.$key] = $value;
            }
        }

        if ((int)$image['img_width'] < 1) {
            $image['img_width'] = null;
        }

        if ((int)$image['img_height'] < 1) {
            $image['img_height'] = null;
        }

        if (!is_array($image['img_bg_color'])) {
            $image['img_bg_color'] = $this->convertHexToRgb($image['img_bg_color']);
        }

        $listBlock->addData($image);
        return $this;
    }

    /**
     * Convert HEX string color to RGB array
     *
     * @param  string $hex
     * @return array
     */
    protected function convertHexToRgb($hex)
    {
        if (is_string($hex) && strpos($hex, '#') === 0 ) {
            return sscanf(
                $hex,
                "#%02x%02x%02x"
            );
        }

        return array(255, 255, 255);
    }
}
