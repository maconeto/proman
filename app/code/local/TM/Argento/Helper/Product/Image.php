<?php

class TM_Argento_Helper_Product_Image extends Mage_Core_Helper_Abstract
{
    /**
     * @var null|array
     */
    protected $_config = null;

    /**
     * Resize product image
     *
     * @param  Mage_Catalog_Model_Product $product
     * @param  string $imageType ('small_image', 'base_image' etc.)
     * @param  array $params
     * @param  strin|null $image
     * @return string
     */
    public function resize(
        Mage_Catalog_Model_Product $product,
        $imageType,
        array $params,
        $image = null
    ) {
        return Mage::helper('catalog/image')
            ->init($product, $imageType, $image)
            ->backgroundColor($params['img_bg_color'])
            ->keepFrame($params['img_keep_frame'])
            ->resize($params['img_width'], $params['img_height'])
            ->__toString();
    }

    /**
     * Get configuration for image on product page
     *
     * @return array
     */
    public function getConfig()
    {
        if (!isset($this->_config)) {
            $c = array();
            list($c['img_width'], $c['img_height']) = explode(
                'x',
                Mage::getStoreConfig('lightboxpro/size/main')
            );
            if (!isset($c['img_width']) || (int)$c['img_width'] < 1) {
                $c['img_width'] = null;
            }

            if (!isset($c['img_height']) || (int)$c['img_height'] < 1) {
                $c['img_height'] = null;
            }

            $c['img_keep_frame'] = Mage::getStoreConfigFlag(
                'lightboxpro/size/main_keep_frame'
            );
            $c['img_bg_color'] = array(255, 255, 255);
            $this->_config = $c;
        }

        return $this->_config;
    }
}
