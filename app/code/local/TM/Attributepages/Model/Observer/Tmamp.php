<?php

class TM_Attributepages_Model_Observer_Tmamp
{
    /**
     * Add attribute based pages to the TM_Amp configuration
     *
     * @param  Varien_Event_Observer $observer
     * @return void
     */
    public function prepareConfig($observer)
    {
        $pages = $observer->getPages();
        $optionArray = $pages->getData();
        $optionArray['attributepages'] = Mage::helper('attributepages')->__('Attribute Pages');
        $pages->setData($optionArray);
    }

    /**
     * TM_Amp integration
     *
     * @param  object  $observer
     * @return void
     */
    public function isPageSupported($observer)
    {
        $result = $observer->getResult();
        $page = $result->getCurrentPage();
        $supportedPages = $result->getSupportedPages();

        if (0 === strpos($page, 'attributepages_') &&
            in_array('attributepages', $supportedPages)) {

            $result->setIsPageSupported(true);
        }
    }
}
