<?php

class TM_Attributepages_Controller_Adminhtml_AbstractController extends Mage_Adminhtml_Controller_Action
{
    public function duplicateAction()
    {
        if ($id = $this->getRequest()->getParam('entity_id')) {
            try {
                $model = Mage::getModel('attributepages/entity');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This page no longer exists.'));
                    $this->_redirect('*/*/');
                    return;
                }
                $newModel = $model->duplicate();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('attributepages')->__('The page has been duplicated.')
                );
                $this->_redirect('*/*/edit', array('_current' => true, 'entity_id' => $newModel->getId()));
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('entity_id' => $id));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('attributepages')->__('Unable to find a page to duplicate.'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('entity_id')) {
            try {
                $model = Mage::getModel('attributepages/entity');
                $model->load($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('cms')->__('The page has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('entity_id' => $id));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('Unable to find a page to delete.'));
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('entity_id');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select item(s).'));
        } else {
            if (!empty($ids)) {
                try {
                    $options = Mage::getResourceModel('attributepages/entity_collection');
                    $options->addFieldToFilter('entity_id', array('in' => $ids));
                    foreach ($options as $option) {
                        $option->delete();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been deleted.', count($ids))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Validate post data
     *
     * @param array $data
     * @return bool     Return FALSE if someone item is invalid
     */
    protected function _validatePostData($data)
    {
        $errorNo = true;
        if (!empty($data['layout_update_xml'])) {
            /** @var $validatorCustomLayout Mage_Adminhtml_Model_LayoutUpdate_Validator */
            $validatorCustomLayout = Mage::getModel('adminhtml/layoutUpdate_validator');
            if (!$validatorCustomLayout->isValid($data['layout_update_xml'])) {
                $errorNo = false;
            }
            foreach ($validatorCustomLayout->getMessages() as $message) {
                $this->_getSession()->addError($message);
            }
        }
        return $errorNo;
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('templates_master/attributepages/attributepages_page');
    }

    protected function _afterSave($model)
    {
        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('cms')->__('The page has been saved.')
        );
        Mage::getSingleton('adminhtml/session')->setFormData(false);

        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array(
                'entity_id' => $model->getId(),
                '_current'  => true
            ));
        } else {
            $this->_redirect('*/*/');
        }
    }
}
