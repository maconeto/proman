<?php

class TM_Attributepages_Adminhtml_Attributepages_OptionController extends
    TM_Attributepages_Controller_Adminhtml_AbstractController
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('templates_master/attributepages_option/index')
            ->_addBreadcrumb(
                Mage::helper('attributepages')->__('Attribute Options'),
                Mage::helper('attributepages')->__('Attribute Options')
            );
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Attribute Options'));
        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__('Attribute Pages'));

        $id = $this->getRequest()->getParam('entity_id');
        $model = Mage::getModel('attributepages/entity');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('attributepages')->__('This option no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        } else {
            $optionId    = $this->getRequest()->getParam('option_id');
            $attributeId = $this->getRequest()->getParam('attribute_id');
            if (!$optionId || !$attributeId) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('attributepages')->__('Invalid link recieved.'));
                $this->_redirect('*/*/');
                return;
            }
            $model->setOptionId($optionId);
            $model->setAttributeId($attributeId);
        }

        $this->_title($model->getId() ? $model->getName() : $model->getOption()->getValue());

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('attributepages_page', $model);

        $this->_initAction()
            ->_addBreadcrumb(
                Mage::helper('attributepages')->__('Edit Option'),
                Mage::helper('attributepages')->__('Edit Option')
            );

        $this->renderLayout();
    }

    public function saveAction()
    {
        if (!$data = $this->getRequest()->getPost('attributepage')) {
            $this->_redirect('*/*/');
            return;
        }

        $model = Mage::getModel('attributepages/entity');
        if ($id = $this->getRequest()->getParam('entity_id')) {
            $model->load($id);
        }

        if (!$this->_validatePostData($data)) {
            $this->_redirect('*/*/edit', array('entity_id' => $model->getId(), '_current' => true));
            return;
        }

        try {
            $mediaPath = Mage::getBaseDir('media') . DS . TM_Attributepages_Model_Entity::IMAGE_PATH;
            foreach (array('image', 'thumbnail') as $key) {
                if (isset($_FILES[$key]) && $_FILES[$key]['error'] == 0) {
                    try {
                        $uploader = new Varien_File_Uploader($key);
                        $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(true);
                        if (@class_exists('Mage_Core_Model_File_Validator_Image')) {
                            $uploader->addValidateCallback(
                                Mage_Core_Model_File_Validator_Image::NAME,
                                Mage::getModel('core/file_validator_image'),
                                'validate'
                            );
                        }
                        $res = $uploader->save($mediaPath);
                        $data[$key] = $uploader->getUploadedFileName();
                    } catch (Exception $e) {
                        $this->_getSession()->addError($e->getMessage());
                    }
                }

                if (isset($data[$key]) && is_array($data[$key])) {
                    if (!empty($data[$key]['delete'])) {
                        @unlink($mediaPath . $data[$key]['value']);
                        $data[$key] = null;
                    } else {
                        $data[$key] = $data[$key]['value'];
                    }
                }
            }

            $model->addData($data);
            $model->save();

            $this->_afterSave($model);
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('_current'=>true));
        }
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('templates_master/attributepages/attributepages_option/save');
                break;
            case 'delete':
            case 'massDelete':
                return Mage::getSingleton('admin/session')->isAllowed('templates_master/attributepages/attributepages_option/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('templates_master/attributepages/attributepages_option');
                break;
        }
    }
}
