<?php

class TM_ArgentoFlat_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getStickyHeaderStatus()
    {
        if (mage::getStoreConfigFlag('argento_flat/layout/sticky_header')) {
            return 'sticky-header';
        }
        return '';
    }

}
