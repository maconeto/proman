<?php

class TM_ArgentoFlat_Upgrade_1_1_1 extends TM_Core_Model_Module_Upgrade
{
    public function up()
    {
        // change slider type to Swiper slider and theme to light
        $slider = Mage::getModel('easyslide/easyslide')->load('argento_flat');
        if (!$slider->getId()) {
            return;
        }
        $slider->setSliderType(2);
        $slider->setTheme('dark');
        $slider->setModifiedTime(new Zend_Db_Expr('NOW()'));
        $slider->save();
    }
}
