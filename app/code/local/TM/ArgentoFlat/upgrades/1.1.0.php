<?php

class TM_ArgentoFlat_Upgrade_1_1_0 extends TM_Core_Model_Module_Upgrade
{
    public function getOperations()
    {
        return array(
            'easytabs' => $this->_getEasytabsTabs()
        );
    }

    private function _getEasytabsTabs()
    {
        return array(
            array(
                'title' => 'Questions&nbsp;({{eval code="getCount()"}})',
                'alias' => 'askit_tabbed',
                'block' => 'easytabs/tab_template',
                'sort_order' => 90,
                'status' => 1,
                'product_tab' => 1,
                'custom_option' => 'askit/discussion',
                'template' => 'tm/askit/question/list.phtml',
                'unset' => 'product.info.additional::askit'
            )
        );
    }
}
