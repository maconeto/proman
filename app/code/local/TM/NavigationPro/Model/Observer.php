<?php

class TM_NavigationPro_Model_Observer
{

    protected $_nodeId = NULL;

    /*=================================*/
    /* Compatibility with magento 1620 */
    /*=================================*/
    public function addCatalogToTopmenuItems(Varien_Event_Observer $observer)
    {
        if (!@class_exists('Mage_Page_Block_Html_Topmenu')) {
            $this->_addCategoriesToMenu(Mage::helper('catalog/category')->getStoreCategories(), $observer->getMenu());
        }

        $block = $observer->getBlock();
        if ($block && $block->getNavigationproMenu()) {
            if ($observer->getBlock()->getActiveWithSiblings()) {
                $pathToActive = $this->_getPathToActive($observer->getMenu());
                array_pop($pathToActive);
                $this->_reorganizeMenu($observer->getMenu(), $pathToActive);
            } elseif ($observer->getBlock()->getActiveWithChildren()) {
                $pathToActive = $this->_getPathToActive($observer->getMenu());
                $this->_reorganizeMenu($observer->getMenu(), $pathToActive);
            }
        }

    }

    protected function _addCategoriesToMenu($categories, $parentCategoryNode)
    {
        foreach ($categories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }

            $nodeId = 'category-node-' . $category->getId();

            $tree = $parentCategoryNode->getTree();
            $categoryData = array(
                'name' => $category->getName(),
                'id' => $nodeId,
                'url' => Mage::helper('catalog/category')->getCategoryUrl($category),
                'is_active' => $this->_isActiveMenuCategory($category)
            );
            $categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $parentCategoryNode);
            $parentCategoryNode->addChild($categoryNode);

            if (Mage::helper('catalog/category_flat')->isEnabled()) {
                $subcategories = (array)$category->getChildrenNodes();
            } else {
                $subcategories = $category->getChildren();
            }

            $this->_addCategoriesToMenu($subcategories, $categoryNode);
        }
    }

    protected function _isActiveMenuCategory($category)
    {

        $currentCategory = $this->_getCurrentCategory();
        if (!$currentCategory) {
            return false;
        }

        $categoryPathIds = explode(',', $currentCategory->getPathInStore());
        return in_array($category->getId(), $categoryPathIds);
    }

    protected function _getCurrentCategory()
    {
        $catalogLayer = Mage::getSingleton('catalog/layer');
        if (!$catalogLayer) {
            return false;
        }

        $currentCategory = $catalogLayer->getCurrentCategory();
        if (!$currentCategory) {
            return false;
        }

        return $currentCategory;
    }

    protected function _getPathToActive($menu)
    {
        if ($menu->getChildren()->searchById($this->_getNodeIdOfActive())) {
            return array($this->_getNodeIdOfActive());
        }
        foreach ($menu->getChildren() as $child) {
            $arr = $this->_getPathToActive($child);
            if (count($arr)> 0) {
                array_unshift($arr, $child->getId());
                return $arr;
            }
        }
        return array();
    }

    protected function _getNodeIdOfActive()
    {
        if (is_null($this->_nodeId)) {
            $category = $this->_getCurrentCategory();
            if (!$category) {
                $this->_nodeId = '0';
            } else {
                $this->_nodeId = 'category-node-' . $category->getId();
            }
        }
        return $this->_nodeId;
    }

    protected function _reorganizeMenu($menu, $pathToActive)
    {
        $children = $menu->getChildren();
        if ($children->count() < 1) {
            return;
        }
        $found = false;
        foreach ($children as $child) {
            if (in_array($child->getId(), $pathToActive) && !$child->getIgnoreInFound()) {
                $found = true;
            }
        }
        if (!$found) {
            return;
        }
        foreach ($children as $child) {
            if (in_array($child->getId(), $pathToActive)) {
                foreach ($child->getChildren() as $subchild) {
                    $menu->addChild($subchild);
                }
            }
            if ( ($child->getId() != end($pathToActive)) || ($child->getChildren()->count()<1) ) {
                $menu->getChildren()->delete($child);
            } else {
                $child->setIgnoreInFound(true);
            }
        }
        $this->_reorganizeMenu($menu, $pathToActive);
    }

}
