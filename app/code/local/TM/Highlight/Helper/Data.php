<?php

class TM_Highlight_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Retreive highlight page urls as associative array
     *     PAGE_TYPE => url_key
     *
     * @return array
     */
    public function getPageUrls()
    {
        $urls = array();
        $config = Mage::getStoreConfig('highlight/pages');
        foreach ($config as $key => $value) {
            if (false === strpos($key, '_url')) {
                continue;
            }
            $urls[str_replace('_url', '', $key)] = $value;
        }
        return $urls;
    }

    public function getPages()
    {
        $pages = array();
        $config = Mage::getStoreConfig('highlight/pages');
        $curTitle = '';
        foreach ($config as $key => $value) {
            if (false !== strpos($key, '_title')) {
                $curTitle = $value;
                continue;
            }
            if (false === strpos($key, '_url')) {
                continue;
            }
            $pages[str_replace('_url', '', $key)] = array('url' => $value, 'name' => $curTitle);
        }
        return $pages;
    }

    /**
     * Retrieve page url for specific block type. See the PAGE_TYPE constant
     * in some of highlight blocks
     *
     * @param  string $type
     * @return string
     */
    public function getPageUrlKey($type)
    {
        return Mage::getStoreConfig("highlight/pages/{$type}_url");
    }

    /**
     * Remove all forbidden data parameters
     *
     * @param  array $data
     * @return array
     */
    public function filterBlockData(array $data)
    {
        $filtered = array_filter(
            $data,
            array($this, 'isAllowedBlockData'),
            ARRAY_FILTER_USE_KEY
        );
        if (isset($filtered['type'])
            && strpos($filtered['type'], 'highlight/') === false
        ) {
            unset($filtered['type']);
        }

        return $filtered;
    }

    /**
     * Check if data parameter is allowed
     *
     * @param  string  $key
     * @return boolean      [description]
     */
    public function isAllowedBlockData($key)
    {
        $whitelist = array(
            'apply_filters',
            'attribute_code',
            'category_filter',
            'column_count',
            'img_height',
            'img_width',
            'keep_frame',
            'order',
            'period',
            'price_filter',
            'product_type_filter',
            'products_count',
            'qty_filter',
            'stock_filter',
            'template',
            'type'
        );

        return in_array($key, $whitelist);
    }

    public function getCategoryImage($categoryId)
    {
        if (is_array($categoryId)) {
            $categoryId = array_shift($categoryId);
        }

        if ($categoryId != 'current') {
            $category = Mage::getModel('catalog/category')->load($categoryId);
        } else {
            $category = Mage::registry('current_category');
        }

        if (($image = $category->getThumbnail()) || ($image = $category->getImage())) {
            $prefix = Mage::getBaseUrl('media') . 'catalog/category/';
            $url = $prefix . $image;
        } else {
            $url = '';
        }

        return $url;
    }

    /**
     * Get data to initialize Slick Carousel
     *
     * @param  mixed $blockData
     * @param  string $dataSourceUrl [description]
     * @param  string $format        [description]
     * @return string|array
     */
    public function getSlickCarouselData($blockData, $dataSourceUrl = '', $format = 'json')
    {
        $slickParams = array(
            'infinite' => false,
            'swipeToSlide' => true,
            'arrows' => true,
            'dataSourceUrl' => $dataSourceUrl ? $dataSourceUrl : $this->_getUrl('highlight/index/loadData'),
            'blockData' => $this->filterBlockData($blockData)
        );

        if ($format == 'json') {
            return json_encode($slickParams, JSON_HEX_APOS);
        }

        return $slickParams;
    }
}
