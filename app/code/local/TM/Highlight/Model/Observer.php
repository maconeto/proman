<?php

class TM_Highlight_Model_Observer
{
    /**
     * Add Highlight links to HTML Sitemap
     *
     * @param Varien_Event_Observer $observer
     * @return TM_Highlight_Model_Observer
     */
    public function addLinks($observer)
    {
        $links = $observer->getLinks();
        $pages = Mage::helper('highlight')->getPages();
        $links->addData($pages);

        return $this;
    }

    /**
     * Add highlight pages to the TM_Amp configuration
     *
     * @param  Varien_Event_Observer $observer
     * @return void
     */
    public function preparePagesForTmamp($observer)
    {
        $pages = $observer->getPages();
        $optionArray = $pages->getData();
        $optionArray['highlight_index_index'] = Mage::helper('highlight')->__('Highlight Pages');
        $pages->setData($optionArray);
    }
}
