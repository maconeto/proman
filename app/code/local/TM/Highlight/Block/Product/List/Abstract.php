<?php

if (Mage::helper('core')->isModuleOutputEnabled('TM_AjaxPro')) {
    Mage::helper('tmcore')->requireOnce('TM/Highlight/Block/Product/List/TMAjaxPro.php');
} else {
    class TM_Highlight_Block_Product_List_Abstract extends Mage_Catalog_Block_Product_List {}
}
