<?php

class TM_ProLabels_Model_Image_Uploader extends Mage_Core_Model_Abstract
{
    public function upload($object, $dataKey)
    {
        $value = $object->getData($dataKey);
        $path = Mage::getBaseDir('media') . DS . 'prolabel' . DS;
        if (is_array($value) && !empty($value['delete'])) {
            @unlink($path . $value['value']);
            $object->setData($dataKey, '');
            return $this;
        }

        if (empty($_FILES[$dataKey]['name'])) {
            if (is_array($value)) {
                $object->setData($dataKey, $value['value']);
            }

            return $this;
        }

        try {
            $uploader = new Varien_File_Uploader($dataKey);
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png', 'bmp'));
            $uploader->setAllowRenameFiles(true);
            if (@class_exists('Mage_Core_Model_File_Validator_Image')) {
                $uploader->addValidateCallback(
                    Mage_Core_Model_File_Validator_Image::NAME,
                    Mage::getModel('core/file_validator_image'),
                    'validate'
                );
            }
            $uploader->save($path);
            $object-> setData($dataKey, $uploader -> getUploadedFileName());
        } catch (Exception $e) {
            $object -> unsData($dataKey);
            throw $e;
        }

        return $this;
    }
}
