<?php

class TM_ArgentoArgento_Upgrade_1_5_1 extends TM_Core_Model_Module_Upgrade
{
    public function up()
    {
        // change slider type to Swiper slider and its theme to dark
        $slider = Mage::getModel('easyslide/easyslide')->load('argento_default');
        if (!$slider->getId()) {
            return;
        }
        $slider->setSliderType(2);
        $slider->setTheme('dark');
        $slider->setHeight(350);
        $slider->setModifiedTime(new Zend_Db_Expr('NOW()'));
        $slider->save();
    }
}
