<?php
class TM_Easyslide_Adminhtml_Model_System_Config_Source_Slide
{
     public function toOptionArray()
    {
        $sliders = Mage::getModel('easyslide/easyslide')->getCollection();
        $data = array();

        foreach ($sliders as $slider) {
            $data[] = array(
                'value' => $slider->getIdentifier(),
                'label' => $slider->getTitle()
            );
        }
        return $data;
    }
}
