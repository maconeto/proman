<?php
/**
 * Add metadata to head for open graph protocol (used by facebook...)
 * List of images in slider
 */
class TM_Easyslide_Block_Head_Ogp extends Mage_Core_Block_Template
{

    protected $_slider = null;

    public function getSlider()
    {
        if (!isset($this->_slider)) {
            $this->_slider = Mage::getModel('easyslide/easyslide')
                ->load($this->getSliderId());
        }
        return $this->_slider;
    }

    public function getSlideImageUrl($url)
    {
        return $this->helper('easyslide')->getImageUrl($url);
    }

    protected function _toHtml()
    {
        if (!$this->getTemplate()) {
            $this->setTemplate('tm/easyslide/head/ogp.phtml');
        }
        return parent::_toHtml();
    }

}
