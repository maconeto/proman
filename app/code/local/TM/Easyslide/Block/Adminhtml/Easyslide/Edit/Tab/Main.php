<?php

class TM_Easyslide_Block_Adminhtml_Easyslide_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * getter
     * @return TM_Easyslide_Model_Easyslide
     */
    public function getSlider()
    {
        return Mage::registry('slider');
    }

    protected function _prepareForm()
    {
        $model = $this->getSlider();

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('easyslide_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'legend'=> $this->__('General'),
            )
        );

        if ($model->getEasyslideId()) {
            $fieldset->addField('easyslide_id', 'hidden', array(
                'name' => 'easyslide_id',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => $this->__('Title'),
            'title'     => $this->__('Title'),
            'required'  => true
        ));

        $fieldset->addField('identifier', 'text', array(
            'name'      => 'identifier',
            'label'     => $this->__('Identifier'),
            'title'     => $this->__('Identifier'),
            'required'  => true
        ));

        $fieldset->addField('slider_type', 'hidden', array(
                'name' => 'slider_type',
            ));

        $fieldset->addField('status', 'select', array(
            'label'     => $this->__('Status'),
            'title'     => $this->__('Status'),
            'name'      => 'status',
            'options'   => Mage::getSingleton('cms/page')->getAvailableStatuses()
        ));

        $prototype = $form->addFieldset(
            'prototype_fieldset',
            array(
                'legend'=> $this->__('Slider Options'),
            )
        );

        $prototype->addField('theme', 'select', array(
            'name'      => 'theme',
            'label'     => $this->__('Color theme'),
            'title'     => $this->__('Color theme'),
            'options'   => array(
                'default' => $this->__('Default (bluish colors)'),
                'dark' => $this->__('Dark'),
                'light' => $this->__('Light')
            )
        ));

        $prototype->addField('controls_type', 'select', array(
            'label'     => $this->__('Controls type'),
            'title'     => $this->__('Controls type'),
            'name'      => 'controls_type',
            'required'  => false,
            'options'   => array(
                'number' => $this->__('Pagination Bullets'),
                'arrow' => $this->__('Arrows'),
                'all' => $this->__('Both (Bullets and Arrows)'),
                'none' => $this->__('No Controls')
            )
        ));

        $prototype->addField('width', 'text', array(
            'name'      => 'width',
            'label'     => $this->__('Width, px'),
            'title'     => $this->__('Width, px'),
            'note'      => $this->__('Leave "Width" and "Height" fields empty to make slider full width'),
            'required'  => false
        ));

        $prototype->addField('height', 'text', array(
            'name'      => 'height',
            'label'     => $this->__('Height, px'),
            'title'     => $this->__('Height, px'),
            'note'      => $this->__('Leave "Width" and "Height" fields empty to make slider full width'),
            'required'  => false
        ));

        $prototype->addField('effect', 'select', array(
            'label'     => $this->__('Effect type of slides change'),
            'title'     => $this->__('Effect type of slides change'),
            'name'      => 'effect',
            'required'  => false,
            'options'   => array(
                'slide' => $this->__('Slide'),
                'fade' => $this->__('Fade'),
                'coverflow' => $this->__('3D Coverflow'),
                'cube' => $this->__('3D Cube'),
                'flip' => $this->__('3D Flip')
            )
        ));

        $prototype->addField('duration', 'text', array(
            'name'      => 'duration',
            'label'     => $this->__('Effect duration, sec'),
            'title'     => $this->__('Effect duration, sec'),
            'required'  => false
        ));

        $prototype->addField('frequency', 'text', array(
            'name'      => 'frequency',
            'label'     => $this->__('Slides change frequency, sec'),
            'title'     => $this->__('Slides change frequency, sec'),
            'required'  => false
        ));

        $prototype->addField('autoglide', 'select', array(
            'label'     => $this->__('Autoglide'),
            'title'     => $this->__('Autoglide'),
            'name'      => 'autoglide',
            'required'  => false,
            'options'   => Mage::getSingleton('cms/page')->getAvailableStatuses()
        ));

        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _initFormValues()
    {
        $slider = $this->getSlider();
        if (!$slider->getId()) {
            $slider->setStatus(1);
            $slider->setAutoglide(1);
        }
        $this->getForm()->addValues($slider->getData());
        // force slider type change for deprecated slider types
        $this->getForm()->addValues(
            array(
                'slider_type' => TM_Easyslide_Helper_Data::SLIDER_TYPE_SWIPER
            )
        );
        return parent::_initFormValues();
    }

}
