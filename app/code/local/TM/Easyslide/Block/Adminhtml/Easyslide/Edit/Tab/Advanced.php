<?php

class TM_Easyslide_Block_Adminhtml_Easyslide_Edit_Tab_Advanced
    extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * getter
     * @return TM_Easyslide_Model_Easyslide
     */
    public function getSlider()
    {
        return Mage::registry('slider');
    }

    protected function _prepareForm()
    {
        $model = $this->getSlider();

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('easyslide_');

        $fieldset = $form->addFieldset(
            'adnvaced_fieldset',
            array(
                'legend'=> $this->__('Advanced Slider Options'),
            )
        );

        $fieldset->addField('advaced_stopOnHover', 'select', array(
            'label'     => $this->__('Stop play when mouse hover over slider'),
            'title'     => $this->__('Stop play when mouse hover over slider'),
            'name'      => 'advanced_options[stopOnHover]',
            'values'    => Mage::getSingleton('adminhtml/system_config_source_yesno')
                            ->toOptionArray()
        ));

        $fieldset->addField('advaced_lazyLoading', 'select', array(
            'label'     => $this->__('Load images lazily'),
            'title'     => $this->__('Load images lazily'),
            'name'      => 'advanced_options[lazyLoading]',
            'note'      => $this->__('Lazy load can slightly improve page loading time. Slider width and height must be specified.'),
            'values'    => Mage::getSingleton('adminhtml/system_config_source_yesno')
                            ->toOptionArray()
        ));

        $fieldset->addField('advaced_spaceBetween', 'text', array(
            'label'     => $this->__('Space between slides, px'),
            'title'     => $this->__('Space between slides, px'),
            'name'      => 'advanced_options[spaceBetween]',
            'note'      => $this->__('Applies for "Slide" and "3D Coverflow" effects only.'),
        ));

        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _initFormValues()
    {
        $slider = $this->getSlider();
        if ($slider->getAdvancedOptions()) {
            foreach ($slider->getAdvancedOptions() as $key => $value) {
                $this->getForm()->addValues(array('advaced_'.$key => $value));
            }
        }
        return parent::_initFormValues();
    }

}
