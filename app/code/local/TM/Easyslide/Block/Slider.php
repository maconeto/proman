<?php

class TM_Easyslide_Block_Slider extends Mage_Core_Block_Template
{

    public function getSlider()
    {
        if (!$this->getData('slider')) {
            $slider = Mage::getModel('easyslide/easyslide')
                ->load($this->getSliderId());
            $this->setData('slider', $slider);
        }
        return $this->getData('slider');
    }

    public function getTemplate()
    {
        if (!$this->hasData('template')) {
            $this->setData('template', 'tm/easyslide/swiper.phtml');
        }
        return $this->getData('template');
    }

    public function _toHtml()
    {
        if (!$this->_beforeToHtml() || !$sliderId = $this->getSliderId()) {
            return '';
        }
        $slider = Mage::getModel('easyslide/easyslide')->load($sliderId);
        if (!$slider->getStatus()) {
            return '';
        }

        $this->setSlider($slider);
        return parent::_toHtml();
    }

    public function filterDescription($description)
    {
        $processor = Mage::helper('cms')->getPageTemplateProcessor();
        return $processor->filter($description);
    }

    public function getDescriptionClassName($position)
    {
        switch ($position) {
            case 1:
                return "swiper-slide-descr-top";
                break;
            case 2:
                return "swiper-slide-descr-right";
                break;
            case 3:
                return "swiper-slide-descr-bottom";
                break;
            case 4:
                return "swiper-slide-descr-left";
                break;
            case 5:
                return "swiper-slide-descr-center";
                break;
        }
    }

    public function getBackgroundClassName($background)
    {
        switch ($background) {
            case 1:
                return "swiper-slide-descr-light";
                break;
            case 2:
                return "swiper-slide-descr-dark";
                break;
            case 3:
                return "swiper-slide-descr-transparent";
                break;
        }
    }

    public function getSlideClassName($slide)
    {
        switch ($slide->getTargetMode()) {
            case 1:
                return 'target-blank';
            case 2:
                return 'target-popup';
            case 0:
            default:
                return 'target-self';
        }
    }

    public function getSliderConfig()
    {
        $slider = $this->getSlider();

        if ($slider->getAutoglide()) {
            $config['speed'] = (float)$slider->getDuration() * 1000;
            $config['autoplay'] = (float)$slider->getFrequency() * 1000;
        }

        $config['effect'] = $slider->getEffect();

        // set pagination selector
        if (in_array($slider->getControlsType(), array('all', 'number'))) {
            $config['pagination'] = '.swiper-pagination';
        }
        // set control buttons selectors
        if (in_array($slider->getControlsType(), array('all', 'arrow'))) {
            $config['nextButton'] = '.swiper-button-next';
            $config['prevButton'] = '.swiper-button-prev';
        }

        $config['paginationClickable'] = true;
        $config['centeredSlides'] = true;
        $config['autoplayDisableOnInteraction'] = false;
        $config['loop'] = true;
        $config['autoHeight'] = true;
        $config += $this->getSliderAdvancedOptions();

        // unset default values to reduce rendered HTML
        if (isset($config['stopOnHover']) && $config['stopOnHover'] == '0') {
            unset($config['stopOnHover']);
        }
        if (isset($config['lazyLoading'])) {
            if ($config['lazyLoading'] == '1') {
                // set custom option slider size ratio
                if ($slider->getWidth() && $slider->getHeight()) {
                    $config['sliderSizeWidth'] = $slider->getWidth();
                    $config['sliderSizeHeight'] = $slider->getHeight();
                }
                // do not preload images so first slide loads lazy also
                $config['preloadImages'] = false;
                // autoHeight option meeses up slider with lazy loading
                unset($config['autoHeight']);
            } else {
                unset($config['lazyLoading']);
            }
        }

        return json_encode($config);
    }

    public function getSlideImageUrl($url)
    {
        return $this->helper('easyslide')->getImageUrl($url);
    }

    public function getControlsStyle($controlType)
    {
        $classNames = array();
        switch ($this->getSlider()->getTheme()) {
            case 'dark':
                $classNames[] = sprintf('swiper-%s-black', $controlType);
                break;
            case 'light':
                $classNames[] = sprintf('swiper-%s-white', $controlType);
                break;
        }
        return implode(' ', $classNames);
    }

    public function getSliderAdvancedOptions()
    {
        if (!$this->getSlider()->hasData('advanced_options')) {
            $options = array();
            if ($this->getSlider()->getNivoOptions()) {
                $options = unserialize($this->getSlider()->getNivoOptions());
                if (isset($options['spaceBetween'])) {
                    $options['spaceBetween'] = intval($options['spaceBetween']);
                }
            }
            $this->getSlider()->setData('advanced_options', $options);
        }
        return $this->getSlider()->getData('advanced_options');
    }

    public function isLazyLoadingEnabled()
    {
        $options = $this->getSliderAdvancedOptions();
        if (isset($options['lazyLoading']) && $options['lazyLoading']) {
            return true;
        }
        return false;
    }

}
