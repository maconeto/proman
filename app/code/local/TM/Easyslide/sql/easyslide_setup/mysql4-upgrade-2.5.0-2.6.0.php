<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->changeColumn(
        $installer->getTable('easyslide'),
        'controls_type',
        'controls_type',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 8
        )
    );
$installer->getConnection()->changeColumn(
        $installer->getTable('easyslide'),
        'width',
        'width',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'unsigned'  => true,
            'nullable'  => true,
        )
    );
$installer->getConnection()->changeColumn(
        $installer->getTable('easyslide'),
        'height',
        'height',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'unsigned'  => true,
            'nullable'  => true,
        )
    );
$installer->getConnection()->addColumn(
        $installer->getTable('easyslide'),
        'theme',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length' => 8,
            'comment' => 'Slider Color Theme'
        )
    );

$installer->endSetup();
