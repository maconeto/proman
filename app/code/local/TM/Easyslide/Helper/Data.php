<?php
class TM_Easyslide_Helper_Data extends Mage_Core_Helper_Abstract
{

    const SLIDER_TYPE_PROTOTYPE = 0;    // depreceted
    const SLIDER_TYPE_NIVO = 1;         // deprecated
    const SLIDER_TYPE_SWIPER = 2;

    public function getImageUrl($path)
    {

        $image = new Varien_Object();

        if (strncmp($path,'http', 4) == 0 || empty($path)) {
            $image->setFilename('');
            $image->setUrl($path);
        } else {
            $image->setFilename(Mage::getBaseDir('media')
                    . DS . 'easyslide' . DS
                    . str_replace('/', DS, $path)
                );
            $image->setUrl('');
        }

        Mage::dispatchEvent(
            'tmeasyslide_get_image_url_before',
            array('image' => $image)
        );

        if (!$image->getUrl()) {
            $image->setUrl(
                rtrim(Mage::getBaseUrl('media'), '/') . '/easyslide/'. $path
            );
        }

        return $image->getUrl();

    }

    public function getSliderIdFromHomePage()
    {
        $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE);            ;
        $page = Mage::getModel('cms/page')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($pageId, 'identifier');

        if(preg_match_all(Varien_Filter_Template::CONSTRUCTION_PATTERN, $page->getContent(), $constructions, PREG_SET_ORDER)) {

            $tokenizer = new Varien_Filter_Template_Tokenizer_Parameter();
            foreach($constructions as $construction) {
                if ($construction[1] == 'widget') {
                    $tokenizer->setString($construction[2]);
                    $params = $tokenizer->tokenize();
                    if (isset($params['type'])
                        && ($params['type'] == 'easyslide/insert')
                        && isset($params['slider_id'])) {
                        return $params['slider_id'];
                    }
                }
            }

        }

    }
}
