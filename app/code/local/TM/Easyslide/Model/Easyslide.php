<?php

class TM_Easyslide_Model_Easyslide extends Mage_Core_Model_Abstract
{
    protected $_slides = array();

    public function _construct()
    {
        parent::_construct();
        $this->_init('easyslide/easyslide');
    }

    protected function _afterLoad()
    {
        if ($this->getId() && !$this->hasTheme()) {
            $this->setTheme('dark');
        }
        // fallback for Nivo Slider setting
        if ($this->getSliderType() == TM_Easyslide_Helper_Data::SLIDER_TYPE_NIVO) {
            // module old version compatibility
            $nivoData = unserialize($this->getNivoOptions());
            $nivoData['nivoeffect'] = explode(',', $nivoData['effect']);
            unset($nivoData['effect']);
            $this->addData($nivoData);
            $this->setAutoglide(!$this->getData('manualAdvance'));
            $this->setDuration($this->getData('animSpeed') / 1000);
            $this->setFrequency($this->getData('pauseTime') / 1000);
            // controls - arrows, bullets or all
            if ($this->getData('directionNav') && $this->getData('controlNav'))
                $this->setControlsType('all');
            elseif ($this->getData('directionNav'))
                $this->setControlsType('arrow');
            elseif ($this->getData('controlNav'))
                $this->setControlsType('number');
            else
                $this->setControlsType(null);
            // theme
            if ($this->getTheme() != 'light') {
                $this->setTheme('dark');
            }
        }
        parent::_afterLoad();
    }

    public function load($id, $field = null)
    {
        if (!is_numeric($id) && is_string($id)) {
            $field = 'identifier';
        }
        parent::load($id, $field);

        return $this;
    }

    public function getSlidesCollection($activeOnly = true)
    {
        $collection = Mage::getResourceModel('easyslide/easyslide_slides_collection');
        $collection->addFieldToFilter(
            'slider_id',
            array('eq' => $this->getId())
        );
        if ($activeOnly) {
            $collection->addFieldToFilter(
                'is_enabled',
                array('eq' => 1)
            );
        }
        $collection->addOrder('sort_order', 'ASC');
        return $collection;
    }

}
