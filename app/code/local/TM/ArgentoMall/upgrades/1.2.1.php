<?php

class TM_ArgentoMall_Upgrade_1_2_1 extends TM_Core_Model_Module_Upgrade
{
    public function up()
    {
        // change slider type to Swiper slider and its theme to light
        $slider = Mage::getModel('easyslide/easyslide')->load('argento_mall');
        if (!$slider->getId()) {
            return;
        }
        $slider->setSliderType(2);
        $slider->setTheme('light');
        $slider->setModifiedTime(new Zend_Db_Expr('NOW()'));
        $slider->save();
    }
}
