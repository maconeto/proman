<?php

class TM_ArgentoMall_Upgrade_1_0_0 extends TM_Argento_Model_Upgrade
{
    public function up()
    {
        // Create new, recommended and featured products if they are not exists
        $this->setupProducts(array('new', 'recommended', 'featured'));
    }

    public function getOperations()
    {
        return array(
            'configuration' => $this->_getConfiguration(),
            'cmsblock'      => $this->_getCmsBlocks(),
            'cmspage'       => $this->_getCmsPages(),
            'easyslide'     => $this->_getSlider(),
            'easybanner'    => $this->_getEasybanner(),
            'navigationpro' => $this->_getNavigationpro(),
            'productAttribute' => $this->_getProductAttribute()
        );
    }

    private function _getConfiguration()
    {
        $config = array_merge(
            // default argento package configuration
            $this->getDefaultConfiguration('mall'),
            // override configuration with some theme specific values
            array(
                'argento_mall/old_css/use' => 0
            )
        );
        $config['tm_ajaxsearch/general']['width'] = '251';

        return $config;
    }

    /**
     * header_links
     * scroll_up
     * footer_cms
     */
    private function _getCmsBlocks()
    {
        return array(
            'header_links' => array(
                'title'      => 'header_links',
                'identifier' => 'header_links',
                'status'     => 1,
                'content'    => <<<HTML
<ul class="header-links">
    <li class="first"><a href="{{store url="contacts"}}">support</a></li>
    <li><a href="{{store url="faq"}}">faq</a></li>
    <li class="last"><a href="{{store url="knowledgebase"}}">knowledge base</a></li>
</ul>
HTML
            ),
            'video_of_the_day' => array(
                'title'      => 'Video of the Day',
                'identifier' => 'video_of_the_day',
                'status'     => 1,
                'content'    => <<<HTML
<div class="block block-alt video-of-day">
    <div class="block-title"><span>Video of the day</span></div>
    <div class="block-content">
        <div class="video-container">
            <object>
                <param name="movie" value="http://www.youtube.com/v/6BQfCoqbubE">
                <param name="allowFullScreen" value="true">
                <param name="allowScriptAccess" value="always">
                <param wmode="transparent">
                <embed src="http://www.youtube.com/v/6BQfCoqbubE" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="250" height="201" wmode="transparent">
            </object>
        </div>
        <p><small>Amazing Canon Rebel XSi commercial that I saw on TV the other day.</small></p>
    </div>
</div>
HTML
            ),
            'scroll_up' => array(
                'title'      => 'scroll_up',
                'identifier' => 'scroll_up',
                'status'     => 1,
                'content'    => <<<HTML
<p id="scroll-up" class="hidden-desktop hidden-tablet hidden-phone">
    <a href="#">Back to top</a>
</p>
HTML
            ),
            'footer_cms' => array(
                'title' => 'footer_cms',
                'identifier' => 'footer_cms',
                'status' => 1,
                'content' => <<<HTML
<div class="footer-cms-container">
    <div class="footer-cms">
        <div class="row">
            <div class="col-sm-4 hidden-xs">
                <div class="box footer-links-cms">
                    <div class="head"><span>Informational</span></div>
                    <ul class="row">
                        <li class="col-md-6">
                            <ul>
                                <li><a href="{{store direct_url="about"}}">About Us</a></li>
                                <li><a href="{{store direct_url="our-company"}}">Our company</a></li>
                                <li><a href="{{store direct_url="press"}}">Press</a></li>
                                <li><a href="{{store direct_url="contacts"}}">Contact Us</a></li>
                                <li><a href="{{store direct_url="location"}}">Store location</a></li>
                            </ul>
                        </li>
                        <li class="last col-md-6">
                            <ul>
                                <li><a href="{{store direct_url="privacy"}}">Privacy policy</a></li>
                                <li><a href="{{store direct_url="delivery"}}">Delivery information</a></li>
                                <li><a href="{{store direct_url="returns"}}">Returns policy</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                {{block type="highlight/product_reports_viewed" name="footer.product.viewed" template="reports/footer.product_viewed.phtml"}}
            </div>
            <div class="col-sm-4 hidden-xs">
                {{block type="catalogsearch/term" name="footer.seo.searchterm" template="catalogsearch/footer.terms.phtml"}}
            </div>
        </div>
    </div>
</div>
HTML
            )
        );
    }

    /**
     * home
     */
    private function _getCmsPages()
    {
        return array(
            'home' => array(
                'title'             => 'home',
                'identifier'        => 'home',
                'root_template'     => 'one_column',
                'meta_keywords'     => '',
                'meta_description'  => '',
                'content_heading'   => '',
                'is_active'         => 1,
                'content'           => <<<HTML
<div class="row callout-home-top">
    <div class="col-lg-9 col-md-12">
        <div>{{widget type="easyslide/insert" slider_id="argento_mall"}}</div>
    </div>
    <div class="col-md-3 visible-lg-block">{{block type="newsletter/subscribe" name="homepage.newsletter" template="newsletter/subscribe.phtml"}} {{widget type="easybanner/widget_placeholder" placeholder_name="argento-mall-home-top"}}</div>
</div>
<div class="row col-home-set">
    <div class="col-md-3 sidebar visible-lg-block">{{block type="navigationpro/navigation" template="tm/navigationpro/sidebar.phtml" name_in_layout="navpro-homepage-left" menu_name="argento_mall_left" enabled="1"}}</div>
    <div class="col-lg-9 col-md-12">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-6">{{widget type="highlight/product_special" name="homepage.special" title="Deal of the week" class_name="block block-alt" products_count="1" column_count="1" template="tm/highlight/product/grid.phtml"}}</div>
            <div class="col-md-4 col-sm-4 col-xs-6">{{widget type="highlight/product_attribute_yesno" attribute_code="recommended" class_name="editor-choice block block-alt" name="homepage.editor_choice" title="Editor's choice" products_count="1" column_count="1" template="tm/highlight/product/grid.phtml"}}</div>
            <div class="col-md-4 col-sm-4 col-xs-12">{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="video_of_the_day"}}</div>
        </div>
    </div>
    <div class="col-lg-9 col-md-12">
        {{widget type="highlight/product_featured" name="homepage.featured" class_name="block block-featured-homepage" title="Featured products" products_count="6" column_count="3" template="tm/highlight/product/grid-wide.phtml"}}
        <div class="new-products-slider slick-wrapper gray" data-slick-wrapper='{"el": ".products-grid", "slidesToShow": 4, "slidesToScroll": 4, "dots": false, "responsive": [ {"breakpoint": 770, "settings": {"slidesToShow": 4, "slidesToScroll": 4}}, {"breakpoint": 640, "settings": {"slidesToShow": 3, "slidesToScroll": 3}}, {"breakpoint": 480, "settings": {"slidesToShow": 2, "slidesToScroll": 2}}, {"breakpoint": 321, "settings": {"slidesToShow": 1, "slidesToScroll": 1, "dots": false}}]}'>
            {{widget type="highlight/product_new" title="New Products" products_count="30" column_count="30" template="tm/highlight/product/grid.phtml" class_name="highlight-new"}}
        </div>
    </div>
</div>


HTML
,
                'layout_update_xml' => ''
            )
        );
    }

    private function _getSlider()
    {
        return array(
            array(
                'identifier'    => 'argento_mall',
                'title'         => 'Argento Mall',
                'width'         => 700,
                'height'        => 270,
                'duration'      => 0.5,
                'frequency'     => 4.0,
                'autoglide'     => 1,
                'controls_type' => 'number',
                'status'        => 1,
                'slides'        => array(
                    array(
                        'url'   => 'argento/mall/argento_mall_slider1.jpg',
                        'image' => 'Sony VAIO Laptop',
                        'description' => 'Sony VAIO Laptop',
                        'desc_pos' => 4,
                        'background' => 2
                    ),
                    array(
                        'url'   => 'argento/mall/argento_mall_slider2.jpg',
                        'image' => 'Dell Studio 17',
                        'description' => 'Dell Studio 17',
                        'desc_pos' => 4,
                        'background' => 2
                    ),
                    array(
                        'url'   => 'argento/mall/argento_mall_slider3.jpg',
                        'image' => 'HP HDX 16t',
                        'description' => 'HP HDX 16t',
                        'desc_pos' => 4,
                        'background' => 2
                    ),
                    array(
                        'url'   => 'argento/mall/argento_mall_slider4.jpg',
                        'image' => 'Nikon 5000',
                        'description' => 'Nikon 5000',
                        'desc_pos' => 4,
                        'background' => 2
                    ),
                    array(
                        'url'   => 'argento/mall/argento_mall_slider5.jpg',
                        'image' => 'Apple Macbook',
                        'description' => 'Apple Macbook',
                        'desc_pos' => 4,
                        'background' => 2
                    )
                )
            )
        );
    }

    private function _getEasybanner()
    {
        return array(
            array(
                'name'         => 'argento-mall-home-top',
                'parent_block' => 'non-existing-block',
                'limit'        => 1,
                'banners'      => array(
                    array(
                        'identifier' => 'argento-mall-home-top1',
                        'title'      => 'Free Shipping',
                        'url'        => 'free-shipping',
                        'image'      => 'argento/mall/argento_mall_callout_home_top1.gif',
                        'width'          => 225,
                        'height'         => 130,
                        'resize_image'   => 1,
                        'retina_support' => 0
                    )
                )
            )
        );
    }

    private function _getNavigationpro()
    {
        return array(
            'argento_mall_left' => array(
                'name' => 'argento_mall_left',
                'levels_per_dropdown' => 3,
                'columns'  => array(
                    array(
                        'width' => '100%'
                    )
                )
            ),
        );
    }

    private function _getProductAttribute()
    {
        return array(
            array(
                'attribute_code' => 'featured',
                'frontend_label' => array('Featured'),
                'default_value'  => 0
            ),
            array(
                'attribute_code' => 'recommended',
                'frontend_label' => array('Recommended'),
                'default_value'  => 0
            )
        );
    }
}
