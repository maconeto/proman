<?php

class TM_AskIt_Block_Adminhtml_AskIt_Grid_Abstract extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function __prepareColumns()
    {
        $this->addColumn('customer', array(
            'header'    => Mage::helper('askit')->__('Customer'),
            'index'     => 'customer_name'
        ));

        $this->addColumn('email', array(
            'header'    => Mage::helper('askit')->__('Email'),
            'index'     => 'email'
        ));

        $this->addColumn('hint', array(
            'header'    => Mage::helper('askit')->__('Votes'),
            'align'     =>'right',
            'width'     => '50px',
            'type'      => 'number',
            'index'     => 'hint',
        ));

        $this->addColumn('count_answers', array(
            'header'    => Mage::helper('askit')->__('Answers'),
            'width'     => '45px',
            'index'     => 'count_answers',
            'type'      => 'number',
            'sortable'      => false,
            'frame_callback' => array($this, 'decorateCountnumber'),
            'filter_condition_callback' => array($this, '_filterCountNumberCondition'),
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('askit')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::getSingleton('askit/status')->getQuestionOptionArray(),
            'frame_callback' => array($this, 'decorateStatus')
        ));

        $this->addColumn('private', array(
            'header'    => Mage::helper('askit')->__('Private'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'private',
            'type'      => 'options',
            'options'   => array(
                0     => Mage::helper('askit')->__('Public'),
                1     => Mage::helper('askit')->__('Private')
            ),
           'frame_callback' => array($this, 'decoratePrivate')
        ));
    }

    protected function _getCssClassByStatus($value)
    {
        switch ($value) {
            case TM_AskIt_Model_Status::STATUS_DISAPROVED:
                $class = 'critical';
                break;
            case TM_AskIt_Model_Status::STATUS_APROVED:
                $class = 'minor';
                break;
            case TM_AskIt_Model_Status::STATUS_CLOSE:
                $class = 'notice';
                break;
            case TM_AskIt_Model_Status::STATUS_PENDING:
            default:
                $class = 'major';
                break;
        }

        return $class;
    }

    protected function _getCssClassByStatus2($value)
    {
        switch ($value) {
            case TM_AskIt_Model_Status::STATUS_DISAPROVED:
                $class = 'critical';
                break;
            case TM_AskIt_Model_Status::STATUS_APROVED:
                $class = 'notice';
                break;
//            case TM_AskIt_Model_Status::STATUS_CLOSE:
//                $class = 'notice';
//            break;
            case TM_AskIt_Model_Status::STATUS_PENDING:
            default:
                $class = 'minor';
                break;
        }

        return $class;
    }

    protected function _getCell($value, $class = 'notice')
    {
        $class = Mage::helper('core')->quoteEscape($class);
        $value = $this->escapeHtml($value);
        return "<span class=\"grid-severity-{$class}\"><span>{$value}</span></span>";
    }

    /**
     *
     * @param  mixed $value
     * @param  array $row
     * @param  array $column
     * @param  boolean $isExport
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function decorateItemName($value, $row, $column, $isExport)
    {
        $item = Mage::helper('askit')->getItem($row);
        $url = $this->escapeUrl($item->getBackendItemUrl());
        $name = $this->escapeHtml($item->getName());
        return  "<a  href=\"{$url}\" >{$name}</a>";
    }

    /**
     *
     * @param  mixed $value
     * @param  array $row
     * @param  array $column
     * @param  boolean $isExport
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function decorateStatus($value, $row, $column, $isExport)
    {
        $class = $this->_getCssClassByStatus($row->status);
        return $this->_getCell($value, $class);
    }

    /**
     *
     * @param  mixed $value
     * @param  array $row
     * @param  array $column
     * @param  boolean $isExport
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function decoratePrivate($value, $row, $column, $isExport)
    {
        $class = $row->private ? 'major' : 'notice';
        return $this->_getCell($value, $class);
    }

    /**
     *
     * @param  mixed $value
     * @param  array $row
     * @param  array $column
     * @param  boolean $isExport
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function decorateCountnumber($value, $row, $column, $isExport)
    {
        $collection = Mage::getModel('askit/message')->getCollection();
        $collection->addParentIdFilter($row->getId());

        $value = $this->escapeHtml($value);
        $return = "<center>{$value}<br/>";

        if ($collection->count() > 0) {
            $statusses = Mage::getSingleton('askit/status')->getAnswerOptionArray();
            $item = $collection->getLastItem();

            $text = $this->escapeHtml(
                substr($item->getText(), 0, 12)
            ) . '...';
            $url = $this->getUrl('*/*/edit', array('id' => $item->getId()));
            $url = $this->escapeUrl($url);
            $return .=
                Mage::helper('askit')->__('Last answer')
                . ": <br/><a href=\"{$url}\" >{$text}</a><br/>"
            ;

//        foreach ($collection as $item) {
            $_author = $this->escapeHtml($item->getCustomerName());
            $_value = $item->status;
            $_class = $this->_getCssClassByStatus2($_value);
            $return .= Mage::helper('askit')->__('Author') . ' "' . $_author . '" '
                . $this->_getCell($statusses[$_value], $_class)
            ;
//        }
        }
        $return .= "</center>";
        return $return;
    }

    protected function _filterCountNumberCondition($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        if (!$value) {
            return;
        }
        $collection->addCountAnswerFilter(
            $value['from'],
            $value['to']
        );
    }
}
