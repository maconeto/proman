<?php
class TM_AskIt_Block_Adminhtml_AskIt_Product_Grid extends TM_AskIt_Block_Adminhtml_AskIt_Grid_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('product_askit_question');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);

        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('askit/message')->getCollection();

        $product = Mage::registry('product');
        if ($product) {
            $collection->addProductIdFilter($product->getId());
        }
        $collection->addQuestionCountAnswersData();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/askIt_index/edit', array('id' => $row->getId()));
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('askit')->__('ID'),
            'align'     => 'right',
            'width'     => '50px',
            'index'     => 'id',
            'type'      => 'number'
        ));

        $this->addColumn('text', array(
            'header'    => Mage::helper('askit')->__('Question'),
            'align'     => 'left',
            'index'     => 'text',
        ));

        $this->__prepareColumns();
        return parent::_prepareColumns();
    }
}
