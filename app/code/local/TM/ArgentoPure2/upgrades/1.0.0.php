<?php

class TM_ArgentoPure2_Upgrade_1_0_0 extends TM_Argento_Model_Upgrade
{
    public function up()
    {
        // Remove related product from easytabs
        $this->unsetEasytab('easytabs/tab_product_related', $this->getStoreIds());
        // Create new and recommended products if they are not exists
        $this->setupProducts(array('new', 'recommended'));
    }

    public function getOperations()
    {
        return array(
            'configuration' => $this->_getConfiguration(),
            'cmsblock'      => $this->_getCmsBlocks(),
            'cmspage'       => $this->_getCmsPages(),
            'easytabs'      => $this->_getEasytabsTabs(),
            'easybanner'    => $this->_getEasybanner(),
            'easyslide'     => $this->_getSlider(),
            'productAttribute' => $this->_getProductAttribute()
        );
    }

    private function _getConfiguration()
    {
        $config = array_merge(
            // default argento package configuration
            $this->getDefaultConfiguration('pure2'),
            // override configuration with some pure2 specific values
            array(
                'argento_pure2/old_css/use' => 0,

                'catalog/product_image/small_width' => 315,

                'tm_ajaxsearch/general' => array(
                    'enabled'              => 1,
                    'show_category_filter' => 0,
                    'searchfieldtext'      => 'SEARCH',
                    'width'                => 'auto',
                    'enablesuggest'        => 0,
                    'enablecatalog'        => 0,
                    'enablecms'            => 0,
                    'enabletags'           => 0,
                    'enabledescription'    => 0,
                    'descriptionchars'     => 50,
                    'imagewidth'           => 32,
                    'imageheight'          => 32,
                    'attributes'           => 'name,sku'
                ),

                'soldtogether' => array(
                    'general' => array(
                        'enabled' => 1,
                        'random'  => 1
                    ),
                    'order' => array(
                        'enabled'           => 1,
                        'productscount'     => 3,
                        'columns'           => 3,
                        'addtocartcheckbox' => 0,
                        'amazonestyle'      => 1
                    ),
                    'customer' => array(
                        'enabled'       => 1,
                        'productscount' => 3,
                        'columns'       => 3
                    )

                ),

                'lightboxpro' => array(
                    'general/enabled' => 1,
                    'size' => array(
                        'main'      => '512x800',
                        'main_keep_frame' => false,
                        'thumbnail' => '80x120',
                        'thumbnail_keep_frame' => true,
                        'maxWindow' => '800x600',
                        'popup'     => '0x0',
                        'popup_keep_frame' => false
                    )
                )
            )
        );
        $config['facebooklb']['productlike']['image_height'] = '800';
        return $config;
    }

    /**
     * header_links
     * scroll_up
     * footer_links
     */
    private function _getCmsBlocks()
    {
        return array(
            'scroll_up' => array(
                'title'      => 'scroll_up',
                'identifier' => 'scroll_up',
                'status'     => 1,
                'content'    => <<<HTML
<p id="scroll-up" class="hidden-desktop hidden-tablet hidden-phone">
    <a href="#"><i class="fa fa-4x fa-chevron-up">&#8203;</i></a>
</p>
HTML
            ),
            'footer_cms' => array(
                'title' => 'footer_cms',
                'identifier' => 'footer_cms',
                'status' => 1,
                'content' => <<<HTML
<div class="footer-cms-container">
    <div class="footer-cms">
        <div class="block block-social">
                <ul class="icons">
                    <li class="twitter"><a href="twitter.com">Twitter</a></li>
                    <li class="facebook"><a href="facebook.com">Facebook</a></li>
                    <li class="youtube"><a href="youtube.com">YouTube</a></li>
                    <li class="rss"><a href="rss.com">Rss</a></li>
                </ul>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-5">
                <div class="block block-information">
                    <div class="block-title"><span>Company Information</span></div>
                    <div class="block-content">
                        <ul>
                            <li><a href="{{store url='blog'}}">Blog</a></li>
                            <li><a href="{{store url='sales/guest/form'}}">Order Status</a></li>
                            <li><a href="{{store url='storelocator'}}">Store Locator</a></li>
                            <li><a href="{{store url='wishlist'}}">Wishlist</a></li>
                            <li><a href="{{store url='privacy'}}">Privacy Policy</a></li>
                            <li><a href="{{store url='customer/account'}}">Personal Account</a></li>
                            <li><a href="{{store url='terms'}}">Terms of Use</a></li>
                            <li><a href="{{store url='returns'}}">Returns &amp; Exchanges</a></li>
                            <li><a href="{{store url='company'}}">Our Company</a></li>
                            <li><a href="{{store url='careers'}}">Careers</a></li>
                            <li><a href="{{store url='about'}}">About us</a></li>
                            <li><a href="{{store url='shipping'}}">Shipping</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="block block-about">
                    <div class="block-title"><span>Call Us</span></div>
                    <div class="block-content">
                        <a class="footer-phone" href="tel:1.800.555.1903">1.800.555.1903</a>
                        <p>
        We're available 24/7. Please note the more accurate the information you can provide us with the quicker we can respond to your query.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                {{block type="newsletter/subscribe" name="footer.newsletter" template="newsletter/subscribe.phtml"}}
            </div>
        </div>
    </div>
</div>
HTML
            ),
            'footer_payments' => $this->getFooterBlockPayments(),
            'product_sidebar' => $this->getSidebarBlockProduct(),
            'services_sidebar' => $this->getSidebarBlockServices()
        );
    }

    /**
     * home
     */
    private function _getCmsPages()
    {
        return array(
            'home' => array(
                'title'             => 'home',
                'identifier'        => 'home',
                'root_template'     => 'one_column',
                'meta_keywords'     => '',
                'meta_description'  => '',
                'content_heading'   => '',
                'is_active'         => 1,
                'content'           => <<<HTML
<div class="row jumbotron jumbotron-slider jumbotron-image">
    <div class="container wow fadeIn" style="visibility: hidden;">
        {{widget type="easyslide/insert" slider_id="argento_pure2"}}
    </div>
</div>

<div class="row jumbotron">
    <div class="container">
        <div class="block block-dotted">
            <div class="block-title"><span>The Essentials</span></div>
            <div class="block-content">
                {{widget type="easycatalogimg/widget_list" background_color="255,255,255" category_count="4" subcategory_count="6" column_count="4" show_image="1" image_width="200" image_height="200" template="tm/easycatalogimg/list.phtml"}}
            </div>
        </div>
    </div>
</div>

<div class="row jumbotron">
    <div class="container">
        {{widget type="easytabs/widget" filter_tabs="sale,bestsellers,popular,editors-choice,new-arrivals" template="tm/easytabs/tabs.phtml"}}
    </div>
</div>

<div class="row jumbotron">
    <div class="container">
        <div class="block block-brands argento-slider wow fadeIn" data-wow-delay="0.2s">
            <div class="block-title"><span>Our Brands</span></div>
               <div class="block-content">
                <div id="slider-brands-container" class="slider-wrapper">
                    <div class="slick" id="slider-brands" data-slick='{"slidesToShow": 6, "slidesToScroll": 6, "infinite": true, "swipeToSlide": true, "autoplay": true, "autoplaySpeed": 2000, "arrows": true, "responsive": [ {"breakpoint": 770, "settings": {"slidesToShow": 4, "slidesToScroll": 4}}, {"breakpoint": 640, "settings": {"slidesToShow": 3, "slidesToScroll": 3}}, {"breakpoint": 480, "settings": {"slidesToShow": 2, "slidesToScroll": 2}}, {"breakpoint": 321, "settings": {"slidesToShow": 1, "slidesToScroll": 1, "dots": false}}]}'>
                        <a href="#"><img src="{{skin url="images/catalog/brands/gucci.jpg"}}" alt="" width="150" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/lv.jpg"}}" alt="" width="100" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/ck.jpg"}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/chanel.jpg"}}" alt="" width="170" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/guess.jpg"}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/versace.jpg"}}" alt="" width="145" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/gucci.jpg"}}" alt="" width="150" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/lv.jpg"}}" alt="" width="100" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/ck.jpg"}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/chanel.jpg"}}" alt="" width="170" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/guess.jpg"}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url="images/catalog/brands/versace.jpg"}}" alt="" width="145" height="80"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row jumbotron">
    <div class="container block-homepage-banner">
        {{widget type="easybanner/widget_placeholder" placeholder_name="argento-pure2-home"}}
    </div>
</div>
HTML
,
                'layout_update_xml' => ''
            )
        );
    }

    private function _getEasybanner()
    {
        return array(
            array(
                'name'         => 'argento-pure2-home',
                'parent_block' => 'non-existing-block',
                'limit'        => 1,
                'banners'      => array(
                    array(
                        'identifier' => 'argento-pure2-home1',
                        'title'      => 'Special Offer',
                        'url'        => 'free-shipping',
                        'class_name' => 'home-banner',
                        'image'      => 'argento/pure2/argento_pure2_callout_home1.png',
                        'width'          => 1160,
                        'height'         => 130,
                        'resize_image'   => 0,
                        'retina_support' => 0
                    )
                )
            )
        );
    }

    private function _getSlider()
    {
        return array(
            array(
                'identifier'    => 'argento_pure2',
                'title'         => 'Argento Pure 2.0',
                'width'         => 1263,
                'height'        => 375,
                'duration'      => 0.5,
                'frequency'     => 4.0,
                'autoglide'     => 1,
                'controls_type' => 'number',
                'status'        => 1,
                'slides'        => array(
                    array(
                        'url'   => 'argento/pure2/argento_pure2_slider1.jpg',
                        'image' => '25% off',
                        'description' => '',
                        'desc_pos' => TM_Easyslide_Model_Easyslide_Slides::DESCRIPTION_CENTER,
                        'background' => TM_Easyslide_Model_Easyslide_Slides::BACKGROUND_TRANSPARENT
                    ),
                    array(
                        'url'   => 'argento/pure2/argento_pure2_slider2.jpg',
                        'image' => '25% off green',
                        'description' => '',
                        'desc_pos' => TM_Easyslide_Model_Easyslide_Slides::DESCRIPTION_CENTER,
                        'background' => TM_Easyslide_Model_Easyslide_Slides::BACKGROUND_TRANSPARENT
                    ),
                    array(
                        'url'   => 'argento/pure2/argento_pure2_slider3.jpg',
                        'image' => '25% off orange',
                        'description' => '',
                        'desc_pos' => TM_Easyslide_Model_Easyslide_Slides::DESCRIPTION_CENTER,
                        'background' => TM_Easyslide_Model_Easyslide_Slides::BACKGROUND_TRANSPARENT
                    )
                )
            )
        );
    }

    private function _getProductAttribute()
    {
        return array(
            array(
                'attribute_code' => 'recommended',
                'frontend_label' => array('Recommended'),
                'default_value'  => 0
            )
        );
    }

    private function _getEasytabsTabs()
    {
        return array(
            array(
                'title' => 'Sale',
                'alias' => 'sale',
                'block' => 'easytabs/tab_html',
                'sort_order' => 10,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_special" title="" img_width="315" img_keep_frame="0" products_count="6" column_count="3" template="tm/highlight/product/grid.phtml" class_name="highlight-special" page_title="Shop Sale"}}'
            ),
            array(
                'title' => 'Bestsellers',
                'alias' => 'bestsellers',
                'block' => 'easytabs/tab_html',
                'sort_order' => 20,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_bestseller" title="" img_width="315" img_keep_frame="0" products_count="6" column_count="3" template="tm/highlight/product/grid.phtml" class_name="highlight-bestsellers" page_title="Shop Bestsellers"}}'
            ),
            array(
                'title' => 'Popular',
                'alias' => 'popular',
                'block' => 'easytabs/tab_html',
                'sort_order' => 30,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_popular" title="" products_count="6" img_width="315" img_keep_frame="0"  column_count="3" template="tm/highlight/product/grid.phtml" class_name="highlight-popular" page_title="Shop Popular"}}'
            ),
            array(
                'title' => 'Editor\'s Choice',
                'alias' => 'editors-choice',
                'block' => 'easytabs/tab_html',
                'sort_order' => 40,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_attribute_yesno" attribute_code="recommended" title="" img_width="315" img_keep_frame="0" products_count="6" column_count="3" template="tm/highlight/product/grid.phtml" class_name="highlight-attribute-recommended"}}'
            ),
            array(
                'title' => 'New arrivals',
                'alias' => 'new-arrivals',
                'block' => 'easytabs/tab_html',
                'sort_order' => 50,
                'status' => 1,
                'product_tab' => 0,
                'content' => '{{widget type="highlight/product_new" title="" img_width="315" img_keep_frame="0" products_count="6" column_count="3" template="tm/highlight/product/grid.phtml" class_name="highlight-new" page_title="Shop New"}}'
            )
        );
    }

}
