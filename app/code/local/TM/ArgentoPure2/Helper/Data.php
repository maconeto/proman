<?php

class TM_ArgentoPure2_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getStickyHeaderClass()
    {
        if (Mage::getStoreConfigFlag('argento_pure2/layout/sticky_header')) {
            return 'sticky-header';
        }
        return '';
    }

    public function getStickySidebarClass()
    {
        if (Mage::getStoreConfigFlag('argento_pure2/layout/sticky_sidebar')) {
            return 'sticky-sidebar';
        }
        return '';
    }

}
