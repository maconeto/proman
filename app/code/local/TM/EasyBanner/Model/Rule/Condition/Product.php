<?php

class TM_EasyBanner_Model_Rule_Condition_Product extends Mage_SalesRule_Model_Rule_Condition_Product_Combine
{
    public function __construct()
    {
        parent::__construct();
        $this->setType('easybanner/rule_condition_product');
    }

    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml()
            . Mage::helper('salesrule')->__(
                "If customer is viewing the product with %s of these conditions %s:",
                $this->getAggregatorElement()->getHtml(),
                $this->getValueElement()->getHtml()
            );

        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        return $html;
    }

    /**
     * Generate a conditions data
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $conditions = array();
        $conditions = array_merge_recursive(
            $conditions,
            array(
                array(
                    'label' => Mage::helper('catalog')->__('Product Attribute'),
                    'value' => $this->_getAttributeConditions(self::PRODUCT_ATTRIBUTES_TYPE_PRODUCT),
                ),
            )
        );
        return $conditions;
    }

    /**
     * Validate a condition with the checking of the children values
     * @param Varien_Object $object
     *
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $object->getProduct();
        if (!($product instanceof Mage_Catalog_Model_Product)) {
            $product = Mage::getModel('catalog/product')->load($object->getProductId());
        }

        $valid = parent::validate($object);
        if (!$valid && $product->getTypeInstance()->isComposite()) {
            // These methods are safe to use, as they use memory caching and
            // Magento calls them before us.
            $typeInstance = $product->getTypeInstance();
            switch ($product->getTypeId()) {
                case Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE:
                    $children = $typeInstance->getUsedProducts();
                    break;
                case Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE:
                    $children = $typeInstance->getAssociatedProducts();
                    break;
                case 'bundle':
                    $children = $typeInstance->getSelectionsCollection(
                        $typeInstance->getOptionsIds()
                    );
                    break;
                default:
                    // third-party product type?
                    return $valid;
            }

            // Use this method, as it use memory caching
            $attributes = $this->_getAttributeConditions(self::PRODUCT_ATTRIBUTES_TYPE_PRODUCT);
            foreach ($children as $simpleProduct) {
                foreach ($attributes as $key => $values) {
                    list($model, $attributeCode) = explode('|', $key);
                    if (!$simpleProduct->hasData($attributeCode) &&
                        $product->hasData($attributeCode)) {

                        $simpleProduct->setData(
                            $attributeCode,
                            $product->getData($attributeCode)
                        );
                    }
                }

                $valid = parent::validate(new Varien_Object(array(
                    'product' => $simpleProduct
                )));

                if ($valid) {
                    break;
                }
            }
        }

        return $valid;
    }
}
