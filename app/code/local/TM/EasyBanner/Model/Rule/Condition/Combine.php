<?php

class TM_EasyBanner_Model_Rule_Condition_Combine extends Mage_Rule_Model_Condition_Combine
{
    public function __construct()
    {
        parent::__construct();
        $this->setType('easybanner/rule_condition_combine');
    }

    public function getNewChildSelectOptions()
    {
        $conditions = parent::getNewChildSelectOptions();
        $conditions = array_merge_recursive(
            $conditions,
            array(
                array(
                    'label' => Mage::helper('catalog')->__('Product Attributes Combination'),
                    'value' => 'easybanner/rule_condition_product',
                ),
                array(
                    'label' => Mage::helper('catalog')->__('Conditions Combination'),
                    'value' => 'easybanner/rule_condition_combine',
                ),
            ),
            Mage::getModel('easybanner/rule_condition_banner')->getCombinedConditions()
        );
        return $conditions;
    }

    public function collectValidatedAttributes($productCollection)
    {
        foreach ($this->getConditions() as $condition) {
            $condition->collectValidatedAttributes($productCollection);
        }
        return $this;
    }
}
