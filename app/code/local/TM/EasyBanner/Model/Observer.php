<?php

class TM_EasyBanner_Model_Observer
{
    public function incrementDisplayCount($observer)
    {
        Mage::getResourceModel('easybanner/banner_statistic')
            ->incrementDisplayCount($observer->getBannerId());
    }

    public function incrementClicksCount($observer)
    {
        Mage::getResourceModel('easybanner/banner_statistic')
            ->incrementClicksCount($observer->getBannerId());
    }
}
