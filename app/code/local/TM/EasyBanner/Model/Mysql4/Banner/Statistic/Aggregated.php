<?php

class TM_EasyBanner_Model_Mysql4_Banner_Statistic_Aggregated extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('easybanner/banner_statistic_aggregated', 'id');
    }

    public function incrementDisplayCount($bannerId)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(array('banner_statistic_aggregated' => $this->getMainTable()))
            ->where('banner_id = ?', $bannerId);

        $row = $this->_getReadAdapter()->fetchRow($select);

        if ($row) {
            $this->_getWriteAdapter()->update(
                $this->getMainTable(),
                array(
                    'display_count' => ++$row['display_count']
                ),
                array(
                    'banner_id = ?' => $bannerId
                )
            );
        } else {
            $this->_getWriteAdapter()->insert($this->getMainTable(), array(
                'banner_id' => $bannerId,
                'display_count' => 1,
                'clicks_count' => 0
            ));
        }
    }

    public function incrementClicksCount($bannerId)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(array('banner_statistic_aggregated' => $this->getMainTable()))
            ->where('banner_id = ?', $bannerId);

        $row = $this->_getReadAdapter()->fetchRow($select);

        if ($row) {
            $this->_getWriteAdapter()->update(
                $this->getMainTable(),
                array(
                    'clicks_count' => ++$row['clicks_count']
                ),
                array(
                    'banner_id = ?' => $bannerId
                )
            );
        } else {
            $this->_getWriteAdapter()->insert($this->getMainTable(), array(
                'banner_id' => $bannerId,
                'display_count' => 1,
                'clicks_count' => 1
            ));
        }
    }
}
