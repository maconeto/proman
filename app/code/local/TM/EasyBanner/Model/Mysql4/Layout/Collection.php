<?php

class TM_EasyBanner_Model_Mysql4_Layout_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('easybanner/layout');
    }
}
