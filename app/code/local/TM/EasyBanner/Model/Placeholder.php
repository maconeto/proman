<?php

class TM_EasyBanner_Model_Placeholder extends Mage_Core_Model_Abstract
{
    const LAYOUT_NAME_PREFIX = 'easybanner.placeholder.';

    const MODE_ROTATOR    = 'rotator';
    const MODE_LIGHTBOX   = 'lightbox';
    const MODE_AWESOMEBAR = 'awesomebar';

    protected function _construct()
    {
        parent::_construct();
        $this->_init('easybanner/placeholder');
    }

    public function getBannerIds($isActive = false)
    {
        $key = $isActive ? 'banner_ids_active' : 'banner_ids';
        $ids = $this->_getData($key);
        if (is_null($ids)) {
            $this->_getResource()->loadBannerIds($this, $isActive);
            $ids = $this->getData($key);
        }
        return $ids;
    }

    public function getIsRandomSortMode()
    {
        return 'random' === $this->getSortMode();
    }

    public function isPopupMode($mode = null)
    {
        if (null === $mode) {
            $mode = $this->getMode();
        }
        return in_array($mode, array(
            self::MODE_LIGHTBOX,
            self::MODE_AWESOMEBAR
        ));
    }

    public function getBannerTypes()
    {
        return array(
            self::MODE_ROTATOR => Mage::helper('easybanner')->__('Banner'),
            self::MODE_LIGHTBOX => Mage::helper('easybanner')->__('Lightbox'),
            self::MODE_AWESOMEBAR => Mage::helper('easybanner')->__('Awesomebar'),
        );
    }

    public function savePopupPlaceholder($mode)
    {
        // cleanup all layout updates of previous popup placeholder
        Mage::getResourceModel('easybanner/layout')
            ->removeUpdatesByPlaceholderName(self::LAYOUT_NAME_PREFIX . $mode);

        $this->setMode($mode);
        $this->addData(array(
            'name'          => $mode,
            'parent_block'  => 'before_body_end',
            'position'      => 'after="-"',
            'status'        => 1,
            'limit'         => 100,
            'banner_offset' => 1,
            'sort_mode'     => 'sort_order'
        ));
        $this->save();
    }
}
