<?php

class TM_EasyBanner_Block_Adminhtml_Placeholder_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId   = 'id';
        $this->_blockGroup = 'easybanner';
        $this->_controller = 'adminhtml_placeholder';

        $this->_updateButton('save', 'label', Mage::helper('easybanner')->__('Save Placeholder'));
        $this->_updateButton('delete', 'label', Mage::helper('easybanner')->__('Delete Placeholder'));

        $message = Mage::helper('easybanner')
            ->__('Valid xml attribute is required. For example before="-" or after="block.name"');
        $this->_formScripts[] = <<<SCRIPT
Validation.add('validate-xml-attribute-and-value', '{$message}', function(v) {
    return Validation.get('IsEmpty').test(v) ||  /^[a-zA-Z][a-zA-Z0-9_\-]+="[a-zA-Z0-9_\-.]+"$/.test(v)
});
SCRIPT;
    }

    public function getHeaderText()
    {
        if (Mage::registry('easybanner_placeholder') && Mage::registry('easybanner_placeholder')->getId()) {
            return Mage::helper('easybanner')->__("Edit Placeholder '%s'", $this->htmlEscape(Mage::registry('easybanner_placeholder')->getName()));
        } else {
            return Mage::helper('easybanner')->__('Add Placeholder');
        }
    }

}