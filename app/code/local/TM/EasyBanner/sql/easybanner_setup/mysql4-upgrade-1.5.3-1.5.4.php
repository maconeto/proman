<?php

/**
 * @var Mage_Core_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('easybanner/banner_statistic_aggregated'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true
    ), 'Primary Id')
    ->addColumn('banner_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'length'   => 6,
        'unsigned' => true,
        'nullable' => false
    ), 'Banner Id')
    ->addColumn('display_count', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'default'  => 0,
        'nullable' => false
    ), 'Banner Display Count')
    ->addColumn('clicks_count', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'default'  => 0,
        'nullable' => false
    ), 'Banner Clicks Count')
    ->addIndex(
        $installer->getIdxName('easybanner/banner_statistic_aggregated', array('display_count')),
        array('display_count'))
    ->addIndex($installer->getIdxName('easybanner/banner_statistic_aggregated', array('clicks_count')),
        array('clicks_count'))
    ->addForeignKey(
        $installer->getFkName('easybanner/banner_statistic_aggregated', 'banner_id', 'easybanner/banner', 'banner_id'),
        'banner_id',
        $installer->getTable('easybanner/banner'),
        'banner_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Easybanner Aggregated Statistics');
$installer->getConnection()->createTable($table);

$banners = Mage::getModel('easybanner/banner')->getCollection()->addStatistics();
foreach ($banners as $banner) {
    Mage::getModel('easybanner/banner_statistic_aggregated')
        ->setBannerId($banner->getId())
        ->setDisplayCount($banner->getDisplayCount())
        ->setClicksCount($banner->getClicksCount())
        ->save();
}

$installer->endSetup();
