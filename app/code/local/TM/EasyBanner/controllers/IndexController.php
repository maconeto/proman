<?php

class TM_EasyBanner_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Banner click action
     */
    public function clickAction()
    {
        $bannerId = (int) $this->getRequest()->getParam('banner_id', false);
        if (!$bannerId) {
            return $this->_forward('noRoute');
        }

        $model = Mage::getModel('easybanner/banner')->load($bannerId);
        if (!$model->getId() || !$model->check(Mage::app()->getStore()->getId())) {
            return $this->_forward('noRoute');
        }

        Mage::dispatchEvent('tm_easybanner_banner_click', array(
            'banner_id' => $model->getId()
        ));

        $redirectUrl = $model->getUrl();

        if (strpos($redirectUrl, 'www.') === 0) {
            $redirectUrl = 'http://' . $model->getUrl();
        } elseif (strpos($redirectUrl, 'http://') !== 0
            && strpos($redirectUrl, 'https://') !== 0) {

            // trim leading slashes to get correct redirect URL
            $redirectUrl = ltrim($redirectUrl, '/');

            if (strpos($redirectUrl, '.html') !== false) {
                // hotfix for seo links.
                $redirectUrl = Mage::getModel('core/url')->getDirectUrl($redirectUrl);
            } else {
                $redirectUrl = Mage::getUrl($redirectUrl);
            }
        }

        $this->getResponse()->setRedirect($redirectUrl);
    }
}
