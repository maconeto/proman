<?php

class TM_ArgentoLuxury_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getStickyHeaderClass()
    {
        if (Mage::getStoreConfigFlag('argento_luxury/layout/sticky_header')) {
            return 'sticky-header';
        }
        return '';
    }
}
