<?php

class TM_ArgentoLuxury_Upgrade_1_0_0 extends TM_Argento_Model_Upgrade
{
    public function up()
    {
        // Create new products if they are not exists
        $this->setupProducts(array('new'));
    }

    public function getOperations()
    {
        return array(
            'configuration' => $this->_getConfiguration(),
            'cmsblock'      => $this->_getCmsBlocks(),
            'cmspage'       => $this->_getCmsPages(),
            'easytabs'      => $this->_getEasytabsTabs(),
            'easybanner'    => $this->_getEasybanner(),
            'easyslide'     => $this->_getSlider(),
            'productAttribute' => $this->_getProductAttribute()
        );
    }

    private function _getConfiguration()
    {
        $config = array_merge(
            // default argento package configuration
            $this->getDefaultConfiguration('luxury'),
            // override configuration with some luxury specific values
            array(
                'catalog/product_image/small_width' => 200,

                'tm_qtyswitcher' => array(
                    'main/enabled' => 1,
                    'cart/enabled' => 1
                ),

                'tm_ajaxsearch/general' => array(
                    'enabled'              => 1,
                    'show_category_filter' => 0,
                    'width'                => 'auto',
                    'height'               => 620,
                    'productstoshow'       => 5,
                    'enablesuggest'        => 0,
                    'enablecatalog'        => 0,
                    'enablecms'            => 0,
                    'enabletags'           => 0,
                    'enabledescription'    => 1,
                    'descriptionchars'     => 200,
                    'imagewidth'           => 100,
                    'imageheight'          => 100,
                    'attributes'           => 'name,sku'
                ),

                'soldtogether' => array(
                    'general' => array(
                        'enabled' => 1,
                        'random'  => 1
                    ),
                    'order' => array(
                        'enabled'           => 1,
                        'productscount'     => 4,
                        'columns'           => 4,
                        'addtocartcheckbox' => 0,
                        'amazonestyle'      => 1
                    ),
                    'customer' => array(
                        'enabled'       => 1,
                        'productscount' => 4,
                        'columns'       => 4
                    )
                ),

                'lightboxpro' => array(
                    'general' => array(
                        'enabled' => 1,
                        'outlineType' => 'drop-shadow'
                    ),
                    'size' => array(
                        'main'      => '462x800',
                        'main_keep_frame' => false,
                        'thumbnail' => '60x90',
                        'thumbnail_keep_frame' => true,
                        'maxWindow' => '600x800',
                        'popup'     => '0x0',
                        'popup_keep_frame' => false
                    ),
                    'style' => array(
                        'dimming_enable' => 1,
                        'dimmingOpacity' => '0.7',
                    )
                ),

                'configswatches' => array(
                    'layered_nav_dimensions' => array(
                        'height' => '20',
                        'width' => '20'
                    ),
                    'product_listing_dimensions' => array(
                        'height' => '20',
                        'width' => '20'
                    ),
                    'product_detail_dimensions' => array(
                        'height' => '30',
                        'width' => '30'
                    )
                )
            )

        );
        $config['facebooklb']['productlike']['layout'] = 'custom';
        $config['facebooklb']['productlike']['image_width'] = '462';
        $config['facebooklb']['productlike']['image_height'] = '800';

        return $config;
    }

    /**
     * footer_cms
     */
    private function _getCmsBlocks()
    {
        return array(
            'scroll_up' => array(
                'title'      => 'scroll_up',
                'identifier' => 'scroll_up',
                'status'     => 1,
                'content'    => <<<HTML
<p id="scroll-up" class="hidden-desktop hidden-tablet hidden-phone">
    <a href="#"><i class="fa fa-4x fa-chevron-up">&#8203;</i></a>
</p>
HTML
            ),
            'footer_cms' => array(
                'title' => 'footer_cms',
                'identifier' => 'footer_cms',
                'status' => 1,
                'content' => <<<HTML
<div class="footer-cms-container">
    <div class="footer-cms">
        <div class="row">
            <div class="col-md-9">
                <ul class="footer-links row">
                    <li class="col-md-4 col-xs-12">
                        <div class="h4">About us</div>
                        <ul class="links">
                            <li><a href='{{store direct_url="about"}}'>About Us</a></li>
                            <li><a href='{{store direct_url="our-company"}}'>Our company</a></li>
                            <li><a href='{{store direct_url="carriers"}}'>Carriers</a></li>
                            <li><a href='{{store direct_url="shipping"}}'>Shipping</a></li>
                        </ul>
                    </li>
                    <li class="col-md-4 col-xs-12">
                        <div class="h4">Customer center</div>
                        <ul class="links">
                            <li><a href='{{store direct_url="customer/account"}}'>My Account</a></li>
                            <li><a href='{{store direct_url="sales/order/history"}}'>Order Status</a></li>
                            <li><a href='{{store direct_url="wishlist"}}'>Wishlist</a></li>
                            <li><a href='{{store direct_url="exchanges"}}'>Returns and Exchanges</a></li>
                        </ul>
                    </li>
                    <li class="col-md-4 col-xs-12">
                        <div class="h4">Info</div>
                        <ul class="links">
                            <li><a href='{{store direct_url="typography"}}'>Typography page</a></li>
                            <li><a href='{{store direct_url="privacy"}}'>Privacy policy</a></li>
                            <li><a href='{{store direct_url="delivery"}}'>Delivery information</a></li>
                            <li><a href='{{store direct_url="returns"}}'>Returns policy</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="col-md-3 footer-contacts">
                <div class="h4">Call Us</div>
                <span>
                We're available 24/7<br/>
                <strong>1.800.555.1903</strong><br/>
                </span>
                <div>Secure payments by</div>
                <img width="220" height="25" style="margin-top: 10px;" src='{{skin url="images/payments.png"}}' srcset='{{skin url="images/payments@2x.png"}} 2x' alt="Security Seal"/>
            </div>
        </div>

        <div class="block block-social">
            <ul class="icons">
                <li><a href="http://instagram.com"><i class="luxury-icon luxury-instagram"></i></a></li>
                <li><a href="http://twitter.com"><i class="luxury-icon luxury-twitter"></i></a></li>
                <li><a href="http://facebook.com"><i class="luxury-icon luxury-facebook"></i></a></li>
            </ul>
        </div>
    </div>
</div>
HTML
            ),
            'footer_toolbar' => array(
                'title' => 'footer_toolbar',
                'identifier' => 'footer_toolbar',
                'status' => 1,
                'content' => <<<HTML
<div class="footer-toolbar-container">
    <div class="hero">
        {{block type="newsletter/subscribe" template="newsletter/subscribe.phtml"}}
    </div>
</div>
HTML
            ),
            'product_page_additional_tabs' => array(
                'title' => 'product_page_additional_tabs',
                'identifier' => 'product_page_additional_tabs',
                'status' => 1,
                'content' => <<<HTML
<div class="before-footer">
    <div class="container">
        {{widget type="easytabs/widget" filter_tabs="customers_buy_tabbed,viewed_tabbed,askit_tabbed" template="tm/easytabs/tabs.phtml"}}
    </div>
</div>
HTML
            )
        );
    }

    /**
     * home
     */
    private function _getCmsPages()
    {
        return array(
            'home' => array(
                'title'             => 'home',
                'identifier'        => 'home',
                'root_template'     => 'one_column',
                'meta_keywords'     => '',
                'meta_description'  => '',
                'content_heading'   => '',
                'is_active'         => 1,
                'content'           => <<<HTML
<div class="row jumbotron jumbotron-slider">
    {{widget type="easyslide/insert" slider_id="argento_luxury"}}
</div>
<div class="row jumbotron">
    <div class="col-md-12">
        <div class="container">
            {{widget type="easycatalogimg/widget_list" category_count="5" subcategory_count="0" column_count="5" show_image="1" image_width="382" image_height="565" resize_image="0" template="tm/easycatalogimg/list.phtml"}}
        </div>
    </div>
</div>
<div class="row jumbotron">
    <div class="col-md-12">
        <div class="container hero">
            {{widget type="highlight/product_new" title="New Arrivals" show_page_link="1" page_title="Shop Now" products_count="4" column_count="4" template="tm/highlight/product/grid.phtml" class_name="highlight-content-new" img_width="280" img_height="410"}}
        </div>
    </div>
</div>
<div class="row jumbotron">
    <div class="hero block-homepage-banner">
        {{widget type="easybanner/widget_placeholder" placeholder_name="argento-luxury-home"}}
    </div>
</div>
<div class="row jumbotron block-benefits">
    <div class="container hero">
        <div class="col-md-4">
            <div class="a-center">
                <div class="luxury-icon luxury-icon-big luxury-cart-alt"></div>
                <h4>Free Delivery</h4>
                <p>Our store delivers an extensive and expertly curated selection of fashion and lifestyle offerings.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="a-center">
                <div class="luxury-icon luxury-icon-big luxury-lock"></div>
                <h4>Secure Payment</h4>
                <p>Our store delivers an extensive and expertly curated selection of fashion and lifestyle offerings.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="a-center">
                <div class="luxury-icon luxury-icon-big luxury-headphones"></div>
                <h4>24h Customer Service</h4>
                <p>Our store delivers an extensive and expertly curated selection of fashion and lifestyle offerings.</p>
            </div>
        </div>
    </div>
</div>
<div class="row jumbotron jumbotron-slick">
    <div class="container">
        <div class="block block-brands argento-slider">
            <div class="block-content">
                <div id="slider-brands-container" class="slider-wrapper">
                    <div class="slick" id="slider-brands" data-slick='{"slidesToShow": 6, "slidesToScroll": 6, "infinite": true, "autoplay": true, "speed": 1000, "autoplaySpeed": 4000, "arrows": true, "rtl": true, "dots": true, "responsive": [ {"breakpoint": 770, "settings": {"slidesToShow": 4, "slidesToScroll": 4}}, {"breakpoint": 640, "settings": {"slidesToShow": 3, "slidesToScroll": 3}}, {"breakpoint": 480, "settings": {"slidesToShow": 2, "slidesToScroll": 2}}, {"breakpoint": 321, "settings": {"slidesToShow": 1, "slidesToScroll": 1, "dots": false}}]}'>
                        <a href="#"><img src="{{skin url='images/catalog/brands/gucci.jpg'}}" alt="" width="150" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/lv.jpg'}}" alt="" width="100" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/ck.jpg'}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/chanel.jpg'}}" alt="" width="170" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/guess.jpg'}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/versace.jpg'}}" alt="" width="145" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/gucci.jpg'}}" alt="" width="150" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/lv.jpg'}}" alt="" width="100" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/ck.jpg'}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/chanel.jpg'}}" alt="" width="170" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/guess.jpg'}}" alt="" width="130" height="80"/></a>
                        <a href="#"><img src="{{skin url='images/catalog/brands/versace.jpg'}}" alt="" width="145" height="80"/></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
HTML
,
                'layout_update_xml' => ''
            ),
            'typography' => array(
                'title'             => 'Typography page',
                'identifier'        => 'typography',
                'root_template'     => 'one_column',
                'meta_keywords'     => '',
                'meta_description'  => '',
                'content_heading'   => '',
                'is_active'         => 1,
                'content'           => <<<HTML
<section id="headings" > <!-- big headings -->
    <div class="row">
        <div class="col-md-12">
            <div class="hero">
                <h1 class="underlined page-title">H1. Responsive Magento template with extensive functionality</h1>
                <p class="subtitle a-center">Argento gives your online business countless possibilities. Theme comes with 6 awesome designs and 20 feature-rich modules.
                </p>
            </div>
        </div>
    </div>
</section>
<section> <!-- h1 -->
    <div class="row">
        <div class="col-md-5">
            <article>
                <h1>H1. 6 stunning designs that convert</h1>
                <p>
                    Our template offers Luxury, Argento, Flat, Mall, Pure and Pure 2.0 themes to design a magnificent presentation of your store. You can also choose your favorite layout from 3 layout types: standard, boxed and fullwidth.
                </p>
            </article>
        </div>
        <div class="col-md-6 col-md-push-1">
            <article class="media">
                <div class="media-left">
                    <div class="media-object luxury-icon luxury-letter"></div>
                </div>
                <div class="media-body">
                    <h1>H1. Make the products look impressive</h1>
                    <p>
                        <a rel="nofollow" href="https://swissuplabs.com/magento-lightbox-extension.html"
                        title="Magento module">Lightbox Pro</a> extension adds the lightbox popup anywhere on site. Using <a rel="nofollow" href="https://swissuplabs.com/magento-slider-extension.html"
                        title="Easyslider">Image Slider</a> and <a rel="nofollow" href="https://swissuplabs.com/magento-slider-extension.html" title="Magento module">Slick Carousel</a> modules you will easily create nice sliders. <a rel="nofollow" href="https://swissuplabs.com/magento-product-labels-extension.html"
                        title="Magento module">ProLabels</a> module helps you add custom product labels as well as add ready to use labels for New, On Sale, In/Out of stock items.
                    </p>
                    <a rel="nofollow" href="#" class="read-more">Read more</a>
                </div>
            </article>
        </div>
    </div>
</section>
<section> <!-- h2 -->
    <div class="row">
        <div class="col-md-5">
            <article>
                <h2 class="underlined">H2. Designed with best SEO practices</h2>
                <p>
                    Make your site ranking benefit from Argento. The template comes with SEO Suite module that includes Rich Snippets, <a rel="nofollow" href="https://swissuplabs.com/magento-lightbox-extension.html" title="Magento module">HTML</a> and <a rel="nofollow" href="http://docs.swissuplabs.com/m1/extensions/seo-xml-sitemap/" title="Magento module">XML</a>, SEO metadata templates. Use Argento to deliver highly relevant search results quickly in search engines like Yahoo, Google, Bing, etc.
                </p>
            </article>
        </div>
        <div class="col-md-6 col-md-push-1">
            <article class="media">
                <div class="media-left">
                    <div class="media-object luxury-icon luxury-bulb"></div>
                </div>
                <div class="media-body">
                    <h2 class="underlined">H2. Help arrange products perfectly</h2>
                    <p>
                        <a rel="nofollow" href="https://swissuplabs.com/magento-custom-product-list-extension.html" title="Magento module">Highlight</a> module shows New, Featured, Onsale, Bestsellers, Popular product lists with filters. <a rel="nofollow" href="https://swissuplabs.com/easy-catalog-images.html" title="Magento module">Easy Catalog Images</a> adds category/subcategory listing block with assigned images everywhere. <a rel="nofollow" href="https://swissuplabs.com/magento-attributes-and-brands-pages.html" title="Magento module">Attribute/Brand pages</a> creates brands pages, menu with brands. <a rel="nofollow" href="https://swissuplabs.com/product-tabs-magento-extension.html" title="Magento module">Easy Tabs</a> shows a product page content in attractive product tabs.
                    </p>
                    <a rel="nofollow" href="#" class="read-more">Read more</a>
                </div>
            </article>
        </div>
    </div>
</section>
<section> <!-- h3 -->
    <div class="row">
        <div class="col-md-5">
            <article>
                <h3>H3. Highly customizable, easy to style</h3>
                <p>
                    Argento is very flexible. It allows you create custom themes and subthemes without modification of core theme files. Using the override feature, you can easily change css styles, the template and layout files. Via backend configurator you can change color scheme, font, header, etc.
                </p>
            </article>
        </div>
        <div class="col-md-6 col-md-push-1">
            <article class="media">
                <div class="media-left">
                    <div class="media-object luxury-icon luxury-compass"></div>
                </div>
                <div class="media-body">
                    <h3>H3. Bring excellent user experience</h3>
                    <p>
                        <a rel="nofollow" href="https://swissuplabs.com/easy-flags.html" title="Magento module">Easy Flags</a> module comes with nice flag buttons instead of plain store switcher. <a rel="nofollow" href="https://swissuplabs.com/facebook-like-button.html" title="Magento module">Facebook Like button</a> helps users spread a store content. <a rel="nofollow" href="https://swissuplabs.com/magento-products-questions-extension.html" title="Magento module">Ask It</a> extension adds the products questions block on product, category page and CMS pages. <a rel="nofollow" href="https://swissuplabs.com/magento-ajax-extension.html" title="Magento module">Ajax Pro</a> module enables ajax functionality all over.
                    </p>
                    <a rel="nofollow" href="#" class="read-more">Read more</a>
                </div>
            </article>
        </div>
    </div>
</section>
<section> <!-- h4 -->
    <div class="row">
        <div class="col-md-5">
            <article>
                <h4>H4. Works great on mobile devices</h4>
                <p>
                    Argento template was created with mobile web design practices. No need of mobile template. Mobile-friendly theme works perfectly on iOS, Android and BlackBerry. Due to responsive design and built-in AMP support, your site will look excellent on any device.
                </p>
            </article>
        </div>
        <div class="col-md-6 col-md-push-1">
            <article class="media">
                <div class="media-left">
                    <div class="media-object luxury-icon luxury-alert"></div>
                </div>
                <div class="media-body">
                    <h4>H4. Impact user experience in search</h4>
                    <p>
                        <a rel="nofollow" href="https://swissuplabs.com/magento-amp-extension.html" title="Magento module">AMP</a> module makes your site highly visible in Google search for mobile visitors. <a rel="nofollow" href="https://swissuplabs.com/magento-ajax-search-and-autocomplete-extension.html" title="Magento module">Ajax Search</a> adds the search by product description, keywords, CMS pages and catalog categories. <a rel="nofollow" href="https://swissuplabs.com/magento-seo-extension-rich-snippets.html" title="Magento module">Rich Snippets</a> help users see the information about your site. <a rel="nofollow" href="https://swissuplabs.com/magento-navigation-pro-extension.html" title="Magento module">Navigation Pro</a> creates fantastic menu with custom items and dropdown content based on categories of your store.
                    </p>
                    <a rel="nofollow" href="#" class="read-more">Read more</a>
                </div>
            </article>
        </div>
    </div>
</section>
<section> <!-- h3 -->
    <div class="row">
        <div class="col-md-5">
            <article>
                <h5>H5. Fastest loading responsive theme</h5>
                <p>
                    Based on CSS sprite techniques, Argento reduces a number of https requests. In order to boost download speed, CSS and JSS files are based on clean code that can be minified by default Magento merger and other popular modules such as Fooman Speedster or GT Page Speed.
                </p>
            </article>
        </div>
        <div class="col-md-6 col-md-push-1">
            <article class="media">
                <div class="media-left">
                    <div class="media-object luxury-icon luxury-compass"></div>
                </div>
                <div class="media-body">
                    <h5>H5. Help increase your revenue</h5>
                    <p>
                        <a rel="nofollow" href="https://swissuplabs.com/magento-sold-together-extension.html" title="Magento module">Sold Together</a> module blocks help you show more complementary products. <a rel="nofollow" href="https://swissuplabs.com/magento-banners-and-custom-blocks-extension.html" title="Magento module">Easy Banners</a> directs specific products at specific customers groups via placing banners or any other custom content. <a rel="nofollow" href="https://swissuplabs.com/magento-review-reminder-extension.html" title="Magento module">Review Reminder</a> aims to increase the number of reviews on your web pages. Via <a rel="nofollow" href="https://swissuplabs.com/testimonials.html" title="Magento module">Testimonials</a> module you can place testimonials listing anywhere using widgets.
                    </p>
                    <a rel="nofollow" href="#" class="read-more">Read more</a>
                </div>
            </article>
        </div>
    </div>
</section>
<section> <!-- h4 -->
    <div class="row">
        <div class="col-md-5">
            <article>
                <h6>H6. Magento 2 theme for Community edition</h6>
                <p>
                    Argento is available for Magento 2. Five beautiful designs such as Blank, Essence, Flat, Pure2 and Mall will prettify your Magento 2 ecommerce store. Besides you get 18 Magento modules included in package.
                </p>
            </article>
        </div>
        <div class="col-md-6 col-md-push-1">
            <article class="media">
                <div class="media-left">
                    <div class="media-object luxury-icon luxury-alert"></div>
                </div>
                <div class="media-body">
                    <h6>H6. Designed for any kind of store</h6>
                    <p>
                        Whatever site you run, use Argento theme. This is a template with unique designs and elegant layouts. Being modern and multipurpose, it will be suitable for fashion store, jewelry, toys, bags, watches, computer, etc.

                    </p>
                    <a rel="nofollow" href="#" class="read-more">Read more</a>
                </div>
            </article>
        </div>
    </div>
</section>
<section id="highlights" class="typography-3-columns" > <!-- 3-col-blocks -->
    <div class="row">
        <div class="col-md-4">
            <article class="a-center">
                <div class="luxury-icon luxury-cart-alt"></div>
                <h4>H4. Ajax powered</h4>
                <p>
                    With Argento theme you get fully AJAX driven e-commerce store. Ajax search autocomplete feature, ajax login popup, ajax shopping cart, ajax add to cart/ wishlist/ compare options are available.
                </p>
            </article>
        </div>
        <div class="col-md-4">
            <article class="a-center">
                <div class="luxury-icon luxury-lock"></div>
                <h4>H4. Magento Community / Enterprise</h4>
                <p>
                    Argento includes features of Magento Enterprise edition. Our template works with Magento EE 1.11.x-1.14.x, Magento CE 1.6.x-1.9.x. The template is also compatible with Magento CE 2.0.x-2.1.x.
                </p>
            </article>
        </div>
        <div class="col-md-4">
            <article class="a-center">
                <div class="luxury-icon luxury-headphones"></div>
                <h4>H4. Fast theme updates </h4>
                <p>
                    New Argento features are released every month to meet growing demands of our customers. Flexible theme structure supports adding enhancements and fast timely updates. Vote for new features.
                </p>
            </article>
        </div>
    </div>
</section>
<section id="lists"> <!-- lists -->
    <div class="row">
        <div class="col-md-3">
            <h3>H3. The fastest in all</h3>
            <ol>
                <li>quick to install</li>
                <li>enhanced theme editor</li>
                <li>reduced inline JavaScript</li>
                <li>resized homepage images</li>
            </ol>
        </div>
        <div class="col-md-3">
            <h3>H3. Mobile ready</h3>
            <ul>
                <li>floating navigation bar</li>
                <li>crisp logo for mobile</li>
                <li>modern styles of web forms</li>
                <li>mobile Swiper touch slider</li>
            </ul>
        </div>
        <div class="col-md-3">
            <h3>H3. Good usability</h3>
            <ul>
                <li class="icon icon-leaf">Amazon style menu</li>
                <li class="icon icon-pencil">sticky header and sidebar</li>
                <li class="icon icon-heart">product image hover feature</li>
                <li class="icon icon-lens">bootstrap support</li>
            </ul>
        </div>
        <div class="col-md-3">
            <h3>H3. Extended possibilities</h3>
            <ul class="circles">
                <li>changeable radio button color </li>
                <li>fancy stars in Review forms</li>
                <li>advanced layout settings</li>
                <li>unlimited product carousels</li>
            </ul>
        </div>
    </div>
</section>
<!--
<section id="tables">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped data-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
-->
<section id="messages"> <!-- system messages -->
    <div class="row">
        <div class="col-md-12">
             <h2>Important things in Argento</h2>
            <div class="message success-msg">IMPROVED DESIGN OF MAIN UI ELEMENTS.</div>
            <div class="message error-msg">50+ CONFIGURABLE OPTIONS</div>
            <div class="message notice-msg">MAGENTO 2 AVAILABLE.</div>
            <div class="message note-msg">20 MAGENTO MODULES INCLUDED</div>
        </div>
    </div>
</section>
HTML
,
                'layout_update_xml' => ''
            ),
            'noroute' => array(
                'title'             => 'Page 404',
                'identifier'        => 'no-route',
                'root_template'     => 'one_column',
                'meta_keywords'     => '',
                'meta_description'  => '',
                'content_heading'   => '',
                'is_active'         => 1,
                'content'           => <<<HTML
<div class="no-route">
    <div class="glitch">
        <div class="glitch__img"></div>
        <div class="glitch__img"></div>
        <div class="glitch__img"></div>
        <div class="glitch__img"></div>
        <div class="glitch__img"></div>
    </div>
    <div class="info">
        <h1 class="content__title">404</h1>
        <p class="content__text text">Page not found</p>
        <a href="{{store url=''}}" class="button">Go Home</a>
    </div>
</div>
HTML
,
                'layout_update_xml' => <<<XML
<reference name="head">
    <action method="addItem"><type>skin_css</type><name>css/glitch.css</name></action>
    <action method="addItem"><type>skin_js</type><name>js/glitch.js</name></action>
</reference>
<remove name="breadcrumbs"/>
XML
            )
        );
    }

    private function _getEasybanner()
    {
        return array(
            array(
                'name'         => 'argento-luxury-home',
                'parent_block' => 'non-existing-block',
                'limit'        => 1,
                'banners'      => array(
                    array(
                        'identifier' => 'argento-luxury-home1',
                        'title'      => 'Special Offer',
                        'url'        => 'free-shipping',
                        'image'      => 'argento/luxury/argento_luxury_callout_home1.png',
                        'width'          => 0,
                        'height'         => 0,
                        'resize_image'   => 0,
                        'retina_support' => 0
                    )
                )
            )
        );
    }

    private function _getSlider()
    {
        return array(
            array(
                'identifier'    => 'argento_luxury',
                'title'         => 'Argento Luxury',
                'theme'         => 'light',
                'duration'      => 1,
                'frequency'     => 5.0,
                'autoglide'     => 1,
                'controls_type' => 'number',
                'status'        => 1,
                'slides'        => array(
                    array(
                        'url'   => 'argento/luxury/argento_luxury_slider1.jpg',
                        'image' => 'Slide 1',
                        'desc_pos' => 5,
                        'background' => 3,
                        'description' => <<<HTML
<div>
    <h4>New Style</h4>
</div>
<div>
    <h1>Urban Summer</h1>
</div>
<div>
    <button class="button btn-alt"><span><span>Shop Now</span></span></button>
</div>
HTML
                    ),
                    array(
                        'url'   => 'argento/luxury/argento_luxury_slider2.jpg',
                        'image' => 'Slide 2',
                        'desc_pos' => 5,
                        'background' => 3,
                        'description' => <<<HTML
<div>
    <h4>New Style</h4>
</div>
<div>
    <h1>Urban Summer</h1>
</div>
<div>
    <button class="button btn-alt"><span><span>Shop Now</span></span></button>
</div>
HTML
                    ),
                    array(
                        'url'   => 'argento/luxury/argento_luxury_slider3.jpg',
                        'image' => 'Slide 3',
                        'desc_pos' => 5,
                        'background' => 3,
                        'description' => <<<HTML
<div>
    <h4>New Style</h4>
</div>
<div>
    <h1>Urban Summer</h1>
</div>
<div>
    <button class="button btn-alt"><span><span>Shop Now</span></span></button>
</div>
HTML
                    ),
                    array(
                        'url'   => 'argento/luxury/argento_luxury_slider4.jpg',
                        'image' => 'Slide 4',
                        'desc_pos' => 5,
                        'background' => 3,
                        'description' => <<<HTML
<div>
    <h4>New Style</h4>
</div>
<div>
    <h1>Urban Summer</h1>
</div>
<div>
    <button class="button btn-alt"><span><span>Shop Now</span></span></button>
</div>
HTML
                    ),
                    array(
                        'url'   => 'argento/luxury/argento_luxury_slider5.jpg',
                        'image' => 'Slide 5',
                        'desc_pos' => 5,
                        'background' => 3,
                        'description' => <<<HTML
<div>
    <h4>New Style</h4>
</div>
<div>
    <h1>Urban Summer</h1>
</div>
<div>
    <button class="button btn-alt"><span><span>Shop Now</span></span></button>
</div>
HTML
                    )
                )
            )
        );
    }

    private function _getProductAttribute()
    {
        return array();
    }

    private function _getEasytabsTabs()
    {
        // perhapse it's not the best practice update existing tabs in getter method
        // but if move it to up method if will affect new tabs
        //
        // unset product tabs for storeviews with luxury except product describtion
        $unsetTabs = Mage::getModel('easytabs/tab')
            ->getCollection()
            ->addProductTabFilter()
            ->addFieldToFilter('block', array('neq' => 'easytabs/tab_product_description'))
            ->addFieldToFilter('status', '1');
        foreach ($unsetTabs as $tab) {
            $this->unsetEasytab(null, $this->getStoreIds(), $tab->getAlias());
        }

        return array(
            array(
                'title' => 'Reviews&nbsp;({{eval code="getReviewsCollection()->getSize()"}})',
                'alias' => 'reviews_tabbed',
                'block' => 'easytabs/tab_product_review',
                'sort_order' => 90,
                'status' => 1,
                'product_tab' => 1,
                'custom_option' => null,
                'template' => 'review/product/view/list/limited.phtml',
                'unset' => 'product.info::reviews'
            ),
            array(
                'title' => 'Free delivery and returns',
                'alias' => 'delivery_and_returns_tabbed',
                'block' => 'easytabs/tab_html',
                'sort_order' => 99,
                'status' => 1,
                'product_tab' => 1,
                'content' => "<p>Our store delivers an extensive and expertly curated selection of fashion and lifestyle offerings.</p>\n<p>If you are not satisfied with your order - send it back within 30 days after day of purchase!</p>"
            ),
            array(
                'title' => 'Customers also buy',
                'alias' => 'customers_buy_tabbed',
                'block' => 'easytabs/tab_template',
                'sort_order' => 10,
                'status' => 1,
                'product_tab' => 0,
                'custom_option' => 'soldtogether/customer',
                'template' => 'tm/soldtogether/customer.phtml',
                'unset' => 'product.info.additional::product.info.soldtogethercustomer'
            ),
            array(
                'title' => 'Questions&nbsp;({{eval code="getCount()"}})',
                'alias' => 'askit_tabbed',
                'block' => 'easytabs/tab_template',
                'sort_order' => 90,
                'status' => 1,
                'product_tab' => 0,
                'custom_option' => 'askit/discussion',
                'template' => 'tm/askit/question/list.phtml',
                'unset' => 'product.info.additional::askit'
            ),
            array(
                'title' => 'Recently viewed',
                'alias' => 'viewed_tabbed',
                'block' => 'easytabs/tab_template',
                'sort_order' => 99,
                'status' => 1,
                'product_tab' => 0,
                'custom_option' => 'reports/product_viewed',
                'template' => 'reports/recently/viewed/grid.phtml',
                'unset' => null
            )
        );
    }
}
