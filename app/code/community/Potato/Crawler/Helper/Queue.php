<?php

class Potato_Crawler_Helper_Queue extends Mage_Core_Helper_Data
{
    /**
     * Add url to queue
     *
     * @param $url
     * @param $store
     * @param int $priority
     * @return $this
     */
    public function addUrl($url, $store, $priority=0)
    {
        /** @var Magento_Db_Adapter_Pdo_Mysql $write */
        $write =  Mage::getSingleton('core/resource')->getConnection('core_write');
        $queueTable = Mage::getSingleton('core/resource')->getTableName('po_crawler_queue');
        //store priority
        $basePriority = Potato_Crawler_Helper_Config::getPriority($store) . $priority;
        //sort by customer group priority
        foreach (Potato_Crawler_Helper_Config::getCustomerGroup($store) as $groupPriority => $group) {
            //sort by currency priority
            foreach (Potato_Crawler_Helper_Config::getCurrency($store) as $currencyPriority => $currency) {
                //sort by user agent
                foreach (Potato_Crawler_Helper_Config::getUserAgents($store) as $userAgent) {
                    //calculate priority
                    $priority = $basePriority . $groupPriority . $currencyPriority . $groupPriority;
                    $data = array(
                        'store_id'          => $store->getId(),
                        'customer_group_id' => $group,
                        'useragent'         => $userAgent['useragent'],
                        'currency'          => $currency,
                        'priority'          => (int)$priority,
                        'url'               => $url
                    );
                    try {
                        $write->insertOnDuplicate($queueTable, $data);
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                    Mage::helper('po_crawler')->log('Url "%s" has been added to the queue with %s priority .', array($url, $priority));
                }
            }
        }
        return $this;
    }

    /**
     * Get stores sorted by priority
     *
     * @return array
     */
    public function getStores()
    {
        $_result = array();
        foreach (Mage::app()->getStores() as $store) {
            if (!$store->getIsActive() || !Potato_Crawler_Helper_Config::isEnabled($store)) {
                continue;
            }
            $priority = Potato_Crawler_Helper_Config::getPriority($store);
            if (array_key_exists($priority, $_result)) {
                $priority += 1;
            }
            $_result[$priority] = $store;
        }
        ksort($_result);
        return $_result;
    }

    /**
     * Add url to queue by path
     *
     * @param $path
     * @param $store
     * @param int $priority
     * @return $this
     */
    public function addUrlByPath($path, $store, $priority=0)
    {
        //sort by protocol priority
        foreach (Potato_Crawler_Helper_Config::getProtocol($store) as $protocolPriority => $protocol) {
            $baseUrl = Potato_Crawler_Helper_Queue::getStoreBaseUrl($store, $protocol);
            $this->addUrl(htmlspecialchars($baseUrl . $path), $store, $priority . $protocolPriority);
        }
        return $this;
    }

    /**
     * @param $store
     * @param $protocol
     * @return string
     */
    static function getStoreBaseUrl($store, $protocol)
    {
        $baseUrl = $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        if ($protocol == Potato_Crawler_Model_Source_Protocol::HTTPS_VALUE) {
            $baseUrl = $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, true);
        }
        $baseUrl .= $store->getConfig(Mage_Core_Model_Store::XML_PATH_USE_REWRITES) ? '' : 'index.php/';
        if ($store->getConfig(Mage_Core_Model_Store::XML_PATH_STORE_IN_URL)) {
            $baseUrl = trim($baseUrl, '/');
            $baseUrl .= '/' . $store->getCode() . '/';
        }
        return $baseUrl;
    }
}