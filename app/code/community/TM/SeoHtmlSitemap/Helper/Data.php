<?php

class TM_SeoHtmlSitemap_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Path to store config if frontend output is enabled
     *
     * @var string
     */
    const XML_PATH_ENABLED = 'tm_seohtmlsitemap/general/enabled';
    /**
     * Path to store config sitemap title
     *
     * @var string
     */
    const XML_PATH_TITLE = 'tm_seohtmlsitemap/general/title';
    /**
     * Path to store config sitemap description
     *
     * @var string
     */
    const XML_PATH_DESCRIPTION = 'tm_seohtmlsitemap/general/description';
    /**
     * Path to store config sitemap keywords
     *
     * @var string
     */
    const XML_PATH_KEYWORDS = 'tm_seohtmlsitemap/general/keywords';

    /**
     * Path to store config show stores
     *
     * @var string
     */
    const XML_PATH_SHOW_STORES = 'tm_seohtmlsitemap/content/stores';
    /**
     * Path to store config show categories
     *
     * @var string
     */
    const XML_PATH_SHOW_CATEGORIES = 'tm_seohtmlsitemap/content/categories';
    /**
     * Path to store config max categories depth
     *
     * @var string
     */
    const XML_PATH_MAX_DEPTH = 'tm_seohtmlsitemap/content/category_depth';
    /**
     * Path to store config show products
     *
     * @var string
     */
    const XML_PATH_SHOW_PRODUCTS = 'tm_seohtmlsitemap/content/products';
    /**
     * Path to store config show out of stock products
     *
     * @var string
     */
    const XML_PATH_SHOW_OUT_OF_STOCK = 'tm_seohtmlsitemap/content/out_of_stock';
    /**
     * Path to store config products/categories sort by
     *
     * @var string
     */
    const XML_PATH_SORT_BY = 'tm_seohtmlsitemap/content/sort';
    /**
     * Path to store config number of columns
     *
     * @var string
     */
    const XML_PATH_COLUMNS = 'tm_seohtmlsitemap/content/columns';
    /**
     * Path to store config group by first letter
     *
     * @var string
     */
    const XML_PATH_GROUP_BY_LETTER = 'tm_seohtmlsitemap/content/group_by_letter';
    /**
     * Path to store config show cms pages
     *
     * @var string
     */
    const XML_PATH_SHOW_CMS = 'tm_seohtmlsitemap/content/cms';
    /**
     * Path to store config excluded cms pages
     *
     * @var string
     */
    const XML_PATH_EXCLUDED_CMS = 'tm_seohtmlsitemap/content/exclude_cms';
    /**
     * Path to store config show custom links
     *
     * @var string
     */
    const XML_PATH_SHOW_CUSTOM = 'tm_seohtmlsitemap/content/custom';

    /**
     * Checks whether HTML Sitemap can be displayed on the frontend
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function isEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $store);
    }

    /**
     * Get sitemap page title
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return string
     */
    public function getTitle($store = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_TITLE, $store);
    }

    /**
     * Get sitemap description
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return string
     */
    public function getDescription($store = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_DESCRIPTION, $store);
    }

    /**
     * Get sitemap keywrods
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return string
     */
    public function getKeywords($store = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_KEYWORDS, $store);
    }

    /**
     * Checks whether to show stores list on sitemap
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function showStores($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_SHOW_STORES, $store);
    }

    /**
     * Checks whether to show categories list on sitemap
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function showCategories($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_SHOW_CATEGORIES, $store);
    }

    /**
     * Checks whether to show products list on sitemap
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function showProducts($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_SHOW_PRODUCTS, $store);
    }

    /**
     * Checks whether to show out of stock products on sitemap
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function showOutOfStockProducts($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_SHOW_OUT_OF_STOCK, $store);
    }

    /**
     * Get products / categories sort by
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return string
     */
    public function getSortBy($store = null)
    {
        $sortBy = $this->groupByFirstLetter() ? 'name' :
            Mage::getStoreConfig(self::XML_PATH_SORT_BY, $store);
        return $sortBy;
    }

    /**
     * Get number of columns
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return int
     */
    public function getColumnsNumber($store = null)
    {
        return (int)Mage::getStoreConfig(self::XML_PATH_COLUMNS, $store);
    }

    /**
     * Checks whether to group links by first letter
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function groupByFirstLetter($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_GROUP_BY_LETTER, $store);
    }

    /**
     * Checks whether to show cms pages on sitemap
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function showCMSPages($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_SHOW_CMS, $store);
    }

    /**
     * Get excluded cms pages list
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return array
     */
    public function getExcludedCMSPages($store = null)
    {
        return explode(',', Mage::getStoreConfig(self::XML_PATH_EXCLUDED_CMS, $store));
    }

    /**
     * Checks whether to show custom links on sitemap
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function showCustomLinks($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_SHOW_CUSTOM, $store);
    }

    /**
     * Get sitemap url for footer links
     * @return String sitemap url
     */
    public function getSitemapUrl()
    {
        return $this->_getUrl('tm_seohtmlsitemap');
    }

    /**
     * Retrieve current store categories
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection
     */
    public function getStoreCategories()
    {
        $parent = Mage::app()->getStore()->getRootCategoryId();
        $category = Mage::getModel('catalog/category');
        if (!$category->checkId($parent)) {
            return new Varien_Data_Collection();
        }

        $sortBy = $this->getSortBy();
        $recursionLevel = max(0, (int) Mage::getStoreConfig(self::XML_PATH_MAX_DEPTH));
        $storeCategories = $category->getCategories($parent, $recursionLevel, $sortBy, true, false);

        return $storeCategories;
    }

    /**
     * Group items by first letter
     * @return array
     */
    public function groupCollectionByFirstLetter($items)
    {
        $prevGroup = null;
        $groupedItems = array();
        $isArray = is_array($items);
        foreach ($items as $item) {
            $name = $this->escapeHtml($isArray ? $item['name'] : $item->name);
            $curGroup = $this->getGroup($name);
            if (!$this->isSameGroup($prevGroup, $curGroup)) {
                $groupedItems[$curGroup] = array();
                $prevGroup = $curGroup;
            }
            $groupedItems[$prevGroup][] = $item;
        }
        return $groupedItems;
    }

    /**
     * @param string $word Option title
     * @return string
     */
    private function getGroup($word)
    {
        if (function_exists('mb_strtolower')) {
            $word = mb_strtolower($word);
        }
        if (function_exists('mb_substr')) {
            return mb_substr($word, 0, 1, 'UTF-8');
        }
        return substr($word, 0, 1);
    }

    /**
     * @param string $group1
     * @param string $group2
     */
    private function isSameGroup($group1, $group2)
    {
        if (is_numeric($group1) && is_numeric($group2)) {
            return true;
        } else {
            return $group1 == $group2;
        }
    }

    /**
     * @param string $group
     * @return string
     */
    public function getGroupTitle($group)
    {
        if (is_numeric($group)) {
            return '#';
        }
        return $group;
    }
}
