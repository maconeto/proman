<?php

$installer = $this;
$installer->startSetup();

/**
 * Create table 'tm_seohtmlsitemap/links'
 */
if ($installer->getConnection()->isTableExists($installer->getTable('tm_seohtmlsitemap/links')) != true) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('tm_seohtmlsitemap/links'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ), 'Link id')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'nullable' => false,
            'default' => 1
        ), 'Link status')
        ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false
        ), 'Link name')
        ->addColumn('url', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable'  => false
        ), 'Link url')
        ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(),
            'Link creation time')
        ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(),
            'Link update time')
        ->setComment('Templates Master Seo HTML Sitemap Links Table');
    $installer->getConnection()->createTable($table);
}

/**
 * Create table 'tm_seohtmlsitemap/store'
 */
if ($installer->getConnection()->isTableExists($installer->getTable('tm_seohtmlsitemap/store')) != true) {
    $table = $installer->getConnection()
        ->newTable($installer->getTable('tm_seohtmlsitemap/store'))
        ->addColumn('link_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
            ), 'Link ID')
        ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
            ), 'Store ID')
        ->addIndex($installer->getIdxName('tm_seohtmlsitemap/store', array('store_id')),
            array('store_id'))
        ->addForeignKey(
            $installer->getFkName('tm_seohtmlsitemap/store', 'link_id', 'tm_seohtmlsitemap/links', 'id'),
            'link_id',
            $installer->getTable('tm_seohtmlsitemap/links'),
            'id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->addForeignKey(
            $installer->getFkName('tm_seohtmlsitemap/store', 'store_id', 'core/store', 'store_id'),
            'store_id',
            $installer->getTable('core/store'),
            'store_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE)
        ->setComment('Seohtmlsitemap To Store Linkage Table');
    $installer->getConnection()->createTable($table);
}

$installer->endSetup();
