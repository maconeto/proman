<?php
class TM_SeoHtmlSitemap_Block_Cms extends Mage_Core_Block_Template
{
    /**
     * Initialize cms pages collection
     *
     * @return TM_SeoHtmlSitemap_Block_Cms
     */
    protected function _prepareLayout()
    {
        if (Mage::helper('tm_seohtmlsitemap')->showCMSPages()) {
            $excludedPages = Mage::helper('tm_seohtmlsitemap')->getExcludedCMSPages();
            $collection = Mage::getModel('cms/page')
                ->getCollection()
                ->addStoreFilter(Mage::app()->getStore())
                ->addFieldToSelect('page_id')
                ->addFieldToSelect('title', 'name')
                ->addFieldToFilter('identifier', array('nin' => $excludedPages))
                ->setOrder('title', Varien_Data_Collection::SORT_ORDER_ASC);
            $this->setCollection($collection);
        }
        return $this;
    }

    /**
     * Get item URL
     *
     * @param Mage_Cms_Model_Page $page
     * @return string
     */
    public function getItemUrl($page)
    {
        $helper = Mage::helper('cms/page');
        return $helper->getPageUrl($page->getId());
    }
}
