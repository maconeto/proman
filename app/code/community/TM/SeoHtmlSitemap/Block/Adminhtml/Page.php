<?php
class TM_SeoHtmlSitemap_Block_Adminhtml_Page extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'tm_seohtmlsitemap';
        $this->_controller = 'adminhtml_page';
        $this->_headerText = Mage::helper('tm_seohtmlsitemap')->__('Manage Links');

        parent::__construct();

        if (!$this->_isAllowedAction('save')) {
            $this->_removeButton('add');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('templates_master/tm_seocore/tm_seohtmlsitemap/' . $action);
    }
}
