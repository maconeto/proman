<?php
class TM_SeoHtmlSitemap_Block_Categories extends Mage_Catalog_Block_Seo_Sitemap_Category
{
    /**
     * Initialize categories collection
     *
     * @return TM_SeoHtmlSitemap_Block_Categories
     */
    protected function _prepareLayout()
    {
        if (Mage::helper('tm_seohtmlsitemap')->showCategories()) {
            $this->setCollection(
                Mage::helper('tm_seohtmlsitemap')->getStoreCategories()
            );
        }
        return $this;
    }
}
