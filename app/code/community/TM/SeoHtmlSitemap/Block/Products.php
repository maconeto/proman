<?php
class TM_SeoHtmlSitemap_Block_Products extends Mage_Catalog_Block_Seo_Sitemap_Product
{
    /**
     * Initialize products collection
     *
     * @return TM_SeoHtmlSitemap_Block_Products
     */
    protected function _prepareLayout()
    {
        if (!Mage::helper('tm_seohtmlsitemap')->showProducts()) {
            return $this;
        }

        parent::_prepareLayout();

        $collection = $this->getCollection();
        $sortBy = Mage::helper('tm_seohtmlsitemap')->getSortBy();
        $collection->addAttributeToSort($sortBy, Varien_Data_Collection::SORT_ORDER_ASC);

        if (!Mage::helper('tm_seohtmlsitemap')->showOutOfStockProducts()) {
            Mage::getSingleton('cataloginventory/stock')
                ->addInStockFilterToCollection($collection);
        }

        return $this;
    }
}
