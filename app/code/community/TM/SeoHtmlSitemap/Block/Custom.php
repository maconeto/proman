<?php
class TM_SeoHtmlSitemap_Block_Custom extends Mage_Core_Block_Template
{
    /**
     * Initialize custom pages collection
     *
     * @return TM_SeoHtmlSitemap_Block_Custom
     */
    protected function _prepareLayout()
    {
        if (Mage::helper('tm_seohtmlsitemap')->showCustomLinks()) {
            $links = Mage::getModel('tm_seohtmlsitemap/link')
                ->getCollection()
                ->addStoreFilter(Mage::app()->getStore())
                ->addFieldToSelect('name')
                ->addFieldToSelect('url')
                ->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->setOrder('name', Varien_Data_Collection::SORT_ORDER_ASC)
                ->toArray();

            $linksObj = new Varien_Object($links['items']);
            Mage::dispatchEvent(
                'tm_seohtmlsitemap_prepare_custom_links',
                array('links' => $linksObj)
            );
            $linksArray = $linksObj->getData();
            $this->sortLinks($linksArray);

            $this->setCollection($linksArray);
        }
        return $this;
    }

    /**
     * Get item URL
     *
     * @param array $link
     * @return string
     */
    public function getItemUrl($link)
    {
        return rtrim(Mage::getUrl($link['url']), '/');
    }
    /**
     * Sort links by name
     * @param array $links
     * @return array
     */
    private function sortLinks(&$links)
    {
        function compareByName($a, $b) {
            return strcasecmp($a["name"], $b["name"]);
        }
        usort($links, "compareByName");
    }
}
