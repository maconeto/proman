<?php
class TM_SeoHtmlSitemap_Model_Link extends Mage_Core_Model_Abstract
{
    const CACHE_TAG = 'tm_seohtmlsitemap_link';
    protected $_cacheTag = self::CACHE_TAG;

    public function __construct()
    {
        $this->_init('tm_seohtmlsitemap/link');
        parent::_construct();
    }

    /**
     * Get cache tags associated with object id
     *
     * @return array
     */
    public function getCacheIdTags()
    {
        $tags   = parent::getCacheIdTags();
        $tags[] = 'TM_SEOHTMLSITEMAP_' . $this->getId();
        return $tags;
    }
}
