<?php
class TM_SeoHtmlSitemap_Model_Observer
{
    /**
     * Redirect default sitemap urls
     *
     * @param Varien_Event_Observer $observer
     * @return TM_SeoHtmlSitemap_Model_Observer
     */
    public function redirectSitemap($observer)
    {
        if (!Mage::helper('tm_seohtmlsitemap')->isEnabled()) {
            return $this;
        }
        $controller = $observer->getControllerAction();
        $controller->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
        $controller->getResponse()->setRedirect(Mage::getUrl('tm_seohtmlsitemap'), 301);
        return $this;
    }
}
