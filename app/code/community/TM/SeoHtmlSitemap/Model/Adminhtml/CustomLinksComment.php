<?php
class TM_SeoHtmlSitemap_Model_Adminhtml_CustomLinksComment
{
    public function getCommentText()
    {
        $url = Mage::helper("adminhtml")->getUrl('adminhtml/htmlsitemaplinks_manage');
        return Mage::helper('tm_seohtmlsitemap')
            ->__(
                "To add custom links click here: <a href='%s'>Links Grid</a>",
                $url
            );
    }
}
