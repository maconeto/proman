<?php

class TM_SeoHtmlSitemap_Model_Config_ColumnsNumber
{
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label' => Mage::helper('tm_seohtmlsitemap')->__('1 column')),
            array('value' => 2, 'label' => Mage::helper('tm_seohtmlsitemap')->__('2 columns')),
            array('value' => 3, 'label' => Mage::helper('tm_seohtmlsitemap')->__('3 columns')),
            array('value' => 4, 'label' => Mage::helper('tm_seohtmlsitemap')->__('4 columns'))
        );
    }
}
