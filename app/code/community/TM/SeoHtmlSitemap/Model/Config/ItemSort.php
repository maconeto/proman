<?php

class TM_SeoHtmlSitemap_Model_Config_ItemSort
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'name', 'label' => Mage::helper('catalog')->__('Name')),
            array('value' => 'position', 'label' => Mage::helper('catalog')->__('Position'))
        );
    }
}
