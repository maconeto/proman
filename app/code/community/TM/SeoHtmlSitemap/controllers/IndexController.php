<?php
class TM_SeoHtmlSitemap_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Pre dispatch action that allows to redirect to no route page in case
     * of disabled extension through Admin panel
     */
    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::helper('tm_seohtmlsitemap')->isEnabled()) {
            $this->setFlag('', 'no-dispatch', true);
            $this->_redirect('noRoute');
        }
    }

    public function indexAction()
    {
        $this->loadLayout();

        $head = $this->getLayout()->getBlock('head');
        $title = Mage::helper('tm_seohtmlsitemap')->getTitle();
        if ($title) {
            $head->setTitle($title);
            $this->getLayout()
                ->getBlock('seohtmlsitemap.container')
                ->setTitle($title);
        }
        $description = Mage::helper('tm_seohtmlsitemap')->getDescription();
        if ($description) {
            $head->setDescription($description);
        }
        $keywords = Mage::helper('tm_seohtmlsitemap')->getKeywords();
        if ($keywords) {
            $head->setKeywords($keywords);
        }

        $this->renderLayout();
    }
}
