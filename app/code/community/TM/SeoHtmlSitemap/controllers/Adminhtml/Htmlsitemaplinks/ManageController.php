<?php
class TM_SeoHtmlSitemap_Adminhtml_Htmlsitemaplinks_ManageController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('templates_master/tm_seocore/tm_seohtmlsitemap')
            ->_addBreadcrumb(
                Mage::helper('tm_seohtmlsitemap')->__('HTML Sitemap Custom Links'),
                Mage::helper('tm_seohtmlsitemap')->__('Manage Links')
            );
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Templates Master'))
             ->_title($this->__('HTML Sitemap'))
             ->_title($this->__('Manage Links'));

        $this->_initAction();
        $this->renderLayout();
    }

    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_title($this->__('Templates Master'))
             ->_title($this->__('HTML Sitemap'))
             ->_title($this->__('Manage Links'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('tm_seohtmlsitemap/link');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('cms')->__('This page no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Link'));

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('links_data', $model);

        $this->_initAction()
            ->_addBreadcrumb(
                $id ? Mage::helper('tm_seohtmlsitemap')->__('Edit Link')
                    : Mage::helper('tm_seohtmlsitemap')->__('New Link'),
                $id ? Mage::helper('tm_seohtmlsitemap')->__('Edit Link')
                    : Mage::helper('tm_seohtmlsitemap')->__('New Link'));

        $this->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        if (!$data = $this->getRequest()->getPost('links')) {
            $this->_redirect('*/*/');
            return;
        }

        $model = Mage::getModel('tm_seohtmlsitemap/link');
        if ($id = $this->getRequest()->getParam('id')) {
            $model->load($id);
        }

        try {
            $model->addData($data);
            $model->save();

            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('tm_seohtmlsitemap')->__('Link was saved.')
            );
            Mage::getSingleton('adminhtml/session')->setFormData(false);
            if ($this->getRequest()->getParam('back')) {
                $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current' => true));
                return;
            }
            $this->_redirect('*/*/');
            return;
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_getSession()->setFormData($data);
        $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'), '_current' => true));
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('tm_seohtmlsitemap/link');
                $model->load($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('tm_seohtmlsitemap')->__('The link has been deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('tm_seohtmlsitemap')->__('Unable to find a link to delete.')
        );
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $linkIds = $this->getRequest()->getParam('links');
        if (!is_array($linkIds)) {
            $this->_getSession()->addError($this->__('Please select link(s).'));
        } else {
            if (!empty($linkIds)) {
                try {
                    foreach ($linkIds as $linkId) {
                        $link = Mage::getModel('tm_seohtmlsitemap/link')->load($linkId);
                        $link->delete();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been deleted.', count($linkIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $linkIds = (array)$this->getRequest()->getParam('links');
        $status = (int)$this->getRequest()->getParam('status');
        try {
            foreach ($linkIds as $linkId) {
                $link = Mage::getModel('tm_seohtmlsitemap/link')->load($linkId);
                $link->setStatus($status)->save();
            }

            $this->_getSession()->addSuccess(
                $this->__('Total of %d record(s) have been updated.', count($linkIds))
            );
        }
        catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()
                ->addException($e, $this->__('An error occurred while updating the link(s) status.'));
        }

        $this->_redirect('*/*/');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        $action = strtolower($this->getRequest()->getActionName());
        switch ($action) {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('templates_master/tm_seocore/tm_seohtmlsitemap/save');
                break;
            case 'delete':
                return Mage::getSingleton('admin/session')->isAllowed('templates_master/tm_seocore/tm_seohtmlsitemap/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('templates_master/tm_seocore/tm_seohtmlsitemap');
                break;
        }
    }
}
