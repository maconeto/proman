<?php
class TM_SeoXmlSitemap_Block_Sitemap_Grid_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
    public function render(Varien_Object $row)
    {
        $params = array('sitemap_id' => $row->getSitemapId());
        $actions = array(array(
            'url'     => $this->getUrl('*/sitemap/generate', $params),
            'caption' => Mage::helper('sitemap')->__('Generate'),
        ));
        if (Mage::helper('TM_SeoXmlSitemap')->isEnabled()) {
            $actions[] = array(
                'url'     => $this->getUrl('adminhtml/seoxmlsitemap_index/ping', $params),
                'caption' => Mage::helper('TM_SeoXmlSitemap')->__('Ping'),
            );
            $actions[] = array(
                'url'     => $this->getUrl('adminhtml/seoxmlsitemap_index/validate', $params),
                'caption' => Mage::helper('TM_SeoXmlSitemap')->__('Validate'),
            );
        }

        $this->getColumn()->setActions($actions);
        return parent::render($row);
    }
}
