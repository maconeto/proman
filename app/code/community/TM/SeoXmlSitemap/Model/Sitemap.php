<?php
/**
 * Sitemap model
 *
 * @method Mage_Sitemap_Model_Resource_Sitemap _getResource()
 * @method Mage_Sitemap_Model_Resource_Sitemap getResource()
 * @method string getSitemapType()
 * @method Mage_Sitemap_Model_Sitemap setSitemapType(string $value)
 * @method string getSitemapFilename()
 * @method Mage_Sitemap_Model_Sitemap setSitemapFilename(string $value)
 * @method string getSitemapPath()
 * @method Mage_Sitemap_Model_Sitemap setSitemapPath(string $value)
 * @method string getSitemapTime()
 * @method Mage_Sitemap_Model_Sitemap setSitemapTime(string $value)
 * @method int getStoreId()
 * @method Mage_Sitemap_Model_Sitemap setStoreId(int $value)
 *
 */
class TM_SeoXmlSitemap_Model_Sitemap extends Mage_Sitemap_Model_Sitemap
{
    protected $stores = null;

    /**
     *
     * @param  int $storeId
     * @return string
     */
    protected function getLocaleCodeByStoreId($storeId)
    {
        $localeCode = Mage::getStoreConfig('general/locale/code', $storeId);
        $localeCode = substr($localeCode, 0, 2);

        return $localeCode;
    }

    protected function getStores()
    {
        if (null === $this->stores) {
            foreach (Mage::app()->getWebsites() as $website) {
                foreach ($website->getGroups() as $group) {
                    foreach ($group->getStores() as $store) {
                        $storeId = $store->getId();
                        $localeCode = $this->getLocaleCodeByStoreId($storeId);
                        $this->stores[$localeCode] = $storeId;
                    }
                }
            }
        }

        return $this->stores;
    }

    protected function getAlternates($item, $type)
    {
        $alternates = array();
        $id = $item->getId();
        $stores = $this->getStores();
        $currentStoreId = $this->getStoreId();
        $stores = array_diff($stores, array($currentStoreId));
        foreach ($stores as $locale => $storeId) {
            $url = null;
            switch ($type) {
                case 'category':
                    $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load($id);
                    if ($category && $category->getId()) {
                        $baseUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
                        $url = $baseUrl . $category->getUrlPath();
                    }

                    unset($category);
                    break;
                case 'page':
                    $page = Mage::getModel('cms/page')
                        ->setStoreId($storeId)
                        ->load($id);
                    if ($page && $page->getId()) {
                        $url = Mage::helper('cms/page')->getPageUrl($page->getId());
                    }

                    unset($page);
                    break;
                case 'product':
                default:
                    $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($id);
                    if ($product && $product->getId()) {
                        $url = $product->getProductUrl();
                    }

                    unset($product);
                    break;
            }

            if (!empty($url) && !in_array($url, $alternates)) {
                $alternates[$locale] = $url;
            }
        }

        return $alternates;
    }

    /**
     * Generate XML file
     *
     * @return Mage_Sitemap_Model_Sitemap
     */
    public function generateXml()
    {
        $storeId = $this->getStoreId();
        $helper = Mage::helper('TM_SeoXmlSitemap');
        $isEnabled = $helper->isEnabled($storeId);
        if (false == $isEnabled) {
            return parent::generateXml();
        }

        $io = new TM_SeoXmlSitemap_Model_Io_Sitemap();

        $io->streamStart($this->getSitemapFilename(), $this->getPath());

        $io->setStoreId($storeId);
        $date = Mage::getSingleton('core/date')->gmtDate('Y-m-d');
        $io->setDate($date);
        $baseUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

        $io->setBaseUrl($baseUrl);

        $isAlternates = $helper->isAlternates($storeId);
        $alternates = $images = array();

        /**
         * Generate categories sitemap
         */
        $isEnabled = $helper->isCategoryEnable($storeId);
        if ($isEnabled) {
            $changefreq = $helper->getCategoryChangefreq($storeId);
            $priority   = $helper->getCategoryPriority($storeId);
            $collection = Mage::getResourceModel('sitemap/catalog_category')->getCollection($storeId);
            $categories = new Varien_Object();
            $categories->setItems($collection);
            Mage::dispatchEvent(
                'sitemap_categories_generating_before',
                array('collection' => $categories)
            );
            foreach ($categories->getItems() as $item) {
                if ($isAlternates) {
                    $alternates = $this->getAlternates($item, 'category');
                    $alternates = array_diff($alternates, array($baseUrl . $item->getUrl()));
                }

                $io->addItem(
                    $baseUrl . $item->getUrl(),
                    $date,
                    $changefreq,
                    $priority,
                    $images,
                    $alternates
                );
            }

            unset($collection);
        }

        /**
         * Generate products sitemap
         */
        $isEnabled = $helper->isProductEnable($storeId);
        if ($isEnabled) {
            $changefreq = $helper->getProductChangefreq($storeId);
            $priority   = $helper->getProductPriority($storeId);
            $collection = Mage::getResourceModel('sitemap/catalog_product')->getCollection($storeId);
            $products = new Varien_Object();
            $products->setItems($collection);
            Mage::dispatchEvent(
                'sitemap_products_generating_before',
                array('collection' => $products)
            );
            $isImages = $helper->isProductImages($storeId);
            $imageWidth = $helper->getProductImagesWidth($storeId);
            $imageHeight = $helper->getProductImagesHeight($storeId);
            foreach ($products->getItems() as $item) {
                $productUrl = $baseUrl . $item->getUrl();
                $images = array();
                if ($isImages) {
                    try {
                        $productId = $item->getId();
                        $product = Mage::getModel('catalog/product')->load($productId);
                        if ($product->getId()) {
                            // $productUrl = $product->getProductUrl();
                            $imageUrl = Mage::helper('catalog/image')->init($product, 'small_image');
                            if (!empty($imageWidth)) {
                                if (!empty($imageHeight)) {
                                    $imageUrl->resize($imageWidth, $imageHeight);
                                } else {
                                    $imageUrl->resize($imageWidth);
                                }
                            }

                            $imageUrl = (string) $imageUrl;
                            $imageName = Mage::helper('core')->escapeHtml(
                                $product->getName()
                            );
                            $images[] = array(
                                'loc' => $imageUrl,
                                'caption' => $imageName,
                                'title' => $imageName,
                            );
                        }

                        unset($product);
                    } catch (Exception $e) {
                        // $this->_getSession()->addException($e,
                    //     Mage::helper('sitemap')->__('Unable to generate the sitemap.'));
                    }
                }

                if ($isAlternates) {
                    $alternates = $this->getAlternates($item, 'product');
                    $alternates = array_diff($alternates, array($productUrl));
                }

                $io->addItem(
                    $productUrl,
                    $date,
                    $changefreq,
                    $priority,
                    $images,
                    $alternates
                );
            }

            unset($collection);
        }

        /**
         * Generate cms pages sitemap
         */
        $images = $alternates = array();
        $isEnabled = $helper->isPageEnable($storeId);
        if ($isEnabled) {
            $changefreq = $helper->getPageChangefreq($storeId);
            $priority   = $helper->getPagePriority($storeId);
            $collection = Mage::getResourceModel('sitemap/cms_page')->getCollection($storeId);
            foreach ($collection as $item) {
                if ($isAlternates) {
                    // $alternates = $this->getAlternates($item, 'page');
                    // $alternates = array_diff($alternates, array($baseUrl . $item->getUrl()));
                }

                $io->addItem(
                    $baseUrl . $item->getUrl(),
                    $date,
                    $changefreq,
                    $priority,
                    $images,
                    $alternates
                );
            }

            unset($collection);
        }

        /**
         * Generate custom pages sitemap
         */
        $isEnabled = $helper->isCustomEnable($storeId);
        // $isEnabled = Mage::helper('tm_seohtmlsitemap')->showCustomLinks();
        if ($isEnabled) {
            $links = Mage::getModel('tm_seohtmlsitemap/link')
                ->getCollection()
                ->addStoreFilter($storeId)
                ->addFieldToSelect('name')
                ->addFieldToSelect('url')
                ->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
                ->setOrder('name', Varien_Data_Collection::SORT_ORDER_ASC)
                ->toArray();

            $linksObj = new Varien_Object($links['items']);
            Mage::dispatchEvent(
                'tm_seohtmlsitemap_prepare_custom_links',
                array('links' => $linksObj)
            );
            $links = $linksObj->getData();
            unset($linksObj);

            $changefreq = $helper->getCustomChangefreq($storeId);
            $priority   = $helper->getCustomPriority($storeId);
            foreach ($links as $link) {
                $io->addItem(
                    $baseUrl . $link['url'],
                    $date,
                    $changefreq,
                    $priority
                );
            }

            unset($links);
        }

        $isEnabled = $helper->isCustomEnable($storeId)
            && Mage::helper('catalog')->isModuleEnabled('Fishpig_Wordpress');

        if ($isEnabled) {
            $posts = Mage::getResourceModel('wordpress/post_collection');
            if ($posts) {
                $posts
                    ->setFlag('include_all_post_types', true)
                    ->addIsViewableFilter()
                    ->setOrderByPostDate()
                    ->load();

                $changefreq = 'monthly';
                $priority = '0.5';

                foreach ($posts as $post) {
                    $io->addItem(
                        $post->getUrl(),
                        $post->getPostModifiedDate('Y-m-d'),
                        $changefreq,
                        $priority
                    );
                }

                unset($posts);
            }

            $pages = Mage::getResourceModel('wordpress/page_collection');
            if ($pages) {
                $pages
                    ->addIsViewableFilter()
                    ->setOrderByPostDate()
                    ->load();

                $changefreq = 'monthly';
                $priority = '0.8';

                foreach ($pages as $page) {
                    $io->addItem(
                        $page->getUrl(),
                        $page->getPostModifiedDate('Y-m-d'),
                        $changefreq,
                        $priority
                    );
                }

                unset($pages);
            }
        }

        $io->streamEnd();

        $this->setSitemapTime(date("Y-m-d H:i:s"));
        $this->save();

        return $this;
    }
}
