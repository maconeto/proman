<?php
class TM_SeoXmlSitemap_Model_Resource_Cms_Page extends Mage_Sitemap_Model_Resource_Cms_Page
{
    /**
     * Retrieve cms page collection array
     *
     * @param unknown_type $storeId
     * @return array
     */
    public function getCollection($storeId)
    {
        $isEnabled = Mage::helper('TM_SeoXmlSitemap')->isEnabled($storeId);
        if (false == $isEnabled) {
            return parent::getCollection($storeId);
        }

        $pages = array();

        $select = $this->_getWriteAdapter()->select()
            ->from(array('main_table' => $this->getMainTable()), array($this->getIdFieldName(), 'identifier AS url'))
            ->join(
                array('store_table' => $this->getTable('cms/page_store')),
                'main_table.page_id=store_table.page_id',
                array()
            )
            ->where('main_table.is_active=1')
            ->where('store_table.store_id IN(?)', array(0, $storeId));
        $excludedPages = Mage::helper('tm_seohtmlsitemap')->getExcludedCMSPages();
        if (!empty($excludedPages)) {
            $select->where('main_table.identifier NOT IN(?)', $excludedPages);
        }

        $query = $this->_getWriteAdapter()->query($select);
        while ($row = $query->fetch()) {
            if ($row['url'] == Mage_Cms_Model_Page::NOROUTE_PAGE_ID) {
                continue;
            }

            $page = $this->_prepareObject($row);
            $pages[$page->getId()] = $page;
        }

        return $pages;
    }
}
