<?php
class TM_SeoXmlSitemap_Model_Resource_Catalog_Product extends Mage_Sitemap_Model_Resource_Catalog_Product
{
    /**
     * Get product collection array
     *
     * @param int $storeId
     * @return array
     */
    public function getCollection($storeId)
    {
        $isEnabled = Mage::helper('TM_SeoXmlSitemap')->isEnabled($storeId);
        if (false == $isEnabled) {
            return parent::getCollection($storeId);
        }

        /* @var $store Mage_Core_Model_Store */
        $store = Mage::app()->getStore($storeId);
        if (!$store) {
            return false;
        }

        $this->_select = $this->_getWriteAdapter()->select()
            ->from(array('main_table' => $this->getMainTable()), array($this->getIdFieldName()))
            ->join(
                array('w' => $this->getTable('catalog/product_website')),
                'main_table.entity_id = w.product_id',
                array()
            )
            ->where('w.website_id=?', $store->getWebsiteId());

        $storeId = (int)$store->getId();

        /** @var $urlRewrite Mage_Catalog_Helper_Product_Url_Rewrite_Interface */
        $urlRewrite = $this->_factory->getProductUrlRewriteHelper();
        $urlRewrite->joinTableToSelect($this->_select, $storeId);

        $this->_addFilter(
            $storeId,
            'visibility',
            Mage::getSingleton('catalog/product_visibility')->getVisibleInSiteIds(),
            'in'
        );
        $this->_addFilter(
            $storeId,
            'status',
            Mage::getSingleton('catalog/product_status')->getVisibleStatusIds(),
            'in'
        );
        if (!Mage::helper('TM_SeoXmlSitemap')->isProductOutOfStock($storeId)) {
            $this->addInStockFilter($storeId);
        }

        return $this->_loadEntities();
    }

    /**
     * add 'out of stock' filter
     *
     */
    protected function addInStockFilter($storeId = null)
    {
        $manageStock = Mage::getStoreConfig(Mage_CatalogInventory_Model_Stock_Item::XML_PATH_MANAGE_STOCK, $storeId);
        $cond = array(
            '{{table}}.use_config_manage_stock = 0 AND {{table}}.manage_stock=1 AND {{table}}.is_in_stock=1',
            '{{table}}.use_config_manage_stock = 0 AND {{table}}.manage_stock=0',
        );

        if ($manageStock) {
            $cond[] = '{{table}}.use_config_manage_stock = 1 AND {{table}}.is_in_stock=1';
        } else {
            $cond[] = '{{table}}.use_config_manage_stock = 1';
        }

        $alias = 'inventory_in_stock';
        $table = 'cataloginventory/stock_item';
        $field = 'is_in_stock';
        $bind  = 'product_id=entity_id';
        $cond  = '(' . join(') OR (', $cond) . ')';

        // validate table
        if (strpos($table, '/') !== false) {
            $table = Mage::getSingleton('core/resource')->getTableName($table);
        }

        $tableAlias = 'at_' . $alias;
        list($pk, $fk) = explode('=', $bind);
        $pk = $this->_select->getAdapter()->quoteColumnAs(trim($pk), null);
        $bindCond = $tableAlias . '.' . trim($pk) . '=' . 'main_table.' . trim($fk);

        $condArr = array($bindCond);
        // add where condition if needed
        if ($cond !== null) {
            $condArr[] = str_replace('{{table}}', $tableAlias, $cond);
        }

        $cond = '(' . implode(') AND (', $condArr) . ')';
        $this->_select->join(
            array($tableAlias => $table),
            $cond,
            ($field ? array($alias => $field) : array())
        );
        return $this;
    }
}
