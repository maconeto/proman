<?php

class TM_SeoXmlSitemap_Model_Io_Sitemap extends Varien_Io_File
{
    const SCHEMA = 'http://www.sitemaps.org/schemas/sitemap/0.9';
    const SCHEMA_IMAGE = "http://www.google.com/schemas/sitemap-image/1.1";//xmlns:image=
    const SCHEMA_ALTERNATE = "http://www.w3.org/1999/xhtml";//xmlns:xhtml=

    protected $countEOL = 0;

    protected $size = 0;

    protected $splitedFileNames = array();

    protected $urlsetWraper = false;

    protected $lineLimit = null;

    protected $sizeLimit = null;

    protected $baseUrl;

    protected $gmtDate = null;

    /**
     *
     * @var int|null
     */
    protected $storeId = null;

    /**
     *
     * @param int $storeId
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;
        return $this;
    }

    /**
     * [getStoreId description]
     * @return [type] [description]
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     *
     * @param  string $fileName
     * @param  string $path
     * @return bool
     */
    public function streamStart($fileName, $path)
    {
        $this->setAllowCreateFolders(true);
        $this->open(array('path' => $path));

        if ($this->fileExists($fileName) && !$this->isWriteable($fileName)) {
            Mage::throwException(
                Mage::helper('sitemap')->__(
                    'File "%s" cannot be saved. Please, make sure the directory "%s" is writeable by web server.',
                    $fileName,
                    $path
                )
            );
        }

        $this->streamOpen($fileName);

        $this->addXmlHeader();

        return true;
    }

    /**
     *
     * @return bool
     */
    public function streamEnd()
    {
        if (empty($this->splitedFileNames)) {
            $this->streamClose();
        } else {
            $this->createSitemapIndex();
        }

        return true;
    }

    /**
     * Binary-safe file write
     *
     * @param string $str
     * @return bool
     */
    public function streamWrite($str)
    {
        if (!$this->_streamHandler) {
            return false;
        }

        $this->countEOL = $this->countEOL + count(explode(PHP_EOL, $str));
        return @fwrite($this->_streamHandler, $str);
    }

    /**
     * Close an open file pointer
     * Set chmod on a file
     *
     * @return bool
     */
    public function streamClose()
    {
        if (!$this->_streamHandler) {
            return false;
        }


        if ($this->_streamLocked) {
            $this->streamUnlock();
        }

        if (true == $this->urlsetWraper) {
            $this->streamWrite("\n" . '</urlset>');
            $this->urlsetWraper = false;
        }

        @fclose($this->_streamHandler);
        $this->chmod(
            $this->_streamFileName,
            $this->_streamChmod
        );

        return true;
    }

    /**
     *
     * @param string $images
     * @return string
     */
    protected function addItemImages($images)
    {
        $str = '';
        foreach ($images as $image) {
            $str .= "\n\t\t<image:image>";
            foreach ($image as $key => $value) {
                $str .= "\n\t\t\t<image:{$key}>" . $value . "</image:{$key}>";
            }

            $str .= "\n\t\t</image:image>";
        }

        return $str;
    }

    protected function addItemAlternates($alternates)
    {
        $str = '';
        foreach ($alternates as $locale => $alternateUrl) {
            $str .= "\n\t\t" . '<xhtml:link rel="alternate" hreflang="'
                . $locale . '" href="' . $alternateUrl . '" />';
        }

        return $str;
    }

    public function addItem($url, $date, $changefreq, $priority, $images = array(), $alternates = array())
    {
        $helper = Mage::helper('TM_SeoXmlSitemap');
        $storeId = $this->getStoreId();
        $isCheck = $helper->isCheckAvailable($storeId);
        if ($isCheck && !$helper->checkUrlAvailable($url)) {
            return $this;
        }

        $isImages = $helper->isProductImages($storeId) /*&& !empty($images)*/;
        $isAlternates = $helper->isAlternates($storeId) /*&& !empty($alternates)*/;
        if (false === $this->urlsetWraper) {
            $this->urlsetWraper = true;
            $xmlnsImage = $isImages ? '"' . "\n\t" . 'xmlns:image="' . self::SCHEMA_IMAGE : '';
            $xmlnsXhtml = $isAlternates ? '"' . "\n\t" . 'xmlns:xhtml="' . self::SCHEMA_ALTERNATE: '';
            $urlset = '<urlset xmlns="' . self::SCHEMA . $xmlnsImage . $xmlnsXhtml . '">';
            $this->streamWrite($urlset);
        }

        $images = $isImages ? $this->addItemImages($images) : '';
        $alternates = $isAlternates ? $this->addItemAlternates($alternates) : '';

        $date = (string) $date;
        $changefreq = (string) $changefreq;
        $priority = (float) $priority;
        $xml = sprintf(
            "\n\t<url>" .
                "\n\t\t<loc>%s</loc>" .
                "\n\t\t<lastmod>%s</lastmod>" .
                "\n\t\t<changefreq>%s</changefreq>" .
                "\n\t\t<priority>%.1f</priority>" .
                "%s" .
                "%s" .
            "\n\t</url>",
            htmlspecialchars($url),
            $date,
            $changefreq,
            $priority,
            $images,
            $alternates
        );
        $this->streamWrite($xml);

        $lineLimit = $this->getLineLimit();
        if ($this->countEOL > $lineLimit) {
            $this->splitStream();
        }

        $sizeLimit = $this->getSizeLimit();
        // $sizeLimit = 1024 * 20;

        if (0 == $this->size) {
            $this->size = $this->streamStat('size');
        } else {
            $this->size += function_exists('mb_strlen') ? mb_strlen($xml) : strlen($xml);
        }

        if ($this->size > $sizeLimit) {
            $this->splitStream();
        }

        return $this;
    }

    public function splitStream()
    {
        $count = count($this->splitedFileNames);
        $filename = basename($this->_streamFileName, '.xml') . '-' . $count . '.xml';

        $this->streamClose();
        if (!$this->mv($this->_streamFileName, $filename)) {
            throw new Exception("Error Processing Rename  $filename");
        }

        $filename = $this->gzip($filename);

        $date = $this->getDate();
        $this->splitedFileNames[$filename] = $date;

        $this->streamOpen($this->_streamFileName);
        $this->size = $this->streamStat('size');
        $this->countEOL = 0;
        $this->addXmlHeader();

        return true;
    }

    public function setDate($date)
    {
        $this->gmtDate = $date;
        return $this;
    }

    protected function getDate()
    {
        if ($this->gmtDate === null) {
            $this->gmtDate = Mage::getSingleton('core/date')->gmtDate('Y-m-d');
        }
        return $this->gmtDate;
    }

    protected function gzip($filename)
    {
        chdir($this->_cwd);
        $storeId = $this->getStoreId();
        $isGzip = Mage::helper('TM_SeoXmlSitemap')->isGzip($storeId);
        if ($isGzip && function_exists('gzopen')) {
            $gzfilename = $filename . ".gz";
            $fp = gzopen($gzfilename, 'w9'); // w == write, 9 == highest compression
            gzwrite($fp, file_get_contents($filename));
            gzclose($fp);
            @unlink($filename);
            $filename = $gzfilename;
        }

        chdir($this->_iwd);
        return $filename;
    }

    public function createSitemapIndex()
    {
        $this->splitStream();

        $this->streamWrite('<sitemapindex xmlns="' . self::SCHEMA . '">');

        $baseUrl = $this->baseUrl;
        $baseUrl = rtrim($baseUrl, '/');
        $path = $this->_cwd;

        $path = str_replace(Mage::getBaseDir(), '', $path);
        $path = trim($path, '/');
        $path = empty($path) ? $path : $path . '/';
        $baseUrl = $baseUrl . '/' . $path;
        foreach ($this->splitedFileNames as $sitemapFile => $date) {
            $url = (string) $baseUrl . $sitemapFile;
            $date = (string) $date;
            $xml = sprintf(
                "\n\t<sitemap>" .
                    "\n\t\t<loc>%s</loc>" .
                    "\n\t\t<lastmod>%s</lastmod>" .
                "\n\t</sitemap>",
                $url,
                $date
            );
            $this->streamWrite($xml);
        }

        $this->streamWrite("\n" . '</sitemapindex>');
        $this->streamClose();

        return true;
    }

    public function addXmlHeader()
    {
        $this->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
        return $this;
    }

    public function getSplitedFileNames()
    {
        return $this->splitedFileNames;
    }

    public function getIndexFileName()
    {
        return $this->_streamFileName;
    }

    public function getEOLCount()
    {
        return (int) $this->countEOL;
    }

    protected function getLineLimit()
    {
        if (null === $this->lineLimit) {
            $storeId = $this->getStoreId();
            $this->lineLimit = Mage::helper('TM_SeoXmlSitemap')->getLineLimit($storeId);
            if ($this->lineLimit < 200 || $this->lineLimit > 50000) {
                $this->lineLimit = 50000;
            }

            $this->lineLimit -= 5; //offset
        }

        return $this->lineLimit;
    }

    protected function getSizeLimit()
    {
        if (null === $this->sizeLimit) {
            $storeId = $this->getStoreId();
            $this->sizeLimit = Mage::helper('TM_SeoXmlSitemap')->getSizeLimit($storeId);
            if ($this->sizeLimit < 1 || $this->sizeLimit > 50) {
                $this->sizeLimit = 10;
            }

            $this->sizeLimit = $this->sizeLimit * (1024 * 1024);
            $this->sizeLimit -= 1024;// offset
        }

        return $this->sizeLimit;
    }

    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }
}
