<?php
class TM_SeoXmlSitemap_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED               = 'tm_seoxmlsitemap/general/enabled';
    const XML_PATH_LINES_LIMIT           = 'tm_seoxmlsitemap/general/lines_limit';
    const XML_PATH_SIZE_LIMIT            = 'tm_seoxmlsitemap/general/size_limit';
    const XML_PATH_GZIP                  = 'tm_seoxmlsitemap/general/gzip';
    const XML_PATH_CHECK_AVAILABLE       = 'tm_seoxmlsitemap/general/check_available';
    const XML_PATH_ALTERNATES            = 'tm_seoxmlsitemap/general/alternates';

    const XML_PATH_CATEGORY_ENABLE       = 'tm_seoxmlsitemap/category/enable';
    const XML_PATH_CATEGORY_CHANGEFREQ   = 'sitemap/category/changefreq';
    const XML_PATH_CATEGORY_PRIORITY     = 'sitemap/category/priority';

    const XML_PATH_PRODUCT_ENABLE        = 'tm_seoxmlsitemap/product/enable';
    const XML_PATH_PRODUCT_CHANGEFREQ    = 'sitemap/product/changefreq';
    const XML_PATH_PRODUCT_PRIORITY      = 'sitemap/product/priority';
    const XML_PATH_PRODUCT_OUT_OF_STOCK  = 'tm_seoxmlsitemap/product/out_of_stock';
    const XML_PATH_PRODUCT_IMAGES        = 'tm_seoxmlsitemap/product/images';
    const XML_PATH_PRODUCT_IMAGES_WIDTH  = 'tm_seoxmlsitemap/product/images_width';
    const XML_PATH_PRODUCT_IMAGES_HEIGHT = 'tm_seoxmlsitemap/product/images_height';

    const XML_PATH_PAGE_ENABLE           = 'tm_seoxmlsitemap/page/enable';
    const XML_PATH_PAGE_CHANGEFREQ       = 'sitemap/page/changefreq';
    const XML_PATH_PAGE_PRIORITY         = 'sitemap/page/priority';

    const XML_PATH_CUSTOM_ENABLE         = 'tm_seoxmlsitemap/custom/enable';
    const XML_PATH_CUSTOM_CHANGEFREQ     = 'tm_seoxmlsitemap/custom/changefreq';
    const XML_PATH_CUSTOM_PRIORITY       = 'tm_seoxmlsitemap/custom/priority';
    //Mage::helper('TM_SeoXmlSitemap')->

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isEnabled($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return int
     */
    public function getLineLimit($storeId = null)
    {
        return (int) Mage::getStoreConfig(self::XML_PATH_LINES_LIMIT, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return float
     */
    public function getSizeLimit($storeId = null)
    {
        return (float) Mage::getStoreConfig(self::XML_PATH_SIZE_LIMIT, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isGzip($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_GZIP, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isCheckAvailable($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_CHECK_AVAILABLE, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isProductEnable($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PRODUCT_ENABLE, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getProductChangefreq($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_PRODUCT_CHANGEFREQ, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getProductPriority($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_PRODUCT_PRIORITY, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isProductOutOfStock($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PRODUCT_OUT_OF_STOCK, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isProductImages($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PRODUCT_IMAGES, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getProductImagesWidth($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_PRODUCT_IMAGES_WIDTH, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getProductImagesHeight($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_PRODUCT_IMAGES_HEIGHT, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isAlternates($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ALTERNATES, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isCategoryEnable($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_CATEGORY_ENABLE, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getCategoryChangefreq($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_CATEGORY_CHANGEFREQ, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getCategoryPriority($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_CATEGORY_PRIORITY, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isPageEnable($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PAGE_ENABLE, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getPageChangefreq($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_PAGE_CHANGEFREQ, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getPagePriority($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_PAGE_PRIORITY, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return boolean
     */
    public function isCustomEnable($storeId = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_CUSTOM_ENABLE, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getCustomChangefreq($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_CUSTOM_CHANGEFREQ, $storeId);
    }

    /**
     *
     * @param  int  $storeId
     * @return string
     */
    public function getCustomPriority($storeId = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_CATEGORY_PRIORITY, $storeId);
    }

    /**
     *
     * @param  string $url
     * @return boolean
     */
    public function checkUrlAvailable($url, $customSuccessHttpCodes = array())
    {
        $headers = @get_headers($url);
        $httpCode = substr($headers[0], 9, 3);

        if (!$httpCode && function_exists('curl_init')) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HEADER, true); // we want headers
            curl_setopt($ch, CURLOPT_NOBODY, true); // we don't need body
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
        }

        if (!empty($customSuccessHttpCodes)
            && in_array($httpCode, $customSuccessHttpCodes)) {
            return true;
        }

        $restype = floor($httpCode / 100);
        //  ($restype == 3 || $restype == 2 || $restype == 1)
        return $restype == 2;
    }
}
