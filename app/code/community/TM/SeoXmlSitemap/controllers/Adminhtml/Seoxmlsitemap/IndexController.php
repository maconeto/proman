<?php

class TM_SeoXmlSitemap_Adminhtml_Seoxmlsitemap_IndexController extends Mage_Adminhtml_Controller_Action
{
    protected function getSitemapUrl($model)
    {
        $fileName = preg_replace(
            '/^\//',
            '',
            $model->getSitemapPath() . $model->getSitemapFilename()
        );

        $url = Mage::app()->getStore($model->getStoreId())
            ->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
            . $fileName;
        return Mage::helper('core')->escapeHtml($url);
    }

    public function pingAction()
    {
        $sitemapId = $this->getRequest()->getParam('sitemap_id');

        $model = Mage::getModel('sitemap/sitemap');
        $model->load($sitemapId);

        if (!$model->getId()) {
            $this->_redirectReferer();
            return;
        }

        $url = $this->getSitemapUrl($model);

        $pingToolUrls = array(
            'Google' => 'http://google.com/ping?sitemap=',
            //http://www.google.com/webmasters/tools/ping?sitemap=
            'Bing' => 'http://www.bing.com/webmaster/ping.aspx?siteMap=',
            // 'Yahoo' => 'http://www.bing.com/webmaster/ping.aspx?siteMap=',
            //http://search.yahooapis.com/SiteExplorerService/V1/ping?sitemap=
            'Yandex' => 'http://blogs.yandex.ru/pings/?status=success&url=',
            //http://submissions.ask.com/ping?sitemap=
            //http://ping.baidu.com/ping.html
        );
        $helper = Mage::helper('TM_SeoXmlSitemap');
        foreach ($pingToolUrls as $service => $pingUrl) {
            $_url = $pingUrl . $url;
            if ($helper->checkUrlAvailable($_url, array(301))) {
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    "Your Sitemap has been successfully added to our list of Sitemaps to {$service} crawl"
                );
            } else {
                Mage::getSingleton('adminhtml/session')->addWarning("Operation failed ({$service})");
            }
        }

        $this->_redirectReferer();
    }

    public function validateAction()
    {
        $sitemapId = $this->getRequest()->getParam('sitemap_id');

        $model = Mage::getModel('sitemap/sitemap');
        $model->load($sitemapId);

        if (!$model->getId()) {
            $this->_redirectReferer();
            return;
        }

        $url = $this->getSitemapUrl($model);
        // $url = 'http://templates-master.com/sitemap.xml';
        // $url = 'http://templates-master.com/mage';
        $url = Mage::helper('core')->escapeHtml($url);

        $validateToolUrl = "https://validator.w3.org/check";
        //"http://tools.seochat.com/tools/site-validator/";
        $client = new Zend_Http_Client($validateToolUrl);
        // 'SiteMapURL'
        $response = $client
            ->setParameterGet('uri', $url)
            ->request();
        if (200 == $response->getStatus()) {
            $body = $response->getBody();

            $_messageSuccess = $this->parseResponse($body, "//h2[@class='valid']");
            if (!empty($_messageSuccess)) {
                Mage::getSingleton('adminhtml/session')->addSuccess($_messageSuccess);
            }

            $_messageErrors = $this->parseResponse($body, "//h2[@class='invalid']");
            if (!empty($_messageErrors)) {
                Mage::getSingleton('adminhtml/session')->addError($_messageErrors);
            }
        }

        $this->_redirectReferer();
    }

    protected function parseResponse($html, $query = "//h2[@class='valid']")
    {
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        // silence warnings and erros during parsing.
        // More at http://php.net/manual/en/function.libxml-use-internal-errors.php
        $oldUseErrors = libxml_use_internal_errors(true);
        if (function_exists('mb_convert_encoding')) {
            $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        }

        // $dom->encoding = 'UTF-8';
        $dom->loadHTML($html);
        // restore old value
        libxml_use_internal_errors($oldUseErrors);

        // $scripts = array();
        $xpath = new DOMXPath($dom);

        $nodes= $xpath->query($query);

        $return = '';
        foreach ($nodes as $node) {
            $return .= $node->nodeValue;
        }

        return $return;
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return true;
    }
}
