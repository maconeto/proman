<?php
class TM_HoverGallery_Model_Observer
{
    /**
     * Append media gallery before rendering html
     *
     * @param Varien_Event_Observer $observer
     * @return TM_HoverGallery_Model_Observer
     */
    public function catalogBlockProductCollectionBeforeToHtml(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('tm_hovergallery')->isEnabled()) {
            return $this;
        }

        $productCollection = $observer->getEvent()->getCollection();
        if ($productCollection instanceof Varien_Data_Collection) {
            $productCollection->load();

            foreach ($productCollection as $product) {
                $attributes = $product->getTypeInstance(true)->getSetAttributes($product);
                $media_gallery = $attributes['media_gallery'];
                $backend = $media_gallery->getBackend();
                $backend->afterLoad($product);
                $mediaGallery = $product->getMediaGalleryImages();
                if ($mediaGallery && $img = $mediaGallery->getItemByColumnValue('position', '2')) {
                    $product->setHoverImage($img);
                }
            }
        }

        return $this;
    }
}
