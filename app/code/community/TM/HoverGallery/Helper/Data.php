<?php
class TM_HoverGallery_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Path to store config if frontend output is enabled
     *
     * @var string
     */
    const XML_PATH_ENABLED = 'hovergallery/general/enabled';

    /**
     * Checks whether hover gallery can be displayed in the frontend
     *
     * @param integer|string|Mage_Core_Model_Store $store
     * @return boolean
     */
    public function isEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $store);
    }

    /**
     * Get product hover image html
     *
     * @param  Mage_Catalog_Model_Product $product
     * @param  int $width
     * @param  int|null $height
     * @param  boolean $keepFrame
     * @param  array $backgroundColor
     * @return string
     */
    public function getHoverImage(
        Mage_Catalog_Model_Product $product,
        $width,
        $height = null,
        $keepFrame = true,
        array $backgroundColor = array(255, 255, 255)
    ) {
        if (!$this->isEnabled()) {
            return '';
        }

        if ($img = $product->getHoverImage()) {
            $imageUrl = Mage::helper('catalog/image')
                ->init($product, 'small_image', $img->getFile())
                ->keepFrame($keepFrame)
                ->backgroundColor($backgroundColor)
                ->resize($width, $height);

            $whAttrs = (isset($width) ? (' width="' . $width . '"') : '')
                . (isset($height) ? (' height="' . $height . '"') : '');

            return sprintf(
                '<img src="%s"
                    srcset="%s"
                    class="hover-image"
                    %s
                    alt="%s" />',
                $imageUrl,
                $imageUrl,
                $whAttrs,
                $product->getName()
            );
        }
    }
}
