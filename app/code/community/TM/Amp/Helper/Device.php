<?php

if (!@class_exists('Mobile_Detect')) {
    include_once 'TM/Amp/mobiledetect/Mobile_Detect.php';
}

class TM_Amp_Helper_Device extends Mage_Core_Helper_Abstract
{
    const TYPE_MOBILE   = 'mobile';
    const TYPE_TABLET   = 'tablet';
    const TYPE_DESKTOP  = 'desktop';

    public function getDeviceType()
    {
        $detector = new Mobile_Detect();
        if ($detector->isTablet()) {
            return self::TYPE_TABLET;
        } elseif ($detector->isMobile()) {
            return self::TYPE_MOBILE;
        }
        return self::TYPE_DESKTOP;
    }
}
