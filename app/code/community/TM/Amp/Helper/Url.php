<?php

class TM_Amp_Helper_Url
{
    /**
     * Get amp url for current page
     *
     * @return string
     */
    public function getAmpUrl()
    {
        // Not used to improve third-party layered navigation modules
        // return Mage::getModel('core/url')->getUrl('*/*/*', array(
        //     '_current' => true,
        //     '_use_rewrite' => true,
        //     '_query' => array(
        //         'amp' => 1
        //     )
        // ));

        // Not used to improve IIS compatibility
        // $url = Mage::helper('core/url')->getCurrentUrl();

        $url = $this->getCurrentUrl();

        // inject `amp=1` into query params
        $pos = strpos($url, '?');
        if ($pos !== false) {
            $url = substr_replace($url, '?amp=1&', $pos, 1);
        } else {
            $url .= '?amp=1';
        }

        return $url;
    }

    /**
     * Get currently viewed url
     *
     * @return string
     */
    public function getCurrentUrl()
    {
        $request = $this->getRequest()->getOriginalRequest();
        return $request->getScheme()
            . '://'
            . $request->getHttpHost()
            . $request->getRequestUri();
    }

    /**
     * @return Mage_Core_Controller_Request_Http
     */
    private function getRequest()
    {
        return Mage::app()->getRequest();
    }
}
