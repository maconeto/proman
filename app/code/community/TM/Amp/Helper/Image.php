<?php

if (!@class_exists('FastImage')) {
    include_once 'TM/Amp/fastimage/Fastimage.php';
}

class TM_Amp_Helper_Image extends Mage_Core_Helper_Abstract
{
    protected $_remoteImage = null;

    protected $_dimensions = array();

    /**
     * Get image dimensions
     *
     * 1. Try to locate image locally and use getimagesize
     * 2. Use FastImage lib to detect remote image dimensions
     *
     * @param  string $path Public url
     * @return Varien_Object
     */
    public function getDimensions($path)
    {
        if (empty($this->_dimensions[$path])) {
            // 1. Try to locate image locally and use getimagesize
            $localPath = str_replace(
                array(
                    trim(Mage::getBaseUrl(), '/'),
                    trim(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, true), '/'),
                ),
                Mage::getBaseDir(),
                $path
            );

            if (file_exists($localPath)) {
                $dimensions = getimagesize($localPath);
            } else {
                // 2. Use FastImage lib to detect remote image dimensions
                $image = $this->_getRemoteImage($path);
                $dimensions = @$image->getSize();
            }

            $this->_dimensions[$path] = $dimensions ? $dimensions : array(false, false);
        }
        return $this->_dimensions[$path];
    }

    /**
     * Get image width
     *
     * @param  string $path
     * @return string
     */
    public function getWidth($path)
    {
        $dimensions = $this->getDimensions($path);
        return $dimensions[0];
    }

    /**
     * Get image height
     *
     * @param  string $path
     * @return string
     */
    public function getHeight($path)
    {
        $dimensions = $this->getDimensions($path);
        return $dimensions[1];
    }

    /**
     * Get remove image class to determine image demensions on remote server
     *
     * @param  string $path
     * @return FastImage
     */
    protected function _getRemoteImage($path)
    {
        if (null === $this->_remoteImage) {
            $this->_remoteImage = new FastImage();
        }

        @$this->_remoteImage->load($path);

        return $this->_remoteImage;
    }
}
