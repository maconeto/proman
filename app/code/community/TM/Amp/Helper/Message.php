<?php

class TM_Amp_Helper_Message extends Mage_Core_Helper_Abstract
{
    /**
     * Get session storages
     *
     * @param  array|null $storageNames
     * @return array
     */
    public function getStorages(array $storageNames = null)
    {
        $storages = array();
        if (null === $storageNames) {
            $storageNames = array(
                'core/session',
                'checkout/session',
                'wishlist/session',
                'customer/session',
                'catalog/session',
            );
        }
        foreach ($storageNames as $storageName) {
            $storage = Mage::getSingleton($storageName);
            if (!$storage) {
                continue;
            }
            $storages[$storageName] = $storage;
        }
        return $storages;
    }

    /**
     * Get messages grouped by message type
     *
     * @param  boolean $clean
     * @return array
     */
    public function getMessages($clean = false, $useMappedType = false, $stripTags = false)
    {
        $messages = array();
        foreach ($this->getStorages() as $storage) {
            foreach ($storage->getMessages($clean)->getItems() as $message) {
                if ($useMappedType) {
                    $type = $this->getMappedType($message->getType());
                } else {
                    $type = $message->getType();
                }
                $text = $message->getCode();
                if ($stripTags) {
                    $text = strip_tags($text, '<b><i><u><s><em><strong>');
                }
                $messages[$type][] = $text;
            }
        }
        return $messages;
    }

    /**
     * Check if there are failure messages among all of messages
     *
     * @param  array|null $filureTypes [description]
     * @return boolean
     */
    public function hasFailureMessages(array $failureTypes = null)
    {
        if (null === $failureTypes) {
            $failureTypes = array(
                Mage_Core_Model_Message::ERROR,
                Mage_Core_Model_Message::WARNING,
                Mage_Core_Model_Message::NOTICE,
            );
        }
        if (count(array_intersect(array_keys($this->getMessages()), $failureTypes))) {
            return true;
        }
        return false;
    }

    public function getMappedType($type)
    {
        $mapping = array(
            Mage_Core_Model_Message::WARNING => 'error',
            Mage_Core_Model_Message::NOTICE  => 'error',
        );
        if (isset($mapping[$type])) {
            return $mapping[$type];
        }
        return $type;
    }
}
