<?php

class TM_Amp_Helper_Data extends Mage_Core_Helper_Abstract
{
    const DESIGN_PACKAGE = 'tmamp';

    protected $_canUseAmp = null;

    protected $_isPageSupported = null;

    /**
     * Retrieve AMP package to use
     *
     * @return string
     */
    public function getAmpPackage()
    {
        return self::DESIGN_PACKAGE;
    }

    /**
     * Retrieve AMP theme to use
     *
     * @return string
     */
    public function getAmpTheme()
    {
        return Mage::getStoreConfig('tmamp/design/theme');
    }

    /**
     * Checks if AMP can be used at current page
     *
     * This check is used before AMP activation.
     *
     * @return boolean
     */
    public function canUseAmp()
    {
        if (null === $this->_canUseAmp) {
            $flag = $this->_getRequest()->getParam('amp');
            $this->_canUseAmp = (bool)$flag;

            if (!$this->isPhpVersionSupported()
                || !$this->isAmpEnabled()
                || !$this->isPageSupported()) {

                $this->_canUseAmp = false;
            } elseif ($this->isAmpForced()
                && null === $flag
                && !$this->_getRequest()->isAjax()) {

                // force amp theme for all non ajax requests if flag is not sent
                $this->_canUseAmp = true;
            }
        }

        return $this->_canUseAmp;
    }

    /**
     * We use https://github.com/leafo/scssphp wich requires php 5.4.0
     *
     * @return boolean
     */
    public function isPhpVersionSupported()
    {
        return version_compare(phpversion(), '5.4.0', '>=');
    }

    /**
     * Retrieve `enabled` config flag
     *
     * @return boolean
     */
    public function isAmpEnabled()
    {
        return Mage::getStoreConfig('tmamp/general/enabled');
    }

    /**
     * Check if persistent_browsing should be used
     *
     * @return boolean
     */
    public function isPersistentBrowsingEnabled()
    {
        return Mage::getStoreConfig('tmamp/general/persistent_browsing');
    }

    /**
     * Check if AMP should be forced on a current device
     *
     * @param  string  $device Device type to check [mobile|tablet]
     * @return boolean
     */
    public function isAmpForced($device = null)
    {
        // can't use force, when persistent_browsing is disabled
        if (!$this->isPersistentBrowsingEnabled()) {
            return false;
        }

        if (null === $device) {
            $device = Mage::helper('tmamp/device')->getDeviceType();
        }
        return Mage::getStoreConfig('tmamp/general/force_' . $device);
    }

    /**
     * Check if AMP is supported at recieved page
     *
     * @param  $page    module_controller_action
     * @return boolean
     */
    public function isPageSupported($page = null)
    {
        if (null === $this->_isPageSupported) {
            if (null === $page) {
                $request = $this->_getRequest();
                $page = implode('_', array(
                    $request->getModuleName(),
                    $request->getControllerName(),
                    $request->getActionName()
                ));
            }

            $supportedPages = $this->getSupportedPages();
            if ((0 === strpos($page, 'cms_index_') ||
                0 === strpos($page, '__')) && // Mana_Seo fix
                in_array('cms_index_index', $supportedPages)) {

                // defaultIndexAction, noRouteAction, noCookiesAction support
                return true;
            }

            if (0 === strpos($page, 'wordpress') &&
                in_array('wordpress', $supportedPages)) {

                return true;
            }

            $object = new Varien_Object(array(
                'current_page' => $page,
                'supported_pages' => $supportedPages,
                'is_page_supported' => false,
            ));

            Mage::dispatchEvent('tmamp_is_page_supported', array(
                'result' => $object
            ));

            if ($object->getIsPageSupported()) {
                return true;
            }

            $this->_isPageSupported = in_array($page, $object->getSupportedPages());
        }

        return $this->_isPageSupported;
    }

    /**
     * Get the list of supported pages.
     *
     * @return array
     */
    public function getSupportedPages()
    {
        if (Mage::getStoreConfig('tmamp/general/all_pages')) {
            $pages = array_keys(
                Mage::getModel('tmamp/system_config_source_pages')->toArray()
            );
        } else {
            $pages = Mage::getStoreConfig('tmamp/general/pages');
            $pages = array_filter(explode(',', $pages));
        }

        $pages[] = 'checkout_cart_add'; // fix to generate amp=1 urls, when adding product to the cart
        $pages[] = 'tmamp_cart_add'; // fix to generate amp=1 urls, when adding product to the cart
        $pages[] = 'catalog_product_compare_index';

        return $pages;
    }

    /**
     * Retrieve domain name for page, served from google cache
     *
     * See https://developers.google.com/amp/cache/overview#amp-cache-url-format
     * for more information
     *
     * @return string
     */
    public function getAmpCacheDomainName()
    {
        $domain = $this->_getRequest()->getHttpHost();
        $domain = str_replace('-', '--', $domain);
        $domain = str_replace('.', '-', $domain);

        return "https://{$domain}.cdn.ampproject.org";
    }

    /**
     * Check if request is served from google cache
     *
     * @return boolean
     */
    public function isPageServedFromAmpCache()
    {
        if ($origin = $this->_getRequest()->getHeader('Origin')) {
            return $origin === $this->getAmpCacheDomainName();
        }

        return false;
    }

    /**
     * Check if we can skip form key validation for some actions.
     * This feature is used to fix some actions on google cached pages:
     *
     *  - add to cart
     *  - add to compare
     *
     * @return boolean
     */
    public function canSkipFormKeyValidation()
    {
        return $this->isPageServedFromAmpCache();
    }

    /**
     * Check if current locale uses rtl layout direction
     *
     * @return boolean
     */
    public function isRtl()
    {
        $layout = Mage::app()->getLocale()->getTranslationList('layout');
        if (isset($layout['characterOrder'])
            && 'right-to-left' === $layout['characterOrder']) {

            return true;
        }
        return false;
    }
}
