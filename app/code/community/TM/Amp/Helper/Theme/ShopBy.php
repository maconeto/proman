<?php

class TM_Amp_Helper_Theme_ShopBy extends Mage_Core_Helper_Abstract
{
    /**
     * Ono big hack to predict if layered navigation block will be actually
     * rendered.
     *
     * @return boolean
     */
    public function canShowShopBy()
    {
        $block = $this->getLayeredNavigationBlock();
        if (!$block) {
            return false;
        }

        if (method_exists($block, 'canShowBlock') && !$block->canShowBlock()) {
            // catalog/layer_view
            return false;
        } elseif (method_exists($block, 'getCurrentChildCategories')) {
            // catalog/navigation
            $categories = $block->getCurrentChildCategories();
            $count = is_array($categories) ? count($categories) : $categories->count();
            if (!$count) {
                return false;
            }
        }

        $root = $this->getLayout()->getBlock('root');
        if (!$root) {
            return true; // can't predict, so will show the button
        }

        // Hack to hide "Shop by" button, if user changed page layout
        // in backend, and layered navigation will not be rendered.
        $layouts = array(
            'page/1column.phtml',
            'page/3columns.phtml',
            'page/2columns-left.phtml',
            'page/2columns-right.phtml',
        );

        if (!in_array($root->getTemplate(), $layouts)) {
            return true; // we don't know nothing about current template
        }

        if (strpos($root->getTemplate(), '3columns') !== false) {
            return true;
        }

        $parent = $block->getParentBlock();
        if (!$parent) {
            return false;
        }

        $parentName = $parent->getNameInLayout();
        if ($parentName === 'content') {
            return true;
        }

        // compare template name with parent block name:
        // template: 2columns-left, block: left
        // template: 2columns-right, block: right
        return (strpos($root->getTemplate(), $parentName) !== false);
    }

    /**
     * Get active filters count
     *
     * @return array
     */
    public function getActiveFilters()
    {
        $block = $this->getLayeredNavigationBlock();
        if (!$block) {
            return array();
        }
        if (!$block->getLayer() || !$block->getLayer()->getState()) {
            return array();
        }
        return $block->getLayer()->getState()->getFilters();
    }

    /**
     * Get layered navigation block
     *
     * @return Mage_Catalog_Block_Layer_View
     */
    public function getLayeredNavigationBlock()
    {
        $block = $this->getLayout()->getBlock('catalog.leftnav');
        if (!$block) {
            $block = $this->getLayout()->getBlock('catalogsearch.leftnav');
        }
        return $block;
    }
}
