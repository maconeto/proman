<?php

class TM_Amp_Helper_Theme_Review extends Mage_Core_Helper_Abstract
{
    /**
     * Returns array of css classes to render star icons
     *
     * @param  int $summary Rating summary in percents
     * @return array
     */
    public function getSummaryIcons($summary)
    {
        $result = array();
        $stars = $summary / 20;
        $stars = round($stars * 2) / 2;

        $i = 1;
        do {
            if ($i <= $stars) {
                $result[] = 'icon-star';
            } elseif ($i < ($stars + 1)) {
                $result[] = 'icon-star-half';
            } else {
                $result[] = 'icon-star-outline';
            }
        } while ($i++ < 5);

        return $result;
    }
}
