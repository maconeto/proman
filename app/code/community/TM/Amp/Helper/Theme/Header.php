<?php

class TM_Amp_Helper_Theme_Header extends Mage_Core_Helper_Abstract
{
    /**
     * Get logo src for AMP theme
     *
     * Fallback rules:
     *  1. Get logo from AMP config
     *  2. Search for logo image in tmamp theme (by logo name from non-AMP theme)
     *  3. Get logo from original (non-AMP) theme
     *
     * @return string
     */
    public function getLogoSrc()
    {
        $src = Mage::getStoreConfig('tmamp/design/logo');
        if ($src) {
            return Mage::getBaseUrl('media') . 'tm/amp/' . $src;
        }

        // 1. Get logo src from header block or config
        $header = $this->getLayout()->getBlock('header');
        if ($header) {
            $src = $header->getLogoSrc();
        } else {
            $src = Mage::getDesign()->getSkinUrl(
                Mage::getStoreConfig('design/header/logo_src')
            );
        }

        // 2. If logo is from the base theme:
        //    - it does not copied into tmamp
        //    - we should load it from original theme skin
        if (strpos($src, '/base/default/') !== false) {
            if ($values = Mage::registry('tmamp_theme_values')) {
                $src = $values->getLogoSrc();
            }
        }
        return $src;
    }

    /**
     * Any theme can register 2x logo using tmamp_theme_values_prepare event.
     *
     * @return string|boolean
     */
    public function getLogo2xSrc()
    {
        if ($values = Mage::registry('tmamp_theme_values')) {
            return $values->getData('logo_2x_src');
        }
        return false;
    }
}
