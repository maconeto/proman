<?php

class TM_Amp_Helper_Theme_Product extends Mage_Core_Helper_Abstract
{
    /**
     * Detect if nl2br can be used.
     *
     * If text is html formatted - return false, otherwise - true.
     *
     * The logic is poor, but it's not critical, so leave it for now
     * until first issue.
     *
     * @param  string $text
     * @return bolean
     */
    public function canUseNl2Br($text)
    {
        if (strpos($text, '</') !== false) {
            return false;
        }
        return true;
    }

    /**
     * Detect if 'Add to Cart' can be used
     *
     * @param  Mage_Catalog_Model_Product $product
     * @return boolean
     */
    public function canShowAddToCart($product)
    {
        // Dependent options can't be supported with AMP
        if ($product->getTypeId() === Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            return false;
        }

        // Recurring with ability to set start date can't be added.
        // Pure html date input differs from magento POST action requirements
        if ($product->isRecurring()) {
            $profile = $product->getRecurringProfile();
            if (is_array($profile) && !empty($profile['start_date_is_editable'])) {
                return false;
            }
        }

        // Files are not supported by AMP yet
        $unsupportedOptionTypes = array(
            'file'
        );
        if ($product->getOptions()) {
            foreach ($product->getOptions() as $option) {
                if (in_array($option->getType(), $unsupportedOptionTypes)) {
                    return false;
                }
            }
        }

        if (!Mage::getStoreConfigFlag('tmamp/product_page/full_mode')) {
            $supportedTypes = Mage::getStoreConfig('tmamp/product_page/product_types');
            $supportedTypes = explode(',', $supportedTypes);
            if (!in_array($product->getTypeId(), $supportedTypes)) {
                return false;
            }
        }

        return true;
    }

    public function getImageWidth()
    {
        return Mage::getStoreConfig('tmamp/product_page/image_width');
    }

    public function getImageHeight()
    {
        return Mage::getStoreConfig('tmamp/product_page/image_height');
    }
}
