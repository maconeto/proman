<?php

class TM_Amp_Helper_Theme_Head extends Mage_Core_Helper_Abstract
{
    /**
     * Get Mage_Page_Block_Html_Head block
     *
     * @return Mage_Page_Block_Html_Head
     */
    public function getHead()
    {
        return $this->getLayout()->getBlock('head');
    }

    /**
     * Get favicon for AMP theme
     *
     * Fallback rules:
     *  1. Search for image in tmamp theme (by favicon name from non-AMP theme)
     *  2. Get icon from original (non-AMP) theme
     *
     * @return [type] [description]
     */
    public function getFaviconFile()
    {
        // 1. Get favicon from head block or config
        if ($head = $this->getHead()) {
            $src = $head->getFaviconFile();
        } elseif (Mage::getStoreConfig('design/head/shortcut_icon')) {
            $src = Mage::getBaseUrl('media')
                . Mage_Adminhtml_Model_System_Config_Backend_Image_Favicon::UPLOAD_DIR
                . '/'
                . Mage::getStoreConfig('design/head/shortcut_icon');
        } else {
            $src = Mage::getDesign()->getSkinUrl('favicon.ico');
        }

        // 2. If favicon is from the base theme:
        //    - it does not copied into tmamp
        //    - we should load it from original theme skin
        if (strpos($src, '/base/default/') !== false) {
            if ($values = Mage::registry('tmamp_theme_values')) {
                $src = $values->getFaviconFile();
            }
        }
        return $src;
    }

    /**
     * Retrieve canonical link for the current page
     *
     * 1. Get link from html head block
     * 2. Generate current page link manually
     *
     * @return [type] [description]
     */
    public function getCanonicalLink()
    {
        // search in head items
        foreach ($this->getHead()->getData('items') as $item) {
            if (!isset($item['type']) || !isset($item['name']) || empty($item['params'])) {
                continue;
            }
            if ('link_rel' === $item['type'] && (strpos($item['params'], 'canonical') !== false)) {
                // remove `amp=1`. @see TM_Amp_Model_Core_Url
                $url = $item['name'];
                $url = str_replace(array('amp=0', 'amp=1'), '', $url);
                $url = rtrim($url, '&?');
                return $url;
            }
        }

        // @todo: Maybe remove all query params?
        //  Problem: catalogsearch requires q and cat params
        return $this->_getUrl('*/*/*', array(
            '_current' => true,
            '_use_rewrite' => true,
            '_query' => array(
                'amp' => null
            )
        ));
    }

    /**
     * Copy from Mage_Page_Block_Html_Head:
     *     Magento merge canonical and rss links into css/js array.
     *     We need to get the links only.
     *
     * NOT USED CURRENTLY
     *
     * @param  array $items
     * @return string
     */
    public function getOtherHtmlHeadElements()
    {
        $lines = array();
        foreach ($this->getHead()->getData('items') as $item) {
            if (!isset($item['type']) || !isset($item['name'])) {
                continue;
            }
            $if     = !empty($item['if']) ? $item['if'] : '';
            $params = !empty($item['params']) ? ' ' . $item['params'] : '';
            switch ($item['type']) {
                case 'rss':
                    $lines[$if]['other'][] = sprintf(
                        '<link href="%s"%s rel="alternate" type="application/rss+xml" />',
                        $item['name'],
                        $params
                    );
                    break;
                case 'link_rel':
                    if (strpos($item['params'], 'canonical') !== false) {
                        // @see getCanonicalLink method
                        continue;
                    }

                    $lines[$if]['other'][] = sprintf(
                        '<link%s href="%s" />',
                        $params,
                        $item['name']
                    );
                    break;
            }
        }

        $html = '';
        foreach ($lines as $if => $items) {
            if (empty($items)) {
                continue;
            }
            if (!empty($if)) {
                // open !IE conditional using raw value
                if (strpos($if, "><!-->") !== false) {
                    $html .= $if . "\n";
                } else {
                    $html .= '<!--[if '.$if.']>' . "\n";
                }
            }

            if (!empty($items['other'])) {
                $html .= implode("\n", $items['other']) . "\n";
            }

            if (!empty($if)) {
                // close !IE conditional comments correctly
                if (strpos($if, "><!-->") !== false) {
                    $html .= '<!--<![endif]-->' . "\n";
                } else {
                    $html .= '<![endif]-->' . "\n";
                }
            }
        }
        return $html;
    }
}
