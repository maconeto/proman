<?php

/**
 * This class is not currently used because of different datetime format with
 * magento logic.
 */
class TM_Amp_Block_Core_Html_Date extends Mage_Core_Block_Html_Date
{
    protected function _toHtml()
    {
        if (!Mage::helper('tmamp')->canUseAmp()) {
            return parent::_toHtml();
        }

        $html  = '<input type="' . $this->getInputType() . '" name="' . $this->getName() . '" id="' . $this->getId() . '" ';
        $html .= 'value="' . $this->escapeHtml($this->getValue()) . '" class="' . $this->getClass() . '" ' . $this->getExtraParams() . '/> ';

        return $html;
    }

    public function getInputType()
    {
        return $this->getTime() ? 'datetime-local' : 'date';
    }
}
