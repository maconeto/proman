<?php

if (!@class_exists('\Leafo\ScssPhp\Compiler')) {
    require_once 'TM/Amp/scssphp/scss.inc.php';
}

class TM_Amp_Block_Scss extends Mage_Core_Block_Abstract
{
    protected $_items = array();

    /**
     * Initialize block's cache
     */
    protected function _construct()
    {
        parent::_construct();

        $this->addData(array(
            'cache_lifetime' => false
        ));
    }

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'TM_AMP_BLOCK_SCSS',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            implode(',', $this->getItems()),
        );
    }

    protected function _toHtml()
    {
        $scss = new \Leafo\ScssPhp\Compiler();
        $scss->setFormatter('Leafo\ScssPhp\Formatter\Crunched');
        $scss->setImportPaths($this->_getImportPaths());

        // 1. Main should be first to make all variables available in
        //      the third-party modules
        // 2. Do not use '@import main' to provide theme override feature.
        //      When '@import main' is used, relative search will be used
        //      for files inside main file.
        //      For example, you will not be able to redefine
        //      `abstract/_icons` file.
        $string = file_get_contents(
            Mage::getDesign()->getFilename('scss/main.scss', array('_type' => 'skin'))
        );
        if (Mage::helper('tmamp')->isRtl()) {
            $string = "@import \"abstracts/rtl\";\n" . $string;
        }

        $items = $this->getItems();
        array_push($items, 'theme', 'custom');
        foreach ($items as $item) {
            $string .= "@import \"{$item}\";\n";
        }

        try {
            $styles = $scss->compile($string);
        } catch (Exception $e) {
            $styles = '';
            Mage::logException($e);
        }
        return $styles;
    }

    public function addItem($name)
    {
        $this->_items[$name] = $name;
    }

    public function getItems()
    {
        return $this->_items;
    }

    protected function _getImportPaths()
    {
        $design = Mage::getDesign();
        $importPaths = array(
            $design->getSkinBaseDir() . '_custom',
            $design->getSkinBaseDir(),
        );
        if ('default' !== Mage::getDesign()->getTheme('skin')) {
            $default = $design->getSkinBaseDir(array(
                '_theme' => 'default'
            ));
            $importPaths[] = $default . '_custom';
            $importPaths[] = $default;
        }

        foreach ($importPaths as $key => $path) {
            $importPaths[$key] = $path . '/scss';
        }

        // third-party modules support
        $importPaths[] = $design->getSkinBaseDir(array(
            '_package' => 'base',
            '_theme'   => 'default',
        ));

        // custom fallback mechanism
        $importPaths[] = $design->getSkinBaseDir(array(
            '_theme' => 'default'
        )) . '/scss/fallback';

        return $importPaths;
    }
}
