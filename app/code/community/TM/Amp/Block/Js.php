<?php

class TM_Amp_Block_Js extends Mage_Core_Block_Abstract
{
    protected $_items = array();

    public function addItem($type, $src, $async = true)
    {
        $this->_items[$type] = array(
            'src'   => $src,
            'async' => $async
        );
    }

    public function getItems()
    {
        return $this->_items;
    }
}
