<?php

class TM_Amp_Model_Observer
{
    /**
     * Get tmamp.xml merged config
     *
     * @return TM_Amp_Model_Config
     */
    public function getConfig()
    {
        return Mage::getSingleton('tmamp/config');
    }

    public function registerDesktopThemeValues()
    {
        if (!Mage::helper('tmamp')->canUseAmp()) {
            return;
        }

        $design = Mage::getDesign();
        if (TM_Amp_Helper_Data::DESIGN_PACKAGE === $design->getPackageName()) {
            return;
        }

        $themeValues = new Varien_Object(array(
            'logo_2x_src' => false,
            'logo_src' => $design->getSkinUrl(
                Mage::getStoreConfig('design/header/logo_src')
            ),
            'logo_src_small' => $design->getSkinUrl(
                Mage::getStoreConfig('design/header/logo_src_small')
            ),
            'favicon_file' => $design->getSkinUrl('favicon.ico'),
        ));
        Mage::dispatchEvent(
            'tmamp_theme_values_prepare',
            array('theme_values' => $themeValues)
        );

        Mage::unregister('tmamp_theme_values');
        Mage::register('tmamp_theme_values', $themeValues);
    }

    /**
     * Called for the following events:
     *  - controller_action_predispatch
     *  - controller_action_layout_load_before
     *
     * This is made intentionally, to fix bugs with:
     *  - layout update xml caching
     *  - to override custom theme
     *
     * @return [type] [description]
     */
    public function activateTheme()
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp()) {
            return;
        }

        // layout cache fix. Must be called from controller_action_predispatch.
        // @see Mage_Core_Model_Layout_Update::getCacheId
        Mage::app()->getLayout()->getUpdate()->addHandle('tmamp');

        Mage::unregister('tmamp');
        Mage::register('tmamp', true);
        Mage::dispatchEvent('tmamp_activate_before');

        Mage::getSingleton('core/design_package')
            ->setPackageName($helper->getAmpPackage())
            ->setTheme($helper->getAmpTheme());
    }

    /**
     * Add additional layout handles with 'tmamp_' prefix for the third-party modules
     *
     * @param $observer
     */
    public function addFullAcionNameHandle($observer)
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp()) {
            return;
        }

        if (!$observer->getLayout() || !$observer->getAction()) {
            return;
        }

        foreach ($observer->getLayout()->getUpdate()->getHandles() as $handle) {
            if ('tmamp' === $handle) { // @see activateTheme
                continue;
            }
            $observer->getLayout()->getUpdate()->addHandle(
                'tmamp_' . $handle
            );
        }
    }

    /**
     * Apply AMP homepage dynamically
     */
    public function setAmpHomepage($observer)
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp()) {
            return;
        }

        $ampPage = Mage::getModel('cms/page')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load(Mage::getStoreConfig('tmamp/homepage/page_id'));
        if (!$ampPage->getId()) {
            return;
        }

        // AMP homepage support on direct urls. Example: example.com/home
        $controller = $observer->getControllerAction();
        if ($controller->getFullActionName() === 'cms_page_view') {
            $request = $controller->getRequest();
            $homepage = Mage::getModel('cms/page')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load(Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE));

            // replace id params with AMP homepage. @see Mage_Cms_PageController::viewAction
            foreach (array('page_id', 'id') as $key) {
                if ($request->getParam($key) === $homepage->getId()) {
                    $request->setParam($key, $ampPage->getId());
                }
            }
        }

        // replace homepage id with AMP homepage. @see Mage_Cms_IndexController::indexAction
        Mage::app()->getStore()->setConfig(
            Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE,
            $ampPage->getId()
        );
    }

    public function addAmphtmlLink()
    {
        /** @var Mage_Page_Block_Html_Head $head */
        $head = Mage::app()->getLayout()->getBlock('head');
        if (!$head) {
            return;
        }

        /** @var TM_Amp_Helper_Data $helper */
        $helper = Mage::helper('tmamp');
        if (!$helper->isAmpEnabled() || !$helper->isPageSupported()) {
            return;
        }

        /** @var TM_Amp_Helper_Url $urlHelper */
        $urlHelper = Mage::helper('tmamp/url');
        $head->addLinkRel('amphtml', $urlHelper->getAmpUrl());
    }

    /**
     * Remove all non-whitelisted updates
     *
     * Reasons:
     *  - we can't allow to show unknown blocks, because they will cause
     *      invalid markup;
     *  - disable all third-party modules updates, so we have more chances
     *      that standard blocks/templates will be used only;
     *
     * @param  [type] $observer [description]
     * @return [type]           [description]
     */
    public function prepareLayoutUpdates($observer)
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp()) {
            return;
        }

        $node = $this->getConfig()->getNode('whitelist/layout_updates');
        $whitelist = array();
        foreach ($node->asArray() as $nodeName => $files) {
            $whitelist = array_merge($whitelist, array_values($files));
        }

        $updatesRoot = $observer->getUpdates();
        foreach ($updatesRoot->asArray() as $nodeName => $values) {
            if (!isset($values['file']) || in_array($values['file'], $whitelist)) {
                continue;
            }
            unset($updatesRoot->{$nodeName});
        }
    }

    /**
     * Add scss partials, if needed
     *
     * @param $observer
     */
    public function addScssPartials($observer)
    {
        if (!Mage::helper('tmamp')->canUseAmp()) {
            return;
        }

        $html = trim($observer->getTransport()->getHtml());
        if (empty($html)) {
            return;
        }

        $block = $observer->getBlock();
        $head = $block->getLayout() ? $block->getLayout()->getBlock('head') : false;
        if (!$head || !($scssHead = $head->getChild('tmamp.styles'))) {
            return;
        }

        $rules = $this->getConfig()->getNode('includes/blocks');
        foreach ($rules->asArray() as $instance => $includes) {
            if (!isset($includes['styles']) || !($block instanceof $instance)) {
                continue;
            }

            foreach ($includes['styles'] as $key => $file) {
                $scssHead->addItem($file);
            }
        }
    }

    /**
     * Add amp-element scripts, if needed
     *
     * @param $observer
     */
    public function addAmpComponents($observer)
    {
        if (!Mage::helper('tmamp')->canUseAmp()) {
            return;
        }

        $html = trim($observer->getTransport()->getHtml());
        if (empty($html)) {
            return;
        }

        $block = $observer->getBlock();
        $head = $block->getLayout() ? $block->getLayout()->getBlock('head') : false;
        if (!$head || !($jsHead = $head->getChild('tmamp.scripts'))) {
            return;
        }

        $rules = $this->getConfig()->getNode('includes/blocks');
        foreach ($rules->asArray() as $instance => $includes) {
            if (!isset($includes['scripts']) || !($block instanceof $instance)) {
                continue;
            }
            foreach ($includes['scripts'] as $key => $component) {
                $jsHead->addItem(
                    $component['custom-element'],
                    $component['src']
                );
            }
        }
    }

    /**
     * Supress non-whitelisted blocks
     *
     * @param  $observer
     * @return void
     */
    public function supressBlockOutput($observer)
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp()) {
            return;
        }

        $node = $this->getConfig()->getNode('whitelist/block_types');
        $whitelist = array();
        foreach ($node->asArray() as $nodeName => $blockTypes) {
            $whitelist = array_merge($whitelist, array_values($blockTypes));
        }

        $block = $observer->getBlock();

        // Mage_Catalog_Block_Product_View_Options_Type_Select fix.
        if (!$block->getType()) {
            $node = $this->getConfig()->getNode('whitelist/blocks');
            foreach ($node->asArray() as $instance => $values) {
                if ($block instanceof $instance) {
                    // do not supress output
                    return;
                }
            }
        }

        if (!in_array($block->getType(), $whitelist)) {
            $observer->getTransport()->setHtml('');
        }
    }

    /**
     * Wrap some blocks into amp-lightbox component.
     *
     * Lightbox script is always included, so no need to add it as amp component.
     *
     * @param  $observer
     * @return void
     */
    public function prepareLightboxes($observer)
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp()) {
            return;
        }

        $block = $observer->getBlock();
        $node = $this->getConfig()->getNode('lightbox/blocks');
        foreach ($node->asArray() as $instance => $lightboxId) {
            if (!($block instanceof $instance)) {
                continue;
            }
            $html = $observer->getTransport()->getHtml();
            $observer->getTransport()->setHtml(
                '<amp-lightbox id="' . $lightboxId . '" layout="nodisplay" scrollable>'
                    . '<div class="lightbox">'
                        . $html
                        . '<button class="close close-icon" on="tap:' . $lightboxId . '.close"></button>'
                    . '</div>'
                . '</div>'
            );
        }
    }

    public function cleanupHtml($observer)
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp()) {
            return;
        }

        $controller = $observer->getControllerAction();
        if ($controller->getRequest()->getHeader('accept') === 'application/json') {
            // fetch request (Example: add to cart action)
            return;
        }

        $output = $controller->getResponse()->getBody();
        if (empty($output)) {
            return;
        }

        // if (substr($output, 0, 2) !== '<!') {
        //     return;
        // }

        $output = Mage::getModel('tmamp/html_filter')->process($output);
        $controller->getResponse()->setBody($output);
    }

    public function checkoutCartAddAction($observer)
    {
        $request = $observer->getControllerAction()->getRequest();
        if (!$request->isPost() || !$request->getQuery('amp')) {
            return;
        }

        $redirectTo = false;
        $response = $observer->getControllerAction()->getResponse();
        foreach ($response->getHeaders() as $header) {
            if ($header['name'] === 'Location') {
                $redirectTo = $header['value'];
                break;
            }
        }
        if (!$redirectTo) {
            $redirectTo = Mage::helper('checkout/url')->getCartUrl();
        }
        $redirectTo = str_replace('http://', 'https://', $redirectTo);

        $result = array(
            'success'  => true,
            'messages' => array()
        );

        $messageHelper = Mage::helper('tmamp/message');
        if ($messageHelper->hasFailureMessages()) {
            $result['success']  = false;
            $result['messages'] = $messageHelper->getMessages(true, true, true);
        } else {
            $response->setHeader('AMP-Redirect-To', $redirectTo);
            $result['messages'] = $messageHelper->getMessages(false, true, true);
        }

        $response
            ->clearHeader('Location')
            ->setHttpResponseCode($result['success'] ? 200 : 400)
            ->setHeader('Content-Type', 'application/json')
            // https://github.com/ampproject/amphtml/blob/master/spec/amp-cors-requests.md#ensuring-secure-responses
            ->setHeader('Access-Control-Allow-Origin', Mage::helper('tmamp')->getAmpCacheDomainName())
            ->setHeader('Access-Control-Allow-Credentials', 'true')
            ->setHeader(
                'Access-Control-Expose-Headers',
                'AMP-Access-Control-Allow-Source-Origin,AMP-Redirect-To'
            )
            ->setHeader(
                'Amp-Access-Control-Allow-Source-Origin',
                $request->getScheme() . '://' . $request->getHttpHost()
            )
            ->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Init layout messages on compare page
     *
     * @return void
     */
    public function initLayoutMessages()
    {
        $block = Mage::app()->getLayout()->getMessagesBlock();
        if (!$block) {
            return;
        }

        $storages = Mage::helper('tmamp/message')->getStorages(array(
            'checkout/session',
            'catalog/session'
        ));
        foreach ($storages as $storageName => $storage) {
            $block->addMessages($storage->getMessages(true));
            $block->setEscapeMessageFlag($storage->getEscapeMessages(true));
            $block->addStorageType($storageName);
        }
    }

    /**
     * Validate php version
     *
     * @param  [type] $observer [description]
     * @return [type]           [description]
     */
    public function onBeforeConfigView($observer)
    {
        $section = $observer->getControllerAction()->getRequest()->getParam('section');
        if ($section !== 'tmamp') {
            return;
        }

        if (!Mage::helper('tmamp')->isPhpVersionSupported()) {
            Mage::getSingleton('adminhtml/session')->addError(
                'Whoops, it looks like you have an invalid PHP version. AMP supports PHP 5.4.0 or newer.'
            );
        }
        if (!Mage::helper('tmamp/libxml')->isLibxmlVersionSupported()) {
            Mage::getSingleton('adminhtml/session')->addNotice(
                'Libxml library is too old. AMP will work on the old one but we recommend to use 2.8.0 or newer to guarantee correct amp-html output.'
            );
        }
    }

    /**
     * Disable TM_Lazyload, TM_Deferjs (scripts), TM_Botprotection (hidden action)
     * functionality
     *
     * @param  object $observer
     * @return void
     */
    public function disableModule($observer)
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp()) {
            return;
        }
        $observer->getConfig()->setIsEnabled(false);
    }
}
