<?php

class TM_Amp_Model_Config extends Varien_Simplexml_Config
{
    /**
     * Constructor
     *
     * @see Varien_Simplexml_Config
     */
    public function __construct($sourceData=null)
    {
        $this->setCacheId('tmamp_config');
        $this->setCacheTags(array(Mage_Core_Model_Config::CACHE_TAG));
        $this->setCacheChecksum(null);

        parent::__construct($sourceData);
        $this->_construct();
    }

    /**
     * Init configuration for webservices api
     *
     * @return Mage_Api_Model_Config
     */
    protected function _construct()
    {
        if (Mage::app()->useCache('config')) {
            if ($this->loadCache()) {
                return $this;
            }
        }

        $config = Mage::getConfig()->loadModulesConfiguration('tmamp.xml');

        /**
         * @deprecated after 1.1.0
         * support backwards compatibility with config.xml
         */
        $oldConfig = Mage::getConfig()->getNode('tmamp');
        if ($oldConfig) {
            $config->getNode()->extendChild($oldConfig, true);
        }

        $this->setXml($config->getNode('tmamp'));

        if (Mage::app()->useCache('config')) {
            $this->saveCache();
        }
        return $this;
    }

    /**
     * Retrieve cache object
     *
     * @return Zend_Cache_Frontend_File
     */
    public function getCache()
    {
        return Mage::app()->getCache();
    }

    protected function _loadCache($id)
    {
        return Mage::app()->loadCache($id);
    }

    protected function _saveCache($data, $id, $tags=array(), $lifetime=false)
    {
        return Mage::app()->saveCache($data, $id, $tags, $lifetime);
    }

    protected function _removeCache($id)
    {
        return Mage::app()->removeCache($id);
    }
}
