<?php

class TM_Amp_Model_Core_Url extends TM_Amp_Model_Core_Url_Abstract
{
    /**
     * Overriden to add `amp=1` query to all urls, if:
     *  - we are currently on AMP site (with force or without it)
     *  - persitent_browsing is enabled
     *
     * @param  string|null $routePath
     * @param  array|null $routeParams
     * @return string
     */
    public function getUrl($routePath = null, $routeParams = null)
    {
        if ($this->_canAddAmpParameter($routePath)) {
            if (null === $routeParams) {
                $routeParams = array();
            }
            if (!isset($routeParams['_query']) || !$routeParams['_query']) {
                $routeParams['_query'] = array();
            }

            // do not use isset here, to allow to set the NULL
            if (!array_key_exists('amp', $routeParams['_query'])) {
                $routeParams['_query']['amp'] = 1;
            }

            if ($routeParams['_query']['amp'] &&
                Mage::getStoreConfigFlag('tmamp/debug/enabled')) {

                $routeParams['_fragment'] = 'development=1';
            }
        }

        return parent::getUrl($routePath, $routeParams);
    }

    /**
     * Check if `amp=1` parameter should be added
     *
     * @return boolean
     */
    protected function _canAddAmpParameter($routePath = null)
    {
        $helper = Mage::helper('tmamp');
        if (!$helper->canUseAmp() || !$helper->isPersistentBrowsingEnabled()) {
            return false;
        }

        // @todo: add blacklist? (third-party modules will be with amp=1, because we can't detect them)

        return true;
    }
}
