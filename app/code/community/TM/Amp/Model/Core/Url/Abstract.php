<?php

if (Mage::helper('core')->isModuleOutputEnabled('Mana_Seo')) {
    Mage::helper('tmcore')->requireOnce('TM/Amp/Model/Core/Url/ManaSeo.php');
} else {
    class TM_Amp_Model_Core_Url_Abstract extends Mage_Core_Model_Url {}
}
