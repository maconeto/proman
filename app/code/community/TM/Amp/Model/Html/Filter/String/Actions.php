<?php

class TM_Amp_Model_Html_Filter_String_Actions extends TM_Amp_Model_Html_Filter_Abstract
{
    /**
     * Replace some urls to skip form_key validation
     *
     * @param  string $html
     * @return string
     */
    public function process($html)
    {
        return str_replace(
            array(
                'checkout/cart/add',
                'catalog/product_compare/add',
            ),
            array(
                'tmamp/cart/add',
                'tmamp/product_compare/add',
            ),
            $html
        );
    }
}
