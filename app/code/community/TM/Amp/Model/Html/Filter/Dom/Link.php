<?php

class TM_Amp_Model_Html_Filter_Dom_Link extends TM_Amp_Model_Html_Filter_Dom_Abstract
{
    /**
     * Remove link tags without rel attribute
     *
     * @param  DOMDocument $document
     * @return void
     */
    public function process($document)
    {
        $remove = array();
        $nodes = $document->getElementsByTagName('link');
        foreach ($nodes as $node) {
            if ($node->hasAttribute('rel') && $node->getAttribute('rel')) {
                continue;
            }
            $remove[] = $node;
        }

        foreach ($remove as $node) {
            $node->parentNode->removeChild($node);
        }
    }
}
