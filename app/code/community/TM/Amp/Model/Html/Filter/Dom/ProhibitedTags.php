<?php

class TM_Amp_Model_Html_Filter_Dom_ProhibitedTags extends TM_Amp_Model_Html_Filter_Dom_Abstract
{
    /**
     * Removes prohited tags from the DOMDocument
     * @see https://github.com/ampproject/amphtml/blob/master/spec/amp-html-format.md#html-tags
     *
     * @param  DOMDocument $document
     * @return void
     */
    public function process($document)
    {
        $tags = array(
            'base',
            'frame',
            'frameset',
            'object',
            'param',
            'applet',
            'embed',
            'amasty_seo',
        );
        foreach ($tags as $tag) {
            $remove = array();
            $nodes = $document->getElementsByTagName($tag);
            foreach ($nodes as $node) {
                $remove[] = $node;
            }
            foreach ($remove as $node) {
                $node->parentNode->removeChild($node);
            }
        }
    }
}
