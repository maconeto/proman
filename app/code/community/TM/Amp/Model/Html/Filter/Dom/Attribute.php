<?php

class TM_Amp_Model_Html_Filter_Dom_Attribute extends TM_Amp_Model_Html_Filter_Dom_Abstract
{
    /**
     * 1. Removes js attributes from the DOMDocument nodes
     * 2. Add `data-amp-` attributes
     *
     * @param  DOMDocument $document
     * @return void
     */
    public function process($document)
    {
        $blacklist = array(
            'nowrap',
            'style',
            'price',
            'border',
            'noshade',
        );
        // whitelisted attributes for specific tags only
        $whitelist = array(
            'align' => array(
                'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
                'p', 'blockquote', 'div',
                'table', 'tr', 'td', 'th'
            ),
            'border' => array('table', 'a', 'img'),
            'size' => array('input', 'select'),
        );

        $nodes = $document->getElementsByTagName('*');
        foreach ($nodes as $node) {
            // 1. Remove blacklisted attributes
            $remove = array();
            foreach ($node->attributes as $attributeName => $attribute) {
                if (strlen($attributeName) > 2
                    && substr($attributeName, 0, 2) === 'on') {

                    $remove[] = $attributeName;
                }

                // remove inline width
                if ($attributeName === 'width'
                    && strpos($node->nodeName, 'amp-') === false
                    && !in_array($node->nodeName, array('audio', 'iframe', 'img', 'video'))) {

                    $remove[] = $attributeName;
                }

                if (isset($whitelist[$attributeName])) {
                    if (!in_array($node->nodeName, $whitelist[$attributeName])) {
                        $remove[] = $attributeName;
                    }
                }

                if (in_array($attributeName, $blacklist)) {
                    $remove[] = $attributeName;
                }
            }
            foreach ($remove as $attributeName) {
                $node->removeAttribute($attributeName);
            }

            // 2. Convert `data-tmamp-` attributes
            foreach ($this->getAmpAttributes($node) as $key => $value) {
                switch ($key) {
                    case 'class-append':
                        $oldClass = $node->getAttribute('class');
                        $node->setAttribute('class', $oldClass . ' ' . $value);
                        break;
                    default:
                        $node->setAttribute($key, $value);
                }
            }
        }
    }
}
