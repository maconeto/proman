<?php

class TM_Amp_Model_Html_Filter_String_Nocookie extends TM_Amp_Model_Html_Filter_Abstract
{
    /**
     * Add nocookie=1 to some actions
     *
     * @param  string $html
     * @return string
     */
    public function process($html)
    {
        return preg_replace(
            "/(wishlist\/index\/add.*?\?amp=1)/",
            "$1&nocookie=1",
            $html
        );
    }
}
