<?php

class TM_Amp_Model_Html_Filter_Dom_Form extends TM_Amp_Model_Html_Filter_Dom_Abstract
{
    /**
     * Before adding some action, follow the list below:
     *
     *  1. Request MUST return AMP-Access-Control-Allow-Source-Origin header
     *  2. Request MUST return json data
     *  3. Form MUST implement "on" attribute to control returned json

     * @return array
     */
    protected function _getSupportedActions()
    {
        return array(
            'checkout/cart/add',
            'catalogsearch',
        );
    }

    /**
     * Check if the form node can be rendered
     *
     * @param  DomElement $node
     * @return boolean
     */
    protected function _canUse($node)
    {
        foreach ($this->_getSupportedActions() as $action) {
            if (false !== strpos($node->getAttribute('action'), $action)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Prepare form attributes to match amp requirements
     *
     * @param  DOMDocument $document
     * @return void
     */
    public function process($document)
    {
        $remove = array();
        $nodes = $document->getElementsByTagName('form');
        $xpath = new DOMXPath($document);
        foreach ($nodes as $node) {
            if ($node->hasAttribute('action-xhr')) {
                continue;
            }

            if (!$this->_canUse($node)) {
                $remove[] = $node;
                continue;
            }

            $submit = $xpath->query('.//*[@type="submit"]', $node);
            if (!$submit->length) {
                // if form has one button only - transform it to submit
                $button = $xpath->query('.//button[@type="button" and not(@on)]', $node)->item(0);
                if ($button) {
                    $button->setAttribute('type', 'submit');
                } else {
                    // Out of stock items does not have submit button
                }
            }

            $this->_prepareActionAttribute($node);

            if (!$node->hasAttribute('target')) {
                $node->setAttribute('target', '_top');
            }

            // add success/error handling
            $method = strtolower($node->getAttribute('method'));
            if ('post' === $method) {
                $this->_prepareResponseRendering($node, $document);

                // fix to allow to use form from google cache for new visitors
                $hiddenInput = $document->createElement('input');
                $hiddenInput->setAttribute('type', 'hidden');
                $hiddenInput->setAttribute('name', 'nocookie');
                $hiddenInput->setAttribute('value', '1');
                $node->appendChild($hiddenInput);
            } else {
                // provide stateful AMP browsing
                $hiddenInput = $document->createElement('input');
                $hiddenInput->setAttribute('type', 'hidden');
                $hiddenInput->setAttribute('name', 'amp');
                $hiddenInput->setAttribute('value', 1);
                $node->appendChild($hiddenInput);
            }
        }

        foreach ($remove as $node) {
            $node->parentNode->removeChild($node);
        }

        if (count($nodes) > count($remove)) {
            $this->addAmpComponent(
                'amp-form',
                'https://cdn.ampproject.org/v0/amp-form-0.1.js'
            );
        }
    }

    /**
     * 1. Remove http protocol (https is allowed only)
     * 2. Replace action with xhr-action if needed
     * 3. Add amp parameter to the query
     *
     * @param  DOMElement $node
     * @return void
     */
    protected function _prepareActionAttribute($node)
    {
        $actionAttribute = 'action';
        $action = $node->getAttribute($actionAttribute);
        $action = str_replace('http://', '//', $action);
        $method = strtolower($node->getAttribute('method'));

        // provide stateful AMP browsing
        if ('post' === $method && false === strpos($action, 'amp=1')) {
            if (false === strpos($action, '?')) {
                $action .= '?amp=1';
            } else {
                $action .= '&amp=1';
            }
        }

        if ('post' === $method) {
            $node->removeAttribute($actionAttribute);
            $actionAttribute = 'action-xhr';
        }
        $node->setAttribute($actionAttribute, $action);
    }

    /**
     * Replace action with xhr-action. Add amp parameter to the query
     *
     * @param  DOMElement $node
     * @return void
     */
    protected function _prepareResponseRendering($node, $document)
    {
        $xpath = new DOMXPath($document);

        $submitSuccess = $xpath->query('.//div[@submit-success]', $node);
        if (!$submitSuccess->length) {
            $wrapper = $document->createElement('div');
            $wrapper->setAttribute('class', 'form-response-message success');
            $wrapper->appendChild($document->createTextNode($this->_getSuccessTemplate()));

            $template = $document->createElement('template');
            $template->setAttribute('type', 'amp-mustache');
            $template->appendChild($wrapper);

            $submitSuccess = $document->createElement('div');
            $submitSuccess->setAttribute('submit-success', '');
            $submitSuccess->setAttribute('class', 'form-response');
            $submitSuccess->appendChild($template);

            // insert it next to submit button
            $submit = $xpath->query('.//*[@type="submit"]', $node)->item(0);
            if ($submit && $submit->parentNode->tagName === 'form') {
                $submit->parentNode->insertBefore($submitSuccess, $submit->nextSibling);
            } else {
                $node->appendChild($submitSuccess);
            }
        }

        $submitError = $xpath->query('.//div[@submit-error]', $node);
        if (!$submitError->length) {
            $wrapper = $document->createElement('div');
            $wrapper->setAttribute('class', 'form-response-message error');
            $wrapper->appendChild($document->createTextNode($this->_getErrorTemplate()));

            $template = $document->createElement('template');
            $template->setAttribute('type', 'amp-mustache');
            $template->appendChild($wrapper);

            $submitError = $document->createElement('div');
            $submitError->setAttribute('submit-error', '');
            $submitError->setAttribute('class', 'form-response');
            $submitError->appendChild($template);

            // insert it next to submit button
            $submit = $xpath->query('.//*[@type="submit"]', $node)->item(0);
            if ($submit && $submit->parentNode->tagName === 'form') {
                $submit->parentNode->insertBefore($submitError, $submit->nextSibling);
            } else {
                $node->appendChild($submitError);
            }
        }

        $this->addAmpComponent(
            'amp-mustache',
            'https://cdn.ampproject.org/v0/amp-mustache-0.1.js'
        );

        // lightbox can't render dynamic content, such as template

        // $template = $document->createElement('template');
        // $template->setAttribute('type', 'amp-mustache');
        // $template->appendChild($document->createTextNode($this->_getErrorTemplate()));

        // $lightboxContent = $document->createElement('div');
        // $lightboxContent->setAttribute('class', 'lightbox-content');
        // $lightboxContent->appendChild($template);

        // $lighbox = $document->createElement('amp-lightbox');
        // $lighbox->setAttribute('class', 'lightbox');
        // $id = $node->getAttribute('id') ? 'lightbox-' . $node->getAttribute('id') : 'lightbox-test';
        // $lighbox->setAttribute('id', $id);
        // $lighbox->setAttribute('layout', 'nodisplay');
        // $lighbox->appendChild($lightboxContent);

        // $node->appendChild($lighbox);
        // $node->setAttribute('on', 'submit-error:'.$id);
    }

    /**
     * Returns mustache template for success messages
     *
     * @return string
     */
    protected function _getSuccessTemplate()
    {
        return "{{#messages.success}}\n{{{.}}}\n{{/messages.success}}\n\n";
    }

    /**
     * Returns mustache template for error messages
     *
     * @return string
     */
    protected function _getErrorTemplate()
    {
        return "{{#messages.error}}\n{{{.}}}\n{{/messages.error}}\n\n";
    }
}
