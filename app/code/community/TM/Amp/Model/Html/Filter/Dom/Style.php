<?php

class TM_Amp_Model_Html_Filter_Dom_Style extends TM_Amp_Model_Html_Filter_Dom_Abstract
{
    /**
     * Removes <style> tags from the DOMDocument
     *
     * @param  DOMDocument $document
     * @return void
     */
    public function process($document)
    {
        $remove = array();
        $nodes = $document->getElementsByTagName('style');
        foreach ($nodes as $node) {
            // allow amp styles
            if ($node->hasAttribute('amp-boilerplate')
                || $node->hasAttribute('amp-custom')) {

                continue;
            }

            $remove[] = $node;
        }
        foreach ($remove as $node) {
            $node->parentNode->removeChild($node);
        }
    }
}
