<?php

class TM_Amp_Model_Html_Filter_String_Libxml extends TM_Amp_Model_Html_Filter_Abstract
{
    /**
     * libxml < 2.8 fix:
     *     Move <noscript> tag back into head section.
     *
     * @param  string $html
     * @return string
     */
    public function process($html)
    {
        if (Mage::helper('tmamp/libxml')->isLibxmlVersionSupported()) {
            return $html;
        }

        $find = '</head><body><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>';
        $replace = '<noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript></head><body>';

        return str_replace($find, $replace, $html);
    }
}
