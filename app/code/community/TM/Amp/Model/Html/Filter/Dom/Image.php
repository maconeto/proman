<?php

class TM_Amp_Model_Html_Filter_Dom_Image extends TM_Amp_Model_Html_Filter_Dom_Abstract
{
    /**
     * 1. Replace unsupported img tags with amp-img
     * 2. Remove images without required height attribute
     *
     * @param  DOMDocument $document
     * @return void
     */
    public function process($document)
    {
        $replace = array();
        $nodes = $document->getElementsByTagName('img');
        foreach ($nodes as $node) {
            $replace[] = $node;
        }

        foreach ($replace as $node) {
            if (!$src = $this->_getSrc($node)) {
                $node->parentNode->removeChild($node);
                continue;
            }

            // detect image dimensions in a runtime, if needed
            foreach (array('width', 'height') as $attribute) {
                if ($node->getAttribute($attribute)) {
                    continue;
                }

                $method = 'get' . ucfirst($attribute);
                $value  = Mage::helper('tmamp/image')->{$method}($src);

                if (!$value) {
                    $node->parentNode->removeChild($node);
                    continue 2;
                }
                $node->setAttribute($attribute, $value);
            }

            $img = $document->createElement('amp-img');
            $img->setAttribute('layout', $this->_detectAmpLayout($node));
            $img->setAttribute('src', $src);
            if ($srcset = $this->_getSrcset($node)) {
                $img->setAttribute('srcset', $srcset);
            }
            foreach ($this->getNodeAttributes($node) as $key => $value) {
                if (in_array($key, array('src', 'srcset'))) {
                    continue;
                }
                $img->setAttribute($key, $value);
            }
            $node->parentNode->replaceChild($img, $node);
        }
    }

    /**
     * Detects layout to use, if data-amp-layout is not specified directly.
     *
     * @param  DomElement $node
     * @return string
     */
    protected function _detectAmpLayout($node)
    {
        $layout = 'responsive';
        if ($node->getAttribute('width') < 200 ||
            $node->getAttribute('height') < 50) {

            $layout = 'fixed';
        }
        return $layout;
    }

    /**
     * Get image src value
     *
     * @param  NodeElement $node
     * @return mixed
     */
    protected function _getSrc($node)
    {
        foreach ($this->_getSrcAttributes() as $name) {
            if ($src = $node->getAttribute($name)) {
                return $src;
            }
        }
        return false;
    }

    /**
     * Get image srcset value
     *
     * @param  NodeElement $node
     * @return mixed
     */
    protected function _getSrcset($node)
    {
        foreach (array('srcset', 'data-srcset') as $name) {
            if ($srcset = $node->getAttribute($name)) {
                return $srcset;
            }
        }
        return false;
    }

    /**
     * Get popuplar names for src attribute
     *
     * @return array
     */
    protected function _getSrcAttributes()
    {
        return array(
            'src',
            'data-src',
            'data-lazy',
        );
    }
}
