<?php

class TM_Amp_Model_Html_Filter_Dom_Script extends TM_Amp_Model_Html_Filter_Dom_Abstract
{
    /**
     * Removes <script> tags from the DOMDocument
     *
     * @param  DOMDocument $document
     * @return void
     */
    public function process($document)
    {
        $remove = array();
        $nodes = $document->getElementsByTagName('script');
        foreach ($nodes as $node) {
            // allow amp scripts
            if ($node->hasAttribute('custom-element')
                || strpos($node->getAttribute('src'), 'ampproject') !== false
                || $node->getAttribute('type') === 'application/ld+json'
                || $node->getAttribute('type') === 'application/json') {

                continue;
            }

            $remove[] = $node;
        }
        foreach ($remove as $node) {
            $node->parentNode->removeChild($node);
        }
    }
}
