<?php

abstract class TM_Amp_Model_Html_Filter_Abstract
{
    /**
     * Retrieve layout model
     *
     * @return Mage_Core_Model_Layout
     */
    public function getLayout()
    {
        return Mage::app()->getLayout();
    }
}
