<?php

abstract class TM_Amp_Model_Html_Filter_Dom_Abstract extends TM_Amp_Model_Html_Filter_Abstract
{
    /**
     * Extract non-`data-tmamp-` attributes from node element
     *
     * @param  [type] $node [description]
     * @return [type]       [description]
     */
    public function getNodeAttributes($node)
    {
        $result = array();
        foreach ($this->getAllAttributes($node) as $name => $value) {
            if (strpos($name, 'data-tmamp-') === 0) {
                continue;
            }
            $result[$name] = $value;
        }
        return $result;
    }

    /**
     * Extract `data-tmamp-` attributes from node element
     *
     * @param  [type] $node [description]
     * @return [type]       [description]
     */
    public function getAmpAttributes($node)
    {
        $result = array();
        foreach ($this->getAllAttributes($node) as $name => $value) {
            if (strpos($name, 'data-tmamp-') !== 0) {
                continue;
            }
            $name = str_replace('data-tmamp-', '', $name);
            $result[$name] = $value;
        }
        return $result;
    }

    /**
     * Get all node attributes as key => value pairs
     *
     * @param  [type] $node [description]
     * @return [type]       [description]
     */
    public function getAllAttributes($node)
    {
        $result = array();
        foreach ($node->attributes as $attribute) {
            $attributeName = $this->_getAttributeName($attribute->name);
            if (!$attributeName) {
                continue;
            }
            $result[$attributeName] = $attribute->value;
        }
        return $result;
    }

    /**
     * Use this mathod to handle attribute name mapping.
     *
     * Example:
     *     data-desktop-attribute => amp-attribute
     *     dektop-attribute       => false // - will be removed in AMP
     *
     * @param  string $name
     * @return string
     */
    protected function _getAttributeName($name)
    {
        return $name;
    }

    /**
     * Add amp component script intotmamp.scrypt block
     *
     * @param string  $type  amp-element
     * @param string  $src   js src
     * @param boolean $async async
     */
    public function addAmpComponent($type, $src, $async = true)
    {
        if ($this->getScriptsBlock()) {
            $this->getScriptsBlock()->addItem($type, $src, $async);
        }
    }

    /**
     * Retreive tmamp.scripts block from layout
     *
     * @return TM_Amp_Block_Js
     */
    public function getScriptsBlock()
    {
        return $this->getLayout()->getBlock('tmamp.scripts');
    }

    /**
     * Add scss into tmamp.styles block
     *
     * @param string  $type  amp-element
     * @param string  $src   js src
     * @param boolean $async async
     */
    public function addAmpStyle($src)
    {
        if ($this->getStylesBlock()) {
            $this->getStylesBlock()->addItem($src);
        }
    }

    /**
     * Retreive tmamp.styles block from layout
     *
     * @return TM_Amp_Block_Scss
     */
    public function getStylesBlock()
    {
        return $this->getLayout()->getBlock('tmamp.styles');
    }
}
