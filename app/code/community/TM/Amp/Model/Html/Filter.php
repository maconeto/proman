<?php

class TM_Amp_Model_Html_Filter
{
    /**
     * Replace/Add/Modify DomDocument Nodes with these filters
     *
     * @return array
     */
    public function getDomFilters()
    {
        return array(
            'tmamp/html_filter_dom_script',
            'tmamp/html_filter_dom_style',
            'tmamp/html_filter_dom_prohibitedTags',
            'tmamp/html_filter_dom_button', // must be invoked before 'tmamp/html_filter_dom_attribute'
            'tmamp/html_filter_dom_attribute', // must be before img and others as it could change src and other attributes
            'tmamp/html_filter_dom_image',
            'tmamp/html_filter_dom_iframe',
            'tmamp/html_filter_dom_video',
            'tmamp/html_filter_dom_audio',
            'tmamp/html_filter_dom_form',
            'tmamp/html_filter_dom_table',
            'tmamp/html_filter_dom_link',
        );
    }

    /**
     * Some jobs are easier to do with plain str_replace function.
     * Use this filters for such kind of job.
     *
     * @return array
     */
    public function getStringFilters()
    {
        return array(
            'tmamp/html_filter_string_actions',
            'tmamp/html_filter_string_libxml',
            'tmamp/html_filter_string_nocookie',
        );
    }

    /**
     * Prepare AMP-compatible html markup and add AMP components
     *
     * @param  string $html Rendered html
     * @return string       Processed html
     */
    public function process($html)
    {
        if (function_exists('mb_convert_encoding')) {
            $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        }

        libxml_use_internal_errors(true);
        $document = new DOMDocument();
        $document->loadHTML($html);

        $filters = $this->getDomFilters();
        // these filters must be at the bottom
        $filters[] = 'tmamp/html_filter_dom_ampscripts';
        $filters[] = 'tmamp/html_filter_dom_ampstyles';
        foreach ($filters as $type) {
            Mage::getModel($type)->process($document);
        }

        $output = $document->saveHTML();

        $filters = $this->getStringFilters();
        foreach ($filters as $type) {
            $output = Mage::getModel($type)->process($output);
        }

        return $output;
    }
}
