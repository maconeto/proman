<?php

class TM_Amp_Model_System_Config_Source_Pages
{
    public function toOptionArray()
    {
        $object = new Varien_Object(array(
            'cms_index_index'            => Mage::helper('tmamp')->__('Homepage'),
            'catalog_product_view'       => Mage::helper('tmamp')->__('Product Page'),
            'catalog_category_view'      => Mage::helper('tmamp')->__('Category Page'),
            'catalogsearch_result_index' => Mage::helper('tmamp')->__('Catalog Search'),
            'cms_page_view'              => Mage::helper('tmamp')->__('Cms Pages'),
            'wordpress'                  => Mage::helper('tmamp')->__('Fishpig Wordpress Pages (Beta)'),
        ));

        Mage::dispatchEvent('tmamp_prepare_pages_config', array(
            'pages' => $object
        ));

        $result = array();
        foreach ($object->getData() as $value => $label) {
            $result[] = array(
                'value' => $value,
                'label' => $label
            );
        }
        return $result;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $result = array();
        foreach ($this->toOptionArray() as $option) {
            $result[$option['value']] = $option['label'];
        }
        return $result;
    }
}
