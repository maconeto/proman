<?php

class TM_Amp_Model_System_Config_Source_Devices
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'mobile', 'label' => Mage::helper('tmamp')->__('Mobile')),
            array('value' => 'tablet', 'label' => Mage::helper('tmamp')->__('Tablet'))
        );
    }
}
