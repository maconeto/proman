<?php

class TM_Amp_Model_System_Config_Source_Producttypes
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = Mage_Catalog_Model_Product_Type::getOptions();
        foreach ($options as $key => $option) {
            if ($option['value'] === Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
                unset($options[$key]);
                break;
            }
        }
        return $options;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $result = array();
        foreach ($this->toOptionArray() as $item) {
            $result[$item['value']] = $item['label'];
        }
        return $result;
    }
}
