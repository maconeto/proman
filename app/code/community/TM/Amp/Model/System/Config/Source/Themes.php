<?php

class TM_Amp_Model_System_Config_Source_Themes
{
    public function toOptionArray()
    {
        $result = array();
        $themes = $this->getAmpThemes();
        foreach ($themes as $theme) {
            $result[$theme] = array(
                'value' => $theme,
                'label' => ucfirst($theme)
            );
        }
        return array_values($result);
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $result = array();
        foreach ($this->toOptionArray() as $option) {
            $result[$option['value']] = $option['label'];
        }
        return $result;
    }

    /**
     * Get skin and template directory names
     *
     * @return array
     */
    public function getAmpThemes()
    {
        // themes
        $themes = Mage::getModel('core/design_package')
            ->getThemeList(TM_Amp_Helper_Data::DESIGN_PACKAGE);

        // skins
        $skins = array();
        $path = Mage::getBaseDir('skin') . DS . 'frontend' . DS . TM_Amp_Helper_Data::DESIGN_PACKAGE;
        $dir = opendir($path);
        if ($dir) {
            while ($entry = readdir($dir)) {
                if (substr($entry, 0, 1) == '.' || !is_dir($path . DS . $entry)){
                    continue;
                }
                $skins[] = $entry;
            }
            unset($entry);
            closedir($dir);
        }

        $themes += $skins;

        return $themes;
    }
}
