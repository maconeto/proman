<?php

require_once 'Mage/Checkout/controllers/CartController.php';

class TM_Amp_CartController extends Mage_Checkout_CartController
{
    protected function _validateFormKey()
    {
        if (Mage::helper('tmamp')->canSkipFormKeyValidation()) {
            return true;
        }

        return parent::_validateFormKey();
    }
}
