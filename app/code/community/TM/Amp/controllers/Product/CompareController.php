<?php

require_once 'Mage/Catalog/controllers/Product/CompareController.php';

class TM_Amp_Product_CompareController extends Mage_Catalog_Product_CompareController
{
    protected function _validateFormKey()
    {
        if (Mage::helper('tmamp')->canSkipFormKeyValidation()) {
            return true;
        }

        return parent::_validateFormKey();
    }
}
