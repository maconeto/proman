<?php

class TM_Amp_CookieController extends Mage_Core_Controller_Front_Action
{
    public function allowAction()
    {
        $helper = Mage::helper('core/cookie');

        Mage::getSingleton('core/cookie')->set(
            Mage_Core_Helper_Cookie::IS_USER_ALLOWED_SAVE_COOKIE,
            $helper->getAcceptedSaveCookiesWebsiteIds(),
            $helper->getCookieRestrictionLifetime()
        );

        $this->_sendAmpResponse(200);
    }

    public function shouldShowNotificationAction()
    {
        $this->_sendAmpResponse(200, array(
            'showNotification' => Mage::helper('core/cookie')->isUserNotAllowSaveCookie()
        ));
    }

    /**
     * Send response with amp headers
     *
     * @param  integer  $code
     * @param  array    $data
     * @return void
     */
    protected function _sendAmpResponse($code = 200, $data = false)
    {
        $this->getResponse()
            ->setHttpResponseCode($code)
            ->setHeader('Access-Control-Allow-Origin', Mage::helper('tmamp')->getAmpCacheDomainName())
            ->setHeader('Access-Control-Allow-Credentials', 'true')
            ->setHeader(
                'Access-Control-Expose-Headers',
                'AMP-Access-Control-Allow-Source-Origin'
            )
            ->setHeader(
                'Amp-Access-Control-Allow-Source-Origin',
                $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost()
            );

        if ($data !== false) {
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
        }
    }
}
