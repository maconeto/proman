<?php

class TM_Amp_Upgrade_1_0_0 extends TM_Core_Model_Module_Upgrade
{
    public function getOperations()
    {
        return array(
            'configuration' => $this->_getConfiguration(),
            'cmsblock'      => $this->_getCmsBlocks(),
            'cmspage'       => $this->_getCmsPages(),
            'easyslide'     => $this->_getSlider(),
        );
    }

    private function _getConfiguration()
    {
        return array(
            'tmamp/general/enabled' => 1,
            'easycatalogimg' => array(
                'general/enabled' => 1,
                'category' => array(
                    'enabled_for_default' => 0,
                    'enabled_for_anchor'  => 0
                )
            )
        );
    }

    /**
     * footer_links
     */
    private function _getCmsBlocks()
    {
        return array(
            'tmamp_footer' => array(
                'title' => 'tmamp_footer',
                'identifier' => 'tmamp_footer',
                'status' => 1,
                'content' => <<<HTML
<div class="links">
    <a href="{{store direct_url='about'}}">About</a>
    <a href="{{store direct_url='user-agreement'}}">User Agreement</a>
    <a href="{{store direct_url='privacy'}}">Privacy</a>
    <a href="{{store direct_url='cookies'}}">Cookies</a>
</div>
HTML
            )
        );
    }

    /**
     * home
     */
    private function _getCmsPages()
    {
        return array(
            'tmamp_homepage' => array(
                'title'             => 'Homepage',
                'identifier'        => 'tmamp_homepage',
                'root_template'     => 'one_column',
                'meta_keywords'     => '',
                'meta_description'  => '',
                'content_heading'   => '',
                'is_active'         => 1,
                'content'           => <<<HTML
{{widget type="easyslide/insert" slider_id="tmamp"}}
{{widget type="easycatalogimg/widget_list" category_count="6" subcategory_count="4" column_count="4" show_image="1" image_width="200" image_height="200" template="tm/easycatalogimg/list.phtml"}}
HTML
,
                'layout_update_xml' => ''
            ),
            'tmamp_typography' => array(
                'title'             => 'Typography',
                'identifier'        => 'tmamp-typography',
                'root_template'     => 'one_column',
                'meta_keywords'     => '',
                'meta_description'  => '',
                'content_heading'   => '',
                'is_active'         => 1,
                'content'           => <<<HTML
<h1 id="top">CSS Basic Elements</h1>

{{widget type="easyslide/insert" slider_id="tmamp"}}

<p>The purpose of this HTML is to help determine what default settings are with CSS and to make sure that all possible HTML Elements are included in this HTML so as to not miss any possible Elements when designing a site.</p>

<hr />

<h1 id="headings">Headings</h1>

<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>

<small><a href="#top">[top]</a></small>
<hr />


<h1 id="paragraph">Paragraph</h1>

<p>Lorem ipsum dolor sit amet, <a href="#" title="test link">test link</a> adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.</p>

<p>Lorem ipsum dolor sit amet, <em>emphasis</em> consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.</p>

<small><a href="#top">[top]</a></small>
<hr />

<h1 id="list_types">List Types</h1>

<h3>Definition List</h3>
<dl>
    <dt>Definition List Title</dt>
    <dd>This is a definition list division.</dd>
</dl>

<h3>Ordered List</h3>
<ol>
    <li>List Item 1</li>
    <li>List Item 2</li>
    <li>List Item 3</li>
    <li>List Item 3
        <ul>
            <li>List Item 1</li>
            <li>List Item 2</li>
            <li>List Item 3</li>
        </ul>
    </li>
</ol>

<h3>Unordered List</h3>
<ul>
    <li>List Item 1</li>
    <li>List Item 2</li>
    <li>List Item 3</li>
</ul>

<small><a href="#top">[top]</a></small>
<hr />

<h1 id="tables">Tables</h1>

<table cellspacing="0" cellpadding="0">
    <tr>
        <th>Table Header 1</th><th>Table Header 2</th><th>Table Header 3</th>
    </tr>
    <tr>
        <td>Division 1</td><td>Division 2</td><td>Division 3</td>
    </tr>
    <tr>
        <td>Division 1</td><td>Division 2</td><td>Division 3</td>
    </tr>
    <tr>
        <td>Division 1</td><td>Division 2</td><td>Division 3</td>
    </tr>
</table>

<small><a href="#top">[top]</a></small>
<hr />

<h1 id="misc">Misc Stuff - abbr, acronym, pre, code, sub, sup, etc.</h1>

<p>Lorem <sup>superscript</sup> dolor <sub>subscript</sub> amet, consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. <cite>cite</cite>. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. <acronym title="National Basketball Association">NBA</acronym> Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.  <abbr title="Avenue">AVE</abbr></p>

<pre><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. <acronym title="National Basketball Association">NBA</acronym> Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.  <abbr title="Avenue">AVE</abbr></p></pre>


<pre><code>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. <acronym title="National Basketball Association">NBA</acronym> Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.  <abbr title="Avenue">AVE</abbr></code></pre>

<code>Hello world</code>

<blockquote>
    "This stylesheet is going to help so freaking much." <br />-Blockquote
</blockquote>

<small><a href="#top">[top]</a></small>
HTML
,
                'layout_update_xml' => ''
            )
        );
    }

    private function _getSlider()
    {
        return array(
            array(
                'identifier'    => 'tmamp',
                'title'         => 'TM AMP',
                'width'         => 890,
                'height'        => 593,
                'duration'      => 0.5,
                'frequency'     => 4.0,
                'autoglide'     => 1,
                'controls_type' => 'arrow',
                'status'        => 1,
                'slides'        => array(
                    array(
                        'url'   => 'tmamp/default/tmamp_default_slider1.jpg',
                        'image' => '',
                        'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                        'desc_pos' => 3,
                        'background' => 2
                    ),
                    array(
                        'url'   => 'tmamp/default/tmamp_default_slider2.jpeg',
                        'image' => '',
                        'description' => 'Sale 20% off',
                        'desc_pos' => 4,
                        'background' => 2
                    ),
                    array(
                        'url'   => 'tmamp/default/tmamp_default_slider3.jpg',
                        'image' => '',
                        'description' => 'Free shipping',
                        'desc_pos' => 3,
                        'background' => 2
                    )
                )
            )
        );
    }
}
