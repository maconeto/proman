<?php

class TM_SeoTemplates_Helper_Seodata extends Mage_Core_Helper_Abstract
{

    public function getCollection($entityType, $entityId, $storeId)
    {
        if (!is_array($storeId)) {
            $stores = array($storeId);
        } else {
            $stores = $storeId;
        }
        $stores[] = Mage_Core_Model_App::ADMIN_STORE_ID;
        $collection = Mage::getModel('tm_seotemplates/seodata')
            ->getCollection();
        $collection
            ->addFilter('entity_type', array('eq' => $entityType))
            ->addFilter('entity_id', array('eq' => $entityId))
            ->addFilter('store_id', array('in' => $stores), 'public')
            ->addOrder('store_id', 'DESC');
        return $collection;
    }

}
