<?php

class TM_SeoTemplates_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected $_codes = array();

    protected $_collectionNames = array();

    public function isEnabled()
    {
        return Mage::getStoreConfigFlag('tm_seotemplates/general/enabled');
    }

    public function isForced()
    {
        return Mage::getStoreConfigFlag('tm_seotemplates/general/force');
    }

    public function getCodes($key = 'entity_type')
    {
        if (!$this->_codes) {
            $this->_codes = array(
                    'entity_type' => array(
                            'product' => 1001,
                            'category' => 1002
                        ),
                    'seodata' => array(
                            'meta_title' => 2001,
                            'meta_description' => 2002,
                            'meta_keywords' => 2003
                        )
                );
        }
        return $this->_codes[$key];
    }

    /**
     * Get entity type code
     * @param  string $type Entity type name
     * @return mixed        Entity type code or FALSE if not exist
     */
    public function getEntityTypeCode($type = 'product')
    {
        $entityTypes = $this->getCodes('entity_type');
        return array_key_exists($type, $entityTypes)
            ? $entityTypes[$type] : false;
    }

    /**
     * Get entity type name
     * @param  string $type Entity type code
     * @return mixed        Entity type name or FALSE if not exist
     */
    public function getEntityTypeName($code)
    {
        $entityTypes = $this->getCodes('entity_type');
        return array_search($code, $entityTypes);
    }

    /**
     * Get code SEO data by name
     */
    public function getSeodataCode($name = 'meta_title')
    {
        $seoData = $this->getCodes('seodata');
        return array_key_exists($name, $seoData)
            ? $seoData[$name] : false;
    }

    /**
     * Get code SEO data name by code
     */
    public function getSeodataName($code)
    {
        $seoData = $this->getCodes('seodata');
        return array_search($code, $seoData);
    }

    public function currentEntityType()
    {
        if ($this->_getRequest()->getModuleName() == 'catalog' &&
            $this->_getRequest()->getActionName() == 'view') {
            return $this->_getRequest()->getControllerName();
        }
    }

    public function getCollectionForEntityType($entityTypeCode)
    {
        if (!$this->_collectionNames) {
            $this->_collectionNames = array(
                1001 => 'catalog/product_collection',
                1002 => 'catalog/category_collection'
            );
        }
        return Mage::getResourceModel($this->_collectionNames[$entityTypeCode]);
    }

    public function prepareFieldName($field)
    {
        return sprintf('tm_seodata[%s]', $field);
    }

    public function prepareFieldLabel($name)
    {
        $str = str_replace('_', ' ', ucwords($name, '_'));
        return $this->__($str);
    }

    /**
     * get comma separated string with all allowed entity types
     * @return string
     */
    public function getEntityTypeAll()
    {
        return implode($this->getCodes('entity_type'), ',');
    }

}
