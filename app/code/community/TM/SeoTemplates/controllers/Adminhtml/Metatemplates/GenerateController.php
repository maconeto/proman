<?php

class TM_SeoTemplates_Adminhtml_Metatemplates_GenerateController
    extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('templates_master/tm_seocore/templates')
            ->_addBreadcrumb(
                Mage::helper('tm_seocore')->__('SEO Suite'),
                Mage::helper('tm_seocore')->__('SEO Suite')
            );
        $this->_title(Mage::helper('tm_seocore')->__('SEO Suite'))
            ->_title($this->__('Generate SEO data'));
        return $this;
    }

    public function indexAction()
    {
        $canRunGeneration = $this->_canRunGeneration();
        if (!$canRunGeneration) {
            // add message
            $this->_getSession()->addNotice(
                    $this->__('Seems like cron metadata generation still running. Please, wait till it ends.')
                );
        }
        $this->_initAction();
        if (!$canRunGeneration) {
            // hide start button
            $this->getLayout()->getBlock('generate_data_form')->hideStartButton();
        }
        $this->renderLayout();
    }

    public function startAction()
    {
        // fill missing params with default values
        if (!$this->getRequest()->getParam('page_size')) {
            $this->getRequest()->setParam('page_size', '33');
        }
        if (!$this->getRequest()->getParam('cur_page')) {
            $this->getRequest()->setParam('cur_page', '1');
        }
        if (!$this->getRequest()->getParam('entity_type')) {
            $this->getRequest()->setParam(
                    'entity_type',
                    Mage::helper('tm_seotemplates')->getEntityTypeAll()
                );
        }

        $generator = Mage::getModel('tm_seotemplates/generator');
        $entityTypes = explode(',', $this->getRequest()->getParam('entity_type'));
        foreach ($entityTypes as $type) {
            $generator->setEntityType($type)->clearGenerated();
        }

        // forward to execute action
        $this->_forward('exec');
    }

    public function execAction()
    {
        $pageSize = $this->getRequest()->getParam('page_size');
        $curPage = $this->getRequest()->getParam('cur_page');
        $entityType = explode(',', $this->getRequest()->getParam('entity_type'));

        $generator = Mage::getModel('tm_seotemplates/generator')
            ->setPageSize($pageSize)
            ->setCurPage($curPage)
            ->setEntityType($entityType[0])
            ->execute();

        $entityTypeName = Mage::helper('tm_seotemplates')
            ->getEntityTypeName($entityType[0]);
        $response = array();
        $response['log'] = array(
                'lineId' => 'log-line-' . $entityType[0],
                'text' => $this->__(ucfirst($entityTypeName))
                    . $this->__(': items processed - ')
                    . ($pageSize * ($curPage - 1) + $generator->getProcessedItems())
            );

        $nextPage = 1;
        if ($generator->getNextPage()) {
            $nextPage = $generator->getNextPage();
        } else {
            array_shift($entityType);
        }

        if (isset($entityType[0])) {
            $url = $this->getUrl(
                'adminhtml/metatemplates_generate/exec', array(
                    'page_size' => $pageSize,
                    'cur_page' => $nextPage,
                    'entity_type' => implode(',', $entityType)
                )
            );
            $response['url'] = $url;
        }

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setBody(Mage::helper('core')->jsonEncode($response));

    }

    protected function _canRunGeneration()
    {
        // last cron run was more then hour ago
        // (if cron ended successful then last_run is NULL else cron still running or error )
        $lastCronRun = Mage::getModel('tm_seotemplates/cron')
            ->getValue('last_run');
        return ( (time() - strtotime($lastCronRun)) > (1 * 60 * 60) );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('templates_master/tm_seocore/templates/generate');
    }

}
