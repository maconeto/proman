<?php

class TM_SeoTemplates_Adminhtml_Metatemplates_ManageController
    extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('templates_master/tm_seocore/templates')
            ->_addBreadcrumb(
                Mage::helper('tm_seocore')->__('SEO Suite'),
                Mage::helper('tm_seocore')->__('SEO Suite')
            );
        $this->_title(Mage::helper('tm_seocore')->__('SEO Suite'))
            ->_title($this->__('Metadata Templates'));
        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * New template action (forward to edit action)
     *
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Edit widget instance action
     *
     */
    public function editAction()
    {
        $template = $this->_initTemplateInstance();
        if (!$template) {
            $this->_redirect('*/*/');
            return;
        }

        $this->_initAction();
        $this->_title($template->getId() ? $template->getName() : $this->__('New Template'));
        $this->renderLayout();
    }

    /**
     * Initialize SEO Template Instance object and set it to register
     * @return [type] [description]
     */
    protected function _initTemplateInstance()
    {
        $template = Mage::getModel('tm_seotemplates/template');

        $id = $this->getRequest()->getParam('template_id', null);
        $seodataName = $this->getRequest()->getParam('seodata_name', null);
        $entityType = $this->getRequest()->getParam('entity_type', null);

        if ($id) {
            $template->load($id);
            if (!$template->getId()) {
                $this->_getSession()->addError(
                    $this->__('We can not find template you try to edit.')
                );
                return false;
            }
            // JS Form - requiered for contitions interface to work
            $template->getConditions()->setJsFormObject('conditions_fieldset');
        } else {
            $template->setEntityType($entityType)
                ->setSeodataName($seodataName)
                ->setStatus(1);
        }
        Mage::register('current_seo_template', $template);
        return $template;
    }

    /**
     * Save action
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $data['conditions'] = $data['rule']['conditions'];
            unset($data['rule']);

            //init model and set data
            $template = $this->_initTemplateInstance();
            $template->loadPost($data);
            try {
                // save the data
                $template->save();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__(
                        'Template "%s" has been saved.',
                        $template->getName()
                    )
                );
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect(
                        '*/*/edit',
                        array(
                            'template_id' => $template->getId(),
                            '_current'=>true
                        )
                    );
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->addException(
                    $e,
                    $this->__('An error occurred while saving.')
                );
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect(
                '*/*/edit',
                array('template_id' => $this->getRequest()->getParam('template_id'))
            );
            return;
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('template_id')) {
            try {
                // init template model and delete
                $template = Mage::getModel('tm_seotemplates/template');
                $template->load($id);
                $template->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Metadata template \'%s\' has been deleted.',
                    $template->getName())
                );
                // go to grid
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('template_id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(
            $this->__('Unable to find a metadata template to delete.')
        );
        // go to grid
        $this->_redirect('*/*/');
    }


    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('seotemps');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getModel('tm_seotemplates/template')->load($id);
                    $model->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($ids)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function viewlogAction()
    {
        $this->_initTemplateInstance();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Anction to generate new condition
     */
    public function newConditionHtmlAction()
    {
        $id = $this->getRequest()->getParam('id');
        $typeArr = explode('|', str_replace('-', '/', $this->getRequest()->getParam('type')));
        $type = $typeArr[0];

        $model = Mage::getModel($type)
            ->setId($id)
            ->setType($type)
            ->setRule(Mage::getModel('tm_seotemplates/template'))
            ->setPrefix('conditions');

        if (!empty($typeArr[1])) {
            $model->setAttribute($typeArr[1]);
        }

        if ($model instanceof Mage_Rule_Model_Condition_Abstract) {
            $model->setJsFormObject($this->getRequest()->getParam('form'));
            $html = $model->asHtmlRecursive();
        } else {
            $html = '';
        }
        $this->getResponse()->setBody($html);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('templates_master/tm_seocore/templates');
    }

}
