<?php

class TM_SeoTemplates_Model_Cron extends Mage_Core_Model_Abstract
{

    const LOCK_NAME = 'tm_seotemplates_cron_generate';

    /**
     * Locker Object
     *
     * @var Mage_Index_Model_Lock
     */
    protected $_lockInstance = null;

    /**
     * Generator Object
     *
     * @var TM_SeoTemplates_Model_Generator
     */
    protected $_generatorInstance = null;

    /**
     * Use file
     *
     * @var boolean
     */
    protected $_use_file_lock = false;

    /**
     * Prefix for path key in core_config_data
     *
     * @var string
     */
    protected $_path_prefix = 'tm_seotemplates/cron_process/';

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/cron');
    }

    public function generate($schedule)
    {

        if ($this->isLocked()) {
            $this->createNewJob($schedule->getJobCode());
            return $this;
        }

        $this->lock();

        $lastRun = $this->getValue('last_run');

        // if last run was more than 5 hours ago reinit process
        if ( (strtotime($schedule->getScheduledAt()) - strtotime($lastRun)) > (5 * 60 * 60) ) {
            $this->_initProcess();
        }

        $entityType = explode(',', $this->getValue('entity_type'));
        $pageSize = $this->getValue('page_size');
        $curPage = $this->getValue('cur_page');

        $generator = $this->_getGeneratorInstance()
            ->setPageSize($pageSize)
            ->setCurPage($curPage)
            ->setEntityType($entityType[0])
            ->execute();

        $nextPage = 1;
        if ($generator->getNextPage()) {
            $nextPage = $generator->getNextPage();
        } else {
            array_shift($entityType);
        }

        if (isset($entityType[0])) {
            // save data for next cron run
            $this->saveValue('entity_type', implode(',', $entityType))
                ->saveValue('page_size', $pageSize)
                ->saveValue('cur_page', $nextPage)
                ->saveValue('last_run', $schedule->getScheduledAt());
            $this->createNewJob($schedule->getJobCode());
        } else {
            $this->deleteValue('entity_type');
            $this->deleteValue('page_size');
            $this->deleteValue('cur_page');
            $this->deleteValue('last_run');
        }

        $this->unlock();

        return $this;

    }

    public function createNewJob($jobCode)
    {
        $createdAt = time();
        $scheduledAt = $createdAt + 60;
        $newSchedule = Mage::getModel('cron/schedule')
            ->setJobCode($jobCode)
            ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING)
            ->setCreatedAt(strftime('%Y-%m-%d %H:%M:%S', $createdAt))
            ->setScheduledAt(strftime('%Y-%m-%d %H:%M', $scheduledAt));
        $newSchedule->save();
        return $this;
    }

    public function getValue($key)
    {
        $path = $this->_path_prefix . $key;
        return Mage::getModel('core/config_data')
                ->load($path, 'path')
                ->getValue();
    }

    public function saveValue($key, $value)
    {
        $path = $this->_path_prefix . $key;
        Mage::getModel('core/config_data')
            ->load($path, 'path')
            ->setValue($value)
            ->setPath($path)
            ->save();
        return $this;
    }

    public function deleteValue($key)
    {
        $path = $this->_path_prefix . $key;
        Mage::getModel('core/config_data')
            ->load($path, 'path')
            ->delete();
        return $this;
    }

    /**
     * Lock process
     *
     * @return TM_SeoTemplates_Model_Cron
     */
    public function lock()
    {
        $this->_getLockInstance()->setLock(
                self::LOCK_NAME,
                $this->_use_file_lock
            );
        return $this;
    }

    /**
     * Unlock process
     *
     * @return TM_SeoTemplates_Model_Cron
     */
    public function unlock()
    {
        $this->_getLockInstance()->releaseLock(
                self::LOCK_NAME,
                $this->_use_file_lock
            );
        return $this;
    }

    protected function _initProcess()
    {
        $typeAll = Mage::helper('tm_seotemplates')->getEntityTypeAll();
        $pageSize = Mage::getStoreConfig('tm_seotemplates/cron/page_size');
        $this->saveValue('entity_type', $typeAll)
            ->saveValue('page_size', $pageSize)
            ->saveValue('cur_page', 1);

        // clear previously generated data
        $generator = $this->_getGeneratorInstance();
        foreach (explode(',', $typeAll) as $type) {
            $generator->setEntityType($type)->clearGenerated();
        }
    }

    /**
     * Returns Lock object.
     *
     * @return Mage_Index_Model_Lock|null
     */
    protected function _getLockInstance()
    {
        if (is_null($this->_lockInstance)) {
            $this->_lockInstance = Mage_Index_Model_Lock::getInstance();
        }
        return $this->_lockInstance;
    }

    /**
     * Returns Generator object.
     *
     * @return TM_SeoTemplates_Model_Generator|null
     */
    protected function _getGeneratorInstance()
    {
        if (is_null($this->_generatorInstance)) {
            $this->_generatorInstance = Mage::getModel('tm_seotemplates/generator');
        }
        return $this->_generatorInstance;
    }

    /**
     * Check if process is locked
     *
     * @return bool
     */
    public function isLocked()
    {
        return $this->_getLockInstance()->isLockExists(
                self::LOCK_NAME,
                $this->_use_file_lock
            );
    }

}
