<?php

class TM_SeoTemplates_Model_Observer
{

    public function updateMetadata($observer)
    {
        $helper = Mage::helper('tm_seotemplates');
        if (!$helper->isEnabled()) return;

        $block = $observer->getBlock();
        if ($block->getType() != 'page/html_head') return;

        $entityType = $helper->currentEntityType();
        $typeCode = $helper->getEntityTypeCode($entityType);

        if (!$typeCode) return;

        $entity = Mage::registry('current_'.$entityType);

        $collection = Mage::getModel('tm_seotemplates/seodata')
                ->getCollection()
                ->addEntityTypeFilter($typeCode)
                ->addEntityIdFilter($entity->getId())
                ->addStoreFilter(Mage::app()->getStore()->getId())
                ->addOrder('store_id', 'DESC');

        $seoTags = array_keys($helper->getCodes('seodata'));
        // $seotags contains array of meta_title, meta_describtion and meta_keywords
        foreach ($collection as $seo) {
            foreach ($seoTags as $tag) {
                if (!$seo->getData($tag)) {
                    continue;
                }
                if ($entity->getData($tag) && !$helper->isForced()) {
                    continue;
                }
                $key = str_replace('meta_', '', $tag);
                $block->setData($key, $seo->getData($tag));
                unset($seoTags[$tag]);
            }
        }
    }

}
