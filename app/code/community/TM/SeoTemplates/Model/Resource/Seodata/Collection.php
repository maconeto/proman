<?php

class TM_SeoTemplates_Model_Resource_Seodata_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/seodata');
    }

    public function addEntityTypeFilter($entityType)
    {
        $this->addFilter('entity_type', array('eq' => $entityType));
        return $this;
    }

    public function addEntityIdFilter($entityId)
    {
        $this->addFilter('entity_id', array('eq' => $entityId));
        return $this;
    }

    public function addStoreFilter($storeId)
    {
        if (!is_array($storeId)) {
            $stores = array($storeId);
        } else {
            $stores = $storeId;
        }
        $stores[] = Mage_Core_Model_App::ADMIN_STORE_ID;
        $this->addFilter('store_id', array('in' => $stores), 'public');
        return $this;
    }

}
