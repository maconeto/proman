<?php

class TM_SeoTemplates_Model_Resource_Log_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/log');
    }

}
