<?php

class TM_SeoTemplates_Model_Resource_Log
    extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/log', 'id');
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        // Set date of last modification
        $currentTime = Mage::app()->getLocale()->date();
        $object->setGenerationTime(
            $currentTime->toString(Varien_Date::DATETIME_INTERNAL_FORMAT)
        );
        return parent::_beforeSave($object);
    }

    public function reindexTable()
    {
        $write = $this->_getWriteAdapter();

        $select = $write->select()->from($this->getMainTable());

        $temporary = true;
        $tempTableName = $this->getMainTable() . '_TEMP';
        $write->createTableFromSelect($tempTableName, $select, $temporary);

        $write->truncateTable($this->getMainTable());
        $write->query("ALTER TABLE " . $this->getMainTable() . " AUTO_INCREMENT = 1");

        $select = $write->select()->from($tempTableName);

        $pageSize = 100;
        $curPage = 1;
        do {
            $select->limit($pageSize, (($curPage-1) * $pageSize));
            $data = $write->fetchAll($select);
            foreach ($data as &$row) {
                unset($row['id']);
            }
            if (!empty($data)) {
                $write->insertMultiple($this->getMainTable(), $data);
                $curPage++;
            }
        } while (!empty($data));
    }

}
