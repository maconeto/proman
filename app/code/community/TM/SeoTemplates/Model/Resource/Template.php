<?php

class TM_SeoTemplates_Model_Resource_Template
    extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/template', 'id');
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        // Set date of last modification
        $currentTime = Mage::app()->getLocale()->date();
        $object->setUpdateTime(
            $currentTime->toString(Varien_Date::DATETIME_INTERNAL_FORMAT)
        );
        return parent::_beforeSave($object);
    }

    /**
     * Process template data before deleting
     *
     * @param Mage_Core_Model_Abstract $object
     * @return TM_SeoTemplates_Model_Resource_Template
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        $condition = array(
            'template_id = ?'     => (int) $object->getId(),
        );
        $this->_getWriteAdapter()->delete(
                $this->getTable('tm_seotemplates/store'),
                $condition
            );
        $this->_getWriteAdapter()->delete(
                $this->getTable('tm_seotemplates/log'),
                $condition
            );
        return parent::_beforeDelete($object);
    }

    /**
     * Assign template to store views
     *
     * @param Mage_Core_Model_Abstract $object
     * @return TM_SeoTemplates_Model_Resource_Template
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        //  1. SAVE STORES FOR TEMPLATE
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array) $object->getStores();
        if (empty($newStores)) {
            $newStores = (array) $object->getStoreId();
        }

        $table  = $this->getTable('tm_seotemplates/store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);
        if ($delete) {
            $where = array(
                'template_id = ?' => (int) $object->getId(),
                'store_id IN (?)' => $delete
            );
            $this->_getWriteAdapter()->delete($table, $where);
        }
        if ($insert) {
            $data = array();
            foreach ($insert as $storeId) {
                $data[] = array(
                    'template_id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }
            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }

        // //Mark layout cache as invalidated
        // Mage::app()->getCacheInstance()->invalidateType('layout');
        return parent::_afterSave($object);
    }

    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return TM_SeoTemplates_Model_Resource_Template
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            // get stores assigned to deal
            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);
        }
        return parent::_afterLoad($object);
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $templateId
     * @return array
     */
    public function lookupStoreIds($templateId)
    {
        $adapter = $this->_getReadAdapter();
        $select  = $adapter->select()
            ->from($this->getTable('tm_seotemplates/store'), 'store_id')
            ->where('template_id = ?', (int) $templateId);
        return $adapter->fetchCol($select);
    }

    public function clearLog($templateId)
    {
        try {
            $write = $this->_getWriteAdapter();
            $write->beginTransaction();
            $write->delete(
                $this->getTable('tm_seotemplates/log'),
                'template_id = ' . $templateId
            );
            $write->commit();
        } catch (Exception $e) {
            $write -> rollback();
        }
    }

}
