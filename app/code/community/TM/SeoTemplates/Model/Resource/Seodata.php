<?php

class TM_SeoTemplates_Model_Resource_Seodata
    extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/seodata', 'id');
    }

    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        if ($field == 'entity_id') {
            $field = $this->_getReadAdapter()->quoteIdentifier(sprintf('%s.%s', $this->getMainTable(), 'entity_type'));
            $select->where($field . '=?', $object->getData('entity_type'));
            $field = $this->_getReadAdapter()->quoteIdentifier(sprintf('%s.%s', $this->getMainTable(), 'store_id'));
            $select->where($field . '=?', $object->getData('store_id'));
        }
        return $select;
    }

    public function deleteGenerated($entityType)
    {
        try {
            $write = $this->_getWriteAdapter();
            $write->beginTransaction();
            $write->delete(
                $this->getTable('tm_seotemplates/seodata'),
                'entity_type = ' . $entityType
            );
            $write->commit();
        } catch (Exception $e) {
            $write -> rollback();
        }
    }

    public function reindexTable()
    {
        $write = $this->_getWriteAdapter();

        $select = $write->select()->from($this->getMainTable());

        $temporary = true;
        $tempTableName = $this->getMainTable() . '_TEMP';
        $write->createTableFromSelect($tempTableName, $select, $temporary);

        $write->truncateTable($this->getMainTable());
        $write->query("ALTER TABLE " . $this->getMainTable() . " AUTO_INCREMENT = 1");

        $select = $write->select()->from($tempTableName);

        $pageSize = 100;
        $curPage = 1;
        do {
            $select->limit($pageSize, (($curPage-1) * $pageSize));
            $data = $write->fetchAll($select);
            foreach ($data as &$row) {
                unset($row['id']);
            }
            if (!empty($data)) {
                $write->insertMultiple($this->getMainTable(), $data);
                $curPage++;
            }
        } while (!empty($data));
    }

}
