<?php

class TM_SeoTemplates_Model_Rule_Condition_Category
    extends Mage_CatalogRule_Model_Rule_Condition_Product
{
    public function __construct()
    {
        parent::__construct();
        $this->setType('tm_seotemplates/rule_condition_category');
        $this->setAttribute('category_ids');
        $this->setOperator('()');
    }

    /**
     * Validate category attribute value for condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $attrCode = $this->getAttribute();
        if ('category_ids' == $attrCode) {
            return $this->validateAttribute($object->getId());
        }
        return false;
    }

}
