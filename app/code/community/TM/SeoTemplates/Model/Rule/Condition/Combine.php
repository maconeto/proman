<?php

class TM_SeoTemplates_Model_Rule_Condition_Combine
    extends Mage_Rule_Model_Condition_Combine
{
    public function __construct()
    {
        parent::__construct();
        $this->setType('tm_seotemplates/rule_condition_combine');
    }

    public function getNewChildSelectOptions()
    {
        $conditions = parent::getNewChildSelectOptions();
        if ($this->isConditionAllowed('product')) {
            array_push(
                $conditions,
                array(
                    'label' => Mage::helper('catalog')->__('Product Attributes Combination'),
                    'value' => 'tm_seotemplates/rule_condition_product',
                )
            );
        }
        if ($this->isConditionAllowed('category')) {
            array_push(
                $conditions,
                array(
                    'label' => Mage::helper('catalog')->__('Category'),
                    'value' => 'tm_seotemplates/rule_condition_category'
                )
            );
        }
        array_push(
            $conditions,
            array(
                'label' => Mage::helper('catalog')->__('Conditions Combination'),
                'value' => 'tm_seotemplates/rule_condition_combine',
            )
        );
        // print_r($conditions);die;
        return $conditions;
    }

    public function collectValidatedAttributes($productCollection)
    {
        foreach ($this->getConditions() as $condition) {
            $condition->collectValidatedAttributes($productCollection);
        }
        return $this;
    }

    public function isConditionAllowed($entityName = 'product')
    {
        $template = Mage::registry('current_seo_template');
        $entityType = $template ? $template->getEntityType() : null;
        $requieredType = Mage::helper('tm_seotemplates')
            ->getEntityTypeCode($entityName);
        return $entityType == $requieredType;
    }

}
