<?php

class TM_SeoTemplates_Model_Source_Seodata_Name
{

    protected $_options;

    public function toOptionHash()
    {
        if (!$this->_options) {
            $helper = Mage::helper('tm_seotemplates');
            $this->_options = array(
                $helper->getSeodataCode('meta_title') => $helper->__('Meta Title'),
                $helper->getSeodataCode('meta_description') => $helper->__('Meta Description'),
                $helper->getSeodataCode('meta_keywords') => $helper->__('Meta Keywords')
            );
        }
        return $this->_options;
    }

    public function toOptionArray()
    {
        $res = array();
        foreach ($this->toOptionHash() as $value => $label) {
            $res[] = array('value' => $value, 'label' => $label);
        }
        return $res;
    }

}
