<?php

class TM_SeoTemplates_Model_Source_Entity_Type
{

    protected $_options;

    public function toOptionHash()
    {
        if (!$this->_options) {
            $helper = Mage::helper('tm_seotemplates');
            $this->_options = array(
                $helper->getEntityTypeCode('product') => $helper->__('Product'),
                $helper->getEntityTypeCode('category') => $helper->__('Category')
            );
        }
        return $this->_options;
    }

    public function toOptionArray()
    {
        $res = array();
        foreach ($this->toOptionHash() as $value => $label) {
            $res[] = array('value' => $value, 'label' => $label);
        }
        return $res;
    }

}
