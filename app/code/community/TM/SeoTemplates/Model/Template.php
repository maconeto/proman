<?php

class TM_SeoTemplates_Model_Template extends Mage_Rule_Model_Abstract
{

    protected $_logCollection;

    protected $_processor = null;

    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/template');
    }

    /**
     * Check if template has required data
     *
     * @return boolean
     */
    public function isCompleteToCreate()
    {
        return (bool)($this->getEntityType() && $this->getSeodataName());
    }

    public function getConditionsInstance()
    {
        return Mage::getModel('tm_seotemplates/rule_condition_combine');
    }

    /**
     * Getter for rule actions collection instance
     *
     * @return Mage_Rule_Model_Action_Collection
     */
    public function getActionsInstance()
    {
        return Mage::getModel('rule/action_collection');
    }

    public function getLog()
    {
        if (empty($this->_logCollection)) {
            $this->_logCollection = Mage::getModel('tm_seotemplates/log')
                ->getCollection();
            if ($this->getId()) {
                $this->_logCollection->addFieldToFilter(
                    'template_id', array('eq' => $this->getId())
                );
            }
        }
        return $this->_logCollection;
    }

    public function getSeodataNameAsString()
    {
        return Mage::helper('tm_seotemplates')
            ->getSeodataName($this->getSeodataName());
    }

    public function clearLog()
    {
        $this->getResource()->clearLog($this->getId());
        return $this;
    }

    public function generate($entity)
    {
        $processor = $this->_getProcessor()->setScope($entity);
        return $processor->filter($this->getTemplate());
    }

    /**
     * Get processor base on template entity type
     * @return object instanceof TM_SeoTemplates_Model_Filter_Abstract
     */
    protected function _getProcessor()
    {
        if ($this->_processor === null) {
            $modelName = 'tm_seotemplates/filter_'
                . Mage::helper('tm_seotemplates')->getEntityTypeName(
                        $this->getEntityType()
                    );
            $this->_processor = Mage::getModel($modelName);
        }
        return $this->_processor;
    }

}
