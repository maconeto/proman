<?php

class TM_SeoTemplates_Model_Seodata extends Mage_Core_Model_Abstract
{

    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/seodata');
    }

    public function deleteGenerated($entityType)
    {
        $this->getResource()->deleteGenerated($entityType);
        return $this;
    }

    public function loadByEntityId($entityId, $entityType, $storeId)
    {
        $this->setEntityType($entityType)
            ->setStoreId($storeId)
            ->setEntityId($entityId);
        return $this->load($entityId, 'entity_id');
    }

}
