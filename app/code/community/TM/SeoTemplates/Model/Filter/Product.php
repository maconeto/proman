<?php

class TM_SeoTemplates_Model_Filter_Product
    extends TM_SeoTemplates_Model_Filter_Abstract
{

    public function priceDirective($construction)
    {
        $params = $this->_getIncludeParameters($construction[2]);
        $finalPrice = Mage::helper('tax')
            ->getPrice(
                $this->getScope(),
                $this->getScope()->getFinalPrice()
            );
        return $this->_postprocessResult($finalPrice, $params);
    }

}
