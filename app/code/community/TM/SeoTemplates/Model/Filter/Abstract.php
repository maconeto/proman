<?php

abstract class TM_SeoTemplates_Model_Filter_Abstract
    extends Varien_Filter_Template
{

    protected $_scope;

    public function setScope($scope)
    {
        $this->_scope = $scope;
        return $this;
    }

    public function getScope()
    {
        return $this->_scope;
    }

    /**
     * Filter the string as template.
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        if(preg_match_all(self::CONSTRUCTION_PATTERN, $value, $constructions, PREG_SET_ORDER)) {
            foreach($constructions as $construction) {

                $replacedValue = '';

                $callback = array($this, $construction[1].'Directive');
                if(is_callable($callback)) {
                    try {
                        $replacedValue = call_user_func($callback, $construction);
                    } catch (Exception $e) {
                        throw $e;
                    }
                }
                $value = str_replace($construction[0], $replacedValue, $value);

            }
        }
        return $value;

    }

    protected function _underscoreToCamelCase($string)
    {
        $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
        return $str;
    }

    public function attributeDirective($construction)
    {
        $params = $this->_getIncludeParameters($construction[2]);
        $attributeCode = isset($params['code']) ? $params['code'] : '';

        // attribute code can containe multiple attributes separated by comma
        $attributeCodes = array_map('trim', explode(",",$attributeCode));

        foreach ($attributeCodes as $code) {
            $attributeValue = $this->_getAttributeValue($code);
            if ($attributeValue) {
                break;
            }
        }

        return $this->_postprocessResult($attributeValue, $params);
    }

    public function ifexistDirective($construction)
    {
        $params = $this->_getIncludeParameters($construction[2]);
        $attributeCode = isset($params['attribute']) ? $params['attribute'] : '';
        $then = isset($params['then']) ? $params['then'] : '';
        $else = isset($params['else']) ? $params['else'] : '';

        // attribute code can containe multiple attributes separated by comma
        $attributeCodes = array_map('trim', explode(",",$attributeCode));

        foreach ($attributeCodes as $code) {
            $attributeValue = $this->_getAttributeValue($code);
            if ($attributeValue) {
                break;
            }
        }

        if ($attributeValue) {
            return $this->filter($then);
        }
        return $this->filter($else);
    }

    protected function _getAttributeValue($attributeCode)
    {
        $typeInstance = $this->getScope()->getTypeInstance();
        $scope = $this->getScope();

        if (method_exists($typeInstance, 'getConfigurableAttributes')) {
            // search for attribute values in simple products of configurable product
            foreach ($typeInstance->getConfigurableAttributes() as $attribute) {
                if ($attributeCode == $attribute->getProductAttribute()->getAttributeCode()) {
                    // code below inspired by
                    // Mage_Catalog_Model_Product_Type_Configurable::getConfigurableAttributesAsArray
                    $values = $attribute->getPrices() ? $attribute->getPrices()
                                : array();
                    $result = array();
                    foreach ($values as $value) {
                        $result[] = $value['store_label'];
                    }
                    return $result;
                }
            }
        }
        $attribute = $scope->getResource()->getAttribute($attributeCode);
        if ($attribute && $scope->getData($attributeCode)) {
            $attribute->setStoreId($scope->getStoreId());
            return $attribute->getFrontend()->getValue($scope);
        }
        return '';
    }

    protected function _postprocessResult($result, $params)
    {
        $output = '';
        if ($result) {
            if (is_array($result)) {
                $separator = isset($params['separator']) ? $params['separator'] : ', ';
                if (isset($params['exclude'])) {
                    // exclude values
                    $exclude = explode(",", $params['exclude']);
                    $exclude = array_map('trim', $exclude);
                    $result = array_diff($result, $exclude);
                }
                if (isset($params['limit'])) {
                    $limit = $params['limit'];
                    $result = array_slice($result, 0, $limit, true);
                }
                $output = implode($result, $separator);
            } else {
                $output = $result;
            }
            $prefix = isset($params['prefix']) ? $params['prefix'] : '';
            $sufix = isset($params['sufix']) ? $params['sufix'] : '';
            $output = $prefix . $output . $sufix;
        }
        return $output;
    }

    /**
     * Override parent method to prevent notice when paramete contains only '$'
     * Example: {{minprice prefix="$"}}
     *
     * Variables processing removed
     *
     * @param string $value raw parameters
     * @return array
     */
    protected function _getIncludeParameters($value)
    {
        $tokenizer = new Varien_Filter_Template_Tokenizer_Parameter();
        $tokenizer->setString($value);
        $params = $tokenizer->tokenize();
        return $params;
    }

}
