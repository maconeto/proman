<?php

class TM_SeoTemplates_Model_Filter_Category
    extends TM_SeoTemplates_Model_Filter_Abstract
{

    public function productsDirective($construction)
    {
        $params = $this->_getIncludeParameters($construction[2]);
        $attributeCode = isset($params['attribute']) ? $params['attribute'] : '';

        // attribute code can containe multiple attributes separated by comma
        $attributeCodes = array_map('trim', explode(",",$attributeCode));

        $storeId = $this->getScope()->getStoreId();
        if (!$this->getScope()->getStoreId()) {
            // layer object requiers specific store
            $storeId = Mage::app()->getDefaultStoreView()->getId();
            $this->getScope()->setStoreId($storeId);
        }
        Mage::app()->setCurrentStore($storeId);
        $layer = $this->_getLayer();
        $layer->setCurrentCategory($this->getScope());
        $result = array();

        $filterableAttributes = $layer->getFilterableAttributes();
        foreach ($attributeCodes as $code) {
            foreach ($filterableAttributes as $attribute) {
                if ($attribute->getAttributeCode() == $code) {
                    $object = new Varien_Object(
                        array(
                                'layer' => $layer,
                                'store_id' => $this->getScope()->getStoreId(),
                                'attribute_model' => $attribute
                            )
                        );
                    $resource = Mage::getResourceModel('catalog/layer_filter_attribute');
                    $optionsCount = $resource->getCount($object);
                    arsort($optionsCount);
                    $options = $attribute->getFrontend()->getSelectOptions();
                    foreach ($optionsCount as $id => $count) {
                        $label = $this->_getOptionLabelByValue($options, $id);
                        if ($label) {
                            $result[] = $label;
                        }
                    }
                }
            }

            if (empty($result)) {
                $result = $this->_fallbackForNotFilterableAttribute($code);
            }

            if (!empty($result)) {
                break;
            }
        }

        return $this->_postprocessResult($result, $params);
    }

    public function subcatsDirective($construction)
    {
        $params = $this->_getIncludeParameters($construction[2]);
        $result = array();

        foreach ($this->getScope()->getChildrenCategories() as $subcat) {
            $result[] = $subcat->getName();
        }

        return $this->_postprocessResult($result, $params);
    }

    public function minpriceDirective($construction)
    {
        $layer = $this->_getLayer();
        $layer->setCurrentCategory($this->getScope());
        Mage::app()->setCurrentStore($this->getScope()->getStoreId());

        $productCollection = $layer->getProductCollection();
        $params = $this->_getIncludeParameters($construction[2]);

        return $this->_postprocessResult($productCollection->getMinPrice(), $params);
    }

    /**
     * Get layer object
     *
     * @return Mage_Catalog_Model_Layer
     */
    protected function _getLayer()
    {
        return Mage::getSingleton('catalog/layer');
    }


    protected function _getOptionLabelByValue($options, $value)
    {
        $label = '';
        if (is_array($options)) {
            foreach ($options as $option) {
                if (isset($option['value']) && $option['value'] == $value) {
                    $label = isset($option['label']) ? $option['label'] : '';
                    break;
                }
            }
        }
        return $label;
    }

    protected function _fallbackForNotFilterableAttribute($attributeCode)
    {
        $productCollection = $this->getScope()->getProductCollection();
        $productCollection->addAttributeToFilter('status', 1)
            ->addAttributeToFilter(
            'visibility',
            array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
            )
        );
        $result = array();
        foreach ($productCollection as $product) {
            $product->load($product->getId());
            $value = $product->getResource()->getAttribute($attributeCode)
                ->getFrontend()->getValue($product);
            if (isset($result[$value])) {
                $result[$value]++;
            } else {
                $result[$value] = 1;
            }
        }

        arsort($result);
        return array_keys($result);
    }

}
