<?php

class TM_SeoTemplates_Model_Log extends Mage_Core_Model_Abstract
{

    /**
     * Initialize resource model
     */
    protected function _construct()
    {
        $this->_init('tm_seotemplates/log');
    }

    public function addEntityName($entityType = null)
    {
        if ($this->getEntityId() && $entityType) {
            switch ($entityType) {
                case Mage::helper('tm_seotemplates')->getEntityTypeCode('product'):
                    $product = Mage::getModel('catalog/product')
                        ->setStoreId($this->getStoreId())
                        ->load($this->getEntityId());
                    $this->setData('entity_name', $product->getName());
                    break;
                case Mage::helper('tm_seotemplates')->getEntityTypeCode('category'):
                    $category = Mage::getModel('catalog/category')
                        ->setStoreId($this->getStoreId())
                        ->load($this->getEntityId());
                    $this->setData('entity_name', $category->getName());
                    break;
            }
        }
        return $this;
    }

}
