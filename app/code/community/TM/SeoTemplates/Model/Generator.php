<?php

class TM_SeoTemplates_Model_Generator extends Varien_Object
{

    public function getPageSize()
    {
        if (!$this->hasData('page_size')) {
            $this->setData('page_size', 33);
        }
        return $this->getData('page_size');
    }

    public function getCurPage()
    {
        if (!$this->hasData('cur_page')) {
            $this->setData('cur_page', 1);
        }
        return $this->getData('cur_page');
    }

    public function getEntityType()
    {
        if (!$this->hasData('entity_type')) {
            $this->setData('entity_type', 0);
        }
        return $this->getData('entity_type');
    }

    public function execute()
    {
        $collection = Mage::helper('tm_seotemplates')
            ->getCollectionForEntityType($this->getEntityType())
            // ->addFieldToFilter('entity_id', array('eq' => 404)) // jjjhhhjjj
            ->setPageSize($this->getPageSize())
            ->setCurPage($this->getCurPage());

        $templates = Mage::getResourceModel('tm_seotemplates/template_collection')
            ->addFieldToFilter('status', array('eq' => 1))
            ->addFieldToFilter('entity_type', array('eq' => $this->getEntityType()));
        $templates->walk('afterLoad');

        $this->setProcessedItems(0);
        $productNotVisible = Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;
        $stores = $this->_getStoresArray();

        foreach ($collection as $entity) {

            foreach ($stores as $store) {

                $entity->setStoreId($store)->load($entity->getId());
                // skip store if product is not visible on store
                if ($entity->getVisibility() == $productNotVisible) continue;

                foreach ($templates as $template) {

                    // skip template if it does not have store
                    if (!in_array($store, $template->getStoreId())) continue;

                    // skip template if if does not meet conditions
                    if (!$template->getConditions()->validate($entity)) continue;

                    // generate SEO data using template
                    $generatedValue = $template->generate($entity);

                    $seodata = Mage::getModel('tm_seotemplates/seodata');
                    $seodata->loadByEntityId(
                            $entity->getId(),
                            $template->getEntityType(),
                            $store
                        );

                    // save generated only if record exists or generated is not empty
                    if ($seodata->getId() || $generatedValue) {
                        $seodata->setData(
                                $template->getSeodataNameAsString(),
                                $generatedValue
                            );
                        $seodata->save();
                    }

                    // add log record
                    $log = Mage::getModel('tm_seotemplates/log');
                    $log->setTemplateId($template->getId());
                    $log->setEntityId($entity->getId());
                    $log->setStoreId($store);
                    $log->setGeneratedValue($generatedValue);
                    $log->save();
                }
            }

            $this->setProcessedItems($this->getProcessedItems() + 1);
        }

        if ($this->getCurPage() >= $collection->getLastPageNumber()) {
            $this->setNextPage(false);
        } else {
            $this->setNextPage($this->getCurPage() + 1);
        }

        return $this;
    }

    public function clearGenerated()
    {
        // clear template logs
        $templates = Mage::getResourceModel('tm_seotemplates/template_collection')
            ->addFieldToFilter('status', array('eq' => 1))
            ->addFieldToFilter('entity_type', array('eq' => $this->getEntityType()));
        foreach ($templates as $t) {
            $t->clearLog();
        }

        // clear generated data
        Mage::getSingleton('tm_seotemplates/seodata')
            ->deleteGenerated($this->getEntityType());

        return $this;
    }

    protected function _getStoresArray()
    {
        $stores = array();
        foreach (Mage::app()->getStores(true) as $s) {
            $stores[] = $s->getId();
        }
        return $stores;
    }

}
