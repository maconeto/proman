<?php

class TM_SeoTemplates_Model_Adminhtml_Observer
{

    public function addCategoryTab($observer)
    {
        $tabs =  $observer->getTabs();
        $tabs->addTab('tm_seo', array(
            'label'     => Mage::helper('catalog')->__('SEO Suite'),
            'content'   => $this->getLayout()->createBlock(
                                'tm_seotemplates/adminhtml_catalog_category_tab',
                                'tmseotemplates.data'
                            )->toHtml(),
        ));
    }

    public function getLayout()
    {
        return Mage::app()->getLayout();
    }

}
