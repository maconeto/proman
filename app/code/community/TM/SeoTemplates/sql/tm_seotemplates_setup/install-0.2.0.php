<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'tm_seotemplates/template'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('tm_seotemplates/template'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        ), 'Template Id')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => true
        ), 'Template Name')
    ->addColumn('entity_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Entity Type')
    ->addColumn('seodata_name', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'SEO Data Name')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array()
        , 'Status')
    ->addColumn('conditions_serialized', Varien_Db_Ddl_Table::TYPE_TEXT, null, array()
        , 'Conditions Serialized')
    ->addColumn('template', Varien_Db_Ddl_Table::TYPE_TEXT, null, array()
        , 'Template')
    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array()
        , 'Modified at')
    ->setComment('SEO Templates - template data');
$installer->getConnection()->createTable($table);

/**
 * Create table 'tm_seotemplates/store'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('tm_seotemplates/store'))
    ->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Template ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')
    ->addIndex($installer->getIdxName('tm_seotemplates/store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('tm_seotemplates/store', 'template_id', 'tm_seotemplates/template', 'id'),
        'template_id', $installer->getTable('tm_seotemplates/template'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('tm_seotemplates/store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('SEO Templates - template to store link');
$installer->getConnection()->createTable($table);

/**
 * Create table 'tm_seotemplates/seodata'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('tm_seotemplates/seodata'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_BIGINT, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        ), 'Id')
    ->addColumn('entity_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Entity Type')
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Entity Id')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Store ID')
    ->addColumn('meta_title', Varien_Db_Ddl_Table::TYPE_TEXT, null, array()
        , 'Meta Title')
    ->addColumn('meta_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array()
        , 'Meta Description')
    ->addColumn('meta_keywords', Varien_Db_Ddl_Table::TYPE_TEXT, null, array()
        , 'Meta Keywords')
    ->addForeignKey($installer->getFkName('tm_seotemplates/seodata', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('SEO Templates -  SEO data');
$installer->getConnection()->createTable($table);

/**
 * Create table 'tm_seotemplates/seodata'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('tm_seotemplates/log'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_BIGINT, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        ), 'Id')
    ->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Template Id')
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Entity Id')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Store ID')
    ->addColumn('generated_value', Varien_Db_Ddl_Table::TYPE_TEXT, null, array()
        , 'Generated Value')
    ->addColumn('generation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array()
        , 'Generated at')
    ->addForeignKey($installer->getFkName('tm_seotemplates/log', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('tm_seotemplates/log', 'template_id', 'tm_seotemplates/template', 'id'),
        'template_id', $installer->getTable('tm_seotemplates/template'), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('SEO Templates -  Log');
$installer->getConnection()->createTable($table);

$installer->endSetup();
