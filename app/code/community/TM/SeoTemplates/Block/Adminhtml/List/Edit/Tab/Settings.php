<?php

class TM_SeoTemplates_Block_Adminhtml_List_Edit_Tab_Settings
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
     protected function _construct()
    {
        parent::_construct();
        $this->setActive(true);
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Settings');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return !(bool)$this->getSeoTemplate()->isCompleteToCreate();
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Getter
     *
     * @return TM_SeoTemplates_Model_Template
     */
    public function getSeoTemplate()
    {
        return Mage::registry('current_seo_template');
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Settings
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend'=> $this->__('Settings'))
        );

        $this->_addElementTypes($fieldset);

        $fieldset->addField('entity_type', 'select', array(
            'name'     => 'entity_type',
            'label'    => $this->__('Template for'),
            'title'    => $this->__('Template for'),
            'required' => true,
            'values'   => $this->getTypesOptionsArray()
        ));

        $fieldset->addField('seodata_name', 'select', array(
            'name'     => 'seodata_name',
            'label'    => $this->__('SEO Data Name'),
            'title'    => $this->__('SEO Data Name'),
            'required' => true,
            'values'   => $this->getSoedataNamesOptionsArray()
        ));
        $continueButton = $this->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'     => $this->__('Continue'),
                'onclick'   => "setSettings('".$this->getContinueUrl()."', 'entity_type', 'seodata_name')",
                'class'     => 'save'
            ));
        $fieldset->addField('continue_button', 'note', array(
            'text' => $continueButton->toHtml(),
        ));

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Return url for continue button
     *
     * @return string
     */
    public function getContinueUrl()
    {
        return $this->getUrl('*/*/*', array(
            '_current' => true,
            'entity_type' => '{{entity_type}}',
            'seodata_name' => '{{seodata_name}}'
        ));
    }

    /**
     * Retrieve array (entity_type => entity_name) of available entities
     *
     * @return array
     */
    public function getTypesOptionsArray()
    {
        $types =
            array(0 => $this->helper('adminhtml')->__('-- Please Select --'))
            +
            Mage::getSingleton('tm_seotemplates/source_entity_type')
                ->toOptionHash();
        return $types;
    }

    /**
     * Retrieve array of available seodata names
     *
     * @return array
     */
    public function getSoedataNamesOptionsArray()
    {
        $names =
            array(0 => $this->helper('adminhtml')->__('-- Please Select --'))
            +
            Mage::getSingleton('tm_seotemplates/source_seodata_name')
                ->toOptionHash();
        return $names;
    }
}
