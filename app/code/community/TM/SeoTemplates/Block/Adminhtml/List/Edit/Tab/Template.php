<?php

class TM_SeoTemplates_Block_Adminhtml_List_Edit_Tab_Template
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Template');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Template');
    }

    public function isHidden()
    {
        return false;
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return (bool)$this->getSeoTemplate()->isCompleteToCreate();
    }

    /**
     * Getter
     *
     * @return TM_SeoTemplates_Model_Template
     */
    public function getSeoTemplate()
    {
        return Mage::registry('current_seo_template');
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Settings
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'template_fieldset',
            array(
                'legend'=>$this->__('Template'),
                'class' => 'fieldset-wide'
            )
        );

        $fieldset->addField('template', 'textarea', array(
            'name'  => 'template',
            'label' => $this->__('Template string'),
            'style' => 'height: 4rem;',
        ));

        $fieldset->addField('available_directives', 'note', array(
            'name'     => $this->_fieldPrefix . 'stores_list',
            'label'    => $this->__('Available directives'),
            'text'     => $this->getLayout()->createBlock('core/template')
                ->setTemplate('tm/seotemplates/directives.phtml')
                ->setEntityType($this->getSeoTemplate()->getEntityType())
                ->toHtml(),
        ));

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Initialize form fileds values
     *
     * @return TM_SeoTemplates_Block_Adminhtml_List_Edit_Tab_Main
     */
    protected function _initFormValues()
    {
        $this->getForm()->addValues($this->getSeoTemplate()->getData());
        return parent::_initFormValues();
    }
}
