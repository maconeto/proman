<?php

class TM_SeoTemplates_Block_Adminhtml_List_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _construct()
    {
        parent::_construct();
        $this->setActive(true);
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('General');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return (bool)$this->getSeoTemplate()->isCompleteToCreate();
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Getter
     *
     * @return TM_SeoTemplates_Model_Template
     */
    public function getSeoTemplate()
    {
        return Mage::registry('current_seo_template');
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Settings
     */
    protected function _prepareForm()
    {
        $template = $this->getSeoTemplate();
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend'=> $this->__('General'))
        );

        if ($template->getId()) {
            $fieldset->addField('template_id', 'hidden', array(
                'name' => 'template_id',
            ));
        }

        $this->_addElementTypes($fieldset);

        $isElementDisabled = false;

        $fieldset->addField('entity_type', 'select', array(
            'name'     => 'entity_type',
            'label'    => $this->__('Template for'),
            'title'    => $this->__('Template for'),
            'values'   => $this->getTypesOptionsArray(),
            'disabled' => true
        ));

        $fieldset->addField('seodata_name', 'select', array(
            'name'     => 'seodata_name',
            'label'    => $this->__('SEO Data Name'),
            'title'    => $this->__('SEO Data Name'),
            'values'   => $this->getSoedataNamesOptionsArray(),
            'disabled' => true
        ));

        $fieldset->addField('name', 'text', array(
            'name'  => 'name',
            'label' => Mage::helper('widget')->__('Template Name'),
            'title' => Mage::helper('widget')->__('Template Name'),
            'class' => '',
            'required' => true,
            'disabled'  => $isElementDisabled
        ));

        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('cms')->__('Status'),
            'title'     => Mage::helper('cms')->__('Status'),
            'name'      => 'status',
            'options'   => Mage::getSingleton('cms/page')->getAvailableStatuses(),
            'disabled'  => $isElementDisabled
        ));

        /*
        * Check is single store mode
        */
        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('cms')->__('Store View'),
                'title'     => Mage::helper('cms')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')
                                    ->getStoreValuesForForm(false, true),
                'disabled'  => $isElementDisabled,
            ));
            $renderer = $this->getLayout()
                ->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Retrieve array (entity_type => entity_name) of available entities
     *
     * @return array
     */
    public function getTypesOptionsArray()
    {
        $types =
            array(0 => $this->helper('adminhtml')->__('-- Please Select --'))
            +
            Mage::getSingleton('tm_seotemplates/source_entity_type')
                ->toOptionHash();
        return $types;
    }

    /**
     * Retrieve array of available seodata names
     *
     * @return array
     */
    public function getSoedataNamesOptionsArray()
    {
        $names =
            array(0 => $this->helper('adminhtml')->__('-- Please Select --'))
            +
            Mage::getSingleton('tm_seotemplates/source_seodata_name')
                ->toOptionHash();
        return $names;
    }

    /**
     * Initialize form fileds values
     *
     * @return TM_SeoTemplates_Block_Adminhtml_List_Edit_Tab_Main
     */
    protected function _initFormValues()
    {
        $this->getForm()->addValues($this->getSeoTemplate()->getData());
        return parent::_initFormValues();
    }
}
