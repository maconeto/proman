<?php

class TM_SeoTemplates_Block_Adminhtml_List_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('tmSeoTemplatesGrid');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('tm_seotemplates/template')
            ->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('id', array(
          'header'    => $this->__('ID'),
          'index'     => 'id',
          'type'      => 'number'
        ));

        $this->addColumn('name', array(
            'header'    => $this->__('Title'),
            'index'     => 'name',
        ));

        $this->addColumn('entity_type', array(
            'header'    => $this->__('Template for'),
            'index'     => 'entity_type',
            'type'      => 'options',
            'options'   => Mage::getSingleton('tm_seotemplates/source_entity_type')
                                ->toOptionHash()
        ));

        $this->addColumn('seodata_name', array(
            'header'    => $this->__('SEO Data'),
            'index'     => 'seodata_name',
            'type'      => 'options',
            'options'   => Mage::getSingleton('tm_seotemplates/source_seodata_name')
                                ->toOptionHash()
        ));
        $this->addColumn('status', array(
            'header'    => Mage::helper('cms')->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::getSingleton('cms/page')->getAvailableStatuses(),
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'     => Mage::helper('cms')->__('Store View'),
                'index'      => 'store_id',
                'type'       => 'store',
                'store_all'  => true,
                'store_view' => true,
                'sortable'   => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }

        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
        $this->addColumn('update_time', array(
            'header'    => $this->__('Modified at'),
            'index'     => 'update_time',
            'type'      => 'date', // DO NOT use 'datetime'. It has issues with timezone
            'format'    => $dateFormatIso
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('template_id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('seotemps');

        if ($this->_isAllowedAction('delete')) {
            $this->getMassactionBlock()->addItem('delete', array(
                 'label'    => $this->__('Delete'),
                 'url'      => $this->getUrl('*/*/massDelete'),
                 'confirm'  => $this->__('Are you sure?')
            ));
        }

        // $statuses = Mage::getSingleton('easytabs/config_status')->toOptionArray();
        // array_unshift($statuses, array('label'=>'', 'value'=>''));
        // $this->getMassactionBlock()->addItem('status', array(
        //     'label'=> Mage::helper('easytabs')->__('Change status'),
        //     'url' => $this->getUrl('*/*/massStatus', array('_current'=>true)),
        //     'additional' => array(
        //         'visibility' => array(
        //             'name'   => 'status',
        //             'type'   => 'select',
        //             'class'  => 'required-entry',
        //             'label'  => Mage::helper('easytabs')->__('Status'),
        //             'values' => $statuses
        //         )
        //     )
        // ));

        return $this;
    }

    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->addStoreFilter($value);
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('templates_master/tm_seocore/templates/' . $action);
    }

}
