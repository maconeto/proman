<?php

class TM_SeoTemplates_Block_Adminhtml_List_Edit_Tabs
    extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Internal constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('seo_template_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Metadata Template'));
    }

    protected function _beforeToHtml()
    {
        if (Mage::registry('current_seo_template')->getId()) {
            $this->addTab('log', array(
                'label' => $this->__('Generation Log'),
                'url'   => $this->getUrl('*/*/viewlog', array('_current' => true)),
                'class'    => 'ajax'
            ));
        }

        return parent::_beforeToHtml();
    }

}
