<?php

class TM_SeoTemplates_Block_Adminhtml_List_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * Internal constructor
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_objectId = 'template_id';
        $this->_blockGroup = 'tm_seotemplates';
        $this->_controller = 'adminhtml_list';
    }

    /**
     * Getter
     *
     * @return TM_SeoTemplates_Model_Template
     */
    public function getSeoTemplate()
    {
        return Mage::registry('current_seo_template');
    }

    /**
     * Prepare layout.
     * Adding save_and_continue button
     *
     * @return TM_SeoTemplates_Block_Adminhtml_List_Edit
     */
    protected function _preparelayout()
    {
        if ($this->getSeoTemplate()->isCompleteToCreate()) {
            $this->_addButton(
                'save_and_edit_button',
                array(
                    'label'     => $this->__('Save and Continue Edit'),
                    'class'     => 'save',
                    'onclick'   => 'saveAndContinueEdit()'
                ),
                100
            );
            $this->_formScripts[] =
                "function saveAndContinueEdit(){"
                . " editForm.submit($('edit_form').action + 'back/edit/')"
                ."}";
        } else {
            $this->removeButton('save');
            $this->_formScripts[] = <<<'JS'
function setSettings(urlTemplate, typeElement, seoNameElement) {
    if (!editForm.validate()) {
        return false;
    }

    typeElement = $F(typeElement).replace(/\//g, '-');
    seoNameElement = $F(seoNameElement).replace(/\//g, '-');

    setLocation(new Template(urlTemplate, /(^|.|\r|\n)({{(\w+)}})/).evaluate({
        'entity_type': typeElement,
        'seodata_name': seoNameElement
    }));
}
JS;
        }
        return parent::_prepareLayout();
    }

    /**
     * Return translated header text depending on creating/editing action
     *
     * @return string
     */
    public function getHeaderText()
    {
        if ($this->getSeoTemplate()->getId()) {
            return $this->__(
                'Template "%s"',
                $this->escapeHtml($this->getSeoTemplate()->getName())
            );
        }
        else {
            return $this->__('New Metadata Template');
        }
    }

    /**
     * Return save url for edit form
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save', array('_current'=>true, 'back'=>null));
    }

}
