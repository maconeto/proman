<?php

class TM_SeoTemplates_Block_Adminhtml_List_Edit_Tab_Log
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('metadataTemplateLog');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
    }

    /**
     * Getter
     *
     * @return TM_SeoTemplates_Model_Template
     */
    public function getSeoTemplate()
    {
        return Mage::registry('current_seo_template');
    }

    public function getTable($table)
    {
        return Mage::getSingleton('core/resource')->getTableName($table);
    }

    protected function _prepareCollection()
    {
        $this->setCollection($this->getSeoTemplate()->getLog());
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('log_id', array(
            'header'=> $this->__('ID'),
            'type'  => 'number',
            'index' => 'id',
        ));
        $this->addColumn('log_entity_id', array(
            'header'=> $this->__('Entity Id'),
            'index' => 'entity_id',
        ));
        $this->addColumn('log_entity_name', array(
            'header'=> $this->__('Entity Name'),
            'index' => 'entity_name',
        ));
        $this->addColumn('log_store_id', array(
            'header' => $this->__('Store View'),
            'index'  => 'store_id',
            'type'      => 'options',
            'options' => $this->getStoresOptions()
        ));
        $this->addColumn('log_value', array(
            'header'=> $this->__('value'),
            'index' => 'generated_value',
        ));
        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
        $this->addColumn('log_time', array(
            'header'=> $this->__('time'),
            'index' => 'generation_time',
            'type'      => 'date', // DO NOT use 'datetime'. It has issues with timezone
            'format'    => $dateFormatIso
        ));
    }

    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk(
            'addEntityName',
            array($this->getSeoTemplate()->getEntityType())
        );
        parent::_afterLoadCollection();
    }

    public function getStoresOptions()
    {
        $options = array();
        foreach (Mage::app()->getStores(true) as $store) {
            if ($store->getId() == 0) {
                $options[$store->getId()] = $this->__('All Store Views');
            } else {
                $options[$store->getId()] = $store->getName();
            }
        }
        return $options;
    }

}
