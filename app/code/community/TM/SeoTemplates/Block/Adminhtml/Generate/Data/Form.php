<?php

class TM_SeoTemplates_Block_Adminhtml_Generate_Data_Form
    extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {

        // create object form
        $form = new Varien_Data_Form(
            array(
                'id' => 'run_parameters',
                'action' => '#',
                'onsubmit' => 'generateSeoData.start(); return false;',
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );

        $fieldset = $form->addFieldset('select_fieldset', array());

        $fieldset->addField('entity_type', 'select', array(
            'name'     => 'entity_type',
            'label'    => $this->__('Generate metadata for'),
            'values'   => $this->getTypesOptionsArray()
        ));

        $fieldset->addField('page_size', 'text', array(
            'name'     => 'page_size',
            'label'    => $this->__('Records to process per run'),
            'value'    => 50,
            'class'    => 'validate-number',
        ));

        $fieldset = $form->addFieldset(
            'result_fieldset',
            array(
                // 'legend' => $this->__('General'),
                'class' => 'fieldset-wide',
            )
        );

        // inspirede with http://codepen.io/redlabor/pen/idHeG
        $style = '<style>'
            . '#result_fieldset {display:none;}'
            . '.loading span {'
            . 'display:inline-block; vertical-align:middle; width:.5em; '
            . 'height:.5em; margin:.19em .5em; background:#100C08; '
            . 'border-radius:.3em; animation: loading 1s infinite alternate;'
            . '}'
            . '.loading span:nth-of-type(2){background:#100C08;animation-delay:0.2s;} '
            . '.loading span:nth-of-type(3){background:#080C10;animation-delay:0.4s;} '
            . '.loading span:nth-of-type(4){background:#000000;animation-delay:0.6s;} '
            . '.loading span:nth-of-type(5){background:#232D36;animation-delay:0.8s;} '
            . '.loading span:nth-of-type(6){background:#4A535C;animation-delay:1.0s;} '
            . '.loading span:nth-of-type(7){background:#7C7F83;animation-delay:1.2s;} '
            . '@keyframes loading {0% {opacity: 0;} 100% {opacity: 1;}}'
            . '</style>';


        $fieldset->addField('loader', 'note', array(
            'label' => $this->__('Process is running'),
            'text' => '<div class="loading"><span></span><span></span><span></span><span></span><span></span><span></span><span></span></div>',
            'after_element_html' => $style
        ));


        // $form->setValues($model->getData());
        $form->addValues(array('store_id' => 0));
        $form->setUseContainer(true);
        $this->setForm($form);

        // $this->_appendDependencyJavascript();

        return parent::_prepareForm();
    }

    /**
     * Retrieve array (entity_type => entity_name) of available entities
     *
     * @return array
     */
    public function getTypesOptionsArray()
    {
        $types = Mage::getSingleton('tm_seotemplates/source_entity_type')
                ->toOptionHash();
        $typeAll = implode(array_keys($types), ',');
        $types =
            array($typeAll => $this->__('Products and Categories'))
            +
            $types;
        return $types;
    }

}
