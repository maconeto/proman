<?php

class TM_SeoTemplates_Block_Adminhtml_Generate_Data
    extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'tm_seotemplates';
        $this->_controller = 'adminhtml_generate';
        $this->_mode = 'data';

        parent::__construct();

        $this->_removeButton('reset');
        $this->_removeButton('save');

        $this->_addButton('start', array(
            'label'     => $this->__('Start data generation'),
            'onclick'   => 'generateSeoData.start()',
            'class'     => 'save',
        ), -100);
        $this->_formScripts[] =
            "document.observe(\"dom:loaded\", function(){"
            . "generateSeoData."
            .   "setForm('run_parameters')."
            .   "setUrl('".$this->_getStartUrl()."');"
            . "});";

    }

    /**
     * Retrieve text for header element depending on loaded deal
     *
     * @return string
     */
    public function getHeaderText()
    {
        return $this->__("Generate Metadata");
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    // protected function _isAllowedAction($action)
    // {
    //     return Mage::getSingleton('admin/session')
    //             ->isAllowed('templates_master/tm_botprotection/blacklist/' . $action);
    // }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getStartUrl()
    {
        return $this->getUrl('*/*/start', array(
            // '_current'   => true,
            // 'back'       => 'edit'
        ));
    }

    /**
     * Get URL for back button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/metatemplates_manage/');
    }

    public function hideStartButton()
    {
        $this->_removeButton('start');
        return $this;
    }
}
