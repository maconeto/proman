<?php

class TM_SeoTemplates_Block_Adminhtml_Catalog_Category_Tab
    extends Mage_Adminhtml_Block_Catalog_Form
{

    protected $_seodata;

    protected $_category;

    public function getCategory()
    {
        if (!$this->_category) {
            $this->_category = Mage::registry('category');
        }
        return $this->_category;
    }

    /**
     * Getter og generated data
     *
     * @return TM_SeoTemplates_Model_Template
     */
    public function getSeodata()
    {
        if (!$this->_seodata) {
            $id = $this->getCategory()->getId();
            $entityType = $this->helper()->getEntityTypeCode('category');
            $storeId = $this->getCategory()->getStoreId();
            $this->_seodata = Mage::getModel('tm_seotemplates/seodata')
                ->loadByEntityId($id, $entityType, $storeId);
        }

        return $this->_seodata;
    }

    public function _prepareForm()
    {
        parent::_prepareLayout();
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('tm_seodata_');
        $form->setDataObject($this->getCategory());

        if ($this->getSeodata()->getId() && !$this->_getUseDefaultStatus()) {
            $form->addField('id', 'hidden', array(
                'name' => $this->helper()->prepareFieldName('id'),
                'value' => $this->getSeodata()->getId(),
            ));
        }

        $fieldset = $form->addFieldset(
            'seodata_fieldset',
            array(
                'legend' => $this->__('Metadata'),
                'class' => 'fieldset-wide'
            )
        );

        $listOfStores = $this->_getListOfStores();
        if ($listOfStores) {
            $fieldset->addField('stores_list', 'note', array(
                    'name'     => $this->_fieldPrefix . 'stores_list',
                    'label'    => $this->__('There are generated data for'),
                    'title'    => $this->__('There are generated data for'),
                    'text'     => '<i>' . $listOfStores . '</i>'
                ));
        }

        $seodataList = array_keys($this->helper()->getCodes('seodata'));
        foreach ($seodataList as $name) {
            $fieldset->addField($name, 'textarea', array(
                'name'     => $this->helper()->prepareFieldName($name),
                'label'    => $this->helper()->prepareFieldLabel($name),
                'title'    => $this->helper()->prepareFieldLabel($name),
                'style'    => 'height:4em',
                'readonly' => true
            ));
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _initFormValues()
    {
        $seodataList = array_keys($this->helper()->getCodes('seodata'));
        foreach ($seodataList as $name)
            $this->getForm()->addValues(
                array(
                    $name => $this->getSeodata()->getData($name)
                )
            );
        return parent::_initFormValues();
    }

    public function helper($name = 'tm_seotemplates')
    {
        return parent::helper($name);
    }

    protected function _getUseDefaultStatus()
    {
        if (!$this->getSeodata()->getId()
            && $this->getCategory()->getStoreId() != Mage_Core_Model_App::ADMIN_STORE_ID) {
            // disable when there are no Seodata and current store is not 'All store views'
            return true;
        } elseif ($this->getSeodata()->getStoreId() != $this->getCategory()->getStoreId()) {
            // disable when Seodata store is not equal to current store
            return true;
        }
        return false;
    }

    protected function _getListOfStores()
    {
        $entityTypeProduct = Mage::helper('tm_seotemplates')
            ->getEntityTypeCode('category');
        $category = Mage::registry('category');
        $collection = $this->getSeodata()->getCollection();
        $collection
            ->addFilter('entity_type', array('eq' => $entityTypeProduct))
            ->addFilter('entity_id', array('eq' => $category->getId()))
            ->getSelect()->order('store_id');
        $str = '';
        foreach ($collection as $data) {
            if ($data->getStoreId() == Mage_Core_Model_App::ADMIN_STORE_ID) {
                $str .= $this->__('All Store Views');
            } else {
                $str .= Mage::app()->getStore($data->getStoreId())->getName();
            }
            $str .= ', ';
        }
        return rtrim($str, ' ,');
    }

}
