<?php

class TM_SeoTemplates_Block_Adminhtml_Catalog_Product_Tab
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected $_fieldPrefix = 'tm_seodata_';

    protected $_seodata;

    protected $_product;

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('SEO Suite');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('SEO Suite');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return (bool)$this->getProduct()->getId();
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = Mage::registry('product');
        }
        return $this->_product;
    }

    /**
     * Getter of generated data
     *
     * @return TM_SeoTemplates_Model_Template
     */
    public function getSeodata()
    {
        if (!$this->_seodata) {
            $id = $this->getProduct()->getId();
            $entityType = $this->helper()->getEntityTypeCode('product');
            $storeId = $this->getProduct()->getStoreId();
            $this->_seodata = Mage::getModel('tm_seotemplates/seodata')
                ->loadByEntityId($id, $entityType, $storeId);
        }
        return $this->_seodata;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Widget_Block_Adminhtml_Widget_Instance_Edit_Tab_Settings
     */
    protected function _prepareForm()
    {
        // $template = $this->getSeoTemplate();
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post',
        ));
        $form->setHtmlIdPrefix('tm_seodata_');

        if ($this->getSeodata()->getId() && !$this->_getUseDefaultStatus()) {
            $form->addField('id', 'hidden', array(
                'name' => $this->helper()->prepareFieldName('id'),
                'value' => $this->getSeodata()->getId(),
            ));
        }

        $fieldset = $form->addFieldset(
            'seodata_fieldset',
            array(
                'legend'=> $this->__('Metadata'),
                'class' => 'fieldset-wide'
            )
        );

        $listOfStores = $this->_getListOfStores();
        if ($listOfStores) {
            $fieldset->addField('stores_list', 'note', array(
                    'name'     => $this->_fieldPrefix . 'stores_list',
                    'label'    => $this->__('There are generated data for'),
                    'title'    => $this->__('There are generated data for'),
                    'text'     => '<i>' . $listOfStores . '</i>'
                ));
        }

        $seodataList = array_keys($this->helper()->getCodes('seodata'));
        foreach ($seodataList as $name) {
            $fieldset->addField($name, 'textarea', array(
                'name'     => $this->helper()->prepareFieldName($name),
                'label'    => $this->helper()->prepareFieldLabel($name),
                'title'    => $this->helper()->prepareFieldLabel($name),
                'style'    => 'height:4em',
                'readonly' => true,
            ));
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _initFormValues()
    {
        $seodataList = array_keys($this->helper()->getCodes('seodata'));
        foreach ($seodataList as $name)
            $this->getForm()->addValues(
                array(
                    $name => $this->getSeodata()->getData($name)
                )
            );
        return parent::_initFormValues();
    }

    public function helper($name = 'tm_seotemplates')
    {
        return parent::helper($name);
    }

    protected function _getUseDefaultStatus()
    {
        if (!$this->getSeodata()->getId()
            && $this->getProduct()->getStoreId() != Mage_Core_Model_App::ADMIN_STORE_ID) {
            // disable when there are no Seodata and current store is not 'All store views'
            return true;
        } elseif ($this->getSeodata()->getStoreId() != $this->getProduct()->getStoreId()) {
            // disable when Seodata store is not equal to current store
            return true;
        }
        return false;
    }

    protected function _getListOfStores()
    {
        $entityTypeProduct = Mage::helper('tm_seotemplates')
            ->getEntityTypeCode('product');
        $product = Mage::registry('product');
        $collection = $this->getSeodata()->getCollection();
        $collection
            ->addFilter('entity_type', array('eq' => $entityTypeProduct))
            ->addFilter('entity_id', array('eq' => $product->getId()))
            ->getSelect()->order('store_id');
        $str = '';
        foreach ($collection as $data) {
            if ($data->getStoreId() == Mage_Core_Model_App::ADMIN_STORE_ID) {
                $str .= $this->__('All Store Views');
            } else {
                $str .= Mage::app()->getStore($data->getStoreId())->getName();
            }
            $str .= ', ';
        }
        return rtrim($str, ' ,');
    }

}
