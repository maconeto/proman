<?php

class TM_SeoTemplates_Block_Adminhtml_List
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'tm_seotemplates';
        $this->_controller = 'adminhtml_list';

        parent::__construct();

        if ($this->_isAllowedAction('save')) {
            $this->_updateButton(
                    'add',
                    'label',
                    $this->__('New Template')
                );
            $this->_addButton('apply', array(
                'label'     => $this->__('Generate Metadata'),
                'onclick'   => 'setLocation(\'' . $this->getUrl('*/metatemplates_generate') .'\')',
                'title'     => $this->__('Generate Metadata Manualy'),
                'class'     => 'back',
            ));
        } else {
            $this->_removeButton('add');
        }
    }

    public function getHeaderCssClass()
    {
        return 'head-newsletter-list ' . parent::getHeaderCssClass();
    }

    protected function _beforeToHtml()
    {
        $this->_headerText = Mage::helper('tm_seotemplates')
            ->__('Metadata Templates');
        return parent::_beforeToHtml();
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('templates_master/tm_seocore/templates/' . $action);
    }

}
