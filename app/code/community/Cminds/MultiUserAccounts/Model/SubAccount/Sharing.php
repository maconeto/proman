<?php

class Cminds_MultiUserAccounts_Model_SubAccount_Sharing extends Varien_Object
{
    const SHARE_MASTER = 1;
    const SHARE_SIBLINGS = 2;
    const SHARE_MASTER_SIBLINGS = 3;

    /**
     * Retrieve option array
     *
     * @return array
     */
    static public function getOptionArray()
    {
        $helper = Mage::helper('cminds_multiuseraccounts');

        return array(
            self::SHARE_MASTER => $helper->__('Master Only'),
            self::SHARE_SIBLINGS => $helper->__('Siblings Only'),
            self::SHARE_MASTER_SIBLINGS => $helper->__('Master & Siblings'),
        );

    }

    public function toOptionArray() {

        $canSee = array();
        foreach ($this->getOptionArray() as $value => $label) {
            $canSee[] = array(
                'value' => $value,
                'label' => $label,
            );
        }
        return $canSee;
    }

    /**
     * Retrieve all options
     *
     * @return array
     */
    static public function getAllOption()
    {
        $options = self::getOptionArray();
        array_unshift($options, array('value' => '', 'label' => ''));
        return $options;
    }

    /**
     * Retireve all options
     *
     * @return array
     */
    static public function getAllOptions()
    {
        $res = array();
        $res[] = array('value' => '', 'label' => Mage::helper('cminds_multiuseraccounts')->__('-- Please Select --'));
        foreach (self::getOptionArray() as $index => $value) {
            $res[] = array(
                'value' => $index,
                'label' => $value
            );
        }
        return $res;
    }

    /**
     * Retrieve option text
     *
     * @param int $optionId
     * @return string
     */
    static public function getOptionText($optionId)
    {
        $options = self::getOptionArray();
        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
