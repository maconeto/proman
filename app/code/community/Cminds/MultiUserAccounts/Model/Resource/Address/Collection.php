<?php

class Cminds_MultiUserAccounts_Model_Resource_Address_Collection extends Mage_Customer_Model_Resource_Address_Collection
{
    /**
     * Set customer filter
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_Model_Resource_Address_Collection
     */
    public function setCustomerFilter($customer)
    {
        if ($customer->getId()) {
            $dataHelper = Mage::helper("cminds_multiuseraccounts");

            if($dataHelper->isSharingAddressBook()) {
                $allowedCustomerIds = array($customer->getId());
                if (
                    $dataHelper->getSharingAddressRange() == Cminds_MultiUserAccounts_Model_SubAccount_Sharing::SHARE_MASTER
                    || $dataHelper->getSharingAddressRange() == Cminds_MultiUserAccounts_Model_SubAccount_Sharing::SHARE_MASTER_SIBLINGS
                ) {
                    $allowedCustomerIds[] = $customer->getParentCustomer()->getId();
                }

                if (
                    $dataHelper->getSharingAddressRange() == Cminds_MultiUserAccounts_Model_SubAccount_Sharing::SHARE_SIBLINGS
                    || $dataHelper->getSharingAddressRange() == Cminds_MultiUserAccounts_Model_SubAccount_Sharing::SHARE_MASTER_SIBLINGS
                ) {
                    $childrenIds = $customer->getParentCustomer()->getChildrenIds();
                    if (is_array($childrenIds)) {
                        $allowedCustomerIds = array_merge($allowedCustomerIds, $childrenIds);
                    }
                }

                $this->addAttributeToFilter('parent_id', array("in" => $allowedCustomerIds));
            } else {
                $this->addAttributeToFilter('parent_id', $customer->getId());
            }
        } else {
            $this->addAttributeToFilter('parent_id', '-1');
        }
        return $this;
    }
}
