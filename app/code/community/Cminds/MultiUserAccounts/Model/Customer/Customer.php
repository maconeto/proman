<?php

/**
 * @author CreativeMindsSolutions
 */
class Cminds_MultiUserAccounts_Model_Customer_Customer extends Mage_Customer_Model_Customer
{
    public function authenticate($login, $password)
    {
        $helper = Mage::helper('cminds_multiuseraccounts');
        // We need to websiteId value before load By Email
        $subAccountMode = false;
        $websiteId = $this->getWebsiteId();
        $useSubAccount = Mage::helper('cminds_multiuseraccounts')->isEnabled();

        $this->loadByEmail($login);
        $account = $this;

        if ((!$account->getId() && $useSubAccount)
            || ($helper->ifShareSession()
                && $helper->isSubAccount($account->getId())
                && $useSubAccount)
        ) {
            // No Main Account found try with SubAccount
            $account = Mage::getModel('cminds_multiuseraccounts/subAccount')
                ->setWebsiteId($websiteId);

            $account->loadByEmail($login);
            $subAccountMode = true;
        }

        if ($account->getConfirmation() && $account->isConfirmationRequired()) {
            throw Mage::exception(
                'Mage_Core',
                Mage::helper('customer')->__('This account is not confirmed.'),
                self::EXCEPTION_EMAIL_NOT_CONFIRMED
            );
        }

        if (!$account->validatePassword($password)) {
            throw Mage::exception(
                'Mage_Core',
                Mage::helper('customer')->__('Invalid login or password.'),
                self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
            );
        }

        if ($subAccountMode) {
            $this->load($account->getParentCustomerId());
            $customerSession = Mage::getSingleton('customer/session');
            $customerSession->setSubAccount($account);
            $beforeUrl = $customerSession->getBeforeAuthUrl();

            if (strpos($beforeUrl, 'customer/account/subAccount') !== false) {
                $customerSession->setBeforeAuthUrl(
                    Mage::helper('customer')->getAccountUrl()
                );
            }
        }

        Mage::dispatchEvent(
            'customer_customer_authenticated',
            array(
                'model' => $this,
                'password' => $password,
            )
        );

        return true;
    }

    /**
     * @return bool
     */
    public function canManageUsers()
    {
        $allowedGroups = Mage::helper('cminds_multiuseraccounts')->getAllowedGroups();
        return in_array($this->getGroupId(), $allowedGroups);
    }

    /**
     * @return bool|Mage_Core_Model_Abstract
     */
    public function getParentCustomer()
    {
        $subAccount = Mage::getModel('cminds_multiuseraccounts/subAccount')->load($this->getId(), "customer_id");

        if ($subAccount->getParentCustomerId()) {
            return Mage::getModel("customer/customer")->load($subAccount->getParentCustomerId());
        }

        return false;
    }

    public function getSubaccounts()
    {
        return Mage::getModel("cminds_multiuseraccounts/subAccount")->getSubAccounts($this);
    }

    public function getChildrenIds()
    {
        $children = $this->getSubaccounts();

        $ids = array();

        foreach ($children as $child) {
            $ids[] = $child->getCustomerId();
        }

        return $ids;
    }

    public function getAddressesCollection()
    {
        if ($this->_addressesCollection === null) {
            $this->_addressesCollection = $this->getAddressCollection()
                ->setCustomerFilter($this)
                ->addAttributeToSelect('*');
        }

        return $this->_addressesCollection;
    }
}