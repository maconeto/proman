<?php
class Cminds_MultiUserAccounts_Block_Adminhtml_Customer_Grid extends Mage_Adminhtml_Block_Customer_Grid {
    public function getRowClass($_item) {
        $helper = Mage::helper('cminds_multiuseraccounts');

        if ($helper->isEnabled()) {
            $subAccountCollectionAsParent = Mage::getModel('cminds_multiuseraccounts/subAccount')
                ->getCollection()
                ->addFieldToFilter("parent_customer_id", $_item->getId());

            $subAccountCollection = Mage::getModel('cminds_multiuseraccounts/subAccount')
                ->getCollection()
                ->addFieldToFilter("customer_id", $_item->getId());

            if (
                $subAccountCollectionAsParent->getSize() > 0
                || ($subAccountCollectionAsParent->getSize() == 0 && $subAccountCollection->getSize() == 0)
            ) {
                return "parent-customer";
            }

            if ($subAccountCollection->getSize() > 0) {
                return "sub-customer";
            }
        }
    }
}