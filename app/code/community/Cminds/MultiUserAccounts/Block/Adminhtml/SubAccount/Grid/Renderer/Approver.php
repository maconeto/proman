<?php

class Cminds_MultiUserAccounts_Block_Adminhtml_SubAccount_Grid_Renderer_Approver
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $helper = Mage::helper('cminds_multiuseraccounts');
        $permissionValue =  $row->getData('is_approver');

        $returnString = '';

        if ($permissionValue) {
            $returnString = $helper->__("Yes");
        } else {
            $returnString = $helper->__("No");
        }

        if($permissionValue && $row->getData("max_amount_to_approve")) {
            $returnString .= sprintf(" (up to %s)", Mage::helper('core')->currency($row->getData("max_amount_to_approve"), true, false));
        }

        return $returnString;
    }
}