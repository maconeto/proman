<?php

class Cminds_MultiUserAccounts_Block_Adminhtml_SubAccount_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $subAccount = Mage::registry('sub_account');

        $data = $subAccount->getData();

        $mode = 'edit';
        if ($subAccount && $subAccount->getId()) {
            $urlParams = array(
                'id' => $this->getRequest()->getParam('id')
            );
        } else {
            $mode = 'new';
            $urlParams = array(
                'parent_customer_id' => $this->getRequest()->getParam('parent_customer_id')
            );
            $data['parent_customer_id'] = $this->getRequest()->getParam('parent_customer_id');
        }

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/' . $mode . 'Post', $urlParams),
            'method' => 'post',
        ));

        $form->setHtmlIdPrefix('_subaccount');
        $form->setFieldNameSuffix('subaccount');

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
