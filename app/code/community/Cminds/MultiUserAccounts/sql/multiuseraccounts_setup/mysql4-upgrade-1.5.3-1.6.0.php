<?php
$installer=$this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('cminds_multiuseraccounts/subAccount'),
        'max_amount_to_approve',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
            'nullable' => true,
            'scale'     => 4,
            'precision' => 12,
            'default'   => '0.0000',
            'length'    => 255,
            'comment' => 'Maximum amount of the cart that subaccount can approve'
        )
    );
$installer->endSetup();