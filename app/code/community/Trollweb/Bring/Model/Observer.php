<?php

class Trollweb_Bring_Model_Observer
{
    
    public function setActive($observer)
    {
        $config = $observer->getObject();
        $groups = $config->getGroups();
        if (!isset($groups['bring_fraktguiden'])) {
            return;
        }

        $groups['bringdoren']['fields']['active'] = $groups['bring_fraktguiden']['fields']['isactive'];
        $groups['bringlevert']['fields']['active'] = $groups['bring_fraktguiden']['fields']['isactive'];
        $groups['bringhent']['fields']['active'] = $groups['bring_fraktguiden']['fields']['isactive'];

        $config->setGroups($groups);

    }
    
    public function core_config_data_save_after($observer) {
        $configData = $observer->getEvent()->getConfigData();
    
        if ($configData && $configData->getPath() == 'carriers/bring_fraktguiden/isactive' && $configData->getValue() == 1) {
            Mage::register('bring_licens_status', true);
        }

        if (Mage::registry('bring_licens_status')) {
            if (Mage::registry('bring_licens_updated')) {
                return $this;
            }
            else {
                Mage::register('bring_licens_updated', true);
            }
            $fraktGuiden = Mage::getModel('bring/shipping_fraktguiden');
            $fraktGuiden->saveConfigData();
        }
    
        return $this;
    }
}