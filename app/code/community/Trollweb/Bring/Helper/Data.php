<?php

/**
 * Bring Fraktguiden Freight Integration (Norway)
 * 
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any 
 * part of it. Please contact us by email at post@trollweb.no or 
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 * 
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2009 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 * 
 */ 

/**
 * Data helper
 */
class Trollweb_Bring_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function savePostautomat($observer)
    {
        $order = $observer->getOrder();
        $shipping_method = $order->getShippingMethod();
        
        if($shipping_method == 'bring_fraktguiden_POSTAUTOMAT_NEXTDAY') {
            
            $data = Mage::getSingleton('checkout/session')->getData('postautomat', array());
            
            // Confirm the order using API   
            $confirm_url = 'http://fraktguide.bring.no/fraktguide/postautomat/bekreftAutomatreservasjon.do?bookingNumber=' . $data['bookingNumber'];
            
            // Save data from Postautomat to order
            $order->setPostautomatProductCode($data['productCode']);
            $order->setPostautomatMachineId($data['machineId']);
            $order->setPostautomatMachineName($data['machineName']);
            $order->setPostautomatAddress($data['address']);
            $order->setPostautomatPostalCode($data['postalCode']);
            $order->setPostautomatCity($data['city']);
            $order->setPostautomatLocationDescription($data['locationDescription']);
            $order->setPostautomatBookingNumber($data['bookingNumber']);
            $order->setPostautomatShippingDate($data['shippingDate']);
            $order->setPostautomatType($data['type']);
            $order->setPostautomatCorrelationId($data['correlationId']);
            $order->setPostautomatPublicId($data['publicId']);
            
            Mage::getSingleton('checkout/session')->setData('postautomat', array());
        }        
    }

    public function getMeasurementInCentimeters($measurement) {
        $unit = Mage::getStoreConfig('carriers/bring_fraktguiden/volume_unit');
        $divider = ($unit === "mm") ? 10.0 : 1.0;
        return (double)$measurement / $divider;
    }

    public function getVersion() {
        return (string) Mage::getConfig()->getNode()->modules->Trollweb_Bring->version;
    }

    public function getPostofficeId($shippingmethod)
    {
        $parts = explode("_",$shippingmethod);
        $id = array_pop($parts);

        if (preg_match('/^[0-9]+$/',$id)) {
            return $id;
        }

        return false;
    }

    public function getPostofficeData($postofficeId)
    {
        $po = Mage::getModel('bring/postoffice')->load($postofficeId,'postoffice_id');
        if ($po->getId()) {
            return json_decode($po->getPostofficeData());
        }
        return false;
    }

    public function log($message, $level = Zend_Log::DEBUG)
    {
        Mage::log($message, $level, 'trollweb_bring.log', true);
    }

    public function getShowRelativePrice()
    {
        return Mage::getStoreConfig('carriers/bring_fraktguiden/show_relative_price');
    }

    public function getRelativeTo()
    {
        return Mage::getStoreConfig('carriers/bring_fraktguiden/relative_to');
    }

    /**
     * @param $shippingMethod string
     * @param $rates
     *
     * @return string|null
     */
    public function getRelativePrice($rates)
    {
        $relativePrice = null;
        if (!$this->getShowRelativePrice()) {
            $this->log("Relative price not enabled");
        } elseif (!$this->getRelativeTo()) {
            $this->log("Relative price enabled but no method selected");
        } elseif (!$rates) {
            $this->log("Rates is null!");
        } else {
            $relative = 'bringlevert_PA_DOREN';
            $relativeTo = 'bringlevert_' . $this->getRelativeTo();
            $relativeMethodDescription = null;
            $aPrice = null;
            $bPrice = null;

            foreach ($rates as $r) {
                $code = $r->getCode();
                $price = $r->getPrice();

                if ($code == $relative) {
                    $aPrice = $price;
                } elseif ($code == $relativeTo) {
                    $relativeMethodDescription = $r->getMethodTitle();
                    $bPrice = $price;
                }
            }

            if ($aPrice !== null && $bPrice !== null) {
                $relativePrice = $aPrice - $bPrice;
                Mage::log('HUehuehue');
                $relativePrice =
                    '<span class="price">' . ($relativePrice < 0 ? '-' : '+') . $relativePrice . '</span><br>' . '<span class="relative-description">I forhold till ' . $relativeMethodDescription . '</span>';
            }
        }

        return $relativePrice;
    }

    public function getMybringCredentials() {
        $useMybring = Mage::getStoreConfig('carriers/bring_fraktguiden/use_mybring_account') === "1";
        if (!$useMybring) {
            return null;
        }

        $userId = Mage::getStoreConfig('carriers/bring_fraktguiden/mybring_api_user_id');
        $apiKey = Mage::getStoreConfig('carriers/bring_fraktguiden/mybring_api_key');
        $customerNumber = Mage::getStoreConfig('carriers/bring_fraktguiden/mybring_customer_number');

        if (!$userId || !$apiKey || !$customerNumber) {
            return null;
        }

        return [
            "userId" => $userId,
            "apiKey" => $apiKey,
            "customerNumber" => $customerNumber,
        ];
    }

    public function getBringClient() {
        $wsdlPath = Mage::getModuleDir('etc', 'Trollweb_Bring') . '/' . Trollweb_Bring_Model_Shipping_Fraktguiden::FRAKTGUIDEN_WSDL;
        $clientUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);

        $mybringCreds = $this->getMybringCredentials();

        if (!$mybringCreds) {
            return new Trollweb_Bring_Model_Shipping_Fraktguiden_SoapClient($wsdlPath);
        }

        $mybringHeaders = implode("\r\n", array(
            "X-MyBring-API-Uid: {$mybringCreds['userId']}",
            "X-MyBring-API-Key: {$mybringCreds['apiKey']}",
            "X-Bring-Client-URL: {$clientUrl}",
        ));

        $soapOptions = [
            'trace' => true,
            'stream_context' => stream_context_create([
                'http' => ['header' => $mybringHeaders]
            ])
        ];

        return new Trollweb_Bring_Model_Shipping_Fraktguiden_SoapClient($wsdlPath, $soapOptions);
    }
} 
