<?php

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('bring/postoffices'))

    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')

    ->addColumn('postoffice_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
        'nullable'  => false,
    ), 'Servicepoint Id')

    ->addColumn('postoffice_data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false
    ), 'Servicepoint Data')

    ->addIndex(
        $installer->getIdxName('bring/postoffices', array('postoffice_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('postoffice_id'),
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    );

$installer->getConnection()->createTable($table);
$installer->endSetup();

