<?php
/**
 * Bring Fraktguiden Freight Integration (Norway)
 *
 * LICENSE AND USAGE INFORMATION
 * It is NOT allowed to modify, copy or re-sell this file or any
 * part of it. Please contact us by email at post@trollweb.no or
 * visit us at www.trollweb.no if you have any questions about this.
 * Trollweb is not responsible for any problems caused by this file.
 *
 * Visit us at http://www.trollweb.no today!
 *
 * @category   Trollweb
 * @package    Trollweb_Bring
 * @copyright  Copyright (c) 2010 Trollweb (http://www.trollweb.no)
 * @license    Single-site License
 *
 */

class Trollweb_Bring_Block_Adminhtml_Form_Field_Checkbox extends Mage_Core_Block_Abstract
{
    public function setInputName($value)
    {
        return $this->setName($value);
    }


    public function setId($id)
    {
        $this->setData('id', $id);
        return $this;
    }

    public function getId()
    {
        return $this->getData('id');
    }


    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
      return '<input type="checkbox" name="'.$this->getName().'" id="'.$this->getId().'" value="1" #{'.$this->getId().'_checked} />';
    }


    public function checkedHash($name)
    {

    }

}