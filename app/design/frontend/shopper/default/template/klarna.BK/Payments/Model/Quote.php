<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna quote to associate a Klarna quote with a Magento quote
 *
 * @method string getSessionId()
 * @method string getClientToken()
 * @method string getAuthorizationToken()
 * @method int getIsActive()
 * @method int getQuoteId()
 * @method Klarna_Payments_Model_Quote setSessionId(string $value)
 * @method Klarna_Payments_Model_Quote setClientToken(string $value)
 * @method Klarna_Payments_Model_Quote setAuthorizationToken(string $value)
 * @method Klarna_Payments_Model_Quote setIsActive(int $value)
 * @method Klarna_Payments_Model_Quote setQuoteId(int $value)
 */
class Klarna_Payments_Model_Quote extends Mage_Core_Model_Abstract
{
    /**
     * Init
     */
    public function _construct()
    {
        $this->_init('klarna_payments/quote');
    }

    /**
     * Load by session id
     *
     * @param string $sessionId
     *
     * @return Klarna_Payments_Model_Quote
     */
    public function loadBySessionId($sessionId)
    {
        return $this->load($sessionId, 'session_id');
    }

    /**
     * Load active Klarna quote object by quote
     *
     * @param Mage_Sales_Model_Quote $quote
     * @param string                 $paymentMethod
     *
     * @return Klarna_Payments_Model_Quote
     */
    public function loadActiveByQuote(Mage_Sales_Model_Quote $quote, $paymentMethod = 'klarna_payments')
    {
        $this->_getResource()->loadActive($this, $quote->getId(), $paymentMethod);
        $this->_afterLoad();

        return $this;
    }
}
