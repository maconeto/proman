<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna payments payment method
 */
class Klarna_Payments_Model_Payment_Payments extends Klarna_Core_Model_Payment_Method_Abstract
{
    protected $_code          = 'klarna_payments';
    protected $_formBlockType = 'klarna_payments/form_payments';
    protected $_infoBlockType = 'klarna_payments/info_payments';

    /**
     * Availability options
     */
    protected $_isGateway                 = false;
    protected $_canOrder                  = false;
    protected $_canAuthorize              = true;
    protected $_canCapture                = true;
    protected $_canCapturePartial         = true;
    protected $_canRefund                 = true;
    protected $_canRefundInvoicePartial   = true;
    protected $_canVoid                   = true;
    protected $_canUseInternal            = false;
    protected $_canUseCheckout            = true;
    protected $_canUseForMultishipping    = false;
    protected $_canFetchTransactionInfo   = true;
    protected $_canCreateBillingAgreement = false;
    protected $_canReviewPayment          = false;
    protected $_isInitializeNeeded        = false;

    /**
     * @var Klarna_Payments_Model_Quote
     */
    protected $_klarnaQuote = null;

    /**
     * Check if the Klarna Payments is available
     *
     * @param Mage_Sales_Model_Quote|null $quote
     *
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        if (null === $quote) {
            return parent::isAvailable($quote);
        }

        if (!parent::isAvailable($quote)) {
            return false;
        }

        if (!$quote->getIsActive()) {
            return true;
        }

        $isAvailable = false;

        try {
            Mage::helper('klarna_payments/checkout')->initKlarnaPayments();
            $isAvailable = true;
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return !$isAvailable ? false : parent::isAvailable($quote);
    }

    /**
     * Assign method variables
     *
     * @param mixed $data
     *
     * @return $this
     */
    public function assignData($data)
    {
        $info = $this->getInfoInstance();

        if ($data->getAuthorizationToken()) {
            $this->_getKlarnaQuote($info->getQuote())
                ->setAuthorizationToken($data->getAuthorizationToken())
                ->save();
        }

        return $this;
    }

    /**
     * Validate the authorization payment method
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function validate()
    {
        $info  = $this->getInfoInstance();
        $quote = null;

        if ($info instanceof Mage_Sales_Model_Quote_Payment) {
            $quote = $info->getQuote();
        }

        if ($info instanceof Mage_Sales_Model_Order_Payment) {
            $quote = $info->getOrder()->getQuote();
        }

        if (null !== $quote) {
            $klarnaQuote = $this->_getKlarnaQuote($quote);

            if ($info->getAuthorizationToken()) {
                $klarnaQuote = $klarnaQuote->setAuthorizationToken($info->getAuthorizationToken())
                    ->save();
            }

            if (!$klarnaQuote->getAuthorizationToken()) {
                Mage::throwException($this->_getHelper()->__("Authorization Token is a required field."));
            }
        }

        return $this;
    }

    /**
     * Authorize payment method
     *
     * @param Varien_Object $payment
     * @param float         $amount
     *
     * @return $this
     */
    public function authorize(Varien_Object $payment, $amount)
    {
        $klarnaQuote = $this->_getKlarnaQuote($payment->getOrder()->getQuote());
        $result      = $this->getPurchaseApi()->placeOrder(
            $klarnaQuote->getAuthorizationToken(), $this->getPurchaseApi()->getGeneratedPlaceRequest()
        );

        if ($result->getIsSuccessful()) {
            switch ($result->getFraudStatus()) {
                case self::FRAUD_STATUS_REJECTED:
                    $payment->setIsFraudDetected(true);
                    break;
                case self::FRAUD_STATUS_PENDING:
                    $payment->setIsTransactionPending(true);
                    break;
            }

            Mage::unregister('klarna_payments_redirect_url');
            Mage::register('klarna_payments_redirect_url', $result->getRedirectUrl());

            $klarnaOrder = Mage::getModel('klarna_core/order');

            $klarnaOrder->setData(
                array(
                'session_id'     => $result->getId(),
                'reservation_id' => $result->getId(),
                'order_id'       => $payment->getOrder()->getId()
                )
            );
            $klarnaOrder->save();

            if (!$klarnaOrder->getId() || !$klarnaOrder->getReservationId()) {
                Mage::throwException('Unable to authorize payment for this order.');
            }

            $payment->setTransactionId($result->getId())->setIsTransactionClosed(0);
        } else {
            Mage::helper('klarna_payments/checkout')->cancelKlarnaQuoteAuthorizationToken($klarnaQuote);
            Mage::throwException('Unable to authorize payment for this order.');
        }

        return $this;
    }

    /**
     * Get redirect url
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::registry('klarna_payments_redirect_url');
    }

    /**
     * Get Klarna quote for a sales quote
     *
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return Klarna_Payments_Model_Quote
     */
    protected function _getKlarnaQuote($quote)
    {
        if (null === $this->_klarnaQuote) {
            $this->_klarnaQuote = Mage::getModel('klarna_payments/quote')->loadActiveByQuote($quote);
        }

        return $this->_klarnaQuote;
    }
}
