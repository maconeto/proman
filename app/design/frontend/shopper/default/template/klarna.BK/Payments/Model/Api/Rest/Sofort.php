<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna Payments rest integration with Klarna
 */
class Klarna_Payments_Model_Api_Rest_Sofort extends Klarna_Core_Model_Api_Rest_Client_Abstract
{
    /**
     * Create new session
     *
     * @param array $data
     *
     * @return Klarna_Core_Model_Api_Rest_Client_Response
     */
    public function createSession(array $data)
    {
        $url = array(
            'direct-bank-transfer',
            'v1',
            'sessions'
        );

        $request = $this->getNewRequestObject()
            ->setUrl($url)
            ->setIdField('session_id')
            ->setMethod(Klarna_Core_Model_Api_Rest_Client::REQUEST_METHOD_POST)
            ->setDefaultErrorMessage('Error: Unable to create session.')
            ->setParams($data);

        return $this->request($request);
    }

    /**
     * Update session
     *
     * @param string $id
     * @param array  $data
     *
     * @return Klarna_Core_Model_Api_Rest_Client_Response
     * @throws Klarna_Core_Model_Api_Exception
     */
    public function updateSession($id = null, array $data = array())
    {
        if (null === $id) {
            throw new Klarna_Core_Model_Api_Exception('Klarna session id required for update');
        }

        $url = array(
            'direct-bank-transfer',
            'v1',
            'sessions',
            $id
        );

        $request = $this->getNewRequestObject()
            ->setUrl($url)
            ->setIdField('session_id')
            ->setMethod(Klarna_Core_Model_Api_Rest_Client::REQUEST_METHOD_POST)
            ->setDefaultErrorMessage('Error: Unable to update session.')
            ->setParams($data);

        return $this->request($request);
    }

    /**
     * Place order
     *
     * @param string $id
     * @param array  $data
     *
     * @return Klarna_Core_Model_Api_Rest_Client_Response
     * @throws Klarna_Core_Model_Api_Exception
     */
    public function placeOrder($id = null, array $data = array())
    {
        if (null === $id) {
            throw new Klarna_Core_Model_Api_Exception('Klarna token id required to place order');
        }

        $url = array(
            'direct-bank-transfer',
            'v1',
            'authorizations',
            $id,
            'order'
        );

        $request = $this->getNewRequestObject()
            ->setUrl($url)
            ->setIdField('order_id')
            ->setMethod(Klarna_Core_Model_Api_Rest_Client::REQUEST_METHOD_POST)
            ->setDefaultErrorMessage('Error: Unable to create order.')
            ->setParams($data);

        return $this->request($request);
    }

    /**
     * Cancel order authorization
     *
     * @param string $id
     *
     * @return Klarna_Core_Model_Api_Rest_Client_Response
     * @throws Klarna_Core_Model_Api_Exception
     */
    public function cancelAuthorization($id)
    {
        if (null === $id) {
            throw new Klarna_Core_Model_Api_Exception('Klarna token id required to cancel authorization');
        }

        $url = array(
            'direct-bank-transfer',
            'v1',
            'authorizations',
            $id
        );

        $request = $this->getNewRequestObject()
            ->setUrl($url)
            ->setIdField('order_id')
            ->setMethod(Klarna_Core_Model_Api_Rest_Client::REQUEST_METHOD_DELETE)
            ->setDefaultErrorMessage('Error: Unable to cancel authorization.');

        return $this->request($request);
    }
}
