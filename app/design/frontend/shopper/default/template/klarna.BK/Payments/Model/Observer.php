<?php
/**
 * Copyright 2017 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 * @author     Joe Constant <joe.constant@klarna.com>
 */

/**
 * Klarna Payments observer methods
 */
class Klarna_Payments_Model_Observer
{
    /**
     * Because of Klarna Payments's redirect, Magento does not send the order email.
     *
     * This method will trigger the email sending even though there is a redirect
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkoutSubmitAfterAllSendOrderEmail(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        if (!$order->getEmailSent()) {
            switch ($order->getState()) {
                case Mage_Sales_Model_Order::STATE_PENDING_PAYMENT:
                case Mage_Sales_Model_Order::STATE_PROCESSING:
                case Mage_Sales_Model_Order::STATE_COMPLETE:
                case Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW:
                    $order->sendNewOrderEmail();
                    break;
            }
        }
    }

    /**
     * Ensure Klarna payments is the preselected payment method on a quote
     *
     * @param Varien_Event_Observer $observer
     */
    public function preselectKlarnaOnQuote(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getQuote();
        if (Mage::getStoreConfigFlag('payment/klarna_payments/force_default') && !$quote->getPayment()->getMethod()) {
            $quote->getPayment()->setMethod('klarna_payments');
        }
    }

    /**
     * Clear Klarna Payment session variables when the checkout session is cleared
     *
     * @param Varien_Event_Observer $observer
     */
    public function checkoutSessionClear(Varien_Event_Observer $observer)
    {
        $checkoutSession = Mage::getSingleton('checkout/session');
        $checkoutSession->setKlarnaPaymentsPayloadToken(null);
        $checkoutSession->setKlarnaPaymentsItemCheckToken(null);
    }

    /**
     * Update User-Agent with module version info
     *
     * @param $event
     */
    public function klarnaCoreClientUserAgentString($event)
    {
        $version = Mage::getConfig()->getModuleConfig('Klarna_Payments')->version;
        $versionObj = $event->getVersionStringObject();
        $verString = $versionObj->getVersionString();
        $verString .= ";Klarna_Payments_v{$version}";
        $versionObj->setVersionString($verString);
    }
}
