<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna payments kasper purchase class
 */
class Klarna_Payments_Model_Api_Kasper_Direktdebit extends Klarna_Payments_Model_Api_Kasper_Purchase
{
    /**
     * Get the api for payments api
     *
     * @return Klarna_Payments_Model_Api_Rest_Payments
     */
    protected function _getPaymentsApi()
    {
        return Mage::getSingleton('klarna_payments/api_rest_direktDebit')
            ->setConfig($this->getConfig())
            ->setStore($this->getStore());
    }
}
