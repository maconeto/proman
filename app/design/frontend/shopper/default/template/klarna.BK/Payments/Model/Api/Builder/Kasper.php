<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Payments
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna payments kasper order payload builder
 */
class Klarna_Payments_Model_Api_Builder_Kasper extends Klarna_Core_Model_Api_Builder_Abstract
{
    /**
     * @var Klarna_Payments_Helper_Checkout
     */
    protected $_checkoutHelper = null;

    /**
     * @var Klarna_Payments_Helper_Data
     */
    protected $_paymentsHelper = null;

    /**
     * Generate types
     */
    const GENERATE_TYPE_PLACE         = 'place';
    const GENERATE_TYPE_CLIENT_UPDATE = 'client_update';

    /**
     * Init
     */
    public function _construct()
    {
        Klarna_Core_Model_Api_Builder_Abstract::_construct();
        $this->_paymentsHelper = Mage::helper('klarna_payments');
        $this->_checkoutHelper = Mage::helper('klarna_payments/checkout');
    }

    /**
     * Generate request
     *
     * @param string $type
     *
     * @return array
     */
    public function _generateRequest($type = self::GENERATE_TYPE_CREATE)
    {
        switch ($type) {
            case self::GENERATE_TYPE_CREATE:
            case self::GENERATE_TYPE_UPDATE:
                return $this->_generateCreateUpdate();
            case self::GENERATE_TYPE_PLACE:
                return $this->_generatePlace();
            case self::GENERATE_TYPE_CLIENT_UPDATE:
                return $this->_generateClientUpdate();
        }

        return array();
    }

    /**
     * Generate body for create and update
     *
     * @return array
     * @throws Klarna_Payments_BuilderException
     */
    protected function _generateCreateUpdate()
    {
        $requiredAttributes = array(
            'purchase_country', 'purchase_currency', 'locale', 'order_amount', 'order_lines'
        );

        /** @var Mage_Sales_Model_Quote $quote */
        $quote  = $this->getObject();
        $store  = $quote->getStore();
        $create = array();

        $create['purchase_country']  = $this->getDefaultCountry();
        $create['purchase_currency'] = $quote->getBaseCurrencyCode();
        $create['locale']            = str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode());
        $address                     = $quote->isVirtual() ? $quote->getBillingAddress()
            : $quote->getShippingAddress();
        $create['order_amount']      = $this->_helper->toApiFloat($address->getBaseGrandTotal());
        $create['order_tax_amount']  = $this->_helper->toApiFloat($address->getBaseTaxAmount());
        $create['order_lines']       = $this->getOrderLines();

        // @todo customer payment methods

        if ($this->_paymentsHelper->getDataSharingEnabled($store)) {
            $create['billing_address']  = $this->_getAddressData($quote, Mage_Sales_Model_Quote_Address::TYPE_BILLING);
            $create['shipping_address'] = $this->_getAddressData($quote, Mage_Sales_Model_Quote_Address::TYPE_SHIPPING);
            $create['customer']         = $this->_getCustomerData($quote);
        }

        /**
         * Urls
         */
        $urlParams = array(
            '_nosid'         => true,
            '_forced_secure' => true
        );

        $create['merchant_urls'] = array(
            'confirmation' => Mage::getUrl('checkout/onepage/success', $urlParams),
            'notification' => Mage::getUrl('klarna/notification', $urlParams)
        );

        /**
         * Merchant reference
         */
        $merchantReferences = new Varien_Object(
            array(
            'merchant_reference1' => $quote->getReservedOrderId()
            )
        );

        Mage::dispatchEvent(
            'klarna_payments_merchant_reference_update', array(
            'quote'                     => $quote,
            'merchant_reference_object' => $merchantReferences
            )
        );

        if ($merchantReferences->getData('merchant_reference1')) {
            $create['merchant_reference1'] = $merchantReferences->getData('merchant_reference1');
        }

        if (!empty($merchantReferences['merchant_reference2'])) {
            $create['merchant_reference2'] = $merchantReferences->getData('merchant_reference2');
        }

        /**
         * Options
         */
        $create['options'] = array_map('trim', array_filter($this->_checkoutHelper->getCheckoutDesignConfig($store)));

        // @todo attachments

        $create            = array_filter($create);
        $missingAttributes = array();
        foreach ($requiredAttributes as $requiredAttribute) {
            if (empty($create[$requiredAttribute])) {
                $missingAttributes[] = $requiredAttribute;
            }
        }

        if (isset($create['billing_address']) && !isset($create['billing_address']['email'])) {
            $missingAttributes[] = 'email';
        }

        if (isset($create['shipping_address']) && !isset($create['shipping_address']['email'])) {
            $missingAttributes[] = 'email';
        }

        if (!empty($missingAttributes)) {
            throw new Klarna_Payments_BuilderException(sprintf('Missing required attribute(s) on place: "%s".', implode(', ', $missingAttributes)));
        }

        $total = 0;
        foreach ($create['order_lines'] as $orderLine) {
            $total += $orderLine['total_amount'];
        }

        if ($total != $create['order_amount']) {
            throw new Klarna_Payments_BuilderException(sprintf('Order line totals do not total order_amount "%s" != "%s"', $total, $create['order_amount']));
        }

        foreach ($create['order_lines'] as $orderLine) {
            if ($orderLine['total_amount'] != $orderLine['quantity'] * $orderLine['unit_price']) {
                throw new Klarna_Payments_BuilderException('Order line totals do not total unit_price x qty');
            }
        }

        return $create;
    }

    /**
     * Generate place order body
     *
     * @return array
     */
    protected function _generatePlace()
    {
        $requiredAttributes = array(
            'purchase_country', 'purchase_currency', 'locale', 'order_amount', 'order_lines', 'merchant_urls',
            'billing_address', 'shipping_address'
        );

        /** @var Mage_Sales_Model_Quote $quote */
        $quote  = $this->getObject();
        $create = array();

        $create['locale']            = str_replace('_', '-', Mage::app()->getLocale()->getLocaleCode());
        $create['purchase_country']  = $this->getDefaultCountry();
        $create['purchase_currency'] = $quote->getBaseCurrencyCode();
        $create['billing_address']   = $this->_getAddressData($quote, Mage_Sales_Model_Quote_Address::TYPE_BILLING);
        $create['shipping_address']  = $this->_getAddressData($quote, Mage_Sales_Model_Quote_Address::TYPE_SHIPPING);

        $address                    = $quote->isVirtual() ? $quote->getBillingAddress()
            : $quote->getShippingAddress();
        $create['order_amount']     = $this->_helper->toApiFloat($address->getBaseGrandTotal());
        $create['order_tax_amount'] = $this->_helper->toApiFloat($address->getBaseTaxAmount());
        $create['order_lines']      = $this->getOrderLines();

        /**
         * Urls
         */
        $urlParams = array(
            '_nosid'         => true,
            '_forced_secure' => true
        );

        $create['merchant_urls'] = array(
            'confirmation' => Mage::getUrl('checkout/onepage/success', $urlParams),
            'notification' => Mage::getUrl('klarna/notification', $urlParams)
        );

        /**
         * Merchant reference
         */
        $merchantReferences = new Varien_Object(
            array(
            'merchant_reference1' => $quote->getReservedOrderId()
            )
        );

        Mage::dispatchEvent(
            'klarna_payments_merchant_reference_update', array(
            'quote'                     => $quote,
            'merchant_reference_object' => $merchantReferences
            )
        );

        if ($merchantReferences->getData('merchant_reference1')) {
            $create['merchant_reference1'] = $merchantReferences->getData('merchant_reference1');
        }

        if (!empty($merchantReferences['merchant_reference2'])) {
            $create['merchant_reference2'] = $merchantReferences->getData('merchant_reference2');
        }

        $create = array_filter($create);

        $missingAttributes = array();
        foreach ($requiredAttributes as $requiredAttribute) {
            if (empty($create[$requiredAttribute])) {
                $missingAttributes[] = $requiredAttribute;
            }
        }

        if (!empty($missingAttributes)) {
            Mage::throwException(sprintf('Missing required attribute(s) on place: "%s".', implode(', ', $missingAttributes)));
        }

        $total = 0;
        foreach ($create['order_lines'] as $orderLine) {
            $total += $orderLine['total_amount'];
        }

        if ($total != $create['order_amount']) {
            Mage::throwException('Order line totals do not total order_amount');
        }

        return $create;
    }

    /**
     * Generate request for client side update
     *
     * @return array
     */
    protected function _generateClientUpdate()
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $this->getObject();

        return array(
            'billing_address'  => $this->_getAddressData($quote, Mage_Sales_Model_Quote_Address::TYPE_BILLING),
            'shipping_address' => $this->_getAddressData($quote, Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
        );
    }
}
