<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_KcoDACH
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna Kco DACH Observer
 */
class Klarna_KcoDACH_Model_Observer
{
    /**
     * Add pack station parameter to request
     *
     * @param Varien_Event_Observer $observer
     */
    public function builderPackStationSupport(Varien_Event_Observer $observer)
    {
        /** @var Klarna_Kco_Model_Api_Builder_Abstract $builder */
        $builder        = $observer->getBuilder();
        $create         = $builder->getRequest();
        $checkoutHelper = Mage::helper('klarna_kco/checkout');
        $helper         = Mage::helper('klarna_kcodach');

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $builder->getObject();
        $store = $quote->getStore();

        if ($checkoutHelper->getCheckoutConfigFlag('packstation_enabled', $store)
            && $helper->getPackstationSupport($store)
        ) {
            $create['options']['packstation_enabled']             = true;
            $create['options']['allow_separate_shipping_address'] = true;
            $observer->getBuilder()->setRequest($create);
        }
    }

    /**
     * Save care of on an order with packstation
     *
     * @param Varien_Event_Observer $observer
     */
    public function orderConfirmationPackstationSave(Varien_Event_Observer $observer)
    {
        $checkout = $observer->getCheckout();
        if (!$observer->getQuote()->isVirtual() && ($careOf = $checkout->getData('shipping_address/care_of'))) {
            $helper          = Mage::helper('klarna_kcodach');
            $checkoutHelper  = Mage::helper('klarna_kco/checkout');
            $shippingAddress = new Varien_Object($checkout->getShippingAddress());
            $shippingAddress->setStreetAddress2($helper->__('C/O ') . $careOf);
            $checkoutHelper->updateKcoCheckoutAddress($shippingAddress, Mage_Sales_Model_Quote_Address::TYPE_SHIPPING);
        }
    }

    /**
     * Check if the pre-fill notice has been accepted
     *
     * @param Varien_Event_Observer $observer
     */
    public function preFillNoticeCheckAccept(Varien_Event_Observer $observer)
    {
        /** @var Mage_Core_Controller_Varien_Action $controllerAction */
        $controllerAction = $observer->getControllerAction();
        $termsParam       = $controllerAction->getRequest()->getParam('terms');
        $checkoutSession  = Mage::helper('checkout')->getCheckout();

        if ($termsParam) {
            $checkoutSession->setKlarnaFillNoticeTerms($termsParam);
        }

        if ('accept' == $termsParam) {
            $quote       = Mage::helper('checkout')->getQuote();
            $klarnaQuote = Mage::getModel('klarna_kco/klarnaquote')->loadActiveByQuote($quote);

            if ($klarnaQuote->getId()) {
                $klarnaQuote->setIsActive(0);
                $klarnaQuote->save();
            }

            $controllerAction->setRedirectWithCookieCheck('checkout/klarna');
            $controllerAction->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
        }
    }

    /**
     * Handle if a user accepts pre-fill terms
     *
     * @param Varien_Event_Observer $observer
     */
    public function preFillNotice(Varien_Event_Observer $observer)
    {
        /** @var Klarna_Kco_Model_Api_Builder_Abstract $builder */
        $builder         = $observer->getBuilder();
        $create          = $builder->getRequest();
        $checkoutSession = Mage::helper('checkout')->getCheckout();

        if ('accept' != $checkoutSession->getKlarnaFillNoticeTerms()
            && Mage::helper('klarna_kcodach')->isPrefillNoticeEnabled($builder->getObject()->getStore())
        ) {
            unset($create['customer']);
            unset($create['billing_address']);
            unset($create['shipping_address']);
            $observer->getBuilder()->setRequest($create);
        }
    }

    /**
     * Download and store Kred invoices
     *
     * @param Varien_Event_Observer $observer
     *
     * @return $this
     */
    public function downloadAndStoreInvoice(Varien_Event_Observer $observer)
    {
        if (!Mage::getStoreConfigFlag('checkout/klarna_kco/invoice_download')) {
            return;
        }

        try {
            /** @var Mage_Sales_Model_Order_Payment $payment */
            $payment = $observer->getPayment();

            if ($payment->getMethod() != 'klarna_kco') {
                return $this;
            }

            $invoiceId    = $payment->getTransactionId();
            $store        = $payment->getOrder()->getStore();
            $mid          = Mage::getStoreConfig('payment/klarna_kco/merchant_id', $store);
            $sharedSecret = Mage::getStoreConfig('payment/klarna_kco/shared_secret', $store);

            $authHash = urlencode(base64_encode(hash('sha512', sprintf('%s:%s:%s', $mid, $invoiceId, $sharedSecret), true)));

            if ($invoiceId
                && 'kred' == Mage::helper('klarna_kco/checkout')->getCheckoutType($store)
            ) {
                $invoiceUrl    = Mage::getStoreConfigFlag('payment/klarna_kco/test_mode', $store)
                    ? sprintf('https://online.testdrive.klarna.com/invoices/%s.pdf?secret=%s', $invoiceId, $authHash)
                    : sprintf('https://online.klarna.com/invoices/%s.pdf?secret=%s', $invoiceId, $authHash);
                $saveDirectory = Mage::getConfig()->getVarDir() . DS . 'klarnainvoices';

                if (!Mage::getConfig()->createDirIfNotExists($saveDirectory)) {
                    Mage::throwException(sprintf('Unable to create Klarna invoice directory "%s"', $saveDirectory));
                }

                $client = new Zend_Http_Client(
                    $invoiceUrl, array(
                    'keepalive' => true
                    )
                );

                $client->setStream();
                $response = $client->request('GET');
                if ($response->isSuccessful()) {
                    copy($response->getStreamName(), $saveDirectory . DS . $invoiceId . '.pdf');
                } else {
                    Mage::throwException(
                        sprintf(
                            'Header status "%s" when attempted to download invoice pdf #%s for order #%s.',
                            $response->getStatus(), $invoiceId, $payment->getOrder()->getIncrementId()
                        )
                    );
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
            throw $e;
        }

        return $this;
    }
}
