<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_KcoKred
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna Kred API Observer
 */
class Klarna_KcoKred_Model_Observer
{
    /**
     * Acknowledge Kred orders on confirmation page and avoid push notifcation
     *
     * @param Varien_Event_Observer $observer
     */
    public function acknowledgeKredOrderOnConfirmation(Varien_Event_Observer $observer)
    {
        $helper         = Mage::helper('klarna_kco');
        $checkoutHelper = Mage::helper('klarna_kco/checkout');

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        /** @var Klarna_Kco_Model_Klarnaorder $klarnaOrder */
        $klarnaOrder = $observer->getKlarnaOrder();

        $checkoutId   = $klarnaOrder->getKlarnaCheckoutId();
        $checkoutType = $checkoutHelper->getCheckoutType($order->getStore());
        $pushQueue    = Mage::getModel('klarna_kcokred/pushqueue')->loadByCheckoutId($checkoutId);

        try {
            if ('kred' == $checkoutType && !$klarnaOrder->getIsAcknowledged() && $pushQueue->getId()) {
                /** @var Mage_Sales_Model_Order_Payment $payment */
                $payment = $order->getPayment();
                $payment->registerPaymentReviewAction(Mage_Sales_Model_Order_Payment::REVIEW_ACTION_UPDATE, true);
                $status = false;

                if ($order->getState() == Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW) {
                    $statusObject = new Varien_Object(
                        array(
                        'status' => $checkoutHelper->getProcessedOrderStatus($order->getStore())
                        )
                    );

                    Mage::dispatchEvent(
                        'kco_push_notification_before_set_state', array(
                        'order'         => $order,
                        'klarna_order'  => $klarnaOrder,
                        'status_object' => $statusObject
                        )
                    );

                    if (Mage_Sales_Model_Order::STATE_PROCESSING == $order->getState()) {
                        $status = $statusObject->getStatus();
                    }
                }

                $order->addStatusHistoryComment($helper->__('Order processed by Klarna.'), $status);

                $api = Mage::helper('klarna_kco')->getApiInstance($order->getStore());
                $api->updateMerchantReferences($checkoutId, $order->getIncrementId());
                $api->acknowledgeOrder($checkoutId);
                $order->addStatusHistoryComment('Acknowledged request sent to Klarna');
                $order->save();

                $klarnaOrder->setIsAcknowledged(1);
                $klarnaOrder->save();
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    /**
     * Log orders that were not found during push notification
     *
     * @param Varien_Event_Observer $observer
     */
    public function logOrderPushNotification(Varien_Event_Observer $observer)
    {
        $checkoutHelper = Mage::helper('klarna_kco/checkout');
        $klarnaQuote    = Mage::getModel('klarna_kco/klarnaquote')->loadByCheckoutId($observer->getKlarnaOrderId());
        if ($klarnaQuote->getId()) {
            $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($klarnaQuote->getQuoteId());
            if ($quote->getId() && !$checkoutHelper->getDelayedPushNotification($quote->getStore())) {
                $pushQueue = Mage::getModel('klarna_kcokred/pushqueue')
                    ->loadByCheckoutId($klarnaQuote->getKlarnaCheckoutId());
                if (!$pushQueue->getId()) {
                    $pushQueue->setKlarnaCheckoutId($observer->getKlarnaOrderId());
                }

                $pushQueue->setCount((int)$pushQueue->getCount() + 1);
                $pushQueue->save();

                $cancelCountObject = new Varien_Object(
                    array(
                    'cancel_ceiling' => 2
                    )
                );

                Mage::dispatchEvent(
                    'kco_add_to_push_queue', array(
                    'klarna_quote'        => $klarnaQuote,
                    'quote'               => $quote,
                    'push_queue'          => $pushQueue,
                    'cancel_count_object' => $cancelCountObject
                    )
                );

                if (false !== $cancelCountObject->getCancelCeiling()
                    && $pushQueue->getCount() >= $cancelCountObject->getCancelCeiling()
                ) {
                    try {
                        $helper = Mage::helper('klarna_kco');
                        $api    = $helper->getApiInstance($quote->getStore());

                        $api->initKlarnaCheckout($observer->getKlarnaOrderId());

                        if ($api->getReservationId()) {
                            $api->cancel($api->getReservationId());
                        }
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }

                if ($observer->hasResponseCodeObject()) {
                    $observer->getResponseCodeObject()->setResponseCode(200);
                }
            }
        }
    }

    /**
     * Disable partial payments for orders with discounts
     *
     * This is due to some limitations using Klarna with Magento in certain markets on Kred
     *
     * @param Varien_Event_Observer $observer
     */
    public function disablePartialPaymentsForOrdersWithDiscounts(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        if (0 > $order->getBaseDiscountAmount()) {
            $observer->getFlagObject()->setCanPartial(false);
        }
    }

    /**
     * Clean claimed and expired orders from the push queue
     *
     * @param Varien_Event_Observer $observer
     */
    public function pushQueueCleanup(Varien_Event_Observer $observer)
    {
        Mage::getResourceModel('klarna_kcokred/pushqueue')->clean($observer->getLog());
    }
}
