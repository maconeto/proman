<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Kco
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Generate shipping order line details
 */
class Klarna_KcoKred_Model_Checkout_Orderline_Shipping extends Klarna_Kco_Model_Checkout_Orderline_Shipping
{
    /**
     * Add order details to checkout request
     *
     * @param Klarna_Kco_Model_Api_Builder_Abstract $checkout
     *
     * @return $this
     */
    public function fetch($checkout)
    {
        if ($checkout->getShippingTotalAmount()) {
            $checkout->addOrderLine(
                array(
                'type'          => self::ITEM_TYPE_SHIPPING,
                'reference'     => $checkout->getShippingReference(),
                'name'          => $checkout->getShippingTitle(),
                'quantity'      => 1,
                'unit_price'    => $checkout->getShippingTotalAmount(),
                'discount_rate' => 0,
                'tax_rate'      => $checkout->getShippingTaxRate()
                )
            );
        }

        return $this;
    }
}
