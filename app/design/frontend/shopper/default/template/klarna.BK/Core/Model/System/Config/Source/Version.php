<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Core
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna versions config source
 */
class Klarna_Core_Model_System_Config_Source_Version
{
    /**
     * Get version details
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper  = Mage::helper('klarna_core');
        $options = array();

        if ($versions = $helper->getApiVersion()) {
            foreach ($versions as $version) {
                $options[] = array(
                    'label' => $version->getLabel(),
                    'value' => $version->getCode()
                );
            }
        } else {
            $options[] = array(
                'label' => Mage::helper('klarna_core')->__('No API Versions Available'),
                'value' => null
            );
        }

        return $options;
    }
}
