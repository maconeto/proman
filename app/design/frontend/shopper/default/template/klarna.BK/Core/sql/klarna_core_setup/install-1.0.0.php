<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Core
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/** @var $installer Mage_Sales_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
/**
 * Create table 'klarna_core/order'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('klarna_core/order'))
    ->addColumn(
        'klarna_order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        ), 'Order Id'
    )
    ->addColumn('session_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Session Id')
    ->addColumn('reservation_id', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Reservation Id')
    ->addColumn(
        'order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        ), 'Order Id'
    )
    ->addColumn(
        'is_acknowledged', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default'  => '0',
        ), 'Is Acknowledged'
    )
    ->addForeignKey(
        $installer->getFkName('klarna_core/order', 'order_id', 'sales/order', 'entity_id'),
        'order_id', $installer->getTable('sales/order'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Klarna Order');
$installer->getConnection()->createTable($table);

$installer->endSetup();
