<?php
/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Core
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna api integration abstract
 *
 * @method Klarna_Core_Model_Api_PurchaseAbstract setStore(Mage_Core_Model_Store $store)
 * @method Mage_Core_Model_Store getStore()
 * @method Klarna_Core_Model_Api_PurchaseAbstract setConfig(Varien_Object $config)
 * @method Varien_Object getConfig()
 */
class Klarna_Core_Model_Api_PurchaseAbstract extends Klarna_Core_Model_Api_ApiTypeAbstract
    implements Klarna_Core_Model_Api_PurchaseApiInterface
{
    /**
     * Create or update a session
     *
     * @param string $sessionId
     * @param bool   $createIfNotExists
     * @param bool   $updateAllowed
     *
     * @return Varien_Object
     */
    public function initKlarnaSession($sessionId = null, $createIfNotExists = false, $updateAllowed = false)
    {
        return new Klarna_Core_Model_Api_Response();
    }

    /**
     * Create new session
     *
     * @throws Klarna_Core_Model_Api_Exception
     *
     * @return Varien_Object
     */
    public function createSession()
    {
        return $this->initKlarnaSession();
    }

    /**
     * Update existing session
     *
     * @param string $sessionId
     *
     * @throws Klarna_Core_Model_Api_Exception
     *
     * @return Varien_Object
     */
    public function updateSession($sessionId)
    {
        return $this->initKlarnaSession($sessionId, false, true);
    }
}
