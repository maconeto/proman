(function($) {

    var settings = {
        postalCodeClass : null,
        postalCodeName : 'postalCode',
        openHoursClass : null,
        openHoursName : 'open',
        closeHoursClass : null,
        closeHoursName : 'close',
        chooseAreaClass : null,
        chooseName: 'pickuppoint',
        labelClass: null,
        descriptionClass: null,
        googleMaps : false,
        mapHeight : 256,
        mapWidth : 256,
        url: 'http://fraktguide.bring.no/fraktguide/api/pickuppoint/postalcode/'
    };

    $.fn.utleveringsenhet = function(options) {
        return this.each(function() {
            if (options) {
                $.extend(settings, options);
            }

            var postalCode;
            var openingTime;
            var closingTime;
            var chooseArea;

            var loading = false;
            var lastsearch;
            var i;

            var map = null;
            var markers = [];

            function googleMapsClick(radioButton) {
                return function() {
                    radioButton.attr('checked', true);
                };
            }

            function createRadioButton(pp) {
                var value = pp.id + ";" + pp.name + ";" + pp.address + ";" + pp.postalCode + ";" + pp.city;

                this.base = $('<div>');

                this.radioButton = $('<input>').attr({type : 'radio', name : settings.chooseName, value: value}).css('float', 'left').appendTo(this.base);
                this.text = $('<div>').css('float', 'left').appendTo(this.base);
                $('<span>').css('font-weight', 'bold').text(pp.name).appendTo(this.text);
                this.text.append(", " + pp.visitingAddress + ", " + pp.visitingPostalCode + " " + pp.visitingCity + "<br />" + pp.openingHoursNorwegian);
                this.base.append('<div style="clear: both;"></div>');

                return this;
            }

            function update(data) {
                loading = false;
                if (settings.googleMaps) {
                    for (i = 0; i < markers.length; i++) {
                        google.maps.event.clearInstanceListeners(markers[i]);
                        markers[i].setMap(null);
                    }
                }
                markers.length = 0;

                var bounds = null;
                if (data && data.pickupPoint) {
                    chooseArea.html('');
                    for (i = 0; i < data.pickupPoint.length; i++) {
                        var pp = data.pickupPoint[i];

                        var radioButtonBase = createRadioButton(pp);

                        if (settings.googleMaps) {
                            var loc = new google.maps.LatLng(pp.latitude, pp.longitude);
                            var marker = new google.maps.Marker({
                                        position: loc,
                                        map: map,
                                        title: pp.name
                                    });
                            markers.push(marker);

                            google.maps.event.addListener(marker, 'click', googleMapsClick(radioButtonBase.radioButton));

                            if (bounds === null) {
                                bounds = new google.maps.LatLngBounds(loc, loc);
                            }
                            bounds.extend(loc);
                        }
                        chooseArea.append(radioButtonBase.base);
                    }
                }
                if (bounds !== null && map !== null) {
                    map.fitBounds(bounds);
                }
            }

            function failure() {
                loading = false;
                lastsearch = {isEqual: function() {return false;}};
                chooseArea.html("Failed to get pickup points");
            }

            function createSearchCriteria() {
                return {
                    postalCode: postalCode.val(),
                    openingTime: openingTime.val(),
                    closingTime: closingTime.val(),
                    isEqual: function(critera) {
                        return (this.postalCode === critera.postalCode &&
                            this.openingTime === critera.openingTime &&
                            this.closingTime === critera.closingTime);
                    }
                };
            }

            function ajaxUtleveringsenheter(event) {
                if (postalCode.val().length === 4 && !loading && !lastsearch.isEqual(createSearchCriteria())) {
                    loading = true;
                    lastsearch = createSearchCriteria();

                    var data = {};
                    if (openingTime.val() !== "") {
                        data.openOnOrBefore = openingTime.val();
                    }
                    if (closingTime.val() !== "") {
                        data.openOnOrAfter = closingTime.val();
                    }

                    $.ajax({
                                url : settings.url + postalCode.val() + ".jsonp",
                                data: data,
                                dataType : 'jsonp',
                                success : update,
                                error : failure
                            });
                }
            }

            function createHtml(rootElement) {
                var selectOptions = '<option value=""> - </option>';
                for (i = 6; i < 24; i++) {
                    var time = i + ":00";
                    var value = i + "00";
                    if (value.length === 3) {
                        value = "0" + value;
                    }
                    selectOptions += '<option value="' + value + '">' + time + '</option>';
                }

                var div = $('<div>').addClass(settings.descriptionClass).appendTo(rootElement);
                $('<label>').text("Postnummer").addClass(settings.labelClass).appendTo(div);
                postalCode = $('<input>').attr({
                            type : 'text',
                            name : settings.postalCodeName,
                            size : '4'
                        }).addClass(settings.postalCodeClass).appendTo(div);

                div = $('<div>').addClass(settings.descriptionClass).appendTo(rootElement);
                $('<span>').text("Minst én dag med åpningstidfra kl").appendTo(div);
                $('<label>').text("fra kl").addClass(settings.labelClass).appendTo(div);
                openingTime = $('<select>').attr({
                            name : settings.openHoursName
                        }).addClass(settings.openHoursClass).html(selectOptions).appendTo(div);
                $('<label>').text("til kl").addClass(settings.labelClass).appendTo(div);
                closingTime = $('<select>').attr({
                            name : settings.closeHoursName
                        }).addClass(settings.closeHoursClass).html(selectOptions).appendTo(div);

                chooseArea = $('<div>').addClass(settings.chooseAreaClass).appendTo(rootElement);
            }

            function init(rootElement) {
                createHtml(rootElement);
                postalCode.keyup(ajaxUtleveringsenheter);
                openingTime.change(ajaxUtleveringsenheter).keyup(ajaxUtleveringsenheter);
                closingTime.change(ajaxUtleveringsenheter).keyup(ajaxUtleveringsenheter);
                lastsearch = createSearchCriteria();

                if (settings.googleMaps === true && google.maps.Map !== null) {
                    var jqMap = $("<div>").css({
                                height : settings.mapHeight + "px",
                                width : settings.mapWidth + "px"
                            }).appendTo(rootElement);

                    var oslo = new google.maps.LatLng(59.92, 10.75);

                    var myOptions = {
                        zoom: 6,
                        center: oslo,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        panControl: true,
                        zoomControl: true,
                        mapTypeControl: true,
                        scaleControl: false,
                        streetViewControl: false,
                        overviewMapControl: false
                    };
                    map = new google.maps.Map(jqMap.get(0), myOptions);
                }
            }

            init(this);

        });
    };
})(jQuery);


jQuery.noConflict();